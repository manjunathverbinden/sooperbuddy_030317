package com.three_embed.datum.Aramis_home.User_preference_setter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.gson.Gson;
import com.three_embed.datum.AdapterClass.Match_caht_list_adapeter;
import com.three_embed.datum.Aleret_dialogs.Block_user_popup_layout;
import com.three_embed.datum.Aleret_dialogs.Net_work_failed_aleret;
import com.three_embed.datum.DataBaseClass.Match_List_Data_holder;
import com.three_embed.datum.DataBaseClass.Match_list_data_pojo;
import com.three_embed.datum.Pojo_classes.MatchesListData;
import com.three_embed.datum.Pojo_classes.MatchesListPojo;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.chat_lib.AppController;
import com.three_embed.datum.chat_lib.ChatList_Chat_Item;
import com.three_embed.datum.chat_lib.CouchDbController;
import com.three_embed.datum.chat_lib.RecyclerItemClickListener;
import com.three_embed.datum.chat_lib.ServerEvents;
import com.three_embed.datum.chat_lib.SocketResponseHandler;
import com.three_embed.datum.chat_lib.TimestampSorter;
import com.three_embed.datum.chat_lib.Utilities;
import com.three_embed.datum.Aramis_home.Chat;
import com.three_embed.datum.Aramis_home.HomePageActivity;
import com.three_embed.datum.Aramis_home.R;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class Chat_Fragment_list_data extends Fragment implements SwipeRefreshLayout.OnRefreshListener
{
    private View view;
    private Match_caht_list_adapeter mAdapter;
    private RecyclerView  recyclerView_chat;
    private ArrayList<ChatList_Chat_Item> mChatData;
    private ArrayList<ChatList_Chat_Item> mChatData_temp;
    private Match_List_Data_holder match_list_data_holder;
    private SessionManager sessionManager;
    private Activity mactivity;
    private RelativeLayout progressDialog;
    private RelativeLayout connection_failed_rl;
    private Net_work_failed_aleret net_work_failed_aleret;
    private HomePageActivity homePageActivity;
    private ArrayList<MatchesListData> mathes_user_list_data;
    private RelativeLayout no_user_rl;
    private Chat_Fragment_list_data fragment_list_data;
    private SwipeRefreshLayout swipe_refresh_layout;
    /**
     * Initalization of constructure .*/
    public Chat_Fragment_list_data()
    {
        fragment_list_data=this;
        mChatData = new ArrayList<>();
        mChatData_temp=new ArrayList<>();
        net_work_failed_aleret=Net_work_failed_aleret.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mactivity=getActivity();
        /**
         * Holding the home page reference.*/
        homePageActivity= (HomePageActivity) mactivity;
        match_list_data_holder=new Match_List_Data_holder(mactivity);
        sessionManager=new SessionManager(mactivity);
        mathes_user_list_data=new ArrayList<>();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        if(view==null)
        {
            view= inflater.inflate(com.three_embed.datum.chat_lib.R.layout.socket_chatlist,container, false);
        } else
        {
            if( view.getParent()!=null)((ViewGroup)view.getParent()).removeView(view);
        }
        /**
         * Clearing the list.*/
        mChatData.clear();
        mChatData_temp.clear();

        swipe_refresh_layout= (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);

        TextView loadingtext = (TextView) view.findViewById(R.id.loadingtext);
        loadingtext.setTypeface(homePageActivity.GothamRoundedLight);

        TextView user_hint_text = (TextView) view.findViewById(R.id.user_hint_text);
        user_hint_text.setTypeface(homePageActivity.GothamRoundedMedium);

        TextView user_button_hint_text = (TextView) view.findViewById(R.id.user_button_hint_text);
        user_button_hint_text.setTypeface(homePageActivity.GothamRoundedLight);

        progressDialog=(RelativeLayout)view.findViewById(R.id.progress_bar_rl);

        Button retry_button = (Button) view.findViewById(R.id.retry_button);
        retry_button.setTypeface(homePageActivity.GothamRoundedLight);
        retry_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Load_match_list();
            }
        });
        /**
         * Connection failed data.*/
        connection_failed_rl=(RelativeLayout)view.findViewById(R.id.connection_failed_rl);
        recyclerView_chat = (RecyclerView) view.findViewById(R.id.rv);

        no_user_rl=(RelativeLayout)view.findViewById(R.id.no_user);
        RelativeLayout discover_new_people_bt = (RelativeLayout) view.findViewById(R.id.discover_new_people_bt);
        discover_new_people_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePageActivity.setCurrent_Tab();
            }
        });

        recyclerView_chat.setHasFixedSize(true);
        mAdapter = new Match_caht_list_adapeter(mactivity,mChatData);
        recyclerView_chat.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView_chat.setItemAnimator(new DefaultItemAnimator());
        recyclerView_chat.setAdapter(mAdapter);



        AppController.initSocketHandlerInstance(new SocketResponseHandler(view.getContext())
        {
            @Override
            public void execute(String event, JSONObject jsonObject) throws JSONException
            {
                if (event.equals(ServerEvents.chatHistory.value))
                {
                    Log.d("Log23", jsonObject.toString());

                    String sender = jsonObject.getString("from");
                    String timestamp = jsonObject.getString("timestamp");
                    String messageType = jsonObject.getString("type");
                    String message;

                    switch (messageType) {
                        case "0":
                            message = jsonObject.getString("payload").trim();
                            try {
                                message = new String(Base64.decode(message, Base64.DEFAULT), "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            break;
                        case "2":
                            message = "New Video";
                            break;
                        case "3":
                            message = "New Gif";
                            break;
                        default:
                            message = "New Image";
                            break;
                    }
                    /**
                     * Checking user is already in match list or not.
                     */
                    int alreadyInContact = alreadyInContact(sender);
                    ChatList_Chat_Item chat;
                    if(alreadyInContact == -1)
                    {
                        chat = new ChatList_Chat_Item();
                        chat.setReceiverName("Unknown user");
                        chat.setStatus("1");
                    }else
                    {
                        chat=mChatData.get(alreadyInContact);
                    }
                    chat.setReceiverUid(sender);
                    chat.setNewMessage(message);
                    chat.sethasNewMessage(true);
                    chat.setDocumentId(AppController.getInstance().findDocumentIdOfReceiver(sender));
                    chat.setNewMessageDate(Utilities.epochtoGmt(timestamp));
                    chat.setMessage_Time_stamp(timestamp);
                    chat.setNewMessageTime(Utilities.epochtoGmt(timestamp));
                    chat.setReceiverImageUrl(AppController.getInstance().getDbController().getReceiverImageUrl(AppController.getInstance().findDocumentIdOfReceiver(sender)));
                    chat.setReceiver_fb_id(AppController.getInstance().getDbController().getReceiverFB_id(AppController.getInstance().findDocumentIdOfReceiver(sender)));
                    /**
                     * this is not required as of now as we are not storing messages on server but if in future we start saving chat then this is useful
                     * */
                    chat.setChatId(String.valueOf(System.currentTimeMillis()));
                    /**
                     * Updating the list.*/
                    Collections.sort(mChatData);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                        }
                    });

                } else if (event.equals(ServerEvents.sendMessageToServer.value))
                {
                    String sender = jsonObject.getString("from");
                    String messageType = jsonObject.getString("type");
                    String timestamp = jsonObject.getString("timestamp");
                    String message;

                    switch (messageType) {
                        case "0":
                            message = jsonObject.getString("payload").trim();
                            try {
                                message = new String(Base64.decode(message, Base64.DEFAULT), "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            break;
                        case "2":
                            message = "New Video";
                            break;
                        case "3":
                            message = "New Gif";
                            break;
                        default:
                            message = "New Image";
                            break;
                    }
                    /**
                     * Checking user is already in match list or not.
                     */
                    int alreadyInContact = alreadyInContact(sender);
                    ChatList_Chat_Item chat;
                    if(alreadyInContact == -1)
                    {
                        chat = new ChatList_Chat_Item();
                        chat.setReceiverName("Unknown user");
                        chat.setStatus("1");
                    }else
                    {
                        chat=mChatData.get(alreadyInContact);
                    }
                    chat.setReceiverUid(sender);
                    chat.setNewMessage(message);
                    chat.sethasNewMessage(true);
                    chat.setDocumentId(AppController.getInstance().findDocumentIdOfReceiver(sender));
                    chat.setNewMessageDate(Utilities.epochtoGmt(timestamp));
                    chat.setMessage_Time_stamp(timestamp);
                    chat.setNewMessageTime(Utilities.epochtoGmt(timestamp));
                    chat.setNewMessageCount(AppController.getInstance().getDbController().getNewMessageCount(AppController.getInstance().findDocumentIdOfReceiver(sender)));
                    chat.setReceiverImageUrl(AppController.getInstance().getDbController().getReceiverImageUrl(AppController.getInstance().findDocumentIdOfReceiver(sender)));
                    chat.setReceiver_fb_id(AppController.getInstance().getDbController().getReceiverFB_id(AppController.getInstance().findDocumentIdOfReceiver(sender)));
                    /**
                     * this is not required as of now as we are not storing messages on server but if in future we start saving chat then this is useful
                     * */
                    chat.setChatId(String.valueOf(System.currentTimeMillis()));
                    /**
                     * Updating the list.*/
                    Collections.sort(mChatData);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                }

            }
        });


        recyclerView_chat.addOnItemTouchListener(new RecyclerItemClickListener(view.getContext(), recyclerView_chat, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view,int position)
            {
                if(position>=0)
                {
                    final ChatList_Chat_Item item = mChatData.get(position);
                    Intent intent = new Intent(mactivity,Chat.class);
                    intent.putExtra("USER_EMAIL", item.getReceiverUid());
                    intent.putExtra("USER_FIRST_NAME", item.getReceiverName());
                    intent.putExtra("USER_PROFILE_IMAGE_URL", item.getReceiverImageUrl());
                    intent.putExtra("USER_AGE", "22");
                    intent.putExtra("USER_FB_ID",item.getReceiver_fb_id());


                    if (item.hasNewMessage())
                    {
                        CouchDbController db = AppController.getInstance().getDbController();
                        db.updateChatListOnViewingMessage(item.getDocumentId());
                    }

                    if(item.getStatus().equals("1"))
                    {
                        getActivity().finish();
                        startActivity(intent);
                    }else
                    {
                        Toast.makeText(mactivity.getApplicationContext(), R.string.block_user_aleret,Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onItemLongClick(View view, int position)
            {
                final ChatList_Chat_Item item = mChatData.get(position);
                if(!item.getStatus().equals("1"))
                {
                    Block_user_popup_layout block_user_popup = Block_user_popup_layout.getInstance();
                    block_user_popup.show_popup_block_user(view, mactivity,mChatData.get(position).getReceiver_fb_id(),fragment_list_data);
                }

            }
        }));
        /**
         * Loading the user id.*/
        Load_match_list();

        if(homePageActivity.doc_id_to_update_count!=null)
        {
            update_count(homePageActivity.doc_id_to_update_count);
        }
        return view;
    }

    /**
     * <h2>initializListener</h2>
     * <P>
     *
     * </P>*/
    private void initializListener()
    {
        recyclerView_chat.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                swipe_refresh_layout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }
    /**
     * <h2>update_count</h2>
     * <P>
     *     Update count update the count value of the user id.
     * </P>*/
    private void update_count(String doc_id)
    {
        CouchDbController db = AppController.getInstance().getDbController();
        db.updateChatListOnViewingMessage(doc_id);
    }

    /**
     * <h2>getDataFromDB</h2>
     * <P>
     *   Here first extracting the Data from Db .Weather db contains data or not .If Db contains the Data then
     * calling a method { @code : getMatchedProfile(mactivity,false); } .setting false to say that i have got the Data so don't show the Data not found Ui .Because list already showing user list.
     * </P>
     */
    private void getDataFromDB()
    {
        /**
         * Extracting the data from db.*/
        ArrayList<Match_list_data_pojo> match_list_data=match_list_data_holder.getAllMatch_Data(sessionManager.getFbId());

        if(match_list_data!=null&& match_list_data.size()>0)
        {
            addChat(match_list_data);
            /**
             * Loading with background event.*/
            getMatchedProfile(mactivity, false);

        }else
        {
            /**
             * True to say data not in Db.*/
            getMatchedProfile(mactivity, true);

        }
    }

    /**
     * <h2>addChat</h2>
     * <P>
     *     Method First compare the Given List with the user Message Db List user.
     *     if the user present in List Also present in user message Db then Updating the user details with the user present details from the list. then
     *     putting that in the showing list and notifying the list.
     * </P>
     * @param profile_list contains the new fresh list of user having the user details from the server.*/
    @SuppressWarnings("unchecked")
    public void addChat( ArrayList<Match_list_data_pojo> profile_list)
    {
        mChatData_temp.clear();

        CouchDbController db = AppController.getInstance().getDbController();
        Map<String, Object> map = db.getAllChatDetails(AppController.getInstance().getChatDocId());
        ArrayList<String> receiverUidArray=(ArrayList<String>)map.get("receiverUidArray");
        ArrayList<String> receiverDocIdArray=(ArrayList<String>)map.get("receiverDocIdArray");
        Map<String, Object> chat_item;
        ChatList_Chat_Item chat;


        ArrayList<Map<String, Object>> chats = new ArrayList<>();

        for (int i = 0; i < receiverUidArray.size(); i++)
        {
            chats.add(db.getParticularChatInfo(receiverDocIdArray.get(i)));
            Collections.sort(chats,new TimestampSorter());
        }


        for (int i = 0; i < chats.size(); i++)
        {
            chat_item = chats.get(i);
            chat = new ChatList_Chat_Item();
            chat.setChatId((String) chat_item.get("chatId"));
            boolean hasNewMessage = (Boolean) chat_item.get("hasNewMessage");
            chat.sethasNewMessage(hasNewMessage);

            if (hasNewMessage)
            {
                chat.setNewMessageTime((String) chat_item.get("newMessageTime"));
                chat.setNewMessage((String) chat_item.get("newMessage"));
                chat.setNewMessageDate((String) chat_item.get("newMessageDate"));
                chat.setNewMessageCount((String) chat_item.get("newMessageCount"));
            }
            else
            {
                Map<String,Object> map2=db.getLastMessageDetails((String)chat_item.get("selfDocId" ));
                String time= (String)map2.get("lastMessageTime");
                String date= (String) map2.get("lastMessageDate");
                String message=(String)map2.get("lastMessage");
                chat.setNewMessageTime(time);
                chat.setNewMessage(message);
                chat.setNewMessageDate(date);
                chat.setNewMessageCount("0");
                if(message==null)
                {
                    chat.setNewMessage("");
                }
            }
            chat.setDocumentId((String) chat_item.get("selfDocId"));
            chat.setReceiverUid((String) chat_item.get("selfUid"));
            chat.setReceiverImageUrl((String) chat_item.get("receiverImageUrl"));
            chat.setReceiver_fb_id((String) chat_item.get("userFbid"));
            chat.setReceiverName((String) chat_item.get("receiverName"));
            chat.setProductId((String) chat_item.get("productId"));
            chat.setStatus((String)chat_item.get("status"));
            /**
             * Adding chat to the list*/
            mChatData_temp.add(chat);
        }

        /**
         * Checking user is in my match list or not
         * */

        ChatList_Chat_Item match_data_chat;

        if(profile_list!=null)
        {
            /**
             * Clearing the list data.For re entry the new Data.*/
            mChatData.clear();

            recyclerView_chat.setVisibility(View.VISIBLE);
            /**
             * Hiding rest of the things.*/
            progressDialog.setVisibility(View.GONE);
            connection_failed_rl.setVisibility(View.GONE);
            no_user_rl.setVisibility(View.GONE);

            for(int count=0;count<profile_list.size()&&profile_list.get(count).getUSER_EMAIL_ID()!=null;count++)
            {
                boolean in_chat_Db=false;

                for(int temp_count=0;temp_count<mChatData_temp.size();temp_count++)
                {

                    if(mChatData_temp.get(temp_count).getReceiverUid().equals(profile_list.get(count).getUSER_EMAIL_ID()))
                    {
                        in_chat_Db=true;
                        /**
                         * Update user details in user db.*/
                        AppController.getInstance().update_user_details(profile_list.get(count).getUSER_EMAIL_ID(),profile_list.get(count).getUSER_FIRST_NAME(), profile_list.get(count).getUSER_PROFILE_PICTURE(), profile_list.get(count).getSTATUS(), profile_list.get(count).getFB_ID());
                        mChatData_temp.get(temp_count).setReceiverImageUrl(profile_list.get(count).getUSER_PROFILE_PICTURE());
                        mChatData_temp.get(temp_count).setStatus(profile_list.get(count).getSTATUS());
                        mChatData_temp.get(temp_count).setReceiverName(profile_list.get(count).getUSER_FIRST_NAME());
                        mChatData_temp.get(temp_count).setReceiver_fb_id(profile_list.get(count).getFB_ID());
                        /**
                         * Adding to the list.*/
                        mChatData.add(mChatData_temp.get(temp_count));

                    }

                }

                if(!in_chat_Db)
                {
                    /**
                     * Creating the chat doc for the new User in match list.*/
                    String  chat_user_doc_id= AppController.getInstance().findDocumentIdOfReceiver(profile_list.get(count).getUSER_EMAIL_ID());

                    if(chat_user_doc_id==null || chat_user_doc_id.equals(""))
                    {
                        AppController.getInstance().create_user_new_doc_id(profile_list.get(count).getUSER_EMAIL_ID(),profile_list.get(count).getUSER_FIRST_NAME(),profile_list.get(count).getUSER_PROFILE_PICTURE(),profile_list.get(count).getFB_ID(),profile_list.get(count).getSTATUS());
                    }

                    match_data_chat=new ChatList_Chat_Item();
                    match_data_chat.setReceiverUid(profile_list.get(count).getUSER_EMAIL_ID());
                    match_data_chat.setReceiverName(profile_list.get(count).getUSER_FIRST_NAME());
                    match_data_chat.setReceiverImageUrl(profile_list.get(count).getUSER_PROFILE_PICTURE());
                    match_data_chat.setReceiver_fb_id(profile_list.get(count).getFB_ID());
                    match_data_chat.setStatus(profile_list.get(count).getSTATUS());
                    mChatData.add(match_data_chat);
                }

                /**
                 * Sorting the list.*/
                Collections.sort(mChatData);
                /**
                 * Updating the Recycle view.*/
                mactivity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mAdapter.notifyDataSetChanged();
                    }
                });

            }
            /**
             * */
            if(mChatData!=null&&mChatData.size()>0)
            {
                recyclerView_chat.scrollToPosition(0);
            }else
            {
                /**
                 * No user having so ..showing it .*/
                recyclerView_chat.setVisibility(View.GONE);
                connection_failed_rl.setVisibility(View.GONE);
                no_user_rl.setVisibility(View.VISIBLE);
            }

        } else
        {
            /**
             * No user having so ..showing it .*/
            recyclerView_chat.setVisibility(View.GONE);
            connection_failed_rl.setVisibility(View.GONE);
            no_user_rl.setVisibility(View.VISIBLE);
        }
    }


    /**
     * Checking Weather a user is already in List .
     * if already in list then return position if not then return -1;
     * @param sender contains the user unique user id.*/
    public int alreadyInContact(String sender)
    {
        int j=-1;
        for(int i=0;i<mChatData.size();i++)
        {
            if(mChatData.get(i).getReceiverUid().equals(sender))
            {
                j=i;
                break;
            }
        }
        return j;
    }


    @SuppressWarnings("unchecked")
    @Override
    public void onResume()
    {
        super.onResume();
        Load_match_list();

    }

    /**
     * <h2>Load_match_list</h2>
     * <P>
     *  Method first check weather socket is connected or not.
     *  if not then try to reconnect that .
     * </P>*/
    public void Load_match_list()
    {
        if(!AppController.getInstance().isSocketConnected())
        {
            AppController.connectSocket();
        }

        /**
         * Extracting data from db.*/
        getDataFromDB();

    }

    /**
     * <h2>getMatchedProfile</h2>
     * <p>
     *     Method call a async task to do the service call to get the data  from the server.
     * </p>
     * @param isData_not_present_in_db contains the boolean flag to say the user list already showing to user or not .if showing then don,t show the progress bar.
     * @param mcontext contains the current calling activity reference.*/
    private void getMatchedProfile(Activity mcontext,final boolean isData_not_present_in_db)
    {
        /**
         * Checking the network filed layout is active or not.*/
        if(connection_failed_rl!=null)
        {
            connection_failed_rl.setVisibility(View.GONE);
        }

        if(progressDialog!=null&&isData_not_present_in_db)
        {
            progressDialog.setVisibility(View.VISIBLE);
        }
        if(no_user_rl!=null&&isData_not_present_in_db)
        {
            no_user_rl.setVisibility(View.GONE);
        }

        if(Utility.isNetworkAvailable(mcontext))
        {
            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id",Utility.getDeviceId(mcontext));
                jsonObject.put("ent_sess_token", sessionManager.getToken());
            } catch (JSONException e)
            {
                e.printStackTrace();
            }

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_MATCHED_PROFILE,OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {

                    if (progressDialog != null)
                    {
                        progressDialog.setVisibility(View.GONE);
                    }
                    if(swipe_refresh_layout!=null)
                    {
                        swipe_refresh_layout.setRefreshing(false);
                    }
                    matchesDataResHandler(result,isData_not_present_in_db);
                }

                @Override
                public void onError(String error)
                {
                    if(swipe_refresh_layout!=null)
                    {
                        swipe_refresh_layout.setRefreshing(false);
                    }
                    if (progressDialog != null)
                    {
                        progressDialog.setVisibility(View.GONE);
                        if(isData_not_present_in_db)
                        {

                            connection_failed_rl.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });
        }else
        {
            if(swipe_refresh_layout!=null)
            {
                swipe_refresh_layout.setRefreshing(false);
            }
            if(progressDialog!=null)
            {
                progressDialog.setVisibility(View.GONE);
            }
            /**
             * Aleret for no Internet Connection.*/
            if(net_work_failed_aleret!=null)
            {
                net_work_failed_aleret.net_work_fail(mcontext, new Net_work_failed_aleret.Internet_connection_Callback()
                {
                    @Override
                    public void onSucessConnection(String connection_Type)
                    {
                        Toast.makeText(mactivity.getApplicationContext(),connection_Type,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onErrorConnection(String error)
                    {
                        Toast.makeText(mactivity.getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if(isData_not_present_in_db)
            {
                connection_failed_rl.setVisibility(View.VISIBLE);
            }
        }

    }
    /**
     * <h2>
     *     matchesDataResHandler
     * </h2>
     * <P>
     *     Method handel the response of the matches service.
     *     and setting visible to a layout is the data is not found and
     *     invisible is the user list is available is found.
     * </P>
     * @param response contains the response data from the server.
     */
    private void matchesDataResHandler(String response,boolean isData_not_present_in_db)
    {
        Log.d("GetmatchRequest",response);

        MatchesListPojo matchesListPojo;
        Gson gson = new Gson();
        matchesListPojo = gson.fromJson(response, MatchesListPojo.class);

        if (matchesListPojo.getErrorFlag().equals("0"))
        {
            if(matchesListPojo.getMatches()!=null&&matchesListPojo.getMatches().size()>0||matchesListPojo.getBlocked().size()>0)
            {
                /**
                 * clearing to the array list before calling the service.*/
                mathes_user_list_data.clear();
                /**
                 * Handling the if and else case. and adding the match list first*/
                mathes_user_list_data.addAll(matchesListPojo.getMatches());
                /**
                 * Adding the blocked list.*/
                mathes_user_list_data.addAll(matchesListPojo.getBlocked());
                /**
                 * Inserting data to the DB.*/
                insert_data_to_db(mathes_user_list_data);

            }else
            {
                /**
                 * Clearing the Data base data to say as no user.*/
                insert_data_to_db(null);
                if(isData_not_present_in_db)
                {
                    recyclerView_chat.setVisibility(View.GONE);
                    connection_failed_rl.setVisibility(View.GONE);
                    no_user_rl.setVisibility(View.VISIBLE);
                }

            }
        }else if(matchesListPojo.getErrorFlag().equals("1"))
        {
            /**
             * Clearing the Data base data to say as no user.*/
            insert_data_to_db(null);
            recyclerView_chat.setVisibility(View.GONE);
            connection_failed_rl.setVisibility(View.GONE);
            no_user_rl.setVisibility(View.VISIBLE);
           /* if(isData_not_present_in_db)
            {
                no_user_rl.setVisibility(View.VISIBLE);
            }*/

        }
        else if(isData_not_present_in_db)
        {
            connection_failed_rl.setVisibility(View.VISIBLE);
        }
    }

    /**
     * <h2>insert_data_to_db</h2>
     * <P>
     *     Inserting data to the DB for later use.
     * </P>*/
    private void insert_data_to_db(ArrayList<MatchesListData> mathes_user_list_data)
    {
        MatchesListData data;
        /**
         * Clearing the Data base.for fresh data.*/
        match_list_data_holder.ClearDataBae();

        if(mathes_user_list_data!=null)
        {
            for(int count=0;count<mathes_user_list_data.size();count++)
            {
                data=mathes_user_list_data.get(count);
                match_list_data_holder.addSingle_Item(sessionManager.getFbId(),data.getFbId(), data.getEmail(),data.getProfilePhoto(), data.getFirstName(),data.getAge(),data.getStatus(),data.getLastActive());
            }
        }

        /**
         * Updating the list*/
        ArrayList<Match_list_data_pojo> match_list_data=match_list_data_holder.getAllMatch_Data(sessionManager.getFbId());
        addChat(match_list_data);

    }
    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    /**
     *onRefresh  method of the sweep to reload data.*/
    @Override
    public void onRefresh()
    {
        Load_match_list();
    }

}
