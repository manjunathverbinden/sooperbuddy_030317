package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.three_embed.datum.AdapterClass.MyProfileDetailsAdap;
import com.three_embed.datum.AdapterClass.Profile_pic_adapter;
import com.three_embed.datum.Aleret_dialogs.User_hint_popup_layout;
import com.three_embed.datum.Pojo_classes.Images_url_pojo_class;
import com.three_embed.datum.Pojo_classes.SetPreferenceAlData;
import com.three_embed.datum.Pojo_classes.SetPreferencePojo;
import com.three_embed.datum.Pojo_classes.UserDetails_pojo;
import com.three_embed.datum.Pojo_classes.user_list_item_details;
import com.three_embed.datum.Utility.CirclePageIndicator;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.ShadowTransformer;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Aramis_home.EditProfile.Edit_profile_pic;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
/**
 * <h1>MyProfileActivity</h1>
 * <P>
 *
 * </P>*/
public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener
{
    private CoustomListview mListView;
    private ViewPager profile_pic_pager;
    private CirclePageIndicator profile_pic_indicator;
    private ShadowTransformer shadowTransformer;
    private RelativeLayout profile_edt_rl;
    private RelativeLayout retry_button;
    private Context context;
    private SessionManager session;
    private UserDetails_pojo profilePojo;
    private int Screenwidth=0;
    private Images_url_pojo_class images_url_pojo_class;
    public Typeface GothamRoundedBold;
    private  ArrayList<user_list_item_details> images_list;
    private ProgressBar loading_progress_bar;
    private User_hint_popup_layout user_hint_popup_layout=null;
    private ImageView retry_image_view;
    private PopupWindow user_hint;
    private MyProfileDetailsAdap profileDetailsAdap;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        /**
         * Reading the screen height to set the max Zoom size of the Coustom Listvew Header..*/
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        Screenwidth= displaymetrics.heightPixels;

        context=MyProfileActivity.this;
        session =new SessionManager(context);

        images_url_pojo_class=new Images_url_pojo_class();
        shadowTransformer=new ShadowTransformer();
        /**
         * User hint popup layout.*/
        user_hint_popup_layout=User_hint_popup_layout.getInsatance();
        /**
         * Font style.*/
        GothamRoundedBold=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Bold.otf");
        /**
         * Creating array List for Video url and Image Url List.*/
        images_list=new ArrayList<>();
        intializationView();

    }

    /**
     * <h2>intializationView</h2>
     * <P>
     *
     * </P>*/
    private void intializationView()
    {
        /**
         *Setting coustom action bar in android.. */
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View customView = inflator.inflate(R.layout.myprofile_activity_title_bar, null);
        actionBar.setCustomView(customView);

        /**
         * setting the coustom action bar left and right margin as Zero by passing the 0,0 absloute value.
         * */
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        /**
         * title bar controlar.*/
        RelativeLayout relative_backbutton = (RelativeLayout) findViewById(R.id.relative_backbutton);
        assert relative_backbutton != null;
        relative_backbutton.setOnClickListener(this);

        profile_edt_rl=(RelativeLayout)findViewById(R.id.profile_edt);
        assert profile_edt_rl != null;
        profile_edt_rl.setOnClickListener(this);

        retry_button=(RelativeLayout)findViewById(R.id.retry_button);
        assert retry_button != null;
        retry_button.setOnClickListener(this);

        retry_image_view=(ImageView)findViewById(R.id.retry_image_view);
        /**
         * Body details..*/
        loading_progress_bar=(ProgressBar) findViewById(R.id.loading_progress_bar);

        mListView = (CoustomListview) findViewById(R.id.layout_listview);
        @SuppressLint("InflateParams") View header = LayoutInflater.from(this).inflate(R.layout.customlistviewheader,null);
        profile_pic_pager=(ViewPager)header.findViewById(R.id.pager);
        profile_pic_pager.setOnPageChangeListener(pageChangeListener);
        profile_pic_pager.setPageTransformer(false,shadowTransformer);
        profile_pic_indicator=(CirclePageIndicator)header.findViewById(R.id.indicator);
        RelativeLayout custoomlistview_heAder = (RelativeLayout) header.findViewById(R.id.listviewheader);
        mListView.setZoomRatio(Screenwidth);
        mListView.setCustomListHeaderImageView(custoomlistview_heAder);
        mListView.addHeaderView(header);

        TextView action_bar_title_txt = (TextView) findViewById(R.id.action_bar_title);
        assert action_bar_title_txt != null;
        action_bar_title_txt.setTypeface(GothamRoundedBold);
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        if(profileDetailsAdap!=null)
        {
            profileDetailsAdap.setDownLoadFlage();
        }

        /**
         * Setting user data because already have.*/
        set_User_data();

        /**
         * Checking if the user is present.*/
        if(!session.get_user_data_status())
        {
            retry_button.setVisibility(View.VISIBLE);
            profile_edt_rl.setVisibility(View.GONE);
            if(Utility.isNetworkAvailable(MyProfileActivity.this))
            {
                getProfileDatas();
            }else
            {
                profile_edt_rl.setVisibility(View.GONE);
                retry_button.setVisibility(View.VISIBLE);
                retry_image_view.setVisibility(View.VISIBLE);
                loading_progress_bar.setVisibility(View.GONE);
                user_hint=user_hint_popup_layout.show_popup_user_hint(retry_image_view,MyProfileActivity.this,getApplicationContext().getString(R.string.networknotavailable));
            }
        }else
        {
            loading_progress_bar.setVisibility(View.GONE);
            profile_edt_rl.setVisibility(View.VISIBLE);
            retry_button.setVisibility(View.GONE);
        }
    }

    /**
     * This method is used to do service call for getting all datas for my profile.
     */
    private void getProfileDatas()
    {

        retry_image_view.setVisibility(View.GONE);
        loading_progress_bar.setVisibility(View.VISIBLE);
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_dev_id",Utility.getDeviceId(context));
            jsonObject.put("ent_data_time",Utility.getCurrentGmtTime());
            jsonObject.put("ent_sess_token",session.getToken());
            jsonObject.put("ent_fbid",session.getFbId());

        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_PROFILE,OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                Log.d("Result12",result);

                if(loading_progress_bar!=null)
                {
                    loading_progress_bar.setVisibility(View.GONE);
                }
                profile_edt_rl.setVisibility(View.VISIBLE);
                retry_button.setVisibility(View.GONE);

                Gson gson = new Gson();
                profilePojo = gson.fromJson(result, UserDetails_pojo.class);

                if ("0".equals(profilePojo.getErrorFlag()))
                {
                    /**
                     * Telling thet user details found.*/
                    session.set_user_data_status(true);
                    /**
                     * Saving user data to Session manager.*/
                    save_User_Data(profilePojo);
                    /**
                     * Setting user data to xml.*/
                    set_User_data();
                }
            }
            @Override
            public void onError(String error)
            {
                /**
                 * Setting this to say that i don,t have user details.*/
                session.set_user_data_status(false);

                if(loading_progress_bar!=null)
                {
                    loading_progress_bar.setVisibility(View.GONE);
                }
                profile_edt_rl.setVisibility(View.GONE);
                retry_button.setVisibility(View.VISIBLE);
                retry_image_view.setVisibility(View.VISIBLE);
                loading_progress_bar.setVisibility(View.GONE);
                /**
                 * Showing the aleret on Connection failed.*/
                user_hint=user_hint_popup_layout.show_popup_user_hint(retry_image_view,MyProfileActivity.this,error);
            }
        });
    }

    /**
     * <h2>save_User_Data</h2>
     * <P>
     *Storing all the user details to the session manager.
     * </P>*/
    private void save_User_Data(UserDetails_pojo userDetails)
    {
        session.setFirstName(userDetails.getFirstName());
        session.setLastName(userDetails.getLastName());
        session.setAge(userDetails.getAge());
        session.setAbout(userDetails.getAbout());
        session.setUser_Gender(userDetails.getGender());
       /* *
          * Clearing the array list..*/
        images_url_pojo_class.getImageListUrl().clear();
        /**
         * Storing user data at first as Image list.*/
        images_url_pojo_class.getImageListUrl().add(profilePojo.getProfilePhoto());
        if (profilePojo.getImages().size() > 0)
        {
            for(int count=0;(count<4 && count<profilePojo.getImages().size());count++)
            {
                images_url_pojo_class.getImageListUrl().add(profilePojo.getImages().get(count));
            }
        }
        String image_url = new Gson().toJson(images_url_pojo_class);
        session.addImagesUrl(image_url);
        /**
         * Storing video url.*/
        session.setVideourl(userDetails.getProfileVideo());
        session.setVideo_thumbnail_url(userDetails.getVideoThumnail());
    }

    /**
     * <h2>set_User_data</h2>
     * <P>
     *   Method set all the Data to the user content.
     * </P>*/
    private void set_User_data()
    {
        /**
         * Clearing the List for refresh Data.*/
        images_list.clear();
        /**
         * Adding video url.
         * if exist.*/
        if(session.getVideourl()!=null&&!session.getVideourl().isEmpty()&&!session.getVideo_thumbnail_url().isEmpty())
        {
            user_list_item_details user_list_item_details=new user_list_item_details();
            user_list_item_details.setThumb_nail_url(session.getVideo_thumbnail_url());
            user_list_item_details.setVideo_url(session.getVideourl());
            images_list.add(user_list_item_details);
        }

        String images_url=session.getIMages_url();
        images_url_pojo_class=new Gson().fromJson(images_url, Images_url_pojo_class.class);

        for(int count=0;count<images_url_pojo_class.getImageListUrl().size();count++)
        {
            user_list_item_details user_list_item_details=new user_list_item_details();
            user_list_item_details.setImage_url(images_url_pojo_class.getImageListUrl().get(count));
            images_list.add(user_list_item_details);
        }
        Profile_pic_adapter profile_pic_adapter = new Profile_pic_adapter(MyProfileActivity.this, images_list);
        profile_pic_pager.setAdapter(profile_pic_adapter);
        profile_pic_indicator.setViewPager(profile_pic_pager);
        /**
         * Setting bottom user Details*/
        ArrayList<String> data_item=new ArrayList<>();
        data_item.add(null);

         profileDetailsAdap = new MyProfileDetailsAdap(context, session.getFirstName(), session.getAge(), session.getUser_Gender(), session.getAbout(), data_item);
        mListView.setAdapter(profileDetailsAdap);

    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.relative_backbutton :
                finish();
                overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                break;

            case R.id.profile_edt:
                startActivity(new Intent(MyProfileActivity.this,Edit_profile_pic.class));
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;
            case R.id.retry_button:
                if(user_hint!=null)
                {
                    user_hint.dismiss();
                }
                getProfileDatas();
                break;

        }
    }

    ViewPager.OnPageChangeListener pageChangeListener=new ViewPager.OnPageChangeListener()
    {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {

        }

        @Override
        public void onPageSelected(int position)
        {
        }

        @Override
        public void onPageScrollStateChanged(int state)
        {

        }
    };
}