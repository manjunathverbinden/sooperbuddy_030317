package com.three_embed.datum.Cardview;

/**
 * Created by shobhit on 11/1/16.
 */
public class ProgressParameter
{
    float scrollProgressPercent;
    int isHorizontal;
    float scrollHorizontalProgressPercent;

    public float getScrollHorizontalProgressPercent() {
        return scrollHorizontalProgressPercent;
    }

    public void setScrollHorizontalProgressPercent(float scrollHorizontalProgressPercent) {
        this.scrollHorizontalProgressPercent = scrollHorizontalProgressPercent;
    }

    public float getScrollProgressPercent()
    {
        return scrollProgressPercent;
    }

    public void setScrollProgressPercent(float scrollProgressPercent)
    {
        this.scrollProgressPercent = scrollProgressPercent;
    }

    public int getIsHorizontal()
    {
        return isHorizontal;
    }

    public void setIsHorizontal(int isHorizontal)
    {
        this.isHorizontal = isHorizontal;
    }
}
