package com.three_embed.datum.AdapterClass;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.three_embed.datum.Aramis_home.Chat_message_fragment;
import com.three_embed.datum.Aramis_home.Chat_user_abouts;

/**
 * @since  10/3/16.
 */
public class Chat_page_sweep_adapter extends FragmentStatePagerAdapter
{
    int mNumOfTabs;
    private   Fragment caht_fragment;

    public Chat_page_sweep_adapter(FragmentManager fm, int NumOfTabs)
    {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position)
    {

        switch (position)
        {
            case 0:
                caht_fragment= new Chat_message_fragment();
                return caht_fragment;
            case 1:
                return new Chat_user_abouts();
            default:
                return null;
        }
    }

    @Override
    public int getCount()
    {
        return mNumOfTabs;
    }

    public Fragment getHostFragment()
    {
      return caht_fragment;
    }
}