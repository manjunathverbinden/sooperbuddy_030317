package com.three_embed.datum.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.three_embed.datum.chat_lib.AppController;

/**
 * <h>SessionManager</h>
 * <P>
 *     Class contains SharedPreferences objects and method to store data in SharedPreferences object in android.
 * </P>
 * @since 22-02-2016
 * @author 3Embed.
 */
public class SessionManager
{
    private SharedPreferences preferences;
    private Editor editor;
    /**
     * "0 " for private mode of acess of data.*/
    private int PRIVATE_MODE=0;
    private Context mcontext;
    public static final String PREF_NAME="datum_session_manager";
    private static final String PUSHTOKEN="pushtoken";
    private static final String VIDEOURL="videourl";
    private static final String VIDEO_THUMBNAIL_URL="video_thumb_nail";
    private static final String ABOUT="userabout";
    private static final String LATITUDE="latitude";
    private static final String LOGITUDE="logitude";
    private static final String FIRSTNAME="firstname";
    private static final String LASTNAME="lastname";
    private static final String AGE="userage";
    private static final String FBID="facebookId";
    private static final String ISFBLOGIN="isfblogin";
    private static final String ISLOGIN="isLoggedIn";
    private static final String PROFILE_PIC_URL="profile_pic_url";
    private static final String ALLiMAGE_URL="image_urls";
    private static final String MY_DETAILS ="my_details";
    private static final String USER_CURRENT_STATUS="current_status";
    private static final String APP_PURCHASED_DURATION="Duration_of_inApp";
    private static final String USER_EMAIL="user_email";
    private static final String USER_GENDER="gender";
    private static final String STATUS_LIST="status_list";


    public SessionManager(Context context)
    {
        this.mcontext = context;
        preferences=mcontext.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor=preferences.edit();
        editor.commit();
    }


    public void clear_session_manager()
    {
        editor.clear();
        editor.commit();
    }

    /**
     * <p>
     *     Storing the Image.
     * </p>
     * @return String
     */
    public String getIMages_url()
    {
        return preferences.getString(ALLiMAGE_URL,"");
    }

    public void addImagesUrl(String image_url)
    {
        editor.putString(ALLiMAGE_URL, image_url);
        editor.commit();
    }

    /**
     * <p>
     *     to save user gender.
     * </p>
     * @return String
     */
    public String getUser_Gender()
    {
        return preferences.getString(USER_GENDER,"");
    }
    public void setUser_Gender(String gender)
    {
        editor.putString(USER_GENDER,gender);
        editor.commit();
    }

    public String get_status_List()
    {
        return preferences.getString(STATUS_LIST, "");
    }

    public void set_status_List(String status[])
    {
        StringBuilder sb = new StringBuilder();
        for (String statu : status) {
            sb.append(statu).append(",");
        }
        editor.putString(STATUS_LIST, sb.toString());
        editor.commit();
    }
    /**
     * Saving user bought app or not.*/
    public void set_Current_status(String current_status)
    {
        editor.putString(USER_CURRENT_STATUS, current_status);
        editor.commit();
    }

    public String get_Current_status()
    {
        return preferences.getString(USER_CURRENT_STATUS,"");
    }

    /**
     * <p>
     *     to save session token
     * </p>
     * @return String
     */
    public String getToken()
    {
        return preferences.getString(PUSHTOKEN,"");
    }
    public void setToken(String token)
    {
        editor.putString(PUSHTOKEN,token);
        editor.commit();
    }

    /**
     * Storing location latitude of location .*/
    public String getLatitude()
    {
        return preferences.getString(LATITUDE,"");
    }
    public void setLatitude(String latitude)
    {
        editor.putString(LATITUDE,latitude);
        editor.commit();
    }

    /**
     * Storing user email.*/
    public void set_User_email(String user_email)
    {
        editor.putString(USER_EMAIL,user_email);
        editor.commit();
    }

    public String get_User_email()
    {
        return preferences.getString(USER_EMAIL,null);
    }

/**
 * Storing Logitude .of location*/

    public String getLogitude()
    {
        return preferences.getString(LOGITUDE,"");
    }
    public void setLogitude(String logitude)
    {
        editor.putString(LOGITUDE,logitude);
        editor.commit();
    }
    /**
     * to save facebook id
     */
    public String getFbId() {
        return preferences.getString(FBID, null);
    }

    public void setFbId(String fbId) {
        editor.putString(FBID, fbId);
        editor.commit();
    }

    /**
     * user is log in.*/
    public boolean getUserLogin_status()
    {
        return preferences.getBoolean(ISLOGIN, false);
    }

    public void setUserLoginStatus(boolean isLoggedIn)
    {
        editor.putBoolean(ISLOGIN, isLoggedIn);
        editor.commit();
    }


    /**
     * user is log in.*/
    public boolean get_User_fb_login_status()
    {
        return preferences.getBoolean(ISFBLOGIN, false);
    }

    public void set_User_fb_login_status(boolean isLoggedIn)
    {
        editor.putBoolean(ISFBLOGIN, isLoggedIn);
        editor.commit();
    }

    /**
     * first name.
     */
    public String getFirstName()
    {
        return preferences.getString(FIRSTNAME, null);
    }

    public void setFirstName(String fbFirstName)
    {
        editor.putString(FIRSTNAME, fbFirstName);
        editor.commit();
    }
/**
 * last name .*/
    public String getLastName()
    {
        return preferences.getString(LASTNAME,null);
    }

    public void setLastName(String fbLastName)
    {
        editor.putString(LASTNAME,fbLastName);
        editor.commit();
    }

    /**
     * profile_pic_url..*/
    public String getFbProfilePictureUri()
    {
        return preferences.getString(PROFILE_PIC_URL,null);
    }

    public void setFbProfilePictureUri(String fbProfilePictureUri)
    {
        editor.putString(PROFILE_PIC_URL,fbProfilePictureUri);
        editor.commit();
    }

    /**
     * profile video url.*/
    public void setVideourl(String videourl)
    {

        editor.putString(VIDEOURL,videourl);
        editor.commit();
    }
    public String getVideourl()
    {
        return preferences.getString(VIDEOURL,"");
    }

    /**
     * profile video url.*/
    public void setVideo_thumbnail_url(String video_thumb_nail_url)
    {

        editor.putString(VIDEO_THUMBNAIL_URL,video_thumb_nail_url);
        editor.commit();
    }
    public String getVideo_thumbnail_url()
    {
        return preferences.getString(VIDEO_THUMBNAIL_URL,"");
    }
    /**
     * Saving user about from facebook Log in.*/
    public void setAbout(String user_about)
    {
        editor.putString(ABOUT,user_about);
        editor.commit();
    }
    public String getAbout()
    {
        return preferences.getString(ABOUT,"");
    }

    /**
     * Saving user about age*/
    public void setAge(String age)
    {

        editor.putString(AGE, age);
        editor.commit();
    }
    public String getAge()
    {
        return preferences.getString(AGE, "22");
    }

    /**
     * Saving user image list found or not*/
    public void set_user_data_status(boolean found)
    {
        editor.putBoolean(MY_DETAILS, found);
        editor.commit();
    }
    public boolean get_user_data_status()
    {
        return preferences.getBoolean(MY_DETAILS, false);
    }



    public void set_app_purchese_typ(String type)
    {
        editor.putString(APP_PURCHASED_DURATION,type);
        editor.commit();
    }

    public String getAppPurchase_type()
    {
        return preferences.getString(APP_PURCHASED_DURATION,"");
    }

}
