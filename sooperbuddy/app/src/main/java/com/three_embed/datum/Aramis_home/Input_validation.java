package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.widget.EditText;
import android.widget.TextView;
import com.three_embed.datum.Utility.Utility;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>Input_validation</h1>
 * Class simply a single-tone class contains method to Validate user Input.
 * <p>
 *    Class contains Method to Validate the user Input Taken By The user.
 *    And also validate the Mandatory field missing input.
 * </p>
 * @author 3Embed
 * @since 26/11/15
 * @version 1.0.
 */
public class Input_validation
{
    private static Input_validation input_validation;
    /**
     * Making this class as Single-tone.*/
  private Input_validation() {}
    /**
     * <h2>getInput_validation_instance</h2>
     * Method used to get the instance of the Input_validation.
     * <p>
     *     Checking the input_validation instance is null or not .
     *     if not null then return that reference .
     *     if null then create the reference by new operator then return.
     * </p>*/
    public static Input_validation getInput_validation_instance()
    {
        if(input_validation==null)
        {
            input_validation=new Input_validation();

        }
        return input_validation;
    }

    /**
     * <h2>isEmptyFirstName</h2>
     * <P>
     *     This method Validate the Edit-text text weather it is null or not .
     *     return true if it is not null else return false.
     * </P>
     * @param  user_text contains the EditText parameter to be validate*/

    public boolean isEmptyFirstName(EditText user_text,Activity activity)
    {
        if(user_text.getText().toString().equals(""))
        {
            user_text.setError(activity.getApplicationContext().getResources().getString(R.string.first_name_empty));
            user_text.setHintTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.redcolor));
            return false;
        }
        else
        {
            return true;
        }
    }
    /**
     * <h2>validateEmail</h2>
     * <P>
     *     This method Validate the Edit-text text wheatehr it is null or not .
     *     if not then check its is a email_id or not .
     *     return true if it is not null and a Valid email id.
     *     else
     *     return false.
     * </P>
     * @param  user_id contains the EditText parameter to be validate*/
    public boolean validateEmail(EditText user_id,Activity activity)
    {
        if(user_id.getText().toString().equals(""))
        {
            user_id.setError(activity.getApplicationContext().getResources().getString(R.string.UsidMandatory));
            user_id.setHintTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.redcolor));
            return false;
        }
        else if(Utility.isEmailValid(user_id.getText().toString().trim()))
        {
            return true;
        }else
        {
            user_id.setError(activity.getApplicationContext().getResources().getString(R.string.emailnotvalid));
            user_id.setTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.redcolor));
            return false;
        }

    }

    /**
     * <h2>isGenderisEmpty</h2>
     * <P>
     *     This method Validate the Edit-text text weather it is null or not .
     *     return true if it is not null else return false.
     * </P>
     * @param  gender_text contains the EditText parameter to be validate*/

    public boolean isGenderisEmpty(TextView gender_text,Activity activity)
    {
        gender_text.setInputType(InputType.TYPE_NULL);

        if(gender_text.getText().toString().equals(activity.getResources().getString(R.string.gender)))
        {
            gender_text.setError(activity.getApplicationContext().getResources().getString(R.string.check_gender));
            gender_text.setTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.redcolor));
            return false;
        }else
        {
            return true;
        }
    }


    /**
     * <h2>isBirthday_empty</h2>
     * <P>
     *     This method Validate the Edit-text text weather it is null or not .
     *     return true if it is not null else return false.
     * </P>
     * @param  birth_text contains the EditText parameter to be validate*/
    public boolean isBirthday_empty(EditText birth_text,Activity activity)
    {
        if(birth_text.getText().toString().equals(""))
        {
            birth_text.setError(activity.getApplicationContext().getResources().getString(R.string.date_of_birth));
            birth_text.setHintTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.redcolor));
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * <h2>isPassword_empty</h2>
     * <P>
     *     This method Validate the Edit-text text weather it is null or not .
     *     return true if it is not null else return false.
     * </P>
     * @param  pasword_edt contains the EditText parameter to be validate*/

    public boolean isPassword_empty(EditText pasword_edt,Activity activity)
    {
        if(pasword_edt.getText().toString().equals(""))
        {
            pasword_edt.setError(activity.getApplicationContext().getResources().getString(R.string.paswrod));
            pasword_edt.setHintTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.redcolor));
            return false;
        }else if(pasword_edt.getText().toString().length()>=6)
        {
            return true;
        }else
        {
            pasword_edt.setError(activity.getApplicationContext().getResources().getString(R.string.weak_password));
            pasword_edt.setTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.redcolor));
            return false;
        }
    }


    /**
     * <h2>validate_Confirm_Password</h2>
     * <P>
     *     This method Validate the Edit-text text weather it is null or not .
     *     return true if it is not null else return false.
     * </P>
     * @param  pasword_edt contains the EditText parameter to be validate*/

    public boolean validate_Confirm_Password(EditText pasword_edt,EditText confirm_edt,Activity activity)
    {
        if(confirm_edt.getText().toString().equals(""))
        {
            confirm_edt.setError(activity.getApplicationContext().getResources().getString(R.string.confirm_empty));
            confirm_edt.setHintTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.redcolor));
            return false;
        }
        else if(pasword_edt.getText().toString().equals(confirm_edt.getText().toString()))
        {
            return true;
        }
        else
        {
            confirm_edt.setError(activity.getApplicationContext().getResources().getString(R.string.pasword_not_match));
            confirm_edt.setTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.redcolor));
            return false;
        }
    }


    /**
     * <h2>adult_age_Validation</h2>
     * Method is used to check user is adult or not
     * <P>
     *     This method is used to validate the weather user is adult or not by subtracting current date with the
     *     user date of birth date.
     *     if User is not Adult then setting text color as Red and error as user is not adult
     *     returning 0.
     *     else
     *     Returning returning age.
     * </P>*/
    public int adult_age_Validation(EditText editText,Activity activity)
    {
        if(validateDate_format(editText.getText().toString().trim()))
        {
        Date current_date,user_dob;
        Calendar current_calendar,dob_calendar;
        int adult_age=0;
        String currentTime= Utility.getCurrentDate();
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            @SuppressLint("SimpleDateFormat") DateFormat dateFormat_current = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            current_date=dateFormat_current.parse(currentTime);
            user_dob=dateFormat.parse(editText.getText().toString());

            current_calendar= getCalendar(current_date);
            dob_calendar=getCalendar(user_dob);
            /**
             * Subtracting the current and other user chosen year. */
            adult_age=current_calendar.get(Calendar.YEAR)-dob_calendar.get(Calendar.YEAR);


        } catch (Exception e)
        {
            e.printStackTrace();
        }
        if(adult_age>=18)
        {
            return adult_age;
        }else
        {
            editText.setError(activity.getApplicationContext().getResources().getString(R.string.not_adult));
            Utility.ShowAlertDialog(activity.getApplicationContext().getResources().getString(R.string.not_adult), activity);
            editText.setTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.redcolor));
            return adult_age;
        }
        }else
        {
            editText.setError(activity.getApplicationContext().getResources().getString(R.string.date_not_in_format));
            editText.setTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.redcolor));
            return 0;
        }
    }
    /**
     * <h2>getCalendar</h2>
     * Method use to get Calender object from the given Date reference.
     * <p>
     *     Method receive a Date reference as parameter and then create the Calendar object and set the date as
     *     given date reference .
     *     Then return that calender object.
     * </p>
     * @param date contains the Date reference to set in calender.*/
    private static Calendar getCalendar(Date date)
    {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    /**
     * <h2>validateDate_format</h2>
     * Method use to validate a given Date as in the format yyyy-mm-dd.
     * If matches then return true or else return false.
     * @param date contain the date String to validate.*/
    private boolean validateDate_format(String date)
    {
        String expression ="^\\d{2}-\\d{2}-\\d{4}$";
        Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(date);
        return matcher.matches();

    }

}
