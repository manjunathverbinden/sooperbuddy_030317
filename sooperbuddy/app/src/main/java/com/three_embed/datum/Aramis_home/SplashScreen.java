package com.three_embed.datum.Aramis_home;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import com.three_embed.datum.Permission_Ap23.App_permission_23;
import com.three_embed.datum.Utility.AppSession;
import com.three_embed.datum.Utility.Application_checker_service;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.chat_lib.AppController;
import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class SplashScreen extends AppCompatActivity implements SplashScreen_controlar.Notifier_data
{
    private SessionManager sessionManager;
    private AppSession appSession;
    private App_permission_23 app_permission_23;
    private ArrayList<App_permission_23.Permission> permissions;
    private SplashScreen_controlar splashScreen_controlar;
    private CountDownTimer countDownTimer=null;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);
        sessionManager=new SessionManager(SplashScreen.this);
        appSession=new AppSession(SplashScreen.this);
        /**
         * Starting application checker service.*/
        startService(new Intent(getBaseContext(),Application_checker_service.class));
        app_permission_23=App_permission_23.getInstance();
        permissions=new ArrayList<>();
        permissions.add(App_permission_23.Permission.PHONE);

    }



    /**
     * Override of OnResume Method of Activity
     */
    @Override
    public void onResume()
    {
        super.onResume();

        splashScreen_controlar=new SplashScreen_controlar(this,this);

        if (Build.VERSION.SDK_INT >= 23)
        {
            if(app_permission_23.getPermission(permissions,SplashScreen.this,true))
            {
                holdScreen();
            }

        }else
        {
            /**
             * Check Settings....*/
            holdScreen();
        }
    }
    @Override
    public void onNewIntent(Intent intent)
    {
        this.setIntent(intent);
    }

    /**
     * <h2>holdScreen</h2>
     * <P>
     *   As name says this method hold the screen until get all the required things.
     * </P>*/
    private void holdScreen()
    {
        splashScreen_controlar.isPlayServe_Available();
    }

    @Override
    public void isPlayService_available(boolean isAvailable)
    {
        if(isAvailable)
        {
            Log.d("skLocation1","bgjhgjhsd");
            splashScreen_controlar.check_Internet_Enable();

        }
    }
    @Override
    public void internet_check(boolean isConnected)
    {
        if(isConnected)
        {
            Log.d("skLocation2", "bgjhgjhsd");
            splashScreen_controlar.getRegister_id();

        }

    }

    @Override
    public void generateRegister_id(String register_id)
    {
        Log.d("skLocation3", register_id);

        appSession.setGcmRegId(register_id);
        /**
         * Waiting for counter.*/
        startCounter();

    }

    @Override
    public void Error(String error)
    {
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
/**
 * Unregister the service (Token generation)*/
        splashScreen_controlar.unRegister_Regd_Broadcast();
    }

    /**
     * <h2>onRequestPermissionsResult</h2>
     * <P>
     *     <h3>
     *        Method called when user is Done with the Runtime permission .Then it check weather all the required permission
     *        is given or not .
     *     </h3>
     * </P>*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        boolean isDenine=false;
        switch (requestCode)
        {
            case 786:
            {
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }

            }
            if(isDenine)
            {
                app_permission_23.getPermission(this.permissions,SplashScreen.this,true);
            }else
            {
                holdScreen();
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * <h2>OpenIntent</h2>
     * <P>
     *
     * </P>*/
    private void startCounter()
    {
        countDownTimer=new CountDownTimer(1000*4,1000)
        {
            @Override
            public void onTick(long millisUntilFinished)
            {

            }
            @Override
            public void onFinish()
            {
                OpenIntent();
            }
        };

        countDownTimer.start();
    }

    /**
     * <h2>OpenIntent</h2>
     * <P>
     *
     * </P>*/
    private void OpenIntent()
    {
        Bundle bundle=getIntent().getExtras();

        if(sessionManager.getUserLogin_status())
        {

            if(bundle!=null)
            {
                String type=bundle.getString("type");
                if(type!=null&&type.equals("1"))
                {
                    /**
                     * Initalizing the chat doc for comming from notification as chat.*/
                    initialize_Chat_Doc();

                    String sender_name,email_id,user_fb_id;
                    user_fb_id=bundle.getString("USER_FB_ID");
                    email_id=bundle.getString("USER_EMAIL");
                    sender_name=bundle.getString("USER_FIRST_NAME");
                    Intent intent=new Intent(SplashScreen.this,Chat.class);
                    intent.putExtra("USER_FB_ID",user_fb_id);
                    intent.putExtra("USER_EMAIL",email_id);
                    intent.putExtra("USER_FIRST_NAME",sender_name);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    this.finish();

                }else
                {
                    Intent intent=new Intent(SplashScreen.this,HomePageActivity.class);
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    this.finish();
                }

            }else
            {
                Intent intent=new Intent(SplashScreen.this,HomePageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                this.finish();
            }

        }else
        {
            Intent intent=new Intent(SplashScreen.this, Landing_page.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }
    }
    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed()
    {
        if(countDownTimer!=null)
        {
            countDownTimer.cancel();
        }
        super.onBackPressed();
    }

    /**
     *<h2>initialize_Chat_Doc</h2>
     * <P>
     *   When ever i am Opening this App in that time in that time i am checking the dock id for then user is there or not.
     *   if there then just holding that doc. reference if not then creating that and assigning that to the user.
     * </P>
     */
    private void initialize_Chat_Doc()
    {
        AppController.getInstance().setUserId(sessionManager.get_User_email());
        AppController.getInstance().setPushToken(appSession.getGcmRegId());

        /**
         * Checking this user doc. id exist or not.*/
        if(!AppController.getInstance().getDbController().checkUserDocExists(AppController.getInstance().getIndexDocId(),sessionManager.getFbId()))
        {
            Map<String,Object> map=new HashMap<>();
            map.put("userName",sessionManager.getFirstName());
            map.put("userId",sessionManager.get_User_email());
            String userDocId= AppController.getInstance().getDbController().createUserInformationDocument(map);
            AppController.getInstance().getDbController().addToIndexDocument(AppController.getInstance().getIndexDocId(),sessionManager.getFbId(),userDocId);
        }
        /**
         * Assigning to the user.*/
        AppController.getInstance().getUserDocIdsFromDb(sessionManager.getFbId());
        AppController.getInstance().emitHeartbeat("1");
    }

}
