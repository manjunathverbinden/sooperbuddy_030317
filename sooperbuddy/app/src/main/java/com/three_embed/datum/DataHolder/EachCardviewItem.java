package com.three_embed.datum.DataHolder;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by shobhit on 8/1/16.
 */
public class EachCardviewItem
{
    public ProgressBar progressBar;
    public TextView DataText;
    public FrameLayout background;
    public ImageView cardImage,video_play_button;
    public TextView userInfo_tv,Image_count;
    static EachCardviewItem eachCardviewItem=new EachCardviewItem();
    private EachCardviewItem(){};

    public static EachCardviewItem getInstance()
    {
        if(eachCardviewItem!=null)
        {
            return  eachCardviewItem;

        }else
        {
            eachCardviewItem=new EachCardviewItem();
            return eachCardviewItem;
        }
    }
}
