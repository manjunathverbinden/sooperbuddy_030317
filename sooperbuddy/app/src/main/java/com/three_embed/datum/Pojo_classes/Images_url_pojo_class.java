package com.three_embed.datum.Pojo_classes;

import java.util.ArrayList;

/**
 * Created by shobhit on 15/2/16.
 */
public class Images_url_pojo_class
{

    private ArrayList<String> imageListUrl=new ArrayList<>();

    public ArrayList<String> getImageListUrl()
    {
        return imageListUrl;
    }

    public void setImageListUrl(ArrayList<String> imageListUrl)
    {
       this.imageListUrl = imageListUrl;
    }

}
