package com.three_embed.datum.AdapterClass;
import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.three_embed.datum.Pojo_classes.user_list_item_details;
import com.three_embed.datum.Aramis_home.R;
import java.util.ArrayList;
/**
 * @since  18/1/16.
 */
public class Profile_pic_adapter extends PagerAdapter
{
    private Activity mcontext;
    private ArrayList<user_list_item_details> al;
    private LayoutInflater inflater;

    /**
     * <h2>Profile_pic_adapter</h2>
     * <P>
     *
     * </P>*/
    public Profile_pic_adapter(Activity context, ArrayList<user_list_item_details> al)
    {
        this.mcontext=context;
        this.al=al;
        this.inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount()
    {
        return al.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container,final int position)
    {
        View view = inflater.inflate(R.layout.profilepicadapterview,container,false);
        ImageView imageView=(ImageView)view.findViewById(R.id.layout_header_image);


            imageView.setVisibility(View.VISIBLE);
            String url=al.get(position).getImage_url();
            if(url!=null&& url.contains("http://"))
            {
                url=url.replace("http://","https://");
            }

            url=url.trim();
            if (!url.equals(""))
            {
                Picasso.with(mcontext).
                        load(url)
                        .error(R.drawable.default_user_rectangle)
                        .placeholder(R.drawable.default_user_rectangle)
                        .into(imageView);
            }
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view==object;
    }

}
