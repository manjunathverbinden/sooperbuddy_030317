package com.three_embed.datum.Aramis_home;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.three_embed.datum.Gcm_Services.RegistrationIntentService;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Aleret_dialogs.Net_work_failed_aleret;
/**
 * <h2>SplashScreen_controlar</h2>
 * <P>
 *  As the name suggested this class Handel all the required setting and
 *  Data required primarily .
 * </P>
 * @author 3Embed
 * @since  28/3/16.
 */

public class SplashScreen_controlar
{
    private static Notifier_data notifier_data = null;
    private Activity mactivity = null;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private TokenGeneratorReceiver receiver;
    private Net_work_failed_aleret net_work_failed_aleret;


    /**
     * <h1>SplashScreen_controlar</h1>
     * <p/>
     * <h3>
     * Constructure of the class.
     * <p/>
     * </h3>
     * </P>
     */
    public SplashScreen_controlar(Notifier_data notifier_data, Activity activity)
    {
        this.notifier_data = notifier_data;
        this.mactivity = activity;
        IntentFilter filter = new IntentFilter(TokenGeneratorReceiver.PROCESS_RESPONSE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new TokenGeneratorReceiver();
        this.mactivity.registerReceiver(receiver,filter);
        net_work_failed_aleret=Net_work_failed_aleret.getInstance();
    }

    /**
     * Check play service is available or not.
     */

    public void isPlayServe_Available()
    {
        notifier_data.isPlayService_available(checkPlayServices());
    }

    /**
     * Checking the Internet connection is available or not.
     */
    public void check_Internet_Enable()
    {
        if (Utility.isNetworkAvailable(mactivity))
        {
            notifier_data.internet_check(true);
        } else
        {
            /**
             * Opening the alert for netWork Connection*/
            doChecking_Internet();
        }
    }

    /**
     * <p/>
     * Creating the gcm register id.
     * </P>
     */
    public void getRegister_id()
    {
        Intent  token_service = new Intent(mactivity, RegistrationIntentService.class);
        mactivity.startService(token_service);
    }

    /**
     * Check Google play Service is available or not
     * <p/>
     * This method check Google play service is available or not.
     * </P>
     *
     * @author 3Embed
     */
    private boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mactivity);

        if (resultCode != ConnectionResult.SUCCESS)
        {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {
				/*
				* if Some Errors occurred Then
				* */
                GooglePlayServicesUtil.getErrorDialog(resultCode, mactivity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                play_service_not_available();
            }
            return false;
        }
        return true;
    }

    /**
     * <h2>gps_Setting_aleret</h2>
     * <p/>
     * <h3>
     * This method Open a alert box to tell the user that Device don,t have the play service.
     * </h3>
     * </P>
     */
    private void play_service_not_available()
    {
        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(mactivity);
        alertDialog.setTitle("Note :");
        alertDialog.setMessage("Your device do not have the Play service !");
        alertDialog.setIcon(R.drawable.launch_con);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                System.exit(0);
            }
        });
        android.support.v7.app.AlertDialog dialog_parent = alertDialog.show();
        dialog_parent.show();
    }

    /**
     * <h2>doChecking_Internet</h2>
     * <P>
     *     Aleret of the internet checking of the internet connection.
     * </P>*/
    private void doChecking_Internet()
    {
        net_work_failed_aleret.net_work_fail(mactivity, new Net_work_failed_aleret.Internet_connection_Callback() {
            @Override
            public void onSucessConnection(String connection_Type) {
                notifier_data.internet_check(true);
            }

            @Override
            public void onErrorConnection(String error) {
                notifier_data.internet_check(false);
            }
        });
    }


    public interface Notifier_data
    {
        void isPlayService_available(boolean isAvailable);

        void internet_check(boolean isConnected);

        void generateRegister_id(String register_id);

        void Error(String error);
    }

    /**
     * This class receive the Data from the Service that provide the Data with */
    public class TokenGeneratorReceiver extends BroadcastReceiver
    {
        public static final String PROCESS_RESPONSE = "com.three_embed.datum.home.intent.action.PROCESS_RESPONSE";

        @Override
        public void onReceive(Context context, Intent intent)
        {
            String reponseMessage = intent.getStringExtra(RegistrationIntentService.RESPONSE_MESSAGE);

            if(reponseMessage!=null&&!reponseMessage.isEmpty())
            {
                notifier_data.generateRegister_id(reponseMessage);
            }else
            {
                notifier_data.Error("Check your internet connection !");
            }
            unRegister_Regd_Broadcast();

        }
    }

    public void unRegister_Regd_Broadcast()
    {
        if(receiver!=null&&mactivity!=null)
        {
            mactivity.unregisterReceiver(receiver);
            receiver=null;
        }

    }
}
