package com.three_embed.datum.Pojo_classes;

/**
 * @since  19/2/16.
 */
public class VisitorsDatas
{
    private String uName;
    private String pPic;
    private String uFbId;
    private String Lastseen;
    private String ViewedCount;
    private String isMatched;

    public String getIsMatched() {
        return isMatched;
    }

    public void setIsMatched(String isMatched) {
        this.isMatched = isMatched;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getpPic() {
        return pPic;
    }

    public void setpPic(String pPic) {
        this.pPic = pPic;
    }

    public String getuFbId() {
        return uFbId;
    }

    public void setuFbId(String uFbId) {
        this.uFbId = uFbId;
    }

    public String getLastseen() {
        return Lastseen;
    }

    public void setLastseen(String lastseen) {
        Lastseen = lastseen;
    }

    public String getViewedCount() {
        return ViewedCount;
    }

    public void setViewedCount(String viewedCount) {
        ViewedCount = viewedCount;
    }
}
