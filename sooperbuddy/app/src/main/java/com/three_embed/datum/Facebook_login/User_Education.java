package com.three_embed.datum.Facebook_login;

/**
 * Created by shobhit on 6/5/16.
 */
public class User_Education
{
    private String id;

    private School school;

    private Education_year year;

    private String type;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public School getSchool ()
    {
        return school;
    }

    public void setSchool (School school)
    {
        this.school = school;
    }

    public Education_year getYear ()
    {
        return year;
    }

    public void setYear (Education_year year)
    {
        this.year = year;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

}
