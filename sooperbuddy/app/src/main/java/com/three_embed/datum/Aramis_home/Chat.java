package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import com.three_embed.datum.AdapterClass.Chat_page_sweep_adapter;
import com.three_embed.datum.Aleret_dialogs.Attachment_popup;
import com.three_embed.datum.Aleret_dialogs.Circule_progress_bar_dialog;
import com.three_embed.datum.Aleret_dialogs.Report_dialog;
import com.three_embed.datum.Permission_Ap23.App_permission_23;
import com.three_embed.datum.Utility.CircleTransform;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import com.three_embed.datum.chat_lib.AppController;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Map;
/**
 * <h1>Chat</h1>
 * <P>
 *
 * </P>*/
public class Chat extends AppCompatActivity implements View.OnClickListener
{
    public int Screenwidth=0;
    private TextView action_bar_title,textView,textView2;
    public String user_first_name;
    public String user_last_name;
    public String user_profile_pic_url;
    public String user_other_images;
    public String user_gender;
    public String user_profile_video_url;
    public String user_fb_id;
    public String user_email;
    public String user_about;
    public String user_tag_line;
    public String user_age;
    SessionManager sessionManager;
    Attachment_popup attachment_popup;
    public String chat_user_doc_id="";
    public String sender_user_id,sender_name,sender_fb_id;
    private Dialog progress_bar_dialog;
    private App_permission_23 app_permission_23;
    private ArrayList<App_permission_23.Permission> permissions;
    private Chat_page_sweep_adapter adapter;
    private Chat_message_fragment chat_message_fragment=null;
    private ArrayList<String> delete_message_item=null;
    public Typeface GothamRoundedMedium,GothamRoundedBold,GothamRoundedLight;
    private boolean isForRequired_Permission=false;
    RelativeLayout attach_file;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_chat);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null)
        {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayUseLogoEnabled(false);
            LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") View customView = inflator.inflate(R.layout.chat_activity_action_bar, null);
            actionBar.setCustomView(customView);
            /**
             * setting the coustom action bar left and right margin as Zero by passing the 0,0 absloute value.
             * */
            Toolbar parent = (Toolbar) customView.getParent();
            parent.setContentInsetsAbsolute(0, 0);
        }
        sessionManager=new SessionManager(this);

        /**
         * Fonts for hum tum*/
        GothamRoundedMedium= Typeface.createFromAsset(getAssets(), "fonts/MyriadPro-Regular.otf");
        GothamRoundedBold=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Bold.otf");
        GothamRoundedLight=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Regular.otf");

        /**
         * Permission_checker instance.*/
        app_permission_23=App_permission_23.getInstance();
        permissions=new ArrayList<>();

        /**
         * Extracting the sender name and the email id .*/
        sender_user_id=sessionManager.get_User_email();
        sender_name=sessionManager.getFirstName();
        sender_fb_id=sessionManager.getFbId();

        Intent intent=getIntent();
        user_first_name=intent.getStringExtra("USER_FIRST_NAME");
        if(user_first_name==null)
        {
            user_first_name="";
        }
        user_last_name=intent.getStringExtra("USER_LAST_NAME");
        user_profile_pic_url=intent.getStringExtra("USER_PROFILE_IMAGE_URL");
        user_profile_video_url=intent.getStringExtra("USER_PROFILE_VIDEO_RUL");
        user_other_images=intent.getStringExtra("USER_OTHER_IMAGES");
        user_gender=intent.getStringExtra("USER_GENDER");
        user_fb_id=intent.getStringExtra("USER_FB_ID");
        user_email=intent.getStringExtra("USER_EMAIL");
        user_about=intent.getStringExtra("USER_ABOUT");
        user_tag_line=intent.getStringExtra("USER_TAG_LINE");
        user_age=intent.getStringExtra("USER_AGE");


        /**
         * This chat id will create when you are comming from the message Notification from the new match user.
         * Setting user doc id to say user id Match status*/
        chat_user_doc_id= AppController.getInstance().findDocumentIdOfReceiver(user_email.trim());

        if(chat_user_doc_id==null || chat_user_doc_id.equals(""))
        {
            chat_user_doc_id=AppController.getInstance().create_user_new_doc_id(user_email,user_first_name,user_profile_pic_url,user_fb_id,"1");
        }

        /**
         * Checking required details is present or not.*/
        if(user_first_name==null||user_first_name.isEmpty())
        {
            Map<String,Object> user_data=AppController.getInstance().getUser_details(chat_user_doc_id);
            user_first_name=  user_data.get("receiverName").toString();
            user_profile_pic_url=user_data.get("receiverImageUrl").toString();
            user_age="22";
        }

        /**
         * Reading the screen height to set the max Zoom size of the Coustom Listvew Header..*/
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        Screenwidth= displaymetrics.heightPixels;
        attachment_popup=Attachment_popup.getInstance();

        /**
         * Clearing the notification if any exist.*/
        NotificationManager nMgr = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancel(0);
/**
 * Initialize Data.to the xml.*/
        initialize();

        checkPermission();
    }

    @Override
    protected void onDestroy()
    {
        VariableConstant.isInChatScreen=false;
        super.onDestroy();
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume()
    {
        VariableConstant.isInChatScreen=true;
        super.onResume();
    }


    /**
     * <h2>checkPermission</h2>
     * <P>
     *
     * </P>*/
    private void checkPermission()
    {
        isForRequired_Permission=true;
        permissions.clear();
        permissions.add(App_permission_23.Permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(App_permission_23.Permission.READ_EXTERNAL_STORAGE);
        if (Build.VERSION.SDK_INT >= 23)
        {
            if(app_permission_23.getPermission(permissions,Chat.this,false))
            {
//handel it
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * On selecting action bar option menu selection.
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.un_friend:
                do_UnMatch_User(user_fb_id);
                return true;
            case R.id.report:
                report_user(user_fb_id);
                return true;
            case R.id.block:
                do_block_user(user_fb_id);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * <h2>initialize</h2>
     * <P>
     *
     * </P>*/
    @SuppressLint("NewApi")
    private void initialize()
    {
        /**
         * Progress bar.*/
        progress_bar_dialog= Circule_progress_bar_dialog.getInstance().get_Circle_Progress_bar(Chat.this);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout_chat);
        /**
         * Tab1 view of the TabLayout...*/
        assert tabLayout != null;
        TabLayout.Tab tab1 = tabLayout.newTab();
        textView=new TextView(this);
        textView.setText(getString( R.string.messageText));
        textView.setTypeface(GothamRoundedBold);
        tab1.setCustomView(textView);
        tabLayout.addTab(tab1);
        /**
         * Tab2 view of the TabLayout..*/
        TabLayout.Tab tab2 = tabLayout.newTab();
        textView2=new TextView(this);
        textView2.setText(R.string.aboutsText);
        textView2.setTypeface(GothamRoundedBold);
        tab2.setCustomView(textView2);
        tabLayout.addTab(tab2);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        textView.setTextColor(ContextCompat.getColor(Chat.this,R.color.SetPreferenceActionBar));
        /**
         *Defining view Pager object ...
         * and assigning to the view Pager adapter */
        final ViewPager viewPager = (ViewPager)findViewById(R.id.chat_pager);
        adapter= new Chat_page_sweep_adapter(getSupportFragmentManager(),tabLayout.getTabCount());
        assert viewPager != null;
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        /**
         * Listing the event of View Pager in android... */
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                /*Handel selected*/
            }

            @Override
            public void onPageSelected(int position)
            {
                if (position == 1) {
                    textView.setTextColor(ContextCompat.getColor(Chat.this, R.color.grayfinedeep));
                    textView2.setTextColor(ContextCompat.getColor(Chat.this, R.color.SetPreferenceActionBar));
                    hide_Soft_Input_key(action_bar_title);
                    attach_file.setVisibility(View.GONE);
                } else {
                    textView.setTextColor(ContextCompat.getColor(Chat.this, R.color.SetPreferenceActionBar));
                    textView2.setTextColor(ContextCompat.getColor(Chat.this, R.color.grayfinedeep));
                    attach_file.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /**
         * Listening the Tab changed listener event  .*/
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        /**
         *
         */
        action_bar_title=(TextView)findViewById(R.id.user_name);
        assert action_bar_title != null;
        action_bar_title.setTypeface(GothamRoundedMedium);
        /**
         * Initializing the title of ation bar as user name.*/
        action_bar_title.setText(user_first_name);

        RelativeLayout backbutton_rl = (RelativeLayout) findViewById(R.id.back_button);
        assert backbutton_rl != null;
        backbutton_rl.setOnClickListener(this);
        ImageView user_image_iv = (ImageView) findViewById(R.id.user_image);

/**
 * Initializing the user Icon image.
 * */

        String url=user_profile_pic_url;
        if(url!=null&&url.contains("http://"))
        {
            url=url.replace("http://","https://");
        }

        Picasso.with(this)
                .load(Uri.parse(url))
                .fit()
                .transform(new CircleTransform())
                .placeholder(R.drawable.chat_profile_default_image_frame)
                .into(user_image_iv);


        attach_file=(RelativeLayout)findViewById(R.id.attach_file);
        assert attach_file != null;
        attach_file.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.back_button:
            {
                /**
                 * Calling the back pressed method of activity.*/
                onBackPressed();
                break;
            }

            case R.id.attach_file:
            {
                attachment_popup.show_popup(attach_file, this);

                break;
            }
        }
    }

    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode contains the request code
     * @param resultCode contains the result code
     * @param data contains the data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Called when the activity has detected the user's press of the back
     * key.  The default implementation simply finishes the current activity,
     * but you can override this to do whatever you want.
     */
    @Override
    public void onBackPressed()
    {
        /**
         * Sending acknowledgement ot the homepage activity.*/
        Bundle bundle=new Bundle();
        bundle.putString("mainTab","1");
        bundle.putString("subTab", "0");
        bundle.putString("user_doc_id",""+chat_user_doc_id);

        Intent intent=new Intent(this,HomePageActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        Chat.this.finish();
    }


    private void do_UnMatch_User(final String user_id)
    {
        if(progress_bar_dialog!=null)
        {
            progress_bar_dialog.show();
        }
        if(Utility.isNetworkAvailable(getApplicationContext()))
        {


            JSONObject jsonObject=new JSONObject();

            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(getApplicationContext()));
                jsonObject.put("ent_sess_token", sessionManager.getToken());
                jsonObject.put("ent_user_fbid",user_id);
                jsonObject.put("ent_data_time", Utility.getCurrentGmtTime_for_block_service());

            } catch (JSONException e)
            {
                e.printStackTrace();
            }

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.UNMATCH, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result) {

                    if (progress_bar_dialog != null) {
                        progress_bar_dialog.dismiss();
                    }

                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("errorFlag").equals("0")) {
                            /**
                             * Deleting the chat history.*/
                            delete_Chat();
                            onBackPressed();
                        }
                        Toast.makeText(getApplicationContext(), jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(String error) {
                    if (progress_bar_dialog != null) {
                        progress_bar_dialog.dismiss();
                    }
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
        }else
        {
            if(progress_bar_dialog!=null)
            {
                progress_bar_dialog.dismiss();
            }
            Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.networknotavailable), Toast.LENGTH_SHORT).show();
        }
    }


    private void do_block_user(final String user_id)
    {
        if(progress_bar_dialog!=null)
        {
            progress_bar_dialog.show();
        }
        if(Utility.isNetworkAvailable(getApplicationContext()))
        {
            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(getApplicationContext()));
                jsonObject.put("ent_sess_token", sessionManager.getToken());
                jsonObject.put("ent_user_fbid",user_id);
                jsonObject.put("ent_flag","4");
                jsonObject.put("ent_data_time",Utility.getCurrentGmtTime_for_block_service());

            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.BLOCKUSER,OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(progress_bar_dialog!=null)
                    {
                        progress_bar_dialog.dismiss();
                    }
                    try
                    {
                        JSONObject jsonObject=new JSONObject(result);
                        if(jsonObject.getString("errorFlag").equals("0"))
                        {
                            onBackPressed();
                        }
                        Toast.makeText(getApplicationContext(), jsonObject.getString("errorMessage"),Toast.LENGTH_SHORT).show();
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onError(String error)
                {

                    if(progress_bar_dialog!=null)
                    {
                        progress_bar_dialog.dismiss();
                    }
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
        }else
        {
            if(progress_bar_dialog!=null)
            {
                progress_bar_dialog.dismiss();
            }
            Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.networknotavailable), Toast.LENGTH_SHORT).show();
        }
    }


    private void report_user(String user_facebook_id)
    {
        Report_dialog report_dialog=new Report_dialog(this,user_facebook_id);
        report_dialog.show();
    }

    /**
     * <h2>Method hide the soft input key if opened.</h2>*/
    private void hide_Soft_Input_key(View view)
    {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }



    private int type_of_intent=0;
    public void Open_attachment(int type)
    {
        isForRequired_Permission=false;
        type_of_intent=type;
        permissions.clear();
        switch (type)
        {
            case 0:
            {
                permissions.add(App_permission_23.Permission.CAMERA);
                permissions.add(App_permission_23.Permission.WRITE_EXTERNAL_STORAGE);
                check_Permission(permissions);
                break;
            }
            case 1:
            {
                permissions.add(App_permission_23.Permission.READ_EXTERNAL_STORAGE);
                check_Permission(permissions);
                break;
            }
        }

    }


    private void check_Permission(ArrayList<App_permission_23.Permission> permissions_list)
    {
        /**
         * Calling the onActivtiy result method externally to set the on Activty result method.*/
        chat_message_fragment= (Chat_message_fragment) adapter.getHostFragment();

        if (Build.VERSION.SDK_INT >= 23)
        {
            if(app_permission_23.getPermission(permissions_list,Chat.this,false))
            {
                chat_message_fragment. OpenRequired_Intent(type_of_intent);
            }

        }else
        {
            chat_message_fragment.OpenRequired_Intent(type_of_intent);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        boolean isAllPermissionNotGranted=false;
        switch (requestCode)
        {
            case 786:
                for (int grantResult : grantResults)
                {
                    if (grantResult != PackageManager.PERMISSION_GRANTED)
                    {
                        isAllPermissionNotGranted = true;
                    }
                }


                if(isAllPermissionNotGranted)
                {
                    if(isForRequired_Permission)
                    {
                        isForRequired_Permission=false;
                        onBackPressed();
                        Toast.makeText(Chat.this,getApplicationContext().getString(R.string.for_messaging_required),Toast.LENGTH_LONG).show();

                    }else
                    {
                        isForRequired_Permission=false;
                        Toast.makeText(Chat.this,getApplicationContext().getString(R.string.aleret_message_for_Camera_permission),Toast.LENGTH_LONG).show();

                        switch (type_of_intent)
                        {
                            case 0:
                            {
                                Toast.makeText(Chat.this,getApplicationContext().getString(R.string.aleret_message_for_Camera_permission),Toast.LENGTH_LONG).show();
                                break;
                            }
                            case 1:
                            {
                                Toast.makeText(Chat.this,getApplicationContext().getString(R.string.external_messaging_required),Toast.LENGTH_LONG).show();
                                break;
                            }
                        }
                    }

                }else
                {
                    isForRequired_Permission=false;
                    chat_message_fragment.OpenRequired_Intent(type_of_intent);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }
    }

    /**
     * <h1>add_Item_To_Delete</h1>
     * <P>
     *
     * </P>*/
    public void add_Item_To_Delete(String message_id)
    {
        if(delete_message_item==null)
        {
            delete_message_item=new ArrayList<>();
        }
        delete_message_item.add(message_id);
    }

    /**
     * <h2>delete_messages</h2>
     * <P>
     *
     * </P>*/
    public void delete_messages()
    {
        for(int count=0;count<delete_message_item.size();count++)
        {
            delete_particular_message(delete_message_item.get(count));
        }
    }

    /**
     * <h2>delete_Chat</h2>
     * <P>
     *     Delete particular chat in between user.
     * </P>*/
    public void delete_Chat()
    {
        AppController.getInstance().delete_Chat(chat_user_doc_id);
    }

    /**
     * <h2>delete_particular_message</h2>
     * <P>
     *     Delete the particular message of the user.
     * </P>*/
    private void delete_particular_message(String message_id)
    {
        AppController.getInstance().delete_Particular_Message(chat_user_doc_id, message_id);
    }

}
