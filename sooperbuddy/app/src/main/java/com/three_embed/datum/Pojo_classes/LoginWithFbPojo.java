package com.three_embed.datum.Pojo_classes;

/**
 * Created by embed-pc on 4/1/16.
 */
public class LoginWithFbPojo
{
    private String errorFlag;
    private String errorMessage;
    private String ProfileVideo;
    private String Profilepic;
    private String FbId;
    private String Token;
    private String ExpiryLocal;
    private String ExpiryGMT;
    private String Joined;
    private String isLoginOrSignup;

    public String getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(String errorFlag) {
        this.errorFlag = errorFlag;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getProfileVideo() {
        return ProfileVideo;
    }

    public void setProfileVideo(String profileVideo) {
        ProfileVideo = profileVideo;
    }

    public String getProfilepic() {
        return Profilepic;
    }

    public void setProfilepic(String profilepic) {
        Profilepic = profilepic;
    }

    public String getFbId() {
        return FbId;
    }

    public void setFbId(String fbId) {
        FbId = fbId;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getExpiryGMT() {
        return ExpiryGMT;
    }

    public void setExpiryGMT(String expiryGMT) {
        ExpiryGMT = expiryGMT;
    }

    public String getJoined() {
        return Joined;
    }

    public void setJoined(String joined) {
        Joined = joined;
    }

    public String getExpiryLocal() {
        return ExpiryLocal;
    }

    public void setExpiryLocal(String expiryLocal) {
        ExpiryLocal = expiryLocal;
    }

    public String getIsLoginOrSignup() {
        return isLoginOrSignup;
    }

    public void setIsLoginOrSignup(String isLoginOrSignup) {
        this.isLoginOrSignup = isLoginOrSignup;
    }
}
