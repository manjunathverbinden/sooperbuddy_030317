package com.three_embed.datum.AdapterClass;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.squareup.picasso.Picasso;
import com.three_embed.datum.chat_lib.ChatList_Chat_Item;
import com.three_embed.datum.chat_lib.Image_circule_trasform;
import com.three_embed.datum.chat_lib.Time_12hr_Converter;
import com.three_embed.datum.chat_lib.Utilities;
import com.three_embed.datum.Aramis_home.R;
import java.util.ArrayList;

/**
 *@since  2/5/16.
 */
public class Match_caht_list_adapeter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private ArrayList<ChatList_Chat_Item> mListData = new ArrayList<>();
    private Activity mContext;
    private Time_12hr_Converter time_12hr_converter=null;

    public Match_caht_list_adapeter(Activity mContext, ArrayList<ChatList_Chat_Item> mListData)
    {
        this.mListData = mListData;
        this.mContext = mContext;
        this.time_12hr_converter=Time_12hr_Converter.getInstance();
    }


    @Override
    public int getItemCount()
    {
        return this.mListData.size();
    }


    @Override
    public int getItemViewType(int position)
    {
        return 1;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(com.three_embed.datum.chat_lib.R.layout.socket_lp_f3_chat, viewGroup, false);
        viewHolder = new View_holder_Chat_list(v);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
    {
        View_holder_Chat_list vh2 = (View_holder_Chat_list)viewHolder;
        configureViewHolderChat(vh2, position);
    }


    private void configureViewHolderChat(View_holder_Chat_list vh, int position)
    {
        final   ChatList_Chat_Item chat =  mListData.get(position);
        if (chat != null)
        {
            vh.storeName.setText(chat.getReceiverName());
            vh.newMessage.setText(chat.getNewMessage());

            try {

                String formatedDate = Utilities.formatDate(Utilities.tsFromGmt(chat.getNewMessageTime()));
                if ((chat.getNewMessageTime().substring(0, 8)).equals((Utilities.tsInGmt().substring(0, 8))))
                {
                    vh.newMessageDate.setText(R.string.Today);
                } else if ((Integer.parseInt((Utilities.tsInGmt().substring(0, 8))) - Integer.parseInt((chat.getNewMessageTime().substring(0, 8)))) == 1)
                {
                    vh.newMessageDate.setText(R.string.Yesterday);

                } else
                {
                    vh.newMessageDate.setText(formatedDate.substring(9, 24));
                }

                vh.newMessageTime.setText(time_12hr_converter.get12hr_formate(formatedDate.substring(0,9)));

                if (chat.hasNewMessage())
                {
                    vh.newMessageCount.setText(chat.getNewMessageCount());
                }

            }catch(NullPointerException e)
            {e.printStackTrace();
            }
            if(chat.getReceiverImageUrl()!=null)
            {
                String url=chat.getReceiverImageUrl();
                if(url.contains("http://"))
                {
                    url=url.replace("http://","https://");
                }
                Picasso.with(mContext).load(url).transform(new Image_circule_trasform()).into(vh.storeImage);

            }



            vh.storeImage.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                  /*  Intent i = new Intent(mContext, AllProductsFromStore.class);
                    i.putExtra("store", chat.getReceiverName());
                    mContext.startActivity(i);*/
                }
            });



        }
    }

}
