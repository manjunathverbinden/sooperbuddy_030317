package com.three_embed.datum.AdapterClass;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.three_embed.datum.Pojo_classes.VisitorsDatas;
import com.three_embed.datum.Utility.CircleTransform;
import com.three_embed.datum.Aramis_home.R;
import java.util.ArrayList;
/**
 * <h1>RecentVisitorsAdap</h1>
 * <P>
 *     <h3>
 *
 *     </h3>
 * </P>
 * @author 3Embed
 * @since  19/2/16.
 */
public class RecentVisitorsAdap extends ArrayAdapter<VisitorsDatas>
{
    /**
     * Declare Used Variables
     */
    private ArrayList<VisitorsDatas> arrayListVisitorsData;
    private Context context;
    private static LayoutInflater inflater=null;

    /**
     * <p>
     *     RecentVisitorsAdap constructor
     * </p>
     * @param context , context of Activity from where it is being called
     * @param arrayListVisitorsData , this array list contains all used datas
     */
    public RecentVisitorsAdap(Context context,ArrayList<VisitorsDatas> arrayListVisitorsData)
    {
        super(context, R.layout.single_row_recent_visitors,arrayListVisitorsData);
        this.context=context;
        this.arrayListVisitorsData=arrayListVisitorsData;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     *  Create a holder Class to contain inflated xml file elements
     */
    public static class ViewHolder
    {
        public TextView tV_userName,tV_lastViewed,tV_total_views,matchedtag;
        public ImageView user_chat_pic;

    }

    /**
     * <p>
     *     Depends upon data size called for each row , Create each ListView row
     * </p>
     * @param position , it gives the position of the particular view
     * @param convertView , it contains the inflated view of xml
     * @param parent parent view.
     * @return View .
     */
    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        if (convertView==null)
        {
            /**
             * Inflate single_row_recent_visitors.xml file for each row ( Defined below )
             */
            convertView=inflater.inflate(R.layout.single_row_recent_visitors,null);

            /**
             * View Holder Object to contain single_row_recent_visitors.xml file elements
             */
            viewHolder=new ViewHolder();
            viewHolder.user_chat_pic= (ImageView) convertView.findViewById(R.id.user_chat_pic);
            viewHolder.tV_userName= (TextView) convertView.findViewById(R.id.tV_userName);
            viewHolder.tV_lastViewed= (TextView) convertView.findViewById(R.id.tV_lastViewed);
            viewHolder.tV_total_views= (TextView) convertView.findViewById(R.id.tV_total_views);
            viewHolder.matchedtag=(TextView)convertView.findViewById(R.id.matchedtag);

            /**
             * Set holder with LayoutInflater
             */
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder= (ViewHolder) convertView.getTag();
        }

        /**
         * setting values for each rows
         */
        viewHolder.tV_userName.setText(arrayListVisitorsData.get(position).getuName());
        String totalViews=context.getResources().getString(R.string.TotalViewed) +": "+arrayListVisitorsData.get(position).getViewedCount()+" "+context.getResources().getString(R.string.times);
        viewHolder.tV_total_views.setText(totalViews);
        String lastViewed=context.getResources().getString(R.string.lastView)+": "+arrayListVisitorsData.get(position).getLastseen();
        viewHolder.tV_lastViewed.setText(lastViewed);
        if(arrayListVisitorsData.get(position).getIsMatched().equalsIgnoreCase("1"))
        {
            viewHolder.matchedtag.setVisibility(View.VISIBLE);
        }else
        {
            viewHolder.matchedtag.setVisibility(View.GONE);
        }

        String user_image_url=arrayListVisitorsData.get(position).getpPic();

        if (!user_image_url.equals(""))
        {
            if (user_image_url.contains(","))
                user_image_url=user_image_url.replace(",","");
            Picasso.with(context)
                    .load(user_image_url)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.chat_profile_default_image_frame)
                    .into(viewHolder.user_chat_pic);
        }
        return convertView;
    }
}
