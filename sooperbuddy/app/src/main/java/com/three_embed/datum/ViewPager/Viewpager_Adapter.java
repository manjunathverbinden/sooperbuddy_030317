package com.three_embed.datum.ViewPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
/**
 * <h1>Viewpager_Adapter</h1>
 * Viewpager_Adapter is class that extends from the FragmentPagerAdapter of support v4 lib.
 * and return next item when user slide page by {@code getItem()}.
 * Created by embed on 21/11/15.
 * @author 3Embed
 * @since 21/11/15
 * @version 1.0
 */
public class Viewpager_Adapter extends FragmentPagerAdapter
{
    protected static final String[] CONTENT = new String[]{"1", "2", "3"};

    public Viewpager_Adapter(FragmentManager fragmentManager)
    {
     super(fragmentManager);
    }
    @Override
    public Fragment getItem(int position)
    {
        return PageFragment.newInstance(CONTENT[position % CONTENT.length]);
    }

    @Override
    public int getCount()
    {
        return CONTENT.length;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        return Viewpager_Adapter.CONTENT[position % CONTENT.length];
    }
}
