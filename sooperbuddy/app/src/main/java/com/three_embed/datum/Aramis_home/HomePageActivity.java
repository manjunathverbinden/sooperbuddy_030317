package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.three_embed.datum.AdapterClass.NavDrawerListViewAdap;
import com.three_embed.datum.Aleret_dialogs.Contact_Us;
import com.three_embed.datum.Pojo_classes.Images_url_pojo_class;
import com.three_embed.datum.Utility.AppSession;
import com.three_embed.datum.Utility.CircleTransform;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import com.three_embed.datum.chat_lib.AppController;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
/**
 * <h2>HomePageActivity</h2>
 * <P>
 *
 * </P>
 */
public class HomePageActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    public Boolean isMatch_found=false;
    private ImageView  selectedicon2, unselectediacon1, unselectediacon2;
    private ListView lV_navigationDrawer;
    public SessionManager sessionManager;
    private Context context;
    private ImageView ivRearImage,ivUserProfileImage;
    public static boolean isPage1_visible=false;
    public boolean isMatch_data_found=false;
    private TabLayout.Tab tab1;
    private TabLayout tabLayout;
    private AppSession appSession;
    private  String mainTab;
    public String doc_id_to_update_count=null;
    public Typeface GothamRoundedMedium,GothamRoundedBold,GothamRoundedLight;
    public ImageView selectedicon1;
    public Contact_Us contact_us=null;
    public boolean isSuperlikeFragmentActive=false;
    public HomeActivity_call_Back homeActivity_call_back=null;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Bundle extras=getIntent().getExtras();

        if(extras!=null)
        {
            mainTab=extras.getString("mainTab");
            doc_id_to_update_count=extras.getString("user_doc_id");
        }
        /**
         * Fonts for hum tum*/
        GothamRoundedMedium=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Regular.otf");
        GothamRoundedBold=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Bold.otf");
        GothamRoundedLight=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Regular.otf");

        context=HomePageActivity.this;
        sessionManager=new SessionManager(context);
        appSession=new AppSession(context);
        /**
         * list view which will contain my profile items
         * like Discovery Preference, App Settings, Share Datum etc.
         */
        lV_navigationDrawer= (ListView) findViewById(R.id.lV_navigationDrawer);
        addDrawerItems();
        /**
         * Contact us aleret*/
        contact_us=Contact_Us.getInstance();

        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (drawer != null) {
            drawer.setDrawerListener(toggle);
        }
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }

        /**
         * setting My profile user name and image
         */
        View headView = null;
        if (navigationView != null)
        {
            headView = navigationView.getHeaderView(0);
        }
        assert headView != null;
        TextView dr_name_text = (TextView) headView.findViewById(R.id.tvUserName);
        dr_name_text.setTypeface(GothamRoundedBold);

        String fbFirstName = sessionManager.getFirstName();

        if (fbFirstName !=null && !fbFirstName.equals(""))
        {
            dr_name_text.setText(fbFirstName);
        }

        TextView dr_title_name = (TextView) headView.findViewById(R.id.tvViewProfile);
        dr_title_name.setTypeface(GothamRoundedMedium);

        ivUserProfileImage= (ImageView) headView.findViewById(R.id.ivUserProfileImage);
        ivRearImage= (ImageView) headView.findViewById(R.id.ivRearImage);


        RelativeLayout rlUserProfile = (RelativeLayout) headView.findViewById(R.id.rlUserProfile);
        rlUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomePageActivity.this, MyProfileActivity.class));
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer != null) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        /**
         * Tab1 view of the TabLayout...*/
        if (tabLayout != null) {
            tab1 = tabLayout.newTab();
        }
        @SuppressLint("InflateParams") View coustomView1 = layoutInflater.inflate(R.layout.tablayout1, null);
        selectedicon1 = (ImageView) coustomView1.findViewById(R.id.icon);
        selectedicon2 = (ImageView) coustomView1.findViewById(R.id.icon2);
        selectedicon2.setAlpha(0f);
        tab1.setCustomView(coustomView1);
        tabLayout.addTab(tab1);

        /**
         * Tab2 view of the TabLayout..*/
        TabLayout.Tab tab2 = tabLayout.newTab();
        @SuppressLint("InflateParams") View coustomView2 = layoutInflater.inflate(R.layout.tablayout2, null);
        unselectediacon1 = (ImageView) coustomView2.findViewById(R.id.icon);
        unselectediacon2 = (ImageView) coustomView2.findViewById(R.id.icon2);
        unselectediacon2.setAlpha(0f);

        tab2.setCustomView(coustomView2);
        tabLayout.addTab(tab2);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        /**
         *Defining view Pager object ...
         * and assigning to the view Pager adapter */
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final Tab_PagerAdapter adapter = new Tab_PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        if (viewPager != null) {
            viewPager.setAdapter(adapter);
        }
        assert viewPager != null;
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        /**
         * Listing the event of View Pager in android... */
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionoOffsetPixels)
            {
                if (selectedicon1 != null && selectedicon2 != null && unselectediacon1 != null && unselectediacon2 != null)
                {
                    selectedicon1.setAlpha(1 - positionOffset);
                    selectedicon2.setAlpha(positionOffset);
                    unselectediacon1.setAlpha(1 - positionOffset);
                    unselectediacon2.setAlpha(positionOffset);
                }

                if (position == 0 && positionOffset == 0.0)
                {
                    assert selectedicon1 != null;
                    selectedicon1.setAlpha(1f);
                    selectedicon2.setAlpha(0f);
                    unselectediacon1.setAlpha(1f);
                    unselectediacon2.setAlpha(0f);
                } else if (positionOffset == 0.0) {
                    assert selectedicon1 != null;
                    selectedicon1.setAlpha(0f);
                    selectedicon2.setAlpha(1f);
                    unselectediacon1.setAlpha(0f);
                    unselectediacon2.setAlpha(1f);
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });

        /**
         * Listening the Tab changed listener event  .*/
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                if (tab.getPosition() == 0&&isMatch_found)
                {
                    isPage1_visible = true;
                    /**
                     * Setting false if first fragment is opened*/
                    isSuperlikeFragmentActive=false;

                } else
                {
                    Log.d("isMatchfound2",""+isMatch_found);
                    isPage1_visible = false;
                }
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        /**
         * Initializing the chat doc.*/
        initialize_Chat_Doc();
    }


    /**
     *<h2>initialize_Chat_Doc</h2>
     * <P>
     *   When ever i am Opening this App in that time in that time i am checking the dock id for then user is there or not.
     *   if there then just holding that doc. reference if not then creating that and assigning that to the user.
     * </P>
     */
    private void initialize_Chat_Doc()
    {
        AppController.getInstance().setUserId(sessionManager.get_User_email());
        AppController.getInstance().setPushToken(appSession.getGcmRegId());

        /**
         * Checking this user doc. id exist or not.*/
        if(!AppController.getInstance().getDbController().checkUserDocExists(AppController.getInstance().getIndexDocId(),sessionManager.getFbId()))
        {
            Map<String,Object> map=new HashMap<>();
            map.put("userName",sessionManager.getFirstName());
            map.put("userId",sessionManager.get_User_email());
            String userDocId= AppController.getInstance().getDbController().createUserInformationDocument(map);
            AppController.getInstance().getDbController().addToIndexDocument(AppController.getInstance().getIndexDocId(),sessionManager.getFbId(),userDocId);
        }
        /**
         * Assigning to the user.*/
        AppController.getInstance().getUserDocIdsFromDb(sessionManager.getFbId());
        AppController.getInstance().emitHeartbeat("1");
    }

    /**
     * Setting the current Tab.
     * */

    public void setCurrent_Tab()
    {
        if(!tab1.isSelected())
        {
            tab1.select();
        }

    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Images_url_pojo_class images_url_pojo_class=new Gson().fromJson(sessionManager.getIMages_url(), Images_url_pojo_class.class);
        String imageUrl="";
        if(images_url_pojo_class!=null&&images_url_pojo_class.getImageListUrl().size()>0)
        {
            imageUrl=images_url_pojo_class.getImageListUrl().get(0);
        }
        if (imageUrl!=null && !imageUrl.equals(""))
        {
            Picasso.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.default_user_rectangle)
                    .error(R.drawable.default_user_rectangle)
                    .resize(225,125)
                    .into(ivRearImage);

            /**
             * profile image
             */
            Picasso.with(context)
                    .load(imageUrl).transform(new CircleTransform())
                    .placeholder(R.drawable.default_user_circle)
                    .error(R.drawable.default_user_circle)
                    .resize(130, 130)
                    .into(ivUserProfileImage);
        }


        if(mainTab!=null)
        {
            int tab_no=Integer.parseInt(mainTab);
            tabLayout.getTabAt(tab_no).select();
            /**
             * Setting null for the once opened.*/
            mainTab=null;
        }
    }
    /**
     * Dispatch incoming result to the correct fragment.
     * @param requestCode request code
     * @param resultCode result code
     * @param data contains the call back data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.d("Listen", "Activtity Result");

            if(isSuperlikeFragmentActive)
            {
                Log.d("Listenin", "Activtity Result");

                if (requestCode == 12345)
                {
                    if (resultCode == RESULT_OK)
                    {
                        String reponse_Data = data.getStringExtra("MESSAGE");
                        String userFBID=data.getStringExtra("FBID");
                        /**
                         * Result code 2 for Like and
                         * 3 for dislike.
                         * */
                        switch (reponse_Data) {
                            case "2":
                                likesOrDislikeService(VariableConstant.SET_LIKES, userFBID, homeActivity_call_back);

                                break;
                            case "3":
                                likesOrDislikeService(VariableConstant.SET_DIS_LIKES, userFBID, homeActivity_call_back);
                                break;
                        }
                    }
                }
            }else
            {
                super.onActivityResult(requestCode,resultCode,data);
            }

    }

    @Override
    public void onBackPressed()
    {
        /**
         * Checking the current selected Tab position is first one or not.if not then setting first one.
         * */
        if(tabLayout.getSelectedTabPosition()!=0)
        {
            tabLayout.getTabAt(0).select();
        }else
        {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer != null && drawer.isDrawerOpen(GravityCompat.START))
            {
                drawer.closeDrawer(GravityCompat.START);
            } else
            {
                exist_on_doble_press();
            }

        }

    }
    private boolean backPressedToExitOnce=false;

    /**
     * <h2>exist_on_doble_press</h2>
     * <P>
     *
     * </P>*/
    private void exist_on_doble_press()
    {
        if(backPressedToExitOnce)
        {
            super.onBackPressed();
        }
        else
        {
            this.backPressedToExitOnce = true;
            Toast.makeText(HomePageActivity.this,"press again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backPressedToExitOnce = false;
                }
            }, 2000);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null)
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    private void addDrawerItems()
    {
        int[] drawerIcon={R.drawable.discovery_setting_icon,R.drawable.app_setting_icon_off,R.drawable.support_icon,R.drawable.datum_profile_share_icon_off};
        String[] drawerHeadingTitle=getResources().getStringArray(R.array.myProfileItemTitle);
        String[] drawerHeadingName=getResources().getStringArray(R.array.myProfileItemName);
        NavDrawerListViewAdap drawerListViewAdap=new NavDrawerListViewAdap(HomePageActivity.this,drawerHeadingTitle,drawerHeadingName,drawerIcon);
        lV_navigationDrawer.setAdapter(drawerListViewAdap);
        lV_navigationDrawer.setOnItemClickListener(drawerListViewAdap);

    }

    /**
     * <h>LikesOrDislikeService</h>
     * <p>
     *     This is likesOrDislikeService used to like or disLike the image.
     *     when user click on like or dislike button. that will be performed
     *     depending upon the likesOrDislikesVar values. if it is 1 then
     *     setLikes service will be called and if value is 2 then dislike
     *     service will be called.
     * </p>
     * @see OkHttp3Connection .
     * @param likesOrDislikesVar .This value may be 1(like) or 2(disLike)
     */
    public void likesOrDislikeService(String likesOrDislikesVar,final String fb_Id,final HomeActivity_call_Back homeActivity_call_back)
    {
        if(inter_Check())
        {
            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(context));
                jsonObject.put("ent_sess_token", sessionManager.getToken());
                jsonObject.put("fbid",fb_Id);
                jsonObject.put("ent_data_time",Utility.getCurrentGmtTime());
                jsonObject.put("like",likesOrDislikesVar);
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.SET_LIKES, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    Log.d("Likedislike",result);

                    homeActivity_call_back.onSuperlikeSucess(result);
                }

                @Override
                public void onError(String error)
                {
                    homeActivity_call_back.onSuperlikeError(error);
                }
            });
        }else
        {
            homeActivity_call_back.onSuperlikeError(context.getString(R.string.networknotavailable));
        }
    }

    /**
     * <h2>inter_Check</h2>
     * <P>
     * Checking the internet connection is available or not.
     * </P>*/
    private boolean inter_Check()
    {

        return Utility.isNetworkAvailable(this);
    }

    /**
     * <h2>inValideToken</h2>
     * <P>
     * method kill the activity and start the splash screen to set session expired.
     * </P>
     * @param context contain the activtiy reference to kill.
     * */
    public void inValideToken(Activity context)
    {
        Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
        /**
         * if session expired then opening the landing page.*/
        Intent  starting_intent = new Intent(this,SplashScreen.class);
        starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        /**
         * Clearing the session manager for fresh user.*/
        sessionManager.clear_session_manager();
        appSession.clear_Appsession();
        context.finish();
        startActivity(starting_intent);
    }

}
