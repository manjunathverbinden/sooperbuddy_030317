package com.three_embed.datum.Pojo_classes;

import android.util.Log;
import java.util.ArrayList;
/**
 *@since  11/12/15.
 */
public class SetPreferenceAlData implements Comparable
{

    private String id;
    private String selected_max;
    private String End;
    private String ActiveStatus;
    private String TypeOfPreference;
    private String Start;
    private String PreferenceTitle;
    private ArrayList<String> OptionsValue=new ArrayList<>();
    private ArrayList<String> Selected=new ArrayList<>();
    private String Unit;
    private String selected_min;
    private String Priority;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSelected_max ()
    {
        return selected_max;
    }

    public void setSelected_max (String selected_max)
    {
        this.selected_max = selected_max;
    }

    public String getEnd ()
    {
        return End;
    }

    public void setEnd (String End)
    {
        this.End = End;
    }

    public String getActiveStatus ()
    {
        return ActiveStatus;
    }

    public void setActiveStatus (String ActiveStatus)
    {
        this.ActiveStatus = ActiveStatus;
    }

    public String getTypeOfPreference ()
    {
        return TypeOfPreference;
    }

    public void setTypeOfPreference (String TypeOfPreference)
    {
        this.TypeOfPreference = TypeOfPreference;
    }

    public String getStart ()
    {
        return Start;
    }

    public void setStart (String Start)
    {
        this.Start = Start;
    }

    public String getPreferenceTitle ()
    {
        return PreferenceTitle;
    }

    public void setPreferenceTitle (String PreferenceTitle)
    {
        this.PreferenceTitle = PreferenceTitle;
    }

    public ArrayList<String> getOptionsValue ()
    {
        return OptionsValue;
    }

    public void setOptionsValue (ArrayList<String> OptionsValue)
    {
        this.OptionsValue = OptionsValue;
    }

    public void setSelected (ArrayList<String> OptionsValue)
    {
        this.Selected = OptionsValue;
    }

    public ArrayList<String> getSelected ()
    {
        return Selected;
    }

    public String getUnit ()
    {
        return Unit;
    }

    public void setUnit (String Unit)
    {
        this.Unit = Unit;
    }

    public String getSelected_min ()
    {
        return selected_min;
    }

    public void setSelected_min (String selected_min)
    {
        this.selected_min = selected_min;
    }

    public String getPriority ()
    {
        return Priority;
    }

    public void setPriority (String Priority)
    {
        this.Priority = Priority;
    }
    /**
     * Compares this object to the specified object to determine their relative
     * order.
     *
     * @param another the object to compare to this instance.
     * @return a negative integer if this instance is less than {@code another};
     * a positive integer if this instance is greater than
     * {@code another}; 0 if this instance has the same order as
     * {@code another}.
     * @throws ClassCastException if {@code another} cannot be converted into something
     *                            comparable to {@code this} instance.
     */
    @Override
    public int compareTo(Object another)
    {
        SetPreferenceAlData newObject=(SetPreferenceAlData)another;
        return Integer.parseInt(this.getPriority()) - Integer.parseInt(newObject.getPriority());
    }

    /**
     * Compares this instance with the specified object and indicates if they
     * are equal. In order to be equal, {@code o} must represent the same object
     * as this instance using a class-specific comparison. The general contract
     * is that this comparison should be reflexive, symmetric, and transitive.
     * Also, no object reference other than null is equal to null.
     * <p/>
     * <p>The default implementation returns {@code true} only if {@code this ==
     * o}. See <a href="{@docRoot}reference/java/lang/Object.html#writing_equals">Writing a correct
     * {@code equals} method</a>
     * if you intend implementing your own {@code equals} method.
     * <p/>
     * <p>The general contract for the {@code equals} and {@link
     * #hashCode()} methods is that if {@code equals} returns {@code true} for
     * any two objects, then {@code hashCode()} must return the same value for
     * these objects. This means that subclasses of {@code Object} usually
     * override either both methods or neither of them.
     *
     * @param o the object to compare this instance with.
     * @return {@code true} if the specified object is equal to this {@code
     * Object}; {@code false} otherwise.
     * @see #hashCode
     */
    public boolean equals(SetPreferenceAlData o)
    {
        if(!this.getSelected().equals(o.getSelected()))
        {
            return false;
        }else if(!this.getSelected_max().equals(o.getSelected_max()))
        {
            return false;
        }else if(!this.getSelected_min().equals(o.getSelected_min()))
        {
           return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "SetPreferenceAlData{" +
                "selected_max='" + selected_max + '\'' +
                ", End='" + End + '\'' +
                ", ActiveStatus='" + ActiveStatus + '\'' +
                ", TypeOfPreference='" + TypeOfPreference + '\'' +
                ", Start='" + Start + '\'' +
                ", PreferenceTitle='" + PreferenceTitle + '\'' +
                ", OptionsValue=" + OptionsValue +
                ", Selected=" + Selected +
                ", Unit='" + Unit + '\'' +
                ", selected_min='" + selected_min + '\'' +
                ", Priority='" + Priority + '\'' +
                '}';
    }
}
