package com.three_embed.datum.Aramis_home.EditProfile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.three_embed.aws_s3_client.Upload_file_AmazonS3;
import com.three_embed.datum.Aleret_dialogs.Circule_progress_bar_dialog;
import com.three_embed.datum.Aleret_dialogs.Net_work_failed_aleret;
import com.three_embed.datum.Aramis_home.SplashScreen;
import com.three_embed.datum.Aramis_home.User_preference_setter.User_preference_Item_Controlar;
import com.three_embed.datum.Pojo_classes.Delete_list_pojoclass;
import com.three_embed.datum.Pojo_classes.Images_url_pojo_class;
import com.three_embed.datum.Pojo_classes.SetPreferenceAlData;
import com.three_embed.datum.Pojo_classes.SetPreferencePojo;
import com.three_embed.datum.Pojo_classes.UserDetails_pojo;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import com.three_embed.datum.Aramis_home.Profile_picture_setting;
import com.three_embed.datum.Aramis_home.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import eu.janmuller.android.simplecropimage.CropImage;
/**
 * <h2>Edit_profile_pic</h2>
 * <P>
 *   Contains the detail of the user to edit.
 * </P>
 * @author 3Embed.
 * @since 12.04.2016.
 * */
public class Edit_profile_pic extends AppCompatActivity implements View.OnDragListener,View.OnLongClickListener,View.OnClickListener
{
    private ImageView profile_opt_1;
    private ImageView profile_opt_2;
    private ImageView profile_opt_3;
    private ImageView profile_opt_4;
    private ImageView profile_opt_5;
    private boolean isgot_intoview = false;
    private View longPress_temp_View = null, temp_click_view = null, temp_view_destination = null;
    private int Screenwidth = 0, Screenheight = 0;
    private EditText user_about_edt;
    private TextView charcter_count;
    private View temp_Imageview_to_set = null;
    private ArrayList<String> imageLink = null;
    private ArrayList<ImageView> ref_proflie_view = null;
    private boolean[] isImage_exist;
    private int maxImage_avaliabe = 0;
    private RelativeLayout actionbar_delete_rl,loading_view,connectionFailed_view;
    private RelativeLayout actionbar_edit_rl;
    private RelativeLayout getActionbar_revert_rl;
    private ArrayList<Delete_list_pojoclass> deleted_item_list = null;
    private boolean isIn_delete_mode = false;
    private ProgressBar imag_progressbar_list[];
    private SessionManager sessionManager;
    private Upload_file_AmazonS3 uploadAmazonS3;
    private Images_url_pojo_class images_url_pojo_class;
    private Dialog progress_bar_dialog;
    private Profile_picture_setting profile_picture_setting;
    private ScrollView parent_scroll_view;
    private Net_work_failed_aleret.Internet_connection_Callback connection_callback;
    private Net_work_failed_aleret net_work_failed_aleret;
    private TextView connection_failed_error;
    Intent starting_intent;
    ArrayList<SetPreferenceAlData> preferenceAlDatas_before;
    User_preference_Item_Controlar user_preference_item_controlar;
    JSONArray serverSeleted;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_pic);


        /**
         * if session expired then opening the landing page.*/
        starting_intent = new Intent(this,SplashScreen.class);
        profile_picture_setting=new Profile_picture_setting(this);

        /**
         * if session expired then opening the landing page.*/
        /**
         * Litening the network failed aleret.*/
        net_work_failed_aleret=Net_work_failed_aleret.getInstance();
        addConnnectionCallback_Listener();
        /**
         * Creating the progress bar.*/
        progress_bar_dialog= Circule_progress_bar_dialog.getInstance().get_Circle_Progress_bar(this);
        sessionManager = new SessionManager(getApplicationContext());
        images_url_pojo_class=new Images_url_pojo_class();

         /* Creating the Response of Data inflater object.*/
        user_preference_item_controlar=User_preference_Item_Controlar.getInstance(Edit_profile_pic.this);
        /**
         * Creating image list array.*/
        imageLink=new ArrayList<>();
        preferenceAlDatas_before=new ArrayList<>();

        uploadAmazonS3=Upload_file_AmazonS3.getInstance(Edit_profile_pic.this,VariableConstant.COGNITO_POOL_ID);
        /**
         * Reading the Screen Height of android device..*/
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        Screenwidth = displaymetrics.widthPixels;
        Screenheight = displaymetrics.heightPixels;

        /**
         * Initializing the required xml content.*/
        initializeXmlContent();
        /**
         * Checking if the user is present.*/
        if(sessionManager.get_user_data_status())
        {
            set_User_data();
        }else
        {
            getProfileDatas();
        }
        /* Getting user setting details.*/
        getUSerSettingDetails();
    }

    /**
     * <h2>addConnnectionCallback_Listener</h2>
     * <P>
     *     It is the connection callback of the internet connection.
     * </P>
     * */
    private void addConnnectionCallback_Listener()
    {
        connection_callback=new Net_work_failed_aleret.Internet_connection_Callback() {
            @Override
            public void onSucessConnection(String connection_Type)
            {

                //Handel it
            }

            @Override
            public void onErrorConnection(String error)
            {
                //Handel it
            }
        };
    }
    /**
     * <h2>getProfileDatas</h2>
     * <P>
     * This method is used to do service call for getting all datas for my profile.
     * </P>
     */
    private void getProfileDatas()
    {
        actionbar_edit_rl.setVisibility(View.GONE);
        parent_scroll_view.setVisibility(View.GONE);
        connectionFailed_view.setVisibility(View.GONE);
        loading_view.setVisibility(View.VISIBLE);
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_dev_id",Utility.getDeviceId(getApplicationContext()));
            jsonObject.put("ent_data_time",Utility.getCurrentGmtTime());
            jsonObject.put("ent_sess_token",sessionManager.getToken());
            jsonObject.put("ent_fbid",sessionManager.getFbId());

        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_PROFILE, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result) {
                Log.d("Result", result);

                if (loading_view != null) {
                    loading_view.setVisibility(View.GONE);
                }
                connectionFailed_view.setVisibility(View.GONE);
                parent_scroll_view.setVisibility(View.VISIBLE);
                actionbar_edit_rl.setVisibility(View.VISIBLE);
                Gson gson = new Gson();
                UserDetails_pojo profilePojo = gson.fromJson(result, UserDetails_pojo.class);

                if ("0".equals(profilePojo.getErrorFlag())) {
                    /**
                     * Telling thet user details found.*/
                    sessionManager.set_user_data_status(true);
                    /**
                     * Saving user data to Session manager.*/
                    save_User_Data(profilePojo);
                    /**
                     * Setting user data to xml.*/
                    set_User_data();
                }
            }

            @Override
            public void onError(String error) {
                /**
                 * Setting this to say that i don,t have user details.*/
                sessionManager.set_user_data_status(false);
                if (loading_view != null) {
                    loading_view.setVisibility(View.GONE);
                }
                connectionFailed_view.setVisibility(View.VISIBLE);
                parent_scroll_view.setVisibility(View.GONE);
                actionbar_edit_rl.setVisibility(View.GONE);
                connection_failed_error.setText(error);
                /**
                 * Showing aleret.*/
                net_work_failed_aleret.net_work_fail(Edit_profile_pic.this, connection_callback);
            }
        });
    }
    /**
     * <h2>save_User_Data</h2>
     * <P>
     *Storing all the user details to the session manager.
     * </P>*/
    private void save_User_Data(UserDetails_pojo userDetails)
    {
        sessionManager.setFirstName(userDetails.getFirstName());
        sessionManager.setLastName(userDetails.getLastName());
        sessionManager.setAge(userDetails.getAge());
        sessionManager.setAbout(userDetails.getAbout());
        sessionManager.setUser_Gender(userDetails.getGender());
       /* *
          * Clearing the array list..*/
        images_url_pojo_class.getImageListUrl().clear();
        /**
         * Storing user data at first as Image list.*/
        images_url_pojo_class.getImageListUrl().add(userDetails.getProfilePhoto());
        if (userDetails.getImages().size() > 0)
        {
            for(int count=0;(count<4 && count<userDetails.getImages().size());count++)
            {
                images_url_pojo_class.getImageListUrl().add(userDetails.getImages().get(count));
            }
        }
        String image_url = new Gson().toJson(images_url_pojo_class);
        sessionManager.addImagesUrl(image_url);
        /**
         * Storing video url.*/
        sessionManager.setVideourl(userDetails.getProfileVideo());
        sessionManager.setVideo_thumbnail_url(userDetails.getVideoThumnail());
    }

    /**
     * <h3>set_User_data</h3>
     * <P>
     *
     * </P>*/
    private void  set_User_data()
    {
        /**
         * Creating a list view for profile reference profile pic View. list and
         * image link list and delete_item_list.*/
        ref_proflie_view = new ArrayList<>();
        images_url_pojo_class=new Gson().fromJson(sessionManager.getIMages_url(),Images_url_pojo_class.class);
        imageLink =images_url_pojo_class.getImageListUrl();
        /**
         * Creating image boolean list data.*/
        isImage_exist = new boolean[6];
        imag_progressbar_list = new ProgressBar[6];
        deleted_item_list = new ArrayList<>();
        /**
         *Calling this method to initialize Image data.. */
        intializeImage();
    }
    /**
     * <h3>initializeXmlContent</h3>
     * <P>
     *     initialize the required xml content.
     * </P>*/
    private void initializeXmlContent()
    {
        /**
         *Setting coustom action bar in android.. */
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View customView = inflator.inflate(R.layout.editprofileactionbar, null);
        actionBar.setCustomView(customView);

        /**
         * setting the coustom action bar left and right margin as Zero by passing the 0,0 absloute value.
         * */
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);

        /**
         * Back button selector.*/
        RelativeLayout actionbar_back_rl = (RelativeLayout) customView.findViewById(R.id.actionbar_back);
        actionbar_back_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Calling the back pressed method.*/
                onBackPressed();

            }
        });

/**
 * Revert Button of delete.*/
        getActionbar_revert_rl = (RelativeLayout) customView.findViewById(R.id.revert_previous_data);
        getActionbar_revert_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp_click_view = null;
                removeAll_delete_item(deleted_item_list);
            }
        });
/**
 * Edit image Button.*/
        actionbar_edit_rl = (RelativeLayout) customView.findViewById(R.id.actionbar_edit_image);
        actionbar_edit_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!VariableConstant.IS_FOR_FIRST_TIME_CLICK)
                {
                    if (temp_click_view != null)
                    {
                        temp_Imageview_to_set = temp_click_view;
                        temp_click_view = null;
                        profile_picture_setting.shoeAlert_Profile_picture_chooser();

                    } else if (maxImage_avaliabe >=6)
                    {
                        Toast.makeText(Edit_profile_pic.this, R.string.there_no_room_new_image, Toast.LENGTH_SHORT).show();
                    } else {
                        temp_Imageview_to_set = ref_proflie_view.get(maxImage_avaliabe);
                        profile_picture_setting.shoeAlert_Profile_picture_chooser();
                    }
                } else {
                    show_User_hint_aleret();
                }
            }
        });
        /**
         * Action bar delete button.*/
        actionbar_delete_rl = (RelativeLayout) customView.findViewById(R.id.actionbar_delete);
        actionbar_delete_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteImage_Controller();
            }
        });



        /**
         * Initializing the video view.*/
        charcter_count=(TextView)findViewById(R.id.charcter_count);
        parent_scroll_view=(ScrollView)findViewById(R.id.parent_scroll_view);
        //TextView connection_failed_heading = (TextView) findViewById(R.id.Error_header);
        connection_failed_error=(TextView)findViewById(R.id.connection_failed_text);
        Button retry_button = (Button) findViewById(R.id.retry_button);
        assert retry_button != null;
        retry_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProfileDatas();
            }
        });
        /**
         * Connection failed view.*/
        loading_view=(RelativeLayout)findViewById(R.id.progress_bar_rl);
        connectionFailed_view=(RelativeLayout)findViewById(R.id.connection_failed_rl);
    }
    /**
     * <h1>Method to initialization method</h1>
     * <p/>
     * Method assigning all the xml related content to the local variable of this class.
     * </P>
     */
    private void intializeImage()
    {

        /**
         * Setting the view.*/
        parent_scroll_view.setVisibility(View.VISIBLE);
        connectionFailed_view.setVisibility(View.GONE);
        loading_view.setVisibility(View.GONE);
        actionbar_edit_rl.setVisibility(View.VISIBLE);

        TextView textView_user_name = (TextView) findViewById(R.id.user_about_name);
        assert textView_user_name != null;
        textView_user_name.setText(String.format("About %s :", sessionManager.getFirstName()));

        /**
         * user abouts.*/
        user_about_edt = (EditText) findViewById(R.id.user_about);
        user_about_edt.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                parent_scroll_view.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        user_about_edt.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                int text_length=s.length();
                charcter_count.setText(String.format("%d", 500 - text_length));

            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });


        if(!sessionManager.getAbout().equals(""))
        {
            user_about_edt.setText(sessionManager.getAbout());
        }


        ImageView profile_picture = (ImageView) findViewById(R.id.prfile_picture);
        ProgressBar image_progress_bar = (ProgressBar) findViewById(R.id.prfile_picture_progressbar);
        imag_progressbar_list[0] = image_progress_bar;
        assert profile_picture != null;
        profile_picture.setTag("0");


        profile_opt_1 = (ImageView) findViewById(R.id.image1);
        ProgressBar image1_progressbar = (ProgressBar) findViewById(R.id.progressBar1);
        imag_progressbar_list[1] = image1_progressbar;
        profile_opt_1.setTag("1");

        profile_opt_2 = (ImageView) findViewById(R.id.image2);
        ProgressBar image2_progressbar = (ProgressBar) findViewById(R.id.progressBar2);
        imag_progressbar_list[2] = image2_progressbar;
        profile_opt_2.setTag("2");


        profile_opt_3 = (ImageView) findViewById(R.id.image3);
        ProgressBar image3_progressbar = (ProgressBar) findViewById(R.id.progressBar3);
        imag_progressbar_list[3] = image3_progressbar;
        profile_opt_3.setTag("3");


        profile_opt_4 = (ImageView) findViewById(R.id.image4);
        ProgressBar image4_progressbar = (ProgressBar) findViewById(R.id.progressBar4);
        imag_progressbar_list[4] = image4_progressbar;
        profile_opt_4.setTag("4");


        profile_opt_5 = (ImageView) findViewById(R.id.image5);
        ProgressBar image5_progressbar = (ProgressBar) findViewById(R.id.progressBar5);
        imag_progressbar_list[5] = image5_progressbar;
        profile_opt_5.setTag("5");

        if (ref_proflie_view != null)
        {
            ref_proflie_view.add(profile_picture);
            ref_proflie_view.add(profile_opt_1);
            ref_proflie_view.add(profile_opt_2);
            ref_proflie_view.add(profile_opt_3);
            ref_proflie_view.add(profile_opt_4);
            ref_proflie_view.add(profile_opt_5);
        }

        RelativeLayout rootView = (RelativeLayout) findViewById(R.id.parentview);
        /**
         * Setting the Drag listener to the Parent layout in to cover whole screen.*/
        assert rootView != null;
        rootView.setOnDragListener(this);
   /**
     * Calling this method to set LongClick listener according to that given image url list.
     */
        settingTheListener();
    }

    /**
     * Override of onResume. */
    public void onResume()
    {
        super.onResume();
    }

    /**
     * <h2>Initialize the Listener to view according to the image array size.</h2>
     * <p>
     * First counting the image list size and then according to that setting the Long click listener.
     * </p>
     */
    private void settingTheListener()
    {
        isIn_delete_mode=false;
        /**
         * Globally setting the image number.*/
        maxImage_avaliabe = imageLink.size();

        for (int i = 0; i < ref_proflie_view.size(); i++)
        {
            if (i < maxImage_avaliabe)
            {
                isImage_exist[i] = true;
                if(i==0)
                {
                    ref_proflie_view.get(i).setOnLongClickListener(null);
                }else
                {
                    ref_proflie_view.get(i).setOnLongClickListener(this);
                }
                ref_proflie_view.get(i).setAlpha(1f);
                settingImage(ref_proflie_view.get(i),imageLink.get(i));
            } else
            {
                ref_proflie_view.get(i).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.plus_symbole));
                isImage_exist[i] = false;
                ref_proflie_view.get(i).setOnLongClickListener(null);
            }
            ref_proflie_view.get(i).setOnClickListener(this);
        }
    }

    /**
     * Calling this method to set the image using picasso
     */
    private void settingImage(View temp_view, String image_url)
    {
        if(image_url!=null&&!image_url.equals(""))
        {
            ImageView temp_image_view = (ImageView) temp_view;
            Picasso.with(getApplicationContext()).load(image_url).placeholder(R.drawable.default_user_rectangle).into(temp_image_view);
        }
    }

    /**
     * Override of the Drag listener method of View onDragListener interface..
     * <p/>
     * Here First Listening the Event of drag view. and then reading the event weather dragging or not ect.
     * When Drag is started then at location {@code DragEvent.ACTION_DRAG_LOCATION: } case reading the current location of the Drag point
     * and performing action according to that.
     * </P>
     * <p/>
     * First reading the x and y point then according to that point checking weather it is which Image View then according to setting true
     * or false for the boolean value.
     * </P>
     */
    @Override
    public boolean onDrag(View v, DragEvent event)
    {
        switch (event.getAction())
        {
            case DragEvent.ACTION_DRAG_STARTED:
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                /**
                 * If the Drag is started then defining the x and y coordinate...*/
                int x_cord, y_cord, tag_position_source, tag_position_destination;
                break;

            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DRAG_LOCATION:
                /**
                 *Each Location changed or dragging reading the x and y coordinate.
                 * and checking that x y coordinate value is less or equal then setting the image alpha value as 0.5f or setting 1f.
                 * setting the View selected flag as true in less or false.*/
                x_cord = (int) event.getX();
                y_cord = (int) event.getY();

                /**
                 * For profile picture View Reference screen..*/
                if (y_cord > calculationofScarrenHeight(100) && x_cord > calculationofScarrenWidth(50) && y_cord < calculationofScarrenHeight(550) && x_cord < calculationofScarrenWidth(500))
                {
                    ref_proflie_view.get(0).setAlpha(0.5f);
                    temp_view_destination = ref_proflie_view.get(0);
                } else
                {
                    ref_proflie_view.get(0).setAlpha(1f);
                }

                /**
                 * For Image1 View Reference screen..*/
                if (y_cord > calculationofScarrenHeight(60) && x_cord > calculationofScarrenWidth(510) && y_cord < calculationofScarrenHeight(200) && x_cord < calculationofScarrenWidth(650)) {
                    ref_proflie_view.get(1).setAlpha(0.5f);
                    temp_view_destination = ref_proflie_view.get(1);

                } else
                {
                    ref_proflie_view.get(1).setAlpha(1f);
                }


                /**
                 * For Image2 View Reference screen..*/
                if (y_cord > calculationofScarrenHeight(250) && x_cord > calculationofScarrenWidth(510) && y_cord < calculationofScarrenHeight(450) && x_cord < calculationofScarrenWidth(650)) {
                    ref_proflie_view.get(2).setAlpha(0.5f);
                    temp_view_destination = ref_proflie_view.get(2);

                } else {
                    ref_proflie_view.get(2).setAlpha(1f);
                }

                /**
                 * For Image3 View Reference screen..*/
                if (y_cord > calculationofScarrenHeight(460) && x_cord > calculationofScarrenWidth(510) && y_cord < calculationofScarrenHeight(680) && x_cord < calculationofScarrenWidth(650)) {
                    ref_proflie_view.get(3).setAlpha(0.5f);
                    temp_view_destination = ref_proflie_view.get(3);

                } else
                {
                    ref_proflie_view.get(3).setAlpha(1f);
                }

                /**
                 * For Image4 View Reference screen..*/
                if (y_cord > calculationofScarrenHeight(460) && x_cord > calculationofScarrenWidth(300) && y_cord < calculationofScarrenHeight(680) && x_cord < calculationofScarrenWidth(455)) {
                    ref_proflie_view.get(4).setAlpha(0.5f);
                    temp_view_destination = ref_proflie_view.get(4);

                } else {
                    ref_proflie_view.get(4).setAlpha(1f);
                }

                /**
                 * For Image5 View Reference screen..*/
                if (y_cord > calculationofScarrenHeight(460) && x_cord > calculationofScarrenWidth(20) && y_cord < calculationofScarrenHeight(680) && x_cord < calculationofScarrenWidth(230)) {
                    ref_proflie_view.get(5).setAlpha(0.5f);
                    temp_view_destination = ref_proflie_view.get(5);

                } else
                {
                    ref_proflie_view.get(5).setAlpha(1f);
                }

                break;

            case DragEvent.ACTION_DRAG_ENDED:
                break;
            case DragEvent.ACTION_DROP:
                /**
                 * When drag ended ..
                 * First checking flage value as true or not.
                 * if true then swap image using a temp drawable object.*/
                if (isgot_intoview)
                {
                    if (longPress_temp_View != null && temp_view_destination != null && imag_progressbar_list != null) {
                        /**
                         * calling service for image replaceing and on sucess */

                        tag_position_source = Integer.parseInt(longPress_temp_View.getTag().toString());
                        tag_position_destination = Integer.parseInt(temp_view_destination.getTag().toString());

                        if (isImage_exist[tag_position_destination] && tag_position_source != tag_position_destination)
                        {
                            String tempUrl_link_source = imageLink.get(tag_position_source);
                            String tempUrl_link_destination = imageLink.get(tag_position_destination);
                            /**
                             * Replacing the Url link from list.*/
                            imageLink.set(tag_position_source, tempUrl_link_destination);
                            imageLink.set(tag_position_destination, tempUrl_link_source);
                            /**
                             * setting visible for progress bar.*/
                            imag_progressbar_list[tag_position_source].setVisibility(View.VISIBLE);
                            imag_progressbar_list[tag_position_destination].setVisibility(View.VISIBLE);
                            /**
                             * Uploading image on swap..*/
                            uploadImage(tag_position_source, tag_position_destination,true);

                        }
                        else
                        {
                            if(temp_view_destination!=null)
                            {
                                temp_view_destination.setAlpha(1f);
                            }

                        }
                    }
                }

                break;
            default:
                break;
        }

        return true;
    }
    /**
     * <h2>updatePreference</h2>
     * <P>
     *   Method cal a async task to update the all the details of the prference changed to the server.
     *   Create Json object and pass all the parameter to Json object.
     * </P>
     */
    private void updatePreference(boolean isShowProgress)
    {
        if(Utility.isNetworkAvailable(Edit_profile_pic.this))
        {
            if(isShowProgress)
            {
                if(progress_bar_dialog!=null)
                {
                    progress_bar_dialog.show();
                }
            }
            JSONArray jsonArray=creaste_Selected_list();
            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id",Utility.getDeviceId(this));
                jsonObject.put("ent_fbid",sessionManager.getFbId());
                jsonObject.put("ent_sess_token",sessionManager.getToken());
                jsonObject.put("selected",jsonArray);
            }
            catch (Exception e)
            {
                e.printStackTrace();

                if(progress_bar_dialog!=null)
                {
                    progress_bar_dialog.dismiss();
                }
            }

            Log.d("Requesting:",jsonObject.toString());

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.UPDATE_USER_SETTING, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback()
            {
                @Override
                public void onSuccess(String result)
                {
                    Log.d("SettingResult",result);
                    if(progress_bar_dialog!=null)
                    {
                        progress_bar_dialog.dismiss();
                    }
                    try
                    {
                        JSONObject jsonObject=new JSONObject(result);
                        if(jsonObject.getString("errorFlag").equals("0"))
                        {
                            Toast.makeText(Edit_profile_pic.this,getApplicationContext().getString(R.string.updatesSucessfully),Toast.LENGTH_SHORT).show();
                        }else
                        {
                            Toast.makeText(Edit_profile_pic.this,getApplicationContext().getString(R.string.unabletoUpdate),Toast.LENGTH_SHORT).show();
                        }
                        /**
                         * finishing the Activity.*/
                        finish();
                        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(String error)
                {
                    Toast.makeText(getApplicationContext(),error, Toast.LENGTH_SHORT).show();
                    if(progress_bar_dialog!=null)
                    {
                        progress_bar_dialog.dismiss();
                    }
                }
            });

        }else
        {
            if(progress_bar_dialog!=null&&progress_bar_dialog.isShowing())
            {
                progress_bar_dialog.dismiss();
            }
            /**
             * finishing the Activity.*/
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }
    /**
     * End of Drag listener..*/
    /**
     * <h>Listing the long press listener </h>
     * <p>
     * In this method fist assing the view to the Global temp view and setting the tag .
     * then Creating the ClipData.Item and passing that to the View Drag method and starting the Drag.
     * by calling  a Method {@code startDrag()}.
     * </p>
     */
    @Override
    public boolean onLongClick(View v)
    {
        if(!VariableConstant.IS_FOR_FIRST_TIME_CLICK)
        {
            if (!isIn_delete_mode)
            {
                longPress_temp_View = v;
                isgot_intoview = true;
                ClipData.Item item = new ClipData.Item((CharSequence) v.getTag());
                String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
                ClipData dragData = new ClipData(v.getTag().toString(), mimeTypes, item);
                View.DragShadowBuilder myShadow = new View.DragShadowBuilder(v);
                v.startDrag(dragData, myShadow, null, 0);
                return true;
            } else
            {
                return false;
            }
        }else
        {
            show_User_hint_aleret();
            return false;
        }
    }

    /**
     * <h>UploadImage</h>
     * <p/>
     * Method upload Image Image url to the server.
     * <p/>
     */
    private void uploadImage(final int source_position, final int destination_position,final boolean is_replace_Required)
    {
        if(Utility.isNetworkAvailable(Edit_profile_pic.this))
        {
            String other_image_link = "";
            int other_Image_list_size = imageLink.size();
            for (int ImageCount = 1; ImageCount < other_Image_list_size; ImageCount++)
            {
                other_image_link = other_image_link + imageLink.get(ImageCount);

                /**
                 *putting the semicolon on image separetor.
                 * */
                if (other_Image_list_size > 2 && ImageCount < other_Image_list_size - 1)
                {
                    other_image_link = other_image_link + ",";
                }
            }

            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(getApplicationContext()));
                jsonObject.put("ent_sess_token", sessionManager.getToken());
                jsonObject.put("ent_profile_pic",imageLink.get(0));
                jsonObject.put("ent_other_urls",other_image_link);

            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.UPLOAD_IMAGE, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result) {
                    images_url_pojo_class.setImageListUrl(imageLink);
                    String image_url = new Gson().toJson(images_url_pojo_class);
                    sessionManager.addImagesUrl(image_url);
                    if (is_replace_Required) {
                        replaceImage(source_position, destination_position, true);
                    } else {
                        settingTheListener();
                    }
                }

                @Override
                public void onError(String error) {
                    replaceImage(source_position, destination_position, false);
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
        }else
        {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.networknotavailable),Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Handel the image control.*/
    private void deleteImage_Controller()
    {
        int listSize=deleted_item_list.size();

        String url_delete="";
        for (int count=0;count<listSize;count++)
        {
            Delete_list_pojoclass delete_list_pojoclass=deleted_item_list.get(count);
            int tag=Integer.parseInt(delete_list_pojoclass.getDelete_image_tag());
            imag_progressbar_list[tag].setVisibility(View.VISIBLE);
            url_delete = delete_list_pojoclass.getDelete_Image_url();
            /**
             *putting the semicolon on image separetor.
             * */
            if (count > 2 && count < listSize - 1)
            {
                url_delete = url_delete + ",";
            }
        }
/**
 * Delete image from server.*/

        if(Utility.isNetworkAvailable(Edit_profile_pic.this))
        {
            deleteImage(url_delete);
        }else
        {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.networknotavailable),Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * <h>replaceImage</h>
     * Method to replace the Image.
     * <p>
     * Method receives source and destination position and hide the progress bar shown position.
     * </p>
     */
    private void replaceImage(int source_point, int destination_point, boolean sucess)
    {
        imag_progressbar_list[source_point].setVisibility(View.GONE);
        imag_progressbar_list[destination_point].setVisibility(View.GONE);
        if (sucess) {
            ImageView temp_Image_view_source = (ImageView) longPress_temp_View;
            ImageView temp_image_destination = (ImageView) temp_view_destination;
            Drawable drawable = temp_Image_view_source.getDrawable();
            temp_Image_view_source.setImageDrawable(temp_image_destination.getDrawable());
            temp_image_destination.setImageDrawable(drawable);
            temp_image_destination.setAlpha(1f);
        }

        /**
         * Setting all the ref screen in alpha as 1f...
         * */
        profile_opt_1.setAlpha(1f);
        profile_opt_2.setAlpha(1f);
        profile_opt_3.setAlpha(1f);
        profile_opt_4.setAlpha(1f);
        profile_opt_5.setAlpha(1f);

    }

    /**
     * <h>
     * Method to set width point of every Screen width android device.
     * </h>
     * <p>
     * My Layout design width is in 720 width screen so to perform the same width point of
     * layout to every screen size of the point.
     * Method first receive the base point value as parameter and multiply with the Screen width and
     * dividing the base screen width where designing is done.
     * </p>
     *
     * @param widthValue contain the x point in base 720 width screen size.
     * @return int value of the x point where it should be in given screen width.
     * @since 21-01-2016
     */
    public int calculationofScarrenWidth(int widthValue) {
        int temp = widthValue * Screenwidth;
        temp = temp / 720;
        return temp;
    }

    /**
     * <h>
     * Method to set height point in of every Screen height android device.
     * </h>
     * <p>
     * My Layout design height is in 1280 height screen so to perform the same height point of
     * layout to every screen height of the point.
     * Method first receive the base point value as parameter and multiply with the Screen width and
     * dividing the base screen height where designing is done.
     * </p>
     *
     * @param widthValue contain the y point in base 1280 width screen size.
     * @return int value of the y point where it should be in given screen height.
     * @since 21-01-2016
     */
    public int calculationofScarrenHeight(int widthValue)
    {
        int temp = widthValue * Screenheight;
        temp = temp / 1280;
        return temp;
    }
    /**
     * <h2>getUSerSettingDetails</h2>
     * <P>
     *     Getting the user setting details like user height,weight etc.
     *     from the server.
     * </P>*/
    private void getUSerSettingDetails()
    {
        if(Utility.isNetworkAvailable(Edit_profile_pic.this))
        {
            final ProgressBar progressBar=(ProgressBar)findViewById(R.id.usersettingloading);
            if (progressBar != null)
            {
                progressBar.setVisibility(View.VISIBLE);
            }

            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(getApplicationContext()));
                jsonObject.put("ent_sess_token",sessionManager.getToken());
                jsonObject.put("ent_fbid",sessionManager.getFbId());

                Log.d("SettingRequesst", jsonObject.toString());

            } catch (JSONException e)
            {
                if (progressBar != null)
                {
                    progressBar.setVisibility(View.GONE);
                }
                e.printStackTrace();
            }

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_USER_SETTING, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback()
            {
                @Override
                public void onSuccess(String result)
                {
                    if (progressBar != null)
                    {
                        progressBar.setVisibility(View.GONE);
                    }
                    Log.d("SettingResult12",result);
                    Gson gson = new Gson();
                    SetPreferencePojo preferencePojo = gson.fromJson(result, SetPreferencePojo.class);
                    /**
                     * Handel of the response.*/
                    addPreferenceDatas(preferencePojo);
                }

                @Override
                public void onError(String error)
                {
                    Log.d("SettingResulte",error);
                    if (progressBar != null)
                    {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            });


        }else
        {
            Toast.makeText(getApplicationContext(),"No Internet connection..!",Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method will add all set preference datas at run time
     */
    private void addPreferenceDatas(SetPreferencePojo responsedata)
    {
        if ("0".equals(responsedata.getErrorFlag()))
        {
            if(preferenceAlDatas_before.size()>0)
            {
                preferenceAlDatas_before.clear();
            }

            /*
              Contains all the data list.*/
            ArrayList<SetPreferenceAlData> preferenceAlDatas = responsedata.getData();
            preferenceAlDatas_before.addAll(preferenceAlDatas);
            /**
             * Arragnging all the data according to the priority.*/
            Collections.sort(preferenceAlDatas);
            int preferenceSize= preferenceAlDatas.size();
            /**
             * Reading the Linear layout to inflate the List of data in the layout.*/
            LinearLayout linear_inflatePref = (LinearLayout) findViewById(R.id.usersettingparent);

            for (int i=0;i<preferenceSize;i++)
            {
                /**
                 * inflating age range seek bar
                 * <P>
                 *     Type {"3" is for numeric_range view.}.
                 * </P>*/

                if (preferenceAlDatas.get(i).getTypeOfPreference().equals("1")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Age")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Distance")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Gender"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.radio_button(preferenceAlDatas.get(i)));

                }else if (preferenceAlDatas.get(i).getTypeOfPreference().equals("2")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Age")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Distance")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Gender"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.checkBox(preferenceAlDatas.get(i)));

                }else if (preferenceAlDatas.get(i).getTypeOfPreference().equals("3")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Age")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Distance")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Gender"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.numeric_Range(preferenceAlDatas.get(i)));

                } else if (preferenceAlDatas.get(i).getTypeOfPreference().equals("4")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Age")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Distance")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Gender"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.text_Input(preferenceAlDatas.get(i)));
                }
            }

            /**
             * Creating the previously seleted list given server*/
            serverSeleted=creaste_Selected_list();

        }
        else if("121".equals(responsedata.getErrorFlag()))
        {
            Toast.makeText(Edit_profile_pic.this, Edit_profile_pic.this.getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
            starting_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            /**
             * Clearing the session manager for fresh user.*/
            sessionManager.clear_session_manager();
            Edit_profile_pic.this.finish();
            Edit_profile_pic.this.startActivity(starting_intent);

        }
    }
    @Override
    public void onClick(View v)
    {
        if(!VariableConstant.IS_FOR_FIRST_TIME_CLICK)
        {
            temp_click_view = v;
            int clickPosition = Integer.parseInt(v.getTag().toString());
            if (clickPosition >= maxImage_avaliabe && clickPosition < 6) {
                if (!isIn_delete_mode) {
                    actionbar_delete_rl.setVisibility(View.GONE);
                    actionbar_edit_rl.setVisibility(View.VISIBLE);
                    /**
                     * Arranging the image in sequence..*/
                    if (ref_proflie_view != null) {
                        temp_Imageview_to_set = ref_proflie_view.get(maxImage_avaliabe);
                    }

                    profile_picture_setting.shoeAlert_Profile_picture_chooser();
                } else {
                    Toast.makeText(getApplicationContext(), "In Delete mode", Toast.LENGTH_SHORT).show();
                }
            } else if (clickPosition != 0) {
                boolean isImage_already_exist_in_delete_list = true;
                Delete_list_pojoclass delete_list_pojoclass;
                if (deleted_item_list != null) {
                    /**
                     * Checking the current Image view is already exist of not .
                     * if exist then removing the image from the delete list and
                     * setting the image view as visible by setting alpha 1f.
                     * */

                    for (int count_no = 0; count_no < deleted_item_list.size(); count_no++) {
                        delete_list_pojoclass = deleted_item_list.get(count_no);
                        if (v.getTag().equals(delete_list_pojoclass.getDelete_image_tag())) {
                            delete_list_pojoclass.getDelete_Image().setAlpha(1f);
                            deleted_item_list.remove(count_no);
                            isImage_already_exist_in_delete_list = false;
                        } else {
                            temp_click_view = delete_list_pojoclass.getDelete_Image();
                        }
                    }
                }
                /**
                 * Checking weather to hide or not hide the delete image button.*/
                if (isImage_already_exist_in_delete_list) {
                    String tag_of_delete_image = v.getTag().toString();
                    int position = Integer.parseInt(tag_of_delete_image);
                    delete_list_pojoclass = new Delete_list_pojoclass();
                    delete_list_pojoclass.setDelete_Image(v);
                    delete_list_pojoclass.setDelete_Image_url(imageLink.get(position));
                    delete_list_pojoclass.setDelete_image_tag(tag_of_delete_image);
                    deleted_item_list.add(delete_list_pojoclass);
                    v.setAlpha(0.3f);
                }
            /*
             *Checking the current delete item size for is there any item is present for delete.
              */
                if (deleted_item_list.size() > 0) {
                    if (deleted_item_list.size() > 1) {
                        getActionbar_revert_rl.setVisibility(View.VISIBLE);
                        actionbar_edit_rl.setVisibility(View.GONE);
                    } else {
                        getActionbar_revert_rl.setVisibility(View.GONE);
                        actionbar_edit_rl.setVisibility(View.VISIBLE);
                    }
                    isIn_delete_mode = true;
                    actionbar_delete_rl.setVisibility(View.VISIBLE);
                } else {
                    isIn_delete_mode = false;
                    getActionbar_revert_rl.setVisibility(View.GONE);
                    actionbar_delete_rl.setVisibility(View.GONE);
                    actionbar_edit_rl.setVisibility(View.VISIBLE);
                }

            }
        }else
        {
            show_User_hint_aleret();
        }

    }
    /**
     * <p>
     *     This method is used to make array list for preference_id and options for doing
     *     service call of update preference.
     * </p>
     */
    private JSONArray creaste_Selected_list()
    {
        JSONArray selected_data=new JSONArray();
        try
        {

            for(int count=0;count<preferenceAlDatas_before.size();count++)
            {
                if(preferenceAlDatas_before.get(count).getTypeOfPreference().equals("4"))
                {
                    JSONObject edt_data=new JSONObject();
                    edt_data.put("pref_id",preferenceAlDatas_before.get(count).getId());
                    if(preferenceAlDatas_before.get(count).getSelected()==null)
                    {
                        edt_data.put("options","");
                    }else
                    {
                        edt_data.put("options", preferenceAlDatas_before.get(count).getSelected().get(0));
                    }
                    selected_data.put(edt_data);

                }else if(preferenceAlDatas_before.get(count).getTypeOfPreference().equals("3"))
                {
                    /**
                     * Creating a pojo class for data class for the */
                    JSONObject obj=new JSONObject();
                    obj.put("pref_id", preferenceAlDatas_before.get(count).getId());
                    JSONObject obj1 = new JSONObject();
                    obj1.put("Start",preferenceAlDatas_before.get(count).getSelected_min());
                    obj1.put("End",preferenceAlDatas_before.get(count).getSelected_max());
                    obj.put("options", obj1);
                    selected_data.put(obj);

                }else
                {
                    JSONObject choice_data=new JSONObject();
                    choice_data.put("pref_id", preferenceAlDatas_before.get(count).getId());
                    choice_data.put("options", new JSONArray(preferenceAlDatas_before.get(count).getSelected()));
                    selected_data.put(choice_data);
                }

            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        return selected_data;
    }

    /**
     * <h2>Method is used to remove all the item of the delete list .</h2>
     * <p>
     * First checking is there any item is present or not for remove.
     * if availale then removing all the item upto the size of the array list .
     * </p>
     *
     * @param temp_delete_list contains the same reference af the list of delete data.
     */
    private void removeAll_delete_item(ArrayList<Delete_list_pojoclass> temp_delete_list)
    {
        Delete_list_pojoclass delete_list_pojoclass;
        /**
         * Checking is list exist or not.*/
        if (temp_delete_list != null)
        {
            /**
             * Reading the delete array list size.*/
            int delete_array_list_size = temp_delete_list.size();

            /**
             *Removing all the item to remove all the data upto the size of array list.
             *  */
            for (int count_of_delete_item = 0; count_of_delete_item < delete_array_list_size; count_of_delete_item++)
            {
                delete_list_pojoclass = temp_delete_list.get(count_of_delete_item);
                delete_list_pojoclass.getDelete_Image().setAlpha(1f);
            }
            /**
             * Clearing all the data from list.
             * setting {@code isIn_delete_mode=true } to say not in delete mode.
             * Hiding the delete button and then setting the edit to visible.
             */
            temp_delete_list.clear();
            isIn_delete_mode = false;
            getActionbar_revert_rl.setVisibility(View.GONE);
            actionbar_delete_rl.setVisibility(View.GONE);
            actionbar_edit_rl.setVisibility(View.VISIBLE);
        }

    }


    /**
     * Overriding of the onActivityResult Result...
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != RESULT_OK)
        {
            Log.d("Data not Comming:", "Failed");
            return;
        }

        switch (requestCode)
        {
            case ServiceUrl.REQUEST_IMAGE_CAPTURE:
            {
                try
                {
                    /**
                     *Extracting the file .*/
                    File file =new File(VariableConstant.profile_picture_path);
                    /**
                     * Starting the crop intent to crop Image.*/
                    start_Croping_image(file);
                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.Error_text_retry_image),Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                break;
            }
            case ServiceUrl.REQUEST_IMAGE_GALLERY:
            {

                try
                {
                    Uri selectedImageUri = data.getData();
                    String[] projection = { MediaStore.MediaColumns.DATA };
                    CursorLoader cursorLoader = new CursorLoader(this,selectedImageUri, projection, null, null, null);
                    Cursor cursor =cursorLoader.loadInBackground();
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    cursor.moveToFirst();
                    String selectedImagePath = cursor.getString(column_index);
                    File file=new File(selectedImagePath);
                    start_Croping_image(file);

                }catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.Error_text_retry_image),Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                break;
            }
            case ServiceUrl.FACEBOOK_REQUEST_CODE:
            {

                String file_path=data.getStringExtra("FILE_URL");
                try
                {
                    File file=new File(file_path);
                    start_Croping_image(file);
                }catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.Error_text_retry_image),Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                break;
            }
            case ServiceUrl.REQUEST_IMAGE_CROPPED:
            {
                try
                {
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    File actual_image_file=new File(path);
                    /**
                     * Uploading the Image*/
                    upload_Image_amazon(actual_image_file,temp_Imageview_to_set);

                }catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.Error_text_retry_image),Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                break;
            }
        }
    }


    /**
     * <h2>start_Croping_image</h2>
     * <P>
     *
     * </P>*/

    private void start_Croping_image(File image_file)
    {
        /**
         * Opening the cropping Image.*/
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH,image_file.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        startActivityForResult(intent, ServiceUrl.REQUEST_IMAGE_CROPPED);
    }
    /**
     * <h2>onBackPressed</h2>
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     * Destroying the video view if it is active.
     * <P>
     *     Then Checking if the user about is changed or not .
     *     if Changed then uploading the new User about to local server.
     * </P>
     */
    @Override
    public void onBackPressed()
    {

        String new_about=user_about_edt.getText().toString();
        /**
         * Closing the key pad. if opened.*/
        close_The_KeyPad(this, user_about_edt);

        if(new_about.equals(sessionManager.getAbout()))
        {
            if(checkUserSettingUpdateRequired())
            {
                updatePreference(true);
            }else
            {
                super.onBackPressed();
                overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
            }
        }else
        {
            upDate_User_About(new_about,checkUserSettingUpdateRequired());
        }


    }

    /**
     * <h2>checkUserSettingUpdateRequired</h2>
     * <P>
     *     Method check weather update of user setting is required or not.
     * </P>*/
    private boolean checkUserSettingUpdateRequired()
    {
        if(serverSeleted!=null&&serverSeleted.length()>0)
        {
            JSONArray jsonArray=creaste_Selected_list();
            return !serverSeleted.toString().equals(jsonArray.toString());
        }else
        {
            return false;
        }
    }
    /**
     * Closing of the keypad.*/
    private void close_The_KeyPad(Context context,View view)
    {
        if(view!=null)
        {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        }
    }
    /**
     * <h>DeleteImage</h>
     * <p>
     * This method is used to delete given url image as parameter.
     * Method call a Async_Task to delete image from server.
     * </p>
     * @see OkHttp3Connection .
     * @param image_url contains image_url to delete from server.
     */

    private void deleteImage(String image_url)
    {

        JSONObject jsonObject=new JSONObject();

        try
        {
            jsonObject.put("ent_dev_id", Utility.getDeviceId(getApplicationContext()));
            jsonObject.put("ent_sess_token", sessionManager.getToken());
            jsonObject.put("ent_image_name",image_url);

        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.DELTE_IMAGE, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result) {
                int listSize = deleted_item_list.size();

                for (int count = 0; count < listSize; count++) {
                    Delete_list_pojoclass delete_list_pojoclass = deleted_item_list.get(count);
                    int tag = Integer.parseInt(delete_list_pojoclass.getDelete_image_tag());
                    imag_progressbar_list[tag].setVisibility(View.GONE);
                    ImageView imageView = (ImageView) delete_list_pojoclass.getDelete_Image();
                    imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.plus_symbole));
                    imageLink.remove(delete_list_pojoclass.getDelete_Image_url());
                }
                /**
                 * Storing the Latest Image list to shared preference.*/
                images_url_pojo_class.setImageListUrl(imageLink);
                String session_temp = new Gson().toJson(images_url_pojo_class);
                sessionManager.addImagesUrl(session_temp);

                removeAll_delete_item(deleted_item_list);

                /**
                 * Calling this method to set LongClick listener according to that given image url list.
                 */
                settingTheListener();
            }

            @Override
            public void onError(String error) {
                int listSize = deleted_item_list.size();
                for (int count = 0; count < listSize; count++) {
                    Delete_list_pojoclass delete_list_pojoclass = deleted_item_list.get(count);
                    int tag = Integer.parseInt(delete_list_pojoclass.getDelete_image_tag());
                    imag_progressbar_list[tag].setVisibility(View.GONE);
                }
                removeAll_delete_item(deleted_item_list);
                /**
                 * Calling this method to set LongClick listener according to that given image url list.
                 */
                settingTheListener();
            }
        });
    }

    /**
     * <h2>upload_Image_amazon</h2>
     * <P>
     * Calling this method to upload image to aws.
     * </P>
     */
    private void upload_Image_amazon(final File file,View view)
    {
        String tag=view.getTag().toString();
        final int i = Integer.parseInt(tag);
        imag_progressbar_list[i].setVisibility(View.VISIBLE);
        uploadAmazonS3.Upload_data(VariableConstant.PROFILE_PICTURE,file,new Upload_file_AmazonS3.Upload_CallBack() {
            @Override
            public void sucess(String url) {
                imag_progressbar_list[i].setVisibility(View.GONE);
                if (i < maxImage_avaliabe) {
                    imageLink.set(i, url);
                } else {
                    imageLink.add(i, url);
                }
                uploadImage(0, 0, false);
            }

            @Override
            public void error(String errormsg) {
                imag_progressbar_list[i].setVisibility(View.GONE);
            }
        });

    }

    /**
     * <h2>upDate_User_About</h2>
     * <P>
     *     Method updating the user profile about.
     * </P>
     * @param user_new_About contains the user abouts.*/
    private void upDate_User_About(final String user_new_About,final boolean isSettingUpdateRequired)
    {

        if(Utility.isNetworkAvailable(Edit_profile_pic.this))
        {
            if(progress_bar_dialog!=null)
            {
                progress_bar_dialog.show();
            }

            JSONObject jsonObject=new JSONObject();

            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(getApplicationContext()));
                jsonObject.put("ent_sess_token", sessionManager.getToken());
                jsonObject.put("ent_pers_desc",user_new_About);

            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.EDITPROFILE, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result) {
                    if (progress_bar_dialog != null && !isSettingUpdateRequired) {
                        progress_bar_dialog.dismiss();
                    }

                    sessionManager.setAbout(user_new_About);
                    if (isSettingUpdateRequired)
                    {
                        updatePreference(false);
                    } else
                    {
                        Toast.makeText(Edit_profile_pic.this,getApplicationContext().getString(R.string.updatesSucessfully),Toast.LENGTH_SHORT).show();
                        /**
                         * finishing the Activity.*/
                        finish();
                        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                    }

                }

                @Override
                public void onError(String error) {
                    if (progress_bar_dialog != null) {
                        progress_bar_dialog.dismiss();
                    }
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
        }else
        {
            Toast.makeText(Edit_profile_pic.this,getApplicationContext().getString(R.string.unabletoUpdate),Toast.LENGTH_SHORT).show();
            /**
             * finishing the Activity.*/
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {

        boolean isExternalStorage=false;
        switch (requestCode)
        {
            case 786:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isExternalStorage = true;
                    }
                }

                if(isExternalStorage)
                {
                    Toast.makeText(Edit_profile_pic.this,"For setting profile picture App need Camera Acess permission and External storage of device!",Toast.LENGTH_LONG).show();

                }else
                {
                    profile_picture_setting.open_Required_intent(Edit_profile_pic.this);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }
    }

    /**
     * <h2>show_User_hint_aleret</h2>
     * <P>
     *   showing user to hint that there for edit image in edit profile.
     * </P>*/
    private void show_User_hint_aleret()
    {
        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialog.setTitle("Hint :");
        alertDialog.setMessage("Click on Image to Edit or delete the Image ." + "\n" + "Long press on Image to shuffle the Image list .");
        alertDialog.setIcon(R.drawable.launch_con);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                VariableConstant.IS_FOR_FIRST_TIME_CLICK=false;
                dialog.dismiss();
            }
        });
        android.support.v7.app.AlertDialog dialog_parent = alertDialog.show();
        dialog_parent.show();
    }

}
