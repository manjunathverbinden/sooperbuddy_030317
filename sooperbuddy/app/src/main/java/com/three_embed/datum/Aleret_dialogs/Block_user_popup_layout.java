package com.three_embed.datum.Aleret_dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Aramis_home.R;
import com.three_embed.datum.Aramis_home.User_preference_setter.Chat_Fragment_list_data;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @since  7/4/16.
 * @author 3Embed.
 */
public class Block_user_popup_layout
{
    private static Block_user_popup_layout BLOCK_USER_POPUP=new Block_user_popup_layout();
    private PopupWindow block_user_popup=null;
    private Net_work_failed_aleret net_work_failed_aleret;
    private SessionManager sessionManager;
    private Chat_Fragment_list_data mfragment;

    /**
     * <h2>Block_user_popup_layout</h2>
     * <P>
     *     Default constructure of the class.
     * </P>*/
    private Block_user_popup_layout()
    {
        /**
         * Litening the network failed aleret.*/
        net_work_failed_aleret=Net_work_failed_aleret.getInstance();
    }

    /**
     * <h2>Block_user_popup_layout</h2>
     * <P>
     *
     * </P>*/

    public static Block_user_popup_layout getInstance()
    {
        if(BLOCK_USER_POPUP==null)
        {
            BLOCK_USER_POPUP=new Block_user_popup_layout();
            return BLOCK_USER_POPUP;
        }else
        {
            return BLOCK_USER_POPUP;
        }
    }

    /**
     * <h2>show_popup_block_user</h2>
     * <P>
     *
     * </P>*/

    public void show_popup_block_user(View view,final Activity activity,final String user_fb_id,Fragment fragment)
    {
        mfragment=(Chat_Fragment_list_data)fragment;
        sessionManager=new SessionManager(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        if(block_user_popup==null)
        {
            block_user_popup=new PopupWindow(activity);
        }
        /**
         * Fonts for hum tum*/
        Typeface gothamRoundedMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/MyriadPro-Regular.otf");

        int popupWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
        int popupHeight = ViewGroup.LayoutParams.WRAP_CONTENT;
        @SuppressLint("InflateParams") View popupView = inflater.inflate(R.layout.block_user_popup_layout, null);

        TextView unBlock = (TextView) popupView.findViewById(R.id.unBlock);
        unBlock.setTypeface(gothamRoundedMedium);
        unBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Calling unblock service.*/
                do_unblock_user(user_fb_id, activity);
                /**
                 *Hide the popup window.*/
                block_user_popup.dismiss();
            }
        });

        block_user_popup.setFocusable(true);
        block_user_popup.setContentView(popupView);
        block_user_popup.setWidth(popupWidth);
        block_user_popup.setHeight(popupHeight);
        block_user_popup.showAsDropDown(view,10,0);
    }
    /**
     * <h2>do_unblock_user</h2>
     * <h3>
     *     <P>
     *
     *     </P>
     * </h3>
     * @param user_id contains the user id.
     * @param activity  contains the activity reference.*/
    private void do_unblock_user(final String user_id,final Activity activity)
    {

        if(Utility.isNetworkAvailable(activity.getApplicationContext()))
        {
            JSONObject jsonObject=new JSONObject();

            try
            {
                jsonObject.put("ent_dev_id",Utility.getDeviceId(activity.getApplicationContext()));
                jsonObject.put("ent_sess_token",sessionManager.getToken());
                jsonObject.put("ent_user_fbid",user_id);
                jsonObject.put("ent_flag","3");  //3 for unblock user as matched user !.
                jsonObject.put("ent_data_time", Utility.getCurrentGmtTime_for_block_service());

            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.BLOCKUSER, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    try
                    {
                        JSONObject jsonObject = new JSONObject(result);
                        Toast.makeText(activity.getApplicationContext(), jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                        /**
                         * Doing service call to refresh the list.*/
                        mfragment. Load_match_list();

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error)
                {
                    Toast.makeText(activity.getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });
        }else
        {
            /**
             * Showing aleret.*/
            net_work_failed_aleret.net_work_fail(activity, new Net_work_failed_aleret.Internet_connection_Callback()
            {
                @Override
                public void onSucessConnection(String connection_Type)
                {

                }

                @Override
                public void onErrorConnection(String error)
                {
             Toast.makeText(activity.getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
