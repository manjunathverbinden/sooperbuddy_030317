package com.three_embed.datum.Pojo_classes;

import java.util.ArrayList;

/**
 * Created by embed-pc on 11/12/15.
 */
public class SetPreferencePojo
{
    private String errorFlag;
    private String errorMessage;
    private ArrayList<SetPreferenceAlData> data;
    private String PlaceName;

    public String getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(String errorFlag) {
        this.errorFlag = errorFlag;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ArrayList<SetPreferenceAlData> getData() {
        return data;
    }

    public void setData(ArrayList<SetPreferenceAlData> data) {
        this.data = data;
    }

    public String getPlaceName() {
        return PlaceName;
    }

    public void setPlaceName(String placeName)
    {
        PlaceName = placeName;
    }

}
