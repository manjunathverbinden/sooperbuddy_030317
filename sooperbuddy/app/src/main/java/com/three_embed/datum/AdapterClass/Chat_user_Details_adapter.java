package com.three_embed.datum.AdapterClass;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.three_embed.datum.Aramis_home.R;
import java.util.ArrayList;
/**
 * <h1>Chat_user_Details_adapter</h1>
 * <P>
 *     Chat user adapter represent the user details of the chat user and user abouts.
 * </P>
 * @see 16.03.2016
 * @author 3Embed
 */
public class Chat_user_Details_adapter extends ArrayAdapter<String>
{

    private String userName,user_tag;
    private int age;
    private String aboutUser,user_av_distance;
    private LayoutInflater inflater;
    private Typeface sans_regular,sans_semibold;
    Coustom_Hnadeler coustom_hnadeler=null;
    private String gender;

    /**
     * Constructor
     *
     * @param context  The current context.
     */
    public Chat_user_Details_adapter(Context context, String userName, int age, String aboutUser, ArrayList<String> images, String user_distance, String user_Tag, String gender)
    {
        super(context,0, images);
        this.gender=gender;
        this.userName=userName;
        this.age=age;
        this.aboutUser=aboutUser;
        this.user_av_distance=user_distance;
        this.user_tag=user_Tag;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sans_regular= Typeface.createFromAsset(context.getAssets(), "fonts/MyriadPro-Regular.otf");
        sans_semibold=Typeface.createFromAsset(context.getAssets(),"fonts/MyriadPro-Semibold.otf");

    }


    /**
     * {@inheritDoc}
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.chat_user_details_data,null);
            coustom_hnadeler=new Coustom_Hnadeler();
            coustom_hnadeler.textViewname= (TextView) convertView.findViewById(R.id.tV_userName);
            coustom_hnadeler.textViewname.setTypeface(sans_semibold);
            coustom_hnadeler.textViewage= (TextView) convertView.findViewById(R.id.tV_userAge);
            coustom_hnadeler.textViewage.setTypeface(sans_semibold);
            coustom_hnadeler.distance_txt= (TextView) convertView.findViewById(R.id.distance);
            coustom_hnadeler.distance_txt.setTypeface(sans_semibold);
            coustom_hnadeler.tV_AbouUser= (TextView) convertView.findViewById(R.id.user_about);
            coustom_hnadeler.tV_AbouUser.setTypeface(sans_regular);
            coustom_hnadeler.user_Tag= (TextView) convertView.findViewById(R.id.user_tag);
            coustom_hnadeler.user_Tag.setTypeface(sans_regular);
            coustom_hnadeler.user_about_title=(TextView)convertView.findViewById(R.id.user_about_title);
            coustom_hnadeler.user_about_title.setTypeface(sans_regular);
            convertView.setTag(coustom_hnadeler);

        }else
        {
            coustom_hnadeler=(Coustom_Hnadeler)convertView.getTag();
        }

        coustom_hnadeler.textViewage.setText(String.format("%s ,%s", String.valueOf(age), gender));
        coustom_hnadeler.textViewname.setText(userName);
        assert user_av_distance != null;
        if(!user_av_distance.isEmpty())
        {
            coustom_hnadeler.distance_txt.setText(user_av_distance);
        }else
        {
            coustom_hnadeler.distance_txt.setVisibility(View.GONE);
        }

        coustom_hnadeler.tV_AbouUser.setText(aboutUser);
        if(user_tag!=null&&!user_tag.isEmpty())
        {
            coustom_hnadeler.user_Tag.setText(user_tag);
        }else
        {
            coustom_hnadeler.user_Tag.setVisibility(View.GONE);
        }
        coustom_hnadeler.user_about_title.setText(String.format("About %s :", userName));

        return convertView;
    }

    class Coustom_Hnadeler
    {
        TextView textViewname,textViewage,distance_txt,tV_AbouUser,user_Tag,user_about_title;
    }

}
