package com.three_embed.datum.Utility;

/**
 * @since  23/11/15.
 */
public class ServiceUrl
{
    /**
     * Service Link for Discovery.*/
    public static final String URL="http://52.66.118.7:5006/"; /*Use your service url here Ex : http://xxx.xxx.xxx.xxx:xxxx/*/
 /**
  * All chage all the method name of the service according to yours.*/
    /**
     * Log in Method..service*/
    public static final String login="login";

    /**
     * signUP link..*/
    public static final String signUp="signUp";
    /**
     * Forgot_password link */
    public static final String resetPassword="forgotPassword";

    /**
     * Camera request code..*/
   public static final int REQUEST_IMAGE_CAPTURE =1;
    /**
     * facebook
     */
    public static final int FACEBOOK_REQUEST =64206;
    /**
     * Galley request code..*/
    public static final int REQUEST_IMAGE_GALLERY =2;
    /**
     * VIDEO CAPTURESD REQUEST.*/
    public static final int REQUEST_VIDEO_CAPTURED =6;
    /**
     * Galley request code..*/
    public static final int REQUEST_IMAGE_CROPPED =3;

    public static final int FACEBOOK_REQUEST_CODE=4;
    /**
     * Folder Name*/
    public static final String App_folder="Datum";

    /**
     * Directory Path*/
    public static final String Directory_name="/Media/Images/Profile_Pictures";

    /**
     * Directory Path*/
    public static final String Directory_name_video="/Media/Images/Profile_Video";
    /**
     * Directory Path*/
    public static final String Directory_name_video_thumbnail="/Media/Images/Video_Thumbnail";
    /**
     * Directory Path*/
    public static final String Directory_chat_video_thumbnail="/Media/Images/Video_Chat_Thumbnail";
    /**
     * Directory Path*/
    public static final String CHAT_IMAGES="/Media/Images/ChatImages";

    /**
     * findMatches
     */
    public static final String FIND_MATCHES=URL+"findMatches";

    /**
     * getProfile
     */
    public static final String GET_PROFILE=URL+"getProfile";

    /**
     * getPreferences
     */
    public static final String GET_PREFERENCES=URL+"getPreferencesForAndroid";

    /**
     * logout
     */
    public static final String LOG_OUT=URL+"logout";

    /**
     * delete account
     */
    public static final String DELETE_ACCOUNT=URL+"deleteUser";

    /**
     * UpdatePreference
     */
    public static final String UPDATE_PREFERENCE=URL+"UpdatePreference";

    /**
     * facebook login
     */
    public static final String FACEBOOK_LOGIN=URL+"socialLogin";

    /**
     * setLikes
     */
    public static final String SET_LIKES=URL+"setLikes";

    /**
     * getMatchedProfile
     */
    public static final String GET_MATCHED_PROFILE=URL+"getMatchedProfile";

    /**
     * Uploading image url method*/
    public static final String UPLOAD_IMAGE=URL+"uploadImage";

    /**
     * deleting image
     */
    public static final String DELTE_IMAGE=URL+"deleteImage";

    /**
     * uploadVideo
     */
    public static final String  RECENT_VISITER=URL+"getProfileVisitors";

    /**
     * Edit user profile.
     */
    public static final String  EDITPROFILE=URL+"editProfile";

    /**
     * Edit user profile.
     */
    public static final String  REPORT=URL+"reportAbuse";

    /**
     * Edit user profile.
     */
    public static final String  UNMATCH=URL+"unmatch";

    /**
     * Edit user profile.
     */
    public static final String  BLOCKUSER=URL+"blockUser";
 /**
  * Getuser location*/
 public static final String  GET_USER_SETTING=URL+"getUsrSetAndrd";

    /**
     * Edit user user location changed .
     */
    public static final String  CHANGEMYLOCATION=URL+"ChangeMyLocation";

    public static final String REWIND=URL+"ResetLastSwipe";
 /**
  * Getuser location*/
 public static final String  UPDATE_USER_SETTING=URL+"UpdateUserSettings";
    /**
     * Update location*/
    public static final String  UPDATE_LOCATION=URL+"updateLocation";

}
