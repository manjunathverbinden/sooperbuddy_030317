package com.three_embed.datum.Facebook_login;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.three_embed.datum.Aramis_home.R;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *<h>Facebook_login</h>
 * <P>
 *     Class contain a async task to get the data from facebook .
 *     Contains a method to do facebook login .
 *     Here doing facebook login and taking data as user_friends .
 * </P>
 * @author 3Embed
 * @since  4/02/2016
 */
public class Facebook_login
{
    private Activity mactivity;
    private boolean isReady=false;

    public Facebook_login(Activity activity)
    {
        FacebookSdk.sdkInitialize(activity.getApplicationContext(), new FacebookSdk.InitializeCallback() {
            @Override
            public void onInitialized() {
                isReady = true;
            }
        });
        mactivity = activity;
    }

    public void facebook_login(CallbackManager callbackmanager,final ArrayList<String> required_data_list,final Facebook_callback facebook_callback)
    {
        LoginManager.getInstance().logInWithReadPermissions(mactivity, Arrays.asList("public_profile","user_birthday","email","user_photos"));
        LoginManager.getInstance().registerCallback(callbackmanager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json, GraphResponse response) {
                        if (response.getError() != null)
                        {
                            facebook_callback.error(response.getError().toString());
                        } else
                        {
                            Log.d("FbReponsedata",response.toString());
                            facebook_callback.sucess(json);
                        }
                    }

                });
                String requred_data = "";
                int request_size = required_data_list.size();
                for (int count = 0; count < request_size; count++) {
                    requred_data = requred_data + required_data_list.get(count);
                    if (request_size > 1 & count < request_size - 1) {
                        requred_data = requred_data + ",";
                    }
                }
                if (requred_data.equals("")) {
                    facebook_callback.error(mactivity.getResources().getString(R.string.request_vallue_null));
                } else {
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", requred_data);
                    request.setParameters(parameters);
                    request.executeAsync();
                }
            }

            @Override
            public void onCancel() {
                facebook_callback.cancel(mactivity.getResources().getString(R.string.fb_login_canceled));
            }

            @Override
            public void onError(FacebookException error) {
                facebook_callback.error(error.toString());
            }
        });
    }


    /**
     * <h>Facebook_callback</h>
     * <P>
     *     Calback interface of facebook.
     * </P>
     */
    public interface Facebook_callback
    {

        void sucess(JSONObject json);

        void error(String error);

        void cancel(String cancel);

    }


    /**
     * <h2>createFacebook_requestData</h2>
     * <P>
     *   Creating facebook request data to which data you want to access from Facebook.
     * </P>
     */
    public ArrayList<String> createFacebook_requestData()
    {
        ArrayList<String> request_parameter=new ArrayList<>();
        request_parameter.add("id");
        request_parameter.add("gender");
        request_parameter.add("email");
        request_parameter.add("age_range");
        request_parameter.add("birthday");
        request_parameter.add("first_name");
        request_parameter.add("last_name");
        request_parameter.add("albums.limit(5){name,picture{url}}");
        return request_parameter;
    }

    public void Refresh_Token()
    {
        if(isReady)
        {
            LoginManager.getInstance().logOut();
            /**
             * Refreshing the acess token of the Facebook*/
            AccessToken.refreshCurrentAccessTokenAsync();
        }

    }
}
