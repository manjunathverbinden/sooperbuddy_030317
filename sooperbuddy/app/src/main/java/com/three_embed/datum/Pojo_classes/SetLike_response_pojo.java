package com.three_embed.datum.Pojo_classes;

/**
 * @since  23/2/16.
 */
public class SetLike_response_pojo
{
    private String errorMessage;

    private String uFbId;

    private String uName;

    private String pPic;

    private String Email;

    private String errorFlag;

    public String getErrorMessage ()
    {
        return errorMessage;
    }

    public void setErrorMessage (String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getUFbId ()
    {
        return uFbId;
    }

    public void setUFbId (String uFbId)
    {
        this.uFbId = uFbId;
    }

    public String getUName ()
    {
        return uName;
    }

    public void setUName (String uName)
    {
        this.uName = uName;
    }

    public String getPPic ()
    {
        return pPic;
    }

    public void setPPic (String pPic)
    {
        this.pPic = pPic;
    }

    public String getEmail ()
    {
        return Email;
    }

    public void setEmail (String Email)
    {
        this.Email = Email;
    }

    public String getErrorFlag ()
    {
        return errorFlag;
    }

    public void setErrorFlag (String errorFlag)
    {
        this.errorFlag = errorFlag;
    }
}
