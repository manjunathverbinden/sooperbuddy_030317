package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.three_embed.aws_s3_client.Upload_file_AmazonS3;
import com.three_embed.datum.Directory_management.Directory_creation;
import com.three_embed.datum.Utility.CompressImage;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.VariableConstant;
import com.three_embed.datum.chat_lib.AppController;
import com.three_embed.datum.chat_lib.ChatMessageAdapter;
import com.three_embed.datum.chat_lib.Chat_Message_item;
import com.three_embed.datum.chat_lib.CouchDbController;
import com.three_embed.datum.chat_lib.FullScreenImage;
import com.three_embed.datum.chat_lib.RecyclerItemClickListener;
import com.three_embed.datum.chat_lib.ServerEvents;
import com.three_embed.datum.chat_lib.SocketResponseHandler;
import com.three_embed.datum.chat_lib.Utilities;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.GZIPOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.emoji.Emojicon;
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class Chat_message_fragment extends Fragment
{
    private Activity parent_activity_context;
    private Chat chat_main_class;
    private ImageView sendButton;
    private EmojiconEditText sendMessage;
    private Drawable drawable2, drawable1;
    private boolean limit = false;
    private String  encodedImage;
    private String userId;
    private String lastDate;
    private String userName;
    private String receiverUid;
    private String receiverName;
    private String documentId;
    private String picturePath;
    private String tsForServerEpoch;
    private String tsForServer;
    private String videothumbnailPath;
    private ChatMessageAdapter mAdapter;
    private RecyclerView recyclerView_chat;
    private ArrayList<Chat_Message_item> mChatData;
    private int button01pos, MessageType, size, status;
    private CouchDbController db;
    private LinearLayoutManager lm;
    private Directory_creation directory_creation;
    private String message_sending_type="0";
    private Upload_file_AmazonS3 uploadAmazonS3;
    private CompressImage compressImage;
    public Chat_message_fragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        parent_activity_context = getActivity();

        directory_creation=new Directory_creation(parent_activity_context);
      /*  *
         * Converting the Context to the class file.*/
        chat_main_class =(Chat)parent_activity_context;
        button01pos = 0;
        MessageType = 0;
        mChatData = new ArrayList<>();
        mAdapter = new ChatMessageAdapter(parent_activity_context, mChatData,chat_main_class.chat_user_doc_id);
        lm = new LinearLayoutManager(parent_activity_context,LinearLayoutManager.VERTICAL, false);
        /**
         * Defining aleret chooser dilog reference.*/
        /*  *
         * Extracting the user details.
         * From the activity.*/
        receiverUid =chat_main_class.user_email;
        receiverName =chat_main_class.user_first_name;
        documentId = chat_main_class.chat_user_doc_id;

      /*  *
         * Sender ID and Name of the Sender.
         * From activity.*/
        userId =chat_main_class.sender_user_id;
        userName =chat_main_class.sender_name;

       /* *
         * Getting the db controlar to receive the message.*/
        db = AppController.getInstance().getDbController();

     /*   *
         * Read the document form main class.*/
        lastDate = db.getLastDate(documentId);
        /**
         * Creating reference for Amazone upload.*/
        uploadAmazonS3 = Upload_file_AmazonS3.getInstance(parent_activity_context,VariableConstant.COGNITO_POOL_ID);

        /**
         * Initializing the image compressor.*/
        compressImage=CompressImage.getInstance(parent_activity_context);


    }

    /* *
      * Listining the onActivity result for the activity.*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
       /* *
         * Image from Gallery.*/
        int IMAGE_CAPTURED_QUALITY = 20;
        if (requestCode == ServiceUrl.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK)
        {
            try
            {
                picturePath=VariableConstant.chat_picture_path;
                VariableConstant.chat_picture_path="";
                /**
                 * Compressing the Image.*/
                Bitmap bm=decodeSampledBitmapFromPath(picturePath,50,50);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int IMAGE_QUALITY = 20;
                bm.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY,baos);
                byte[] b = baos.toByteArray();
                try
                {
                    baos.close();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
              /* *
                 * Compressing the byte out put stream.*/
                b = compress(b);
               /* *
                 * Encoding the byte to String formate for sending in String dormate.*/
                encodedImage = Base64.encodeToString(b,Base64.DEFAULT);
                /* *
                  * Setting the message type as 1 to say it is image.*/
                if (encodedImage != null)
                {
                    MessageType = 1;
                    Log.d("chatImagepath",encodedImage);
                }else
                {
                    throw new Exception();
                }
                /**
                 * Calling the sending button to send the data.*/
                sendButton.callOnClick();

            } catch (OutOfMemoryError e)
            {
                e.printStackTrace();
                Toast.makeText(parent_activity_context, "Error Sending Image,TryAgain!!", Toast.LENGTH_SHORT).show();
            }catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(parent_activity_context, "Error Sending Image,Un supported file!", Toast.LENGTH_SHORT).show();
            }
            /**
             * setting message to initial formate.*/
            sendMessage.setText("");
            MessageType=0;

        } else if (requestCode == ServiceUrl.REQUEST_IMAGE_GALLERY && resultCode == Activity.RESULT_OK)
        {
            picturePath = getPicturePath(data);

            try
            {
                /**
                 *Generating the thumb nail of the Image*/
                Bitmap bm=decodeSampledBitmapFromPath(picturePath,50,50);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, IMAGE_CAPTURED_QUALITY,baos);
                byte[] b = baos.toByteArray();
                try
                {
                    baos.close();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                b = compress(b);
                encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                if (encodedImage != null)
                {
                    MessageType = 1;
                    Log.d("chatImagepath",encodedImage);
                }else
                {
                    throw new Exception();
                }
                /**
                 * Calling the sending button to send the data.*/
                sendButton.callOnClick();
            } catch (OutOfMemoryError e)
            {
                e.printStackTrace();
                Toast.makeText(parent_activity_context, "Error Sending Image,TryAgain!!", Toast.LENGTH_SHORT).show();
            }catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(parent_activity_context,"Error Sending Image,Un supported file!",Toast.LENGTH_SHORT).show();
            }
            sendMessage.setText("");
            MessageType=0;
        }else if(requestCode==ServiceUrl.REQUEST_VIDEO_CAPTURED&& resultCode == Activity.RESULT_OK)
        {
            picturePath=data.getStringExtra("videopath");
            try
            {
                File file=new File(picturePath);
                file=directory_creation.create_ChatVideo_Thumbnail(file);
                videothumbnailPath =file.getPath();
                /**
                 *Generating the thumb nail of the Image*/
                Bitmap bm=decodeSampledBitmapFromPath(videothumbnailPath,50,50);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, IMAGE_CAPTURED_QUALITY,baos);
                byte[] b = baos.toByteArray();
                try
                {
                    baos.close();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                b = compress(b);
                encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                if (encodedImage != null)
                {
                    MessageType = 2;
                    Log.d("chatImagepath",encodedImage);
                }else
                {
                    throw new Exception();
                }
                /**
                 * Calling the sending button to send the data.*/
                sendButton.callOnClick();

            }catch (Exception e)
            {
                Toast.makeText(parent_activity_context, "Error Sending Video,TryAgain!!", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

        }else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {


        View coustom_view=inflater.inflate(com.three_embed.datum.chat_lib.R.layout.socket_chat_message_screen,container,false);
        recyclerView_chat = (RecyclerView)coustom_view.findViewById(com.three_embed.datum.chat_lib.R.id.list_view_messages);
        recyclerView_chat.setLayoutManager(lm);
        recyclerView_chat.setItemAnimator(new DefaultItemAnimator());
        recyclerView_chat.setAdapter(mAdapter);
        recyclerView_chat.setHasFixedSize(true);

       /* *
         * Initializing the all the Data .*/
        sendButton = (ImageView)coustom_view.findViewById(com.three_embed.datum.chat_lib.R.id.enter_chat1);

        sendMessage = (EmojiconEditText)coustom_view.findViewById(com.three_embed.datum.chat_lib.R.id.chat_edit_text1);
        sendMessage.setHint("Type a message...");

        ImageView selEmoji = (ImageView) coustom_view.findViewById(com.three_embed.datum.chat_lib.R.id.emojiButton);

        ImageView captureImage = (ImageView) coustom_view.findViewById(com.three_embed.datum.chat_lib.R.id.capture_image);

        final View rootView =coustom_view.findViewById(com.three_embed.datum.chat_lib.R.id.mainRelativeLayout);

      /*  *
         * Reading the parent layout of the Screen to the root view as Emoji poup view.*/
        final EmojiconsPopup popup = new EmojiconsPopup(rootView,parent_activity_context);

        drawable1 = ContextCompat.getDrawable(parent_activity_context, com.three_embed.datum.chat_lib.R.drawable.socket_ic_chat_send);
        drawable2 = ContextCompat.getDrawable(parent_activity_context, com.three_embed.datum.chat_lib.R.drawable.socket_ic_chat_send_active);
        sendButton.setImageDrawable(drawable1);

      /*  if(chat_main_class.user_profile_pic_url !=null&&!chat_main_class.user_profile_pic_url.isEmpty())
        {
              Picasso.with(parent_activity_context).load(Uri.parse(chat_main_class.user_profile_pic_url)).transform(new CircleTransform()).into(user_profile_picture);
        }
         user_name=(TextView)coustom_view.findViewById(R.id.user_name);
         user_name.setText(chat_main_class.user_first_name);*/




         /* Registering click  Listener*/
        popup.setSizeForSoftKeyboard();

        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        });


        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {
            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });


        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {
            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (sendMessage == null || emojicon == null) {
                    return;
                }
                int start = sendMessage.getSelectionStart();
                int end = sendMessage.getSelectionEnd();
                if (start < 0) {
                    sendMessage.append(emojicon.getEmoji());
                } else {
                    sendMessage.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });


        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {
            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                sendMessage.dispatchKeyEvent(event);
            }
        });


        selEmoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!popup.isShowing()) {
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();

                    } else {
                        sendMessage.setFocusableInTouchMode(true);
                        sendMessage.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) parent_activity_context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(sendMessage, InputMethodManager.SHOW_IMPLICIT);

                    }
                } else {
                    popup.dismiss();
                }
            }
        });



        sendMessage.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 1) {
                    sendButton.setImageDrawable(drawable2);
                    button01pos = 1;
                }
            }
        });

        captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chat_main_class.Open_attachment(0);
            }
        });



        sendButton.setOnClickListener(new View.OnClickListener()
        {

            @SuppressWarnings("unchecked")
            @Override
            public void onClick(View v)
            {
                if( sendMessage.getText().toString().isEmpty()&&MessageType==0)
                {
                    return;
                }

                final JSONObject obj = setMessageToSend();
                String tempDate= Utilities.formatDate(Utilities.tsFromGmt(tsForServer));
                String messageDate =tempDate.substring(9,24);
                String messageTime=tempDate.substring(0,9);
                try {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MMM/yyyy");
                    Date date1 = sdf.parse(messageDate);
                    Date date2 = sdf.parse(lastDate);
                    if (date1.after(date2))
                    {
                        lastDate = messageDate;
                        Map map = new HashMap<>();
                        map.put("isDate", true);
                        map.put("date", messageDate);
                        db.addNewChatMessage(documentId,map,tsForServer);
                        db.updateLastDate(documentId,lastDate);
                        Chat_Message_item message = new Chat_Message_item();
                        message.setIsDate(true);

                        String m1="",m2="";

                        String month1,month2;

                        String d1,d2;

                        d1=   sdf.format(new Date());

                        d2=messageDate;

                        month1=d1.substring(7,10);

                        month2=d2.substring(7,10);

                        switch (month1) {
                            case "Jan":
                                m1 = "01";
                                break;
                            case "Feb":
                                m1 = "02";
                                break;
                            case "Apr":
                                m1 = "04";
                                break;
                            case "May":
                                m1 = "05";
                                break;
                            case "Jun":
                                m1 = "06";
                                break;
                            case "Jul":
                                m1 = "07";
                                break;
                            case "Aug":
                                m1 = "08";
                                break;
                            case "Sep":
                                m1 = "09";
                                break;
                            case "Oct":
                                m1 = "10";
                                break;
                            case "Nov":
                                m1 = "11";
                                break;
                            case "Dec":
                                m1 = "12";
                                break;
                        }


                        if(month2.equals("Jan")){m2="01";}
                        else if(month2.equals("Feb")){m2="02";}

                        else    if(month1.equals("Mar")){m1="03";}
                        else if(month2.equals("Mar")){m2="03";}

                        else if(month2.equals("Apr")){m2="04";}


                        else if(month2.equals("May")){m2="05";}


                        else if(month2.equals("Jun")){m2="06";}



                        else if(month2.equals("Jul")){m2="07";}

                        else if(month2.equals("Aug")){m2="08";}


                        else if(month2.equals("Sep")){m2="09";}

                        else if(month2.equals("Oct")){m2="10";}


                        else if(month2.equals("Nov")){m2="11";}


                        else if(month2.equals("Dec")){m2="12";}


                        if (messageDate.equals(sdf.format(new Date())))
                        {
                            message.setDate("Today");
                        } else if (  ( Integer.parseInt(d1.substring(11)+m1+d1.substring(4,6)) - Integer.parseInt(  d2.substring(11)+m2+d2.substring(4,6)) )==1) {
                            message.setDate("Yesterday");
                        } else
                            message.setDate(messageDate);

                        mChatData.add(message);
                        parent_activity_context.runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                mAdapter.notifyItemInserted(mChatData.size()-1);
                            }
                        });
                    }

                } catch (ParseException ex)
                {
                    ex.printStackTrace();
                }

                Chat_Message_item message = new Chat_Message_item();
                message.setSenderName(userName);
                message.setIsSelf(true);
                message.setTS(messageTime);
                message.setIsDate(false);
                message.setDeliveryStatus("0");
                message.setMessageId(tsForServerEpoch);
                message.setIsLikeSnap_Chat(message_sending_type);
                message.setIsDeleted("0");

                if (MessageType == 0)
                {
                    message.setTextMessage(sendMessage.getText().toString());
                    message.setMessageType("0");
                    Map<String, Object> map = new HashMap<>();
                    map.put("message", sendMessage.getText().toString());
                    map.put("messageType", "0");
                    map.put("isSelf", true);
                    map.put("from", userName);
                    map.put("Ts", tsForServer);
                    map.put("deliveryStatus", "0");
                    map.put("id", tsForServerEpoch);
                    map.put("isDate", false);
                    map.put("isSnapchat",message_sending_type);
                    map.put("isDeleted","0");
                    db.addNewChatMessage(documentId, map,tsForServer);

                } else if (MessageType == 1)
                {
                    message.setImagePath(picturePath);
                    message.setMessageType("1");
                    Map<String, Object> map = new HashMap<>();
                    map.put("message",picturePath);
                    map.put("messageType", "1");
                    map.put("isSelf", true);
                    map.put("from", userName);
                    map.put("Ts", tsForServer);
                    map.put("deliveryStatus", "0");
                    map.put("id", tsForServerEpoch);
                    map.put("isDate", false);
                    map.put("isSnapchat",message_sending_type);
                    map.put("isDeleted","0");
                    db.addNewChatMessage(documentId,map,tsForServer);
                }else if(MessageType==2)
                {
                    message.setImagePath(videothumbnailPath);
                    message.setFile_url(picturePath);
                    message.setMessageType("2");
                    Map<String, Object> map = new HashMap<>();
                    map.put("video",picturePath);
                    map.put("videothumbnail", videothumbnailPath);
                    map.put("message", videothumbnailPath);
                    map.put("messageType", "2");
                    map.put("isSelf", true);
                    map.put("from", userName);
                    map.put("Ts", tsForServer);
                    map.put("deliveryStatus", "0");
                    map.put("id", tsForServerEpoch);
                    map.put("isDate", false);
                    map.put("isSnapchat",message_sending_type);
                    map.put("isDeleted","0");
                    db.addNewChatMessage(documentId,map,tsForServer);
                }else if(MessageType==3)
                {
                    message.setImagePath(picturePath);
                    message.setMessageType("3");
                    Map<String, Object> map = new HashMap<>();
                    map.put("message",picturePath);
                    map.put("messageType","3");
                    map.put("isSelf", true);
                    map.put("from", userName);
                    map.put("Ts", tsForServer);
                    map.put("deliveryStatus", "0");
                    map.put("id", tsForServerEpoch);
                    map.put("isDate", false);
                    map.put("isSnapchat",message_sending_type);
                    map.put("isDeleted","0");
                    db.addNewChatMessage(documentId,map,tsForServer);
                }

                mChatData.add(message);
                parent_activity_context.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mAdapter.notifyDataSetChanged();
                        recyclerView_chat.scrollToPosition(mChatData.size() - 1);
                    }
                });

                if (button01pos == 1)
                {
                    sendButton.setImageDrawable(drawable1);
                    button01pos = 0;
                }
                /**
                 * <h2>Adding the details</h2>
                 * <P>
                 *   Adding extra thing for push notification
                 * </P>*/
                try
                {
                    obj.put("senderName",chat_main_class.sender_name);
                    obj.put("senderFbid",chat_main_class.sender_fb_id);

                }catch (JSONException e)
                {
                    e.printStackTrace();
                }

                /**
                 * Sending data to server.*/
                sendData_To_Server(obj, null, picturePath, MessageType);
                sendMessage.setText("");
                MessageType=0;
            }
        });

        /**
         * Loading the first 10 msg from the db.*/
        loadFromDbFirstTen();

        AppController.initSocketHandlerInstance(new SocketResponseHandler(parent_activity_context)
        {
            @Override
            public void execute(String event, JSONObject jsonObject) throws JSONException
            {
                if (event.equals(ServerEvents.chatHistory.value))
                {
                    /**
                     * Killing the notification.*/
                    kill_notification();

                    try
                    {
                        String id = jsonObject.getString("id");
                        String sender = jsonObject.getString("from");
                        String messageType = jsonObject.getString("type");
                        String message = jsonObject.getString("payload");
                        String tsFromServer = jsonObject.getString("timestamp");
                        String isSnapchat="0";
                        String isDeleted="0";
                        String source_url="";
                        if(!messageType.equalsIgnoreCase("0"))
                        {
                            isSnapchat=jsonObject.getString("isSnapchat").trim();
                            isDeleted=jsonObject.getString("isDeleted").trim();
                            source_url=jsonObject.getString("url").trim();
                            Log.d("Url",source_url);
                        }
                        JSONObject obj = new JSONObject();
                        obj.put("from", userId);
                        obj.put("msgIds", new JSONArray(Arrays.asList(new String[]{id})));

                        String docId = AppController.getInstance().findDocumentIdOfReceiver(sender);

                        if (docId.equals(documentId))
                        {
                            loadMessageInChatUI(id,messageType,message,tsFromServer,docId,isDeleted,isSnapchat,source_url);
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }
                else if (event.equals(ServerEvents.sendMessageToServer.value))
                {
/**
 * Killing the notification.*/
                    kill_notification();

                    try {

                        String id = jsonObject.getString("id");
                        String sender = jsonObject.getString("from");
                        String messageType = jsonObject.getString("type");
                        String message = jsonObject.getString("payload");
                        String tsFromServer = jsonObject.getString("timestamp");
                        String isSnapchat="0";
                        String isDeleted="0";
                        String source_url="";
                        if(!messageType.equalsIgnoreCase("0"))
                        {
                            isSnapchat=jsonObject.getString("isSnapchat").trim();
                            isDeleted=jsonObject.getString("isDeleted").trim();
                            source_url=jsonObject.getString("url").trim();
                            Log.d("Url",source_url);
                        }

                        JSONObject obj = new JSONObject();
                        obj.put("from", userId);
                        obj.put("msgIds", new JSONArray(Arrays.asList(new String[]{id})));


                        String docId = AppController.getInstance().findDocumentIdOfReceiver(sender);

                        if (docId.equals(documentId))
                        {

                            loadMessageInChatUI(id, messageType, message,tsFromServer,docId,isDeleted,isSnapchat,source_url);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (event.equals(ServerEvents.getMessageResponseFromServer.value))
                {

                    try {
                        if (jsonObject.getInt("err") == 1)
                        {

                            Toast.makeText(parent_activity_context, "Error:Delivering Message", Toast.LENGTH_SHORT).show();

                        } else if (jsonObject.getInt("deliver") == 1)
                        {
                            String docId = jsonObject.getString("doc_id");
                            if (docId.equals(documentId))
                            {
                                palyMessageReceivedTone();
                                drawSingleTick(jsonObject.getString("id"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });


        /**
         * Loading the message from the db on Scroll changed of the Message list.*/
        recyclerView_chat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (lm.findFirstVisibleItemPosition() == 0) {

                    if (size != 0) {


                        loadTenMore();
                    } else {

                        if (!limit) {
                            parent_activity_context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    limit = true;
                                }
                            });
                        }
                    }
                }
            }
        });

        /**
         * Listing the image view click.*/
        recyclerView_chat .addOnItemTouchListener(new RecyclerItemClickListener(parent_activity_context, recyclerView_chat, new RecyclerItemClickListener.OnItemClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onItemClick(View view, int position)
            {
                if(position>=0)
                {
                    Chat_Message_item item = mChatData.get(position);
                    if (!item.isDate())
                    {
                        if(item.isSelf())
                        {
                            if(item.getMessageType().equals("1"))
                            {
                                Intent i = new Intent(parent_activity_context,FullScreenImage.class);
                                i.putExtra("imagePath",item.getImagePath());
                                i.putExtra("isSnapchat","0");
                                startActivity(i);

                            }

                        }else
                        {
                            if (item.getMessageType().equals("1")&&!item.getIsDeleted().equals("1")&&item.isDownloaded())
                            {
                                Intent i = new Intent(parent_activity_context,FullScreenImage.class);
                                i.putExtra("imagePath", item.getFile_url());
                                i.putExtra("isSnapchat",item.getIsLikeSnap_Chat());
                                i.putExtra("RECEIVERID",documentId);
                                i.putExtra("MESSAGEID",item.getMessageId());
                                if(item.getIsLikeSnap_Chat().equalsIgnoreCase("1"))
                                {
                                    item.setIsDeleted("1");
                                }
                                startActivity(i);
                            }
                        }
                    }
                }
            }

            @SuppressLint("NewApi")
            @Override
            public void onItemLongClick(View view, int position)
            {

            }
        }));

        return coustom_view;
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume()
    {
        super.onResume();
        parent_activity_context.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                mAdapter.notifyDataSetChanged();
            }
        });

    }

    /*  *
       * <h2>loadTenMore</h2>
       * <P>
       *
       * </P>*/
    private void loadTenMore()
    {
        final ProgressDialog pDialog = new ProgressDialog(parent_activity_context);
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);

        parent_activity_context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.show();
            }
        });

        status = 1;
        ArrayList<Map<String, Object>> arrMessage = db.retrieveAllMessages(documentId);
        Map<String, Object> mapMessage;


        int s1 = (size > 10) ? (size - 10) : (0);

        for (int i = size - 1; i >= s1; i--)
        {
            mapMessage = (arrMessage.get(i));


            boolean isdate = (boolean) mapMessage.get("isDate");

            if (isdate) {

                String presentDate = (String) mapMessage.get("date");

                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MMM/yyyy");

                String m1="",m2="";

                String month1,month2;

                String d1,d2;

                d1=   sdf.format(new Date());

                d2=presentDate;


                month1=d1.substring(7,10);


                month2=d2.substring(7,10);

                switch (month1) {
                    case "Jan":
                        m1 = "01";
                        break;
                    case "Feb":
                        m1 = "02";
                        break;
                    case "Apr":
                        m1 = "04";
                        break;
                    case "May":
                        m1 = "05";
                        break;
                    case "Jun":
                        m1 = "06";
                        break;
                    case "Jul":
                        m1 = "07";
                        break;
                    case "Aug":
                        m1 = "08";
                        break;
                    case "Sep":
                        m1 = "09";
                        break;
                    case "Oct":
                        m1 = "10";
                        break;
                    case "Nov":
                        m1 = "11";
                        break;
                    case "Dec":
                        m1 = "12";
                        break;
                }

                if(month2.equals("Jan")){m2="01";}
                else if(month2.equals("Feb")){m2="02";}

                else    if(month1.equals("Mar")){m1="03";}
                else if(month2.equals("Mar")){m2="03";}

                else if(month2.equals("Apr")){m2="04";}

                else if(month2.equals("May")){m2="05";}

                else if(month2.equals("Jun")){m2="06";}

                else if(month2.equals("Jul")){m2="07";}

                else if(month2.equals("Aug")){m2="08";}


                else if(month2.equals("Sep")){m2="09";}

                else if(month2.equals("Oct")){m2="10";}


                else if(month2.equals("Nov")){m2="11";}


                else if(month2.equals("Dec")){m2="12";}



                Chat_Message_item message = new Chat_Message_item();

                message.setIsDate(true);
                if (presentDate.equals(sdf.format(new Date()))) {
                    message.setDate("Today");





                } else if ( ( Integer.parseInt(d1.substring(11)+m1+d1.substring(4,6)) - Integer.parseInt(d2.substring(11)+m2+d2.substring(4,6)))==1) {
                    message.setDate("Yesterday");
                } else
                    message.setDate(presentDate);


                mChatData.add(0, message);
                //  mAdapter.notifyItemInserted(0);

                parent_activity_context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            } else
            {
                String ts = (String) mapMessage.get("Ts");
                boolean isSelf = (boolean) mapMessage.get("isSelf");
                String messageType = (String) mapMessage.get("messageType");
                String id = (String) mapMessage.get("id");
                String message = (String) mapMessage.get("message");
                String deliveryStatus = (String) mapMessage.get("deliveryStatus");
                String senderName = (String) mapMessage.get("from");
                String isSnapcaht=(String) mapMessage.get("isSnapchat");
                String isDeleted=(String) mapMessage.get("isDeleted");
                boolean isDownloaded=false;
                String file_Location="";
                String payload="";

                if(mapMessage.get("isDownloaded")!=null&&mapMessage.get("payloadurl")!=null)
                {
                    isDownloaded=(boolean) mapMessage.get("isDownloaded");
                    file_Location=(String) mapMessage.get("payloadurl");
                    payload=(String) mapMessage.get("payload");
                }

                switch (messageType) {
                    case "0":
                        MessageType = 0;

                        break;
                    case "1":
                        MessageType = 1;
                        break;
                    case "2":
                        MessageType = 2;

                        if (isSelf) {
                            file_Location = (String) mapMessage.get("video");
                        }
                        break;
                    case "3":
                        MessageType = 3;
                        break;
                }
                if (message != null)
                {
                    loadFromDb(MessageType, isSelf, message,payload,senderName, ts, deliveryStatus, id, status,isSnapcaht,isDeleted,isDownloaded,file_Location);
                }

            }

        }


        size = (size > 10) ? (size - 10) : (0);

        MessageType = 0;

        if(sendMessage.getText().length()==1 ){sendMessage.setText("");}

        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /* *
      * <h2>loadFromDb</h2>
      * <P>
      *
      * </P>*/
    @SuppressWarnings("unchecked")

    public void loadFromDb(int messageType,boolean isSelf, String message,String payload,String  senderName,String  timestamp, String deliveryStatus, String id, int status,String isSnapchat,String isDeleted,boolean isDownloaded,String filelication)
    {
        String date = Utilities.formatDate(Utilities.tsFromGmt(timestamp));
        Chat_Message_item message_item = new Chat_Message_item();
        if(isSelf)
            message_item.setSenderName(senderName);
        else
            message_item.setSenderName(receiverName);
        message_item.setIsSelf(isSelf);
        message_item.setTS(date.substring(0, 9));
        message_item.setMessageId(id);
        message_item.setDeliveryStatus(deliveryStatus);
        message_item.setIsDate(false);
        message_item.setMessageType(Integer.toString(messageType));
        message_item.setIsLikeSnap_Chat(isSnapchat);
        message_item.setIsDeleted(isDeleted);


        if (MessageType == 0)
        {

            message_item.setTextMessage(message);

        } else if (MessageType == 1)
        {
            message_item.setIsDownloaded(isDownloaded);
            message_item.setFile_url(filelication);
            message_item.setImage_payload(payload);
            message_item.setImagePath(message);

        }else if(MessageType==2)
        {
            message_item.setIsDownloaded(isDownloaded);
            message_item.setFile_url(filelication);
            message_item.setImage_payload(payload);
            message_item.setImagePath(message);
        }else if (MessageType == 3)
        {
            message_item.setIsDownloaded(isDownloaded);
            message_item.setFile_url(filelication);
            message_item.setImage_payload(payload);
            message_item.setImagePath(message);

        }

        sendMessage.setText(" ");
        MessageType = 0;
        mChatData.add(0, message_item);

        parent_activity_context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });

        if (status == 0)
        {
            parent_activity_context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    recyclerView_chat.scrollToPosition(mChatData.size() - 1);
                }
            });

        }

    }


    /*
        *
         * <h2>loadFromDbFirstTen</h2>
         * <P>
         *
         * </P>*/
    public void loadFromDbFirstTen()
    {

        ArrayList<Map<String, Object>> arrMessage = db.retrieveAllMessages(documentId);
        int s = arrMessage.size();

        boolean flag = false;

        size = (s > 10) ? (s - 10) : (0);

        boolean lastMessage = false;

        Map<String, Object> mapMessage;
        for (int i = s - 1; i >= size; i--)
        {
            mapMessage = (arrMessage.get(i));

            boolean isdate = (boolean) mapMessage.get("isDate");

            if (isdate)
            {
                String presentDate = (String) mapMessage.get("date");

                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MMM/yyyy");

                Chat_Message_item message = new Chat_Message_item();

                message.setIsDate(true);
/////

                String m1="",m2="";



                String month1,month2;

                String d1,d2;


                d1=   sdf.format(new Date());

                d2=presentDate;


                month1=d1.substring(7,10);


                month2=d2.substring(7,10);

                switch (month1) {
                    case "Jan":
                        m1 = "01";
                        break;
                    case "Feb":
                        m1 = "02";
                        break;
                    case "Apr":
                        m1 = "04";
                        break;
                    case "May":
                        m1 = "05";
                        break;
                    case "Jun":
                        m1 = "06";
                        break;
                    case "Jul":
                        m1 = "07";
                        break;
                    case "Aug":
                        m1 = "08";
                        break;
                    case "Sep":
                        m1 = "09";
                        break;
                    case "Oct":
                        m1 = "10";
                        break;
                    case "Nov":
                        m1 = "11";
                        break;
                    case "Dec":
                        m1 = "12";
                        break;
                }





                if(month2.equals("Jan")){m2="01";}
                else if(month2.equals("Feb")){m2="02";}

                else    if(month1.equals("Mar")){m1="03";}
                else if(month2.equals("Mar")){m2="03";}

                else if(month2.equals("Apr")){m2="04";}


                else if(month2.equals("May")){m2="05";}


                else if(month2.equals("Jun")){m2="06";}



                else if(month2.equals("Jul")){m2="07";}

                else if(month2.equals("Aug")){m2="08";}


                else if(month2.equals("Sep")){m2="09";}

                else if(month2.equals("Oct")){m2="10";}


                else if(month2.equals("Nov")){m2="11";}


                else if(month2.equals("Dec")){m2="12";}


                if (presentDate.equals(sdf.format(new Date())))
                {
                    message.setDate("Today");
                } else if (( Integer.parseInt(d1.substring(11)+m1+d1.substring(4,6)) - Integer.parseInt(  d2.substring(11)+m2+d2.substring(4,6)) )==1) {
                    message.setDate("Yesterday");
                } else
                    message.setDate(presentDate);

                mChatData.add(0,message);
                parent_activity_context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        parent_activity_context.runOnUiThread(new Runnable() {
                            @Override
                            public void run()
                            {
                                mAdapter.notifyItemInserted(0);
                            }
                        });
                    }
                });



            } else {

                String ts = (String) mapMessage.get("Ts");
                boolean isSelf = (boolean) mapMessage.get("isSelf");
                String messageType = (String) mapMessage.get("messageType");
                String id = (String) mapMessage.get("id");
                String message = (String) mapMessage.get("message");
                String deliveryStatus = (String) mapMessage.get("deliveryStatus");
                String senderName = (String) mapMessage.get("from");
                String isSnapcaht=(String) mapMessage.get("isSnapchat");
                String isDeleted=(String) mapMessage.get("isDeleted");
                boolean isDownloaded=false;
                String file_Location="";
                String payload="";
                if(mapMessage.get("isDownloaded")!=null&&mapMessage.get("payloadurl")!=null)
                {
                    isDownloaded=(boolean) mapMessage.get("isDownloaded");
                    file_Location=(String) mapMessage.get("payloadurl");
                    payload=(String) mapMessage.get("payload");
                }

                if (!isSelf && !lastMessage)
                {
                    lastMessage = true;

                }


                if (!flag && !isSelf) {

                    flag = true;
                }


                switch (messageType) {
                    case "0":
                        MessageType = 0;


                        break;
                    case "1":
                        MessageType = 1;
                        break;
                    case "2":
                        MessageType = 2;
                        if (isSelf) {
                            file_Location = (String) mapMessage.get("video");
                        }

                        break;
                    case "3":
                        MessageType = 3;
                        break;
                }

                if (message != null)
                {
                    loadFromDb(MessageType, isSelf, message,payload,senderName, ts, deliveryStatus, id, status,isSnapcaht,isDeleted,isDownloaded,file_Location);
                }

            }

        }
        MessageType = 0;
    }


    /* *
      * <h2>setImageUri</h2>
      * <P>
      *    Creating the uri for the Image to store.
      * </P>*/
    private Uri setImageUri()
    {
        File file=directory_creation.create_file_for_Chat_Image();
        Uri imgUri = Uri.fromFile(file);
        VariableConstant.chat_picture_path =file.getAbsolutePath();
        return imgUri;
    }

    /* *
      * <h2>setMessageToSend</h2>
      *<P>
      *  Creating the json object ot send .
      *</P> */
    private JSONObject setMessageToSend()
    {
        JSONObject obj = new JSONObject();
        tsForServer = Utilities.tsInGmt();
        tsForServerEpoch = new Utilities().gmtToEpoch(tsForServer);

        if (MessageType == 0)
        {
            try {
                byte[] byteArray = sendMessage.getText().toString().getBytes("UTF-8");
                String messageInbase64 = Base64.encodeToString(byteArray, Base64.DEFAULT).trim();

                if (messageInbase64.isEmpty())
                {
                    messageInbase64 = " ";
                }
                Log.d("log56",messageInbase64);
                obj.put("from", userId);
                obj.put("to", receiverUid);
                obj.put("payload", messageInbase64);
                obj.put("toDocId", documentId);
                obj.put("id", tsForServerEpoch);
                obj.put("isSnapchat","0");
                obj.put("isDeleted","0");
                obj.put("type", "0");
            } catch (JSONException | UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }

        } else if (MessageType == 1)
        {
            try
            {
                obj.put("from", userId);
                obj.put("to", receiverUid);
                obj.put("payload",encodedImage.trim());
                obj.put("toDocId",documentId);
                obj.put("id", tsForServerEpoch);
                obj.put("isSnapchat",message_sending_type);
                obj.put("isDeleted","0");
                obj.put("type", "1");
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
        else if (MessageType == 2)
        {
            try
            {
                obj.put("from", userId);
                obj.put("to", receiverUid);
                obj.put("payload",encodedImage.trim());
                obj.put("toDocId",documentId);
                obj.put("id", tsForServerEpoch);
                obj.put("isSnapchat",message_sending_type);
                obj.put("isDeleted","0");
                obj.put("type","2");
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
        }else if(MessageType==3)
        {
            try
            {
                obj.put("from", userId);
                obj.put("to", receiverUid);
                obj.put("payload","gyphy");
                obj.put("toDocId",documentId);
                obj.put("id", tsForServerEpoch);
                obj.put("isSnapchat",message_sending_type);
                obj.put("isDeleted","0");
                obj.put("type", "3");
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
        return obj;
    }
    /**
     *
     * <h2>loadMessageInChatUI</h2>
     * <P>
     *
     * </P>*/
    private void loadMessageInChatUI(String id, String messageType, String message, String tsFromServer, String docId, String isDeleted, String isSnapchat, String url)
    {
        byte[] data=null;

        if (!messageType.equals("3")&&!message.equals("video"))
        {
            data = Base64.decode(message, Base64.DEFAULT);
            if((messageType.equals("1")) ||(messageType.equals("2"))||(messageType.equals("5")))
            {
                data = decompress(data);
            }
        }

        String ts = Utilities.formatDate(Utilities.tsFromGmt(Utilities.epochtoGmt(tsFromServer)));
        String presentDate = ts.substring(9, 24);
        boolean flag = false;
        int pos = 0;
        String lastDate = db.getLastDate(docId);
        try {

            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MMM/yyyy");
            Date date1 = sdf.parse(presentDate);
            Date date2 = sdf.parse(lastDate);

            if (date1.after(date2))
            {

                Chat_Message_item messageItem = new Chat_Message_item();
                messageItem.setIsDate(true);
                String m1="",m2="";

                String month1,month2;

                String d1,d2;

                d1=   sdf.format(new Date());

                d2=presentDate;


                month1=d1.substring(7,10);


                month2=d2.substring(7,10);

                switch (month1) {
                    case "Jan":
                        m1 = "01";
                        break;
                    case "Feb":
                        m1 = "02";
                        break;
                    case "Apr":
                        m1 = "04";
                        break;
                    case "May":
                        m1 = "05";
                        break;
                    case "Jun":
                        m1 = "06";
                        break;
                    case "Jul":
                        m1 = "07";
                        break;
                    case "Aug":
                        m1 = "08";
                        break;
                    case "Sep":
                        m1 = "09";
                        break;
                    case "Oct":
                        m1 = "10";
                        break;
                    case "Nov":
                        m1 = "11";
                        break;
                    case "Dec":
                        m1 = "12";
                        break;
                }

                if(month2.equals("Jan")){m2="01";}
                else if(month2.equals("Feb")){m2="02";}

                else    if(month1.equals("Mar")){m1="03";}
                else if(month2.equals("Mar")){m2="03";}

                else if(month2.equals("Apr")){m2="04";}


                else if(month2.equals("May")){m2="05";}


                else if(month2.equals("Jun")){m2="06";}



                else if(month2.equals("Jul")){m2="07";}

                else if(month2.equals("Aug")){m2="08";}


                else if(month2.equals("Sep")){m2="09";}

                else if(month2.equals("Oct")){m2="10";}


                else if(month2.equals("Nov")){m2="11";}


                else if(month2.equals("Dec")){m2="12";}


                if (sdf.format(new Date()).equals(presentDate))
                {
                    messageItem.setDate("Today");
                }
                else if( ( Integer.parseInt(d1.substring(11)+m1+d1.substring(4,6)) - Integer.parseInt(d2.substring(11)+m2+d2.substring(4,6)) )==1 )
                {
                    messageItem.setDate("Yesterday");
                }
                else
                {
                    messageItem.setDate(presentDate);
                }
                mChatData.add(messageItem);

                parent_activity_context.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mAdapter.notifyItemInserted(mChatData.size() - 1);
                    }
                });

            } else if (date1.equals(date2))
            {
                flag = false;
            } else
            {
                flag = true;

                Chat_Message_item messageItem = new Chat_Message_item();

                messageItem.setIsDate(true);
                messageItem.setDate(presentDate);

                pos = (positionInRecyclerView(presentDate) - 1);

                if (pos < 0)
                {
                    pos = 0;
                }
                mChatData.add(pos,messageItem);
            }


        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        Chat_Message_item messageItem = new Chat_Message_item();
        messageItem.setSenderName(receiverName);
        messageItem.setIsSelf(false);
        messageItem.setIsDownloaded(false);
        messageItem.setTS(ts.substring(0, 9));
        messageItem.setMessageId(id);
        messageItem.setIsDate(false);
        messageItem.setIsLikeSnap_Chat(isSnapchat);
        messageItem.setIsDeleted(isDeleted);

        switch (messageType) {
            case "0":
                try {
                    messageItem.setMessageType("0");
                    assert data != null;
                    messageItem.setTextMessage(new String(data, "UTF-8"));

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                break;
            case "1":
                messageItem.setMessageType("1");
                messageItem.setImage_payload(message);
                messageItem.setFile_url(url);
                break;
            case "2":
                messageItem.setMessageType("2");
                messageItem.setImage_payload(message);
                messageItem.setFile_url(url);
                break;
            case "3":
                messageItem.setMessageType("3");
                messageItem.setImage_payload(message);
                messageItem.setFile_url(url);
                break;
        }

        if (flag)
        {
            mChatData.add(pos + 1, messageItem);
            parent_activity_context.runOnUiThread(new Runnable()
            {
                @Override
                public void run() {

                    mAdapter.notifyDataSetChanged();

                }
            });
            recyclerView_chat.scrollToPosition(pos+1);

        } else
        {
            Log.d("Gotessage"," "+messageType);

            mChatData.add(messageItem);
            parent_activity_context. runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    mAdapter.notifyItemInserted(mChatData.size()-1);
                    recyclerView_chat.scrollToPosition(mChatData.size()-1);
                }
            });
        }
    }

    /**
     * <h2>sendData_To_Server</h2>
     * <P>
     *     Sending data to server .
     * </P>*/
    private void sendData_To_Server(final JSONObject obj,final ProgressBar loading_progress,String path,int messageType)
    {

        if(messageType!=0)
        {
            /**
             * 1:Images
             * 2:Videos
             * 3:Giphy
             * */
            if(1 == messageType)
            {
                compressImage.compressImage(path, new CompressImage.Compressed_Callback()
                {
                    @Override
                    public void onSucess(String filePath)
                    {
                        Log.d("File_url", filePath);
                        File file_to_upload=new File(filePath);
                        uploadFile(VariableConstant.CHAT_IMAGES, file_to_upload, new UploadCallback()
                        {
                            @Override
                            public void onSucess(String fileurl)
                            {
                                if (loading_progress != null)
                                {
                                    loading_progress.setVisibility(View.GONE);
                                }
                                try
                                {
                                    obj.put("url", fileurl);
                                    Log.d("File_url", fileurl);
                                    send_Messgae(obj);

                                } catch (JSONException e)
                                {
                                    e.printStackTrace();
                                    Log.d("Failed to Upload !", fileurl);
                                }
                            }

                            @Override
                            public void onError(String error)
                            {
                                if (loading_progress != null)
                                {
                                    loading_progress.setVisibility(View.GONE);
                                }
                                Toast.makeText(parent_activity_context.getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                    @Override
                    public void onError(String error)
                    {
                        Log.d("Error:",error);
                    }
                });
            }else if(2 == messageType)
            {
                try
                {
                    Log.d("File_url", path);
                    File file_to_upload=new File(path);
                    uploadFile(VariableConstant.BUCKET_NAME_CHAT_VIDEO,file_to_upload, new UploadCallback()
                    {
                        @Override
                        public void onSucess(String fileurl)
                        {
                            if (loading_progress != null)
                            {
                                loading_progress.setVisibility(View.GONE);
                            }
                            try
                            {
                                obj.put("url", fileurl);
                                Log.d("File_url", fileurl);
                                send_Messgae(obj);

                            } catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error)
                        {
                            if (loading_progress != null)
                            {
                                loading_progress.setVisibility(View.GONE);
                            }
                            Toast.makeText(parent_activity_context.getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                        }
                    });


                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }else if(3 == messageType)
            {
                try
                {
                    obj.put("url", picturePath);
                    Log.d("File_url", picturePath);
                    send_Messgae(obj);

                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }else
        {
            send_Messgae(obj);
        }
    }

    /**
     * <h2>uploadFile</h2>
     * <p>
     *   This method upload a file to the aws server
     * </p>
     * */
    private void uploadFile(String bukket_name,final File file_to_upload,final UploadCallback uploadCallback)
    {
        uploadAmazonS3.Upload_data(bukket_name,file_to_upload, new Upload_file_AmazonS3.Upload_CallBack()
        {
            @Override
            public void sucess(String url)
            {
                uploadCallback.onSucess(url);
            }
            @Override
            public void error(String errormsg)
            {
                uploadCallback.onError(errormsg);
            }
        });
    }

    private interface UploadCallback
    {
        void onSucess(String fileurl);
        void onError(String error);
    }


    /**
     * <h2>send_Messgae</h2>
     * <P>
     *     Sending data to the server.
     * </P>*/
    private void send_Messgae(JSONObject jsonObject)
    {
        Map <String,Object> mapTemp=new HashMap<>();
        mapTemp.put("from", userId);
        mapTemp.put("to", receiverUid);
        mapTemp.put("toDocId", documentId);

        try
        {
            mapTemp.put("isSnapchat",jsonObject.get("isSnapchat"));
            mapTemp.put("isDeleted",jsonObject.get("isDeleted"));
            mapTemp.put("senderName",jsonObject.get("senderName"));
            mapTemp.put("senderFbid",jsonObject.get("senderFbid"));
            mapTemp.put("id",jsonObject.get("id"));
            String type=(String)jsonObject.get("type");
            mapTemp.put("type",type);

            switch ((type)) {
                case "0":
                    mapTemp.put("payload", jsonObject.get("payload"));
                    mapTemp.put("message", sendMessage.getText().toString());
                    break;
                case "1":
                    mapTemp.put("payload", jsonObject.get("payload"));
                    mapTemp.put("message", picturePath);
                    mapTemp.put("url", jsonObject.get("url"));
                    break;
                case "2":
                    mapTemp.put("payload", jsonObject.get("payload"));
                    mapTemp.put("message", picturePath);
                    mapTemp.put("url", jsonObject.get("url"));
                    break;
                case "3":
                    mapTemp.put("payload", jsonObject.get("payload"));
                    mapTemp.put("message", picturePath);
                    mapTemp.put("url", jsonObject.get("url"));
                    break;
            }
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
        db.addUnsentMessage(AppController.getInstance().getunsentMessageDocId(),mapTemp);
        Log.d("Log44", jsonObject.toString());
        try
        {
            Log.d("Log45",jsonObject.getString("url"));

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        AppController.getInstance().emit(ServerEvents.sendMessageToServer.value,jsonObject);
        /**
         * Intializing the null text ot text view.
         * */
        sendMessage.setText("");
        MessageType=0;
    }
    /* *
       * <h2>drawSingleTick</h2>
         * <P>
         *
         * </P>*/
    private void drawSingleTick(String id)
    {
        for (int i = mChatData.size() - 1; i >= 0; i--)
        {
            if (mChatData.get(i).isSelf() && (mChatData.get(i).getMessageId()).equals(id))
            {
                if (!(mChatData.get(i).getDeliveryStatus().equals("1")))
                {
                    mChatData.get(i).setDeliveryStatus("1");

                    final int notifyied_position=i;
                    parent_activity_context.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            mAdapter.notifyItemChanged(notifyied_position);
                            recyclerView_chat.scrollToPosition(mChatData.size()-1);
                        }
                    });
                    break;
                }
            }
        }

    }


    /**
     * <h2>decompress</h2>
     * <P>
     *
     * </P>*/
    public static byte[] decompress(byte[] data)
    {

        ByteArrayOutputStream baoStream2 = new ByteArrayOutputStream();
        try {
            InputStream inStream = new GZIPInputStream(new ByteArrayInputStream(data));
            byte[] buffer = new byte[16384];
            int len;
            while ((len = inStream.read(buffer)) > 0)
            {
                baoStream2.write(buffer, 0, len);
            }
        } catch (IOException | OutOfMemoryError e)
        {
            e.printStackTrace();
        }

        return baoStream2.toByteArray();
    }

    /*
        *
         * <h2>positionInRecyclerView</h2>
         * <P>
         *
         * </P>*/
    public int positionInRecyclerView(String presentDate)
    {
        int lastDatePosition = -1;
        for (int i = mChatData.size() - 1; i >= 0; i--)
        {
            if (mChatData.get(i).isDate())
            {
                try {
                    String date = mChatData.get(i).getDate();
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MMM/yyyy");
                    Date date1 = sdf.parse(presentDate);
                    Date date2 = sdf.parse(date);

                    if (date2.after(date1))
                    {
                        lastDatePosition = i;
                    } else
                        break;

                } catch (ParseException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        return lastDatePosition;
    }

    /*  *
       * <h2>getPicturePath</h2>
       * <P>
       *   Method return the Picature path to be shown.
       * </P>*/
    public String getPicturePath(Intent data)
    {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor =parent_activity_context.getContentResolver().query(selectedImage,filePathColumn, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picture_path = cursor.getString(columnIndex);
        cursor.close();
        return picture_path;
    }
    /**
     * <h2>compress</h2>
     * <P>
     *   Compressing the byte data using GZIPOutputStream .
     * </P>
     *@param  data contains the byte data to to be compressed. */
    public static byte[] compress(byte[] data)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPOutputStream gzos = null;
        try
        {
            gzos = new GZIPOutputStream(baos);
            gzos.write(data);
        } catch (IOException | OutOfMemoryError e)
        {
            e.printStackTrace();
        } finally {
            if (gzos != null) try
            {
                gzos.close();
            } catch (IOException ignore)
            {
                ignore.printStackTrace();
            }
        }
        return baos.toByteArray();
    }

    /**
     * <h2>OpenRequired_Intent</h2>
     * <P>
     *  Opening the required intent for Camera and Gallery intent.
     * </P>
     * @param type contains the int value to say camera or Gallery intent.*/
    public void OpenRequired_Intent(final int type)
    {
        switch (type)
        {
            case 0:
            {
                message_sending_type="0";
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null)
                {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,setImageUri());
                    startActivityForResult(takePictureIntent,ServiceUrl.REQUEST_IMAGE_CAPTURE);
                }
                else
                {
                    Toast.makeText(getActivity().getApplicationContext(),parent_activity_context.getResources().getString(R.string.unable_to_open_camera),Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case 1:
            {
                message_sending_type="0";
                Intent pickfromGallery = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickfromGallery.setType("image/*");
                startActivityForResult(Intent.createChooser(pickfromGallery, "Select File"), ServiceUrl.REQUEST_IMAGE_GALLERY);
                break;
            }
        }
    }

    /**
     * <h2>decodeSampledBitmapFromPath</h2>
     * <P>
     *
     * </P>*/
    public Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,int reqHeight)
    {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }
    /**
     * <h2>calculateInSampleSize</h2>
     * <P>
     *
     * </P>*/
    public int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight)
    {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }
    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }


    private void palyMessageReceivedTone()
    {

        MediaPlayer mediaPlayer= MediaPlayer.create(getActivity(), R.raw.message_recieved);
        mediaPlayer.start();

        /**
         * Killing the notification.*/
        kill_notification();
    }


    private void kill_notification()
    {
        /**
         * Clearing the notification if any exist.*/
        NotificationManager nMgr = (NotificationManager)parent_activity_context.getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancel(0);
    }
}
