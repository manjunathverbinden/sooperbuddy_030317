package com.three_embed.datum.Pojo_classes;

import java.util.ArrayList;

/**
 * @since  5/2/16.
 */
public class UserDetails_pojo
{

    private String errorMessage;

    private String Age;

    private String FbId;

    private String DOB;

    private String errorFlag;

    private ArrayList<String> Images;

    private String LastName;

    private String Skills;

    private String Email;

    private String Gender;

    private String ProfilePhoto;

    private String FirstName;

    private String ProfileVideo;

    private String About;

    private String Tagline;

    private String VideoThumnail;

    private String ImgCount;

    public String getErrorMessage ()
    {
        return errorMessage;
    }

    public void setErrorMessage (String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getAge ()
    {
        return Age;
    }

    public void setAge (String Age)
    {
        this.Age = Age;
    }

    public String getFbId ()
    {
        return FbId;
    }

    public void setFbId (String FbId)
    {
        this.FbId = FbId;
    }

    public String getDOB ()
    {
        return DOB;
    }

    public void setDOB (String DOB)
    {
        this.DOB = DOB;
    }

    public String getErrorFlag ()
    {
        return errorFlag;
    }

    public void setErrorFlag (String errorFlag)
    {
        this.errorFlag = errorFlag;
    }

    public ArrayList<String> getImages ()
    {
        return Images;
    }

    public void setImages (ArrayList<String> Images)
    {
        this.Images = Images;
    }

    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    public String getSkills ()
    {
        return Skills;
    }

    public void setSkills (String Skills)
    {
        this.Skills = Skills;
    }

    public String getEmail ()
    {
        return Email;
    }

    public void setEmail (String Email)
    {
        this.Email = Email;
    }

    public String getGender ()
    {
        return Gender;
    }

    public void setGender (String Gender)
    {
        this.Gender = Gender;
    }

    public String getProfilePhoto ()
    {
        return ProfilePhoto;
    }

    public void setProfilePhoto (String ProfilePhoto)
    {
        this.ProfilePhoto = ProfilePhoto;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getProfileVideo ()
    {
        return ProfileVideo;
    }

    public void setProfileVideo (String ProfileVideo)
    {
        this.ProfileVideo = ProfileVideo;
    }

    public String getAbout ()
    {
        return About;
    }

    public void setAbout (String About)
    {
        this.About = About;
    }

    public String getTagline ()
    {
        return Tagline;
    }

    public void setTagline (String Tagline)
    {
        this.Tagline = Tagline;
    }

    public String getVideoThumnail ()
    {
        return VideoThumnail;
    }

    public void setVideoThumnail (String VideoThumnail)
    {
        this.VideoThumnail = VideoThumnail;
    }

    public String getImgCount ()
    {
        return ImgCount;
    }

    public void setImgCount (String ImgCount)
    {
        this.ImgCount = ImgCount;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [errorMessage = "+errorMessage+", Age = "+Age+", FbId = "+FbId+", DOB = "+DOB+", errorFlag = "+errorFlag+", Images = "+Images+", LastName = "+LastName+", Skills = "+Skills+", Email = "+Email+", Gender = "+Gender+", ProfilePhoto = "+ProfilePhoto+", FirstName = "+FirstName+", ProfileVideo = "+ProfileVideo+", About = "+About+", Tagline = "+Tagline+", VideoThumnail = "+VideoThumnail+", ImgCount = "+ImgCount+"]";
    }
  }