package com.three_embed.datum.Aleret_dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.three_embed.datum.Aramis_home.R;
import com.three_embed.datum.Aramis_home.UserProfileActivity;

/**
 * Created by shobhit on 7/4/16.
 */
public class Report_poup_window
{
    private static Report_poup_window REPORT_POPUP=new Report_poup_window();
    private LayoutInflater inflater = null;
    private PopupWindow report_popup=null;
    private TextView textView=null;
    private Report_poup_window()
    {
    }

    public static Report_poup_window getInstance()
    {
        if(REPORT_POPUP==null)
        {
            REPORT_POPUP=new Report_poup_window();
            return REPORT_POPUP;
        }else
        {
            return REPORT_POPUP;
        }
    }

    public void show_popup(View view,final Activity activity,final boolean isUserProfileActivity)
    {
        inflater = activity.getLayoutInflater();
        if(report_popup==null)
        {
            report_popup=new PopupWindow(activity);
        }

        int popupWidth =ViewGroup.LayoutParams.WRAP_CONTENT;
        int popupHeight = ViewGroup.LayoutParams.WRAP_CONTENT;
        @SuppressLint("InflateParams") View popupView = inflater.inflate(R.layout.report_poup_layout, null);
        textView=(TextView)popupView.findViewById(R.id.popup_open);
        textView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if(isUserProfileActivity)
                {
                    UserProfileActivity userProfileActivity=(UserProfileActivity)activity;
                    userProfileActivity.report_user();

                }
                /**
                 *Hide the popup window.*/
                report_popup.dismiss();
            }
        });

        report_popup.setFocusable(true);
        report_popup.setWidth(popupWidth);
        report_popup.setHeight(popupHeight);
        report_popup.setContentView(popupView);
        report_popup.showAsDropDown(view,30, -1);

    }
}
