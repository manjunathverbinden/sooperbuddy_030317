package com.three_embed.datum.Aleret_dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Aramis_home.R;
import org.json.JSONException;
import org.json.JSONObject;


public class Report_dialog extends Dialog implements android.view.View.OnClickListener
{
    private Activity mactivity;
    private  ColorDrawable cd;
    private RelativeLayout inaap_photo_rl,Sperm_user_rl,other_rl;
    private ProgressBar inaapp_progress,sperm_user_progress_bar,edit_text_progress_bar;
    private SessionManager sessionManager;
    private String fb_id=null;
    private Button ok_button,call_service_button;
    private TextView reponse_msg_tv;
    private LinearLayout first_view_layout,response_content,thired_content;
    private EditText reason_text;
    private TextView bottom_header_text,header_text,spam_user,fell_like,otherreason;
    private Typeface GothamRoundedMedium,GothamRoundedBold;

    public Report_dialog(Activity activity,String user_fb_id)
    {
        super(activity);
        this.mactivity = activity;
        this.fb_id=user_fb_id;
        cd=new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        sessionManager=new SessionManager(mactivity);
        /**
         * Fonts for hum tum*/
        GothamRoundedMedium=Typeface.createFromAsset(mactivity.getAssets(),"fonts/MyriadPro-Regular.otf");
        GothamRoundedBold=Typeface.createFromAsset(mactivity.getAssets(),"fonts/MyriadPro-Bold.otf");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(true);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);
        setContentView(R.layout.report_user_aleret_layout);
        getWindow().setBackgroundDrawable(cd);


        inaap_photo_rl=(RelativeLayout)findViewById(R.id.inapp_photos);
        inaap_photo_rl.setOnClickListener(this);
        Sperm_user_rl=(RelativeLayout)findViewById(R.id.sperm_user);
        Sperm_user_rl.setOnClickListener(this);
        other_rl=(RelativeLayout)findViewById(R.id.other_reason);
        other_rl.setOnClickListener(this);
        inaapp_progress=(ProgressBar)findViewById(R.id.ainn_app_photo);
        sperm_user_progress_bar=(ProgressBar)findViewById(R.id.sprem_progressbar);

        ok_button=(Button)findViewById(R.id.ok_button);
        ok_button.setTypeface(GothamRoundedMedium);
        ok_button.setOnClickListener(this);

        reponse_msg_tv=(TextView)findViewById(R.id.reponse_msg);
        reponse_msg_tv.setTypeface(GothamRoundedMedium);

        first_view_layout=(LinearLayout)findViewById(R.id.first_content);
        first_view_layout.setVisibility(View.VISIBLE);
        response_content=(LinearLayout)findViewById(R.id.response_content);
        response_content.setVisibility(View.GONE);

        thired_content=(LinearLayout)findViewById(R.id.thired_content);
        thired_content.setVisibility(View.GONE);

        call_service_button=(Button)findViewById(R.id.call_service);
        call_service_button.setOnClickListener(this);

        reason_text=(EditText)findViewById(R.id.reason_text);
        reason_text.setTypeface(GothamRoundedMedium);

        edit_text_progress_bar=(ProgressBar)findViewById(R.id.edit_text_progress_bar);

        header_text=(TextView)findViewById(R.id.header_text);
        header_text.setTypeface(GothamRoundedBold);

        bottom_header_text=(TextView)findViewById(R.id.bottom_header_text);
        bottom_header_text.setTypeface(GothamRoundedMedium);

        spam_user=(TextView)findViewById(R.id.spam_user);
        spam_user.setTypeface(GothamRoundedMedium);

        fell_like=(TextView)findViewById(R.id.fell_like);
        fell_like.setTypeface(GothamRoundedMedium);

        otherreason=(TextView)findViewById(R.id.otherreason);
        otherreason.setTypeface(GothamRoundedMedium);

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.inapp_photos:
                do_repost_user(fb_id,inaapp_progress,"In Appropreate photo");
                break;
            case R.id.sperm_user:
                do_repost_user(fb_id, sperm_user_progress_bar, "Spam user !");
                break;
            case R.id.other_reason:
                first_view_layout.setVisibility(View.GONE);
                thired_content.setVisibility(View.VISIBLE);
                break;
            case R.id.ok_button:
                this.dismiss();
                break;
            case R.id.call_service:
                String data=reason_text.getText().toString();
                if(data.isEmpty())
                {
                    Toast.makeText(mactivity,"Please Enter The reason !",Toast.LENGTH_SHORT).show();
                }else
                {
                    hide_Soft_Input_key(reason_text);
                    do_repost_user(fb_id,edit_text_progress_bar,data);
                }
                break;
            default:
                break;
        }
    }

    /**
     * <h3></h3>*/
    private void do_repost_user(String fb_id,final ProgressBar progressBar, final String report_msg)
    {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_dev_id", Utility.getDeviceId(mactivity));
            jsonObject.put("ent_sess_token",sessionManager.getToken());
            jsonObject.put("ent_user_fbid",fb_id);
            jsonObject.put("ent_data_time",Utility.getCurrentGmtTime());
            jsonObject.put("ent_report_message",report_msg);

        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.REPORT, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {

            @Override
            public void onSuccess(String result)
            {
                JSONObject jsonObject;
                if (progressBar != null)
                {
                    progressBar.setVisibility(View.INVISIBLE);
                }
                try
                {
                    jsonObject=new JSONObject(result);
                    first_view_layout.setVisibility(View.GONE);
                    thired_content.setVisibility(View.GONE);
                    response_content.setVisibility(View.VISIBLE);
                    reponse_msg_tv.setText(jsonObject.getString("errorMessage"));

                } catch (JSONException e)
                {
                    e.printStackTrace();
                    Toast.makeText(mactivity,"Json response error!",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(String error)
            {
                if (progressBar != null)
                {
                    progressBar.setVisibility(View.INVISIBLE);
                }
                Toast.makeText(mactivity,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * <h2>Method hide the soft input key if opened.</h2>*/
    private void hide_Soft_Input_key(View view)
    {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)mactivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }
}

