package com.three_embed.datum.Gcm_Services;

import android.app.IntentService;
import android.content.Intent;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.three_embed.datum.Aramis_home.R;
import com.three_embed.datum.Aramis_home.SplashScreen_controlar;
import java.io.IOException;

/**
 * <h1>RegistrationIntentService</h1>
 * <P>
 *     <h3>
 *
 *     </h3>
 * </P>
 */
public class RegistrationIntentService extends IntentService
{

    private static final String TAG ="RegIntentService";
    public static final String RESPONSE_MESSAGE = "myResponseMessage";
    private static final String[] TOPICS = {"global"};

    public RegistrationIntentService()
    {
        super(TAG);
    }
    @Override
    protected void onHandleIntent(Intent intent)
    {
        try
        {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),GoogleCloudMessaging.INSTANCE_ID_SCOPE,null);
            /**
             * Sending the GCM token to send BAck service.*/
            sendRegistrationToServer(token);

            subscribeTopics(token);

        } catch (Exception e)
        {
           e.printStackTrace();
            sendRegistrationToServer("");
        }
    }

    /**
     *<h2>sendRegistrationToServer</h2>
     * <p>
     *     <h3>
     *     </h3>
     * </p>*/
    private void sendRegistrationToServer(String token)
    {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SplashScreen_controlar.TokenGeneratorReceiver.PROCESS_RESPONSE);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(RESPONSE_MESSAGE,token);
        sendBroadcast(broadcastIntent);
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    private void subscribeTopics(String token) throws IOException
    {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS)
        {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
}



