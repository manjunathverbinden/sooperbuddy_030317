package com.three_embed.datum.Pojo_classes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed-pc on 21/12/15.
 */
public class PrefSelectedValue implements Serializable
{
    private String pref_id;
    private PrefOptionValue options;
    private ArrayList<String> alOption;

    public PrefSelectedValue(String pref_id, PrefOptionValue options)
    {
        this.pref_id = pref_id;
        this.options = options;
    }

    public PrefSelectedValue(String pref_id, ArrayList<String> options)
    {
        this.pref_id = pref_id;
        this.alOption = options;
    }
}
