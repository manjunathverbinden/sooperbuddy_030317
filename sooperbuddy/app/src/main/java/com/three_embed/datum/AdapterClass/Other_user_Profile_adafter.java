package com.three_embed.datum.AdapterClass;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.three_embed.datum.Aleret_dialogs.Report_poup_window;
import com.three_embed.datum.Pojo_classes.SetPreferenceAlData;
import com.three_embed.datum.Pojo_classes.SetPreferencePojo;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import com.three_embed.datum.Aramis_home.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
/**
 * @since  3/3/16.
 * @author 3Embed
 */
public class Other_user_Profile_adafter extends ArrayAdapter<String>
{
    private String userName;
    private int age;
    private String aboutUser,user_av_distance;
    private LayoutInflater inflater;
    private Context context;
    private Typeface GothamRoundedMedium;
    private Typeface GothamRoundedBold;
    Coustom_Hnadeler coustom_hnadeler=null;
    private boolean isUserProfileActivity=false;
    private Report_poup_window report_poup_window;
    private String gender;
    private String user_online_status;
    public boolean isDataFound=false;
    private String userFbID;


    public Other_user_Profile_adafter(Context context, String userName, int age, String aboutUser, ArrayList<String> images, String user_distance, String gender, boolean isUserProfileActivity, String online_status,String fbId)
    {
        super(context,0,images);
        this.isUserProfileActivity=isUserProfileActivity;
        this.userName=userName;
        this.context=context;
        this.age=age;
        this.userFbID=fbId;
        this.aboutUser=aboutUser;
        this.gender=gender;
        this.user_av_distance=user_distance;
        this.user_online_status=online_status;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        GothamRoundedBold= Typeface.createFromAsset(this.context.getAssets(), "fonts/MyriadPro-Bold.otf");
        GothamRoundedMedium=Typeface.createFromAsset(this.context.getAssets(),"fonts/MyriadPro-Regular.otf");
        report_poup_window=Report_poup_window.getInstance();
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.other_user_profile_bottom,null);
            coustom_hnadeler=new Coustom_Hnadeler();
            coustom_hnadeler.textViewname= (TextView) convertView.findViewById(R.id.tV_userName);
            coustom_hnadeler.textViewname.setTypeface(GothamRoundedBold);
            coustom_hnadeler.textViewage= (TextView) convertView.findViewById(R.id.tV_userAge);
            coustom_hnadeler.textViewage.setTypeface(GothamRoundedBold);
            coustom_hnadeler.distance_txt= (TextView) convertView.findViewById(R.id.distance);
            coustom_hnadeler.distance_txt.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.tV_AbouUser= (TextView) convertView.findViewById(R.id.user_about);
            coustom_hnadeler.tV_AbouUser.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.user_spinner=(ImageView)convertView.findViewById(R.id.spinner);
            coustom_hnadeler.about_user_title=(TextView)convertView.findViewById(R.id.about_user_title);
            coustom_hnadeler.about_user_title.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.lastseen=(TextView)convertView.findViewById(R.id.lastseen);
            coustom_hnadeler.lastseen.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.online_status=(TextView)convertView.findViewById(R.id.online_status);
            coustom_hnadeler.online_status.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.user_status_bar=(RelativeLayout)convertView.findViewById(R.id.user_status_bar);
            coustom_hnadeler.linearlayout=(LinearLayout)convertView.findViewById(R.id.usersettingparent);
            coustom_hnadeler.textUserSetting=(TextView)convertView.findViewById(R.id.userSettingtext);
            coustom_hnadeler.textUserSetting.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.progress_bar=(ProgressBar)convertView.findViewById(R.id.progress_bar);
            convertView.setTag(coustom_hnadeler);
        }else
        {
            coustom_hnadeler=(Coustom_Hnadeler)convertView.getTag();
        }

        /**
         * Setting user age.*/
        if(age==0)
        {
            coustom_hnadeler.textViewage.setText(String.format(" ,%s", gender));
        }else
        {
            coustom_hnadeler.textViewage.setText(String.format("%s ,%s", String.valueOf(age), gender));
        }

        /**
         * Setting user status .*/
        if(user_online_status.isEmpty())
        {
            coustom_hnadeler.user_status_bar.setVisibility(View.GONE);
        }else if(user_online_status.equalsIgnoreCase("online"))
        {
            coustom_hnadeler.user_status_bar.setVisibility(View.VISIBLE);
            coustom_hnadeler.lastseen.setVisibility(View.GONE);
            coustom_hnadeler.online_status.setText(R.string.user_online);
        }else
        {
            coustom_hnadeler.lastseen.setVisibility(View.VISIBLE);
            coustom_hnadeler.user_status_bar.setVisibility(View.VISIBLE);
            coustom_hnadeler.online_status.setText(Utility.convert_Message_Time(user_online_status));
        }

        coustom_hnadeler.textViewname.setText(userName);
        coustom_hnadeler.about_user_title.setText(String.format("About %s :", userName));
        coustom_hnadeler.distance_txt.setText(user_av_distance);
        if(VariableConstant.isNotCommingFromHomePage)
        {
            coustom_hnadeler.distance_txt.setVisibility(View.GONE);
        }else
        {
            coustom_hnadeler.distance_txt.setVisibility(View.VISIBLE);
        }
        coustom_hnadeler.tV_AbouUser.setText(aboutUser);

        /**
         * Setting the spinner adapter.*/
        coustom_hnadeler.user_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                report_poup_window.show_popup(v, (Activity) context, isUserProfileActivity);
            }
        });
        if(!isDataFound)
        {
            getUSerSettingDetails(coustom_hnadeler.linearlayout, coustom_hnadeler.progress_bar);
        }
        return convertView;
    }
    public void setDownLoadFlage()
    {
        isDataFound=false;
    }
    /**
     * <h2>Coustom_Hnadeler</h2>
     * <P>
     *   Handler class of this given view in android.
     * </P>*/
    class Coustom_Hnadeler
    {
        TextView textViewname,textViewage,distance_txt,tV_AbouUser,textUserSetting;
        ImageView user_spinner;
        LinearLayout linearlayout;
        ProgressBar progress_bar;
        TextView about_user_title,lastseen,online_status;
        RelativeLayout user_status_bar;
    }
    /**
     * <h2>getUSerSettingDetails</h2>
     * <P>
     *     Getting the user setting details like user height,weight etc.
     *     from the server.
     * </P>*/
    private void getUSerSettingDetails(final LinearLayout linearLayout, final ProgressBar progressBar)
    {
        SessionManager sessionManager=new SessionManager(context);
        if(Utility.isNetworkAvailable(context)&&userFbID!=null)
        {
            if (progressBar != null)
            {
                progressBar.setVisibility(View.VISIBLE);
            }

            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(context));
                jsonObject.put("ent_sess_token",sessionManager.getToken());
                jsonObject.put("ent_fbid",userFbID);
            } catch (JSONException e)
            {
                if (progressBar != null)
                {
                    progressBar.setVisibility(View.GONE);
                }
                e.printStackTrace();
            }

            Log.d("Getting user id:",jsonObject.toString());

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_USER_SETTING, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result) {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                    Log.d("Called time",result);
                    isDataFound=true;

                    Gson gson = new Gson();
                    SetPreferencePojo preferencePojo = gson.fromJson(result, SetPreferencePojo.class);
                    /**
                     * Handel of the response.*/
                    addPreferenceDatas(preferencePojo,linearLayout);
                }

                @Override
                public void onError(String error) {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            });


        }else
        {
            Toast.makeText(context, "No Internet connection..!", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * This method will add all set preference datas at run time
     */
    private void addPreferenceDatas(SetPreferencePojo responsedata,LinearLayout linear_inflatePref)
    {
        if ("0".equals(responsedata.getErrorFlag()))
        {
            SetPreferenceAlData setPreferenceAlData;
            TextView header, selectedVallue;
            Activity activity = (Activity) context;
            for (int i = 0; i < responsedata.getData().size(); i++)
            {
                setPreferenceAlData = responsedata.getData().get(i);
                if(!setPreferenceAlData.getPreferenceTitle().equals("Age")&&!setPreferenceAlData.getPreferenceTitle().equals("Distance")&&!setPreferenceAlData.getPreferenceTitle().equals("Gender"))
                {
                    @SuppressLint("InflateParams") View child = activity.getLayoutInflater().inflate(R.layout.usersettingitem, null);
                    header = (TextView) child.findViewById(R.id.headeritem);
                    header.setTypeface(GothamRoundedBold);
                    header.setText(setPreferenceAlData.getPreferenceTitle());
                    selectedVallue = (TextView) child.findViewById(R.id.selected);
                    selectedVallue.setTypeface(GothamRoundedBold);

                    switch (setPreferenceAlData.getTypeOfPreference()) {
                        case "3":

                            if (setPreferenceAlData.getSelected_max() != null) {
                                selectedVallue.setText(setPreferenceAlData.getSelected_max());
                            } else {
                                selectedVallue.setText(context.getString(R.string.notSet));
                            }

                            break;
                        case "4":
                            if (setPreferenceAlData.getSelected() != null && setPreferenceAlData.getSelected().size() > 0)
                            {
                                StringBuilder builder = new StringBuilder();
                                for (String value : setPreferenceAlData.getSelected())
                                {
                                    builder.append(value);
                                    builder.append(" ");
                                }
                                selectedVallue.setText(builder);
                            } else
                            {
                                selectedVallue.setText(context.getString(R.string.notSet));
                            }

                            break;
                        default:
                            if (setPreferenceAlData.getSelected() != null && setPreferenceAlData.getSelected().size() > 0)
                            {
                                StringBuilder builder = new StringBuilder();
                                for (String value : setPreferenceAlData.getSelected())
                                {
                                    builder.append(value);
                                    builder.append(" ");
                                }
                                selectedVallue.setText(builder);
                            } else {
                                selectedVallue.setText(context.getString(R.string.notSet));
                            }
                            break;
                    }
                    linear_inflatePref.addView(child);
                }
            }
        }

    }


}


