package com.three_embed.datum.Pojo_classes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed-pc on 21/12/15.
 */
public class PrefOptionValue implements Serializable
{
    private String Start;
    private String End;

    public PrefOptionValue(String start, String end) {
        Start = start;
        End = end;
    }
}
