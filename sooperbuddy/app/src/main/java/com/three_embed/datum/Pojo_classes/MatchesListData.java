package com.three_embed.datum.Pojo_classes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by adminpc on 19/1/16.
 */
public class MatchesListData implements Serializable
{
    private String FirstName;
    private String LastName;
    private String Email;
    private String FbId;
    private String ProfileVideo;
    private String ProfilePhoto;
    private ArrayList<String> Images;
    private String About;
    private String Gender;
    private String Tagline;
    private String Age;
    private String LastActive;
    private String Status;

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFbId() {
        return FbId;
    }

    public void setFbId(String fbId) {
        FbId = fbId;
    }

    public String getProfileVideo() {
        return ProfileVideo;
    }

    public void setProfileVideo(String profileVideo) {
        ProfileVideo = profileVideo;
    }

    public String getProfilePhoto() {
        return ProfilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        ProfilePhoto = profilePhoto;
    }

    public ArrayList<String> getImages() {
        return Images;
    }

    public void setImages(ArrayList<String> images) {
        Images = images;
    }

    public String getAbout() {
        return About;
    }

    public void setAbout(String about) {
        About = about;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getTagline() {
        return Tagline;
    }

    public void setTagline(String tagline) {
        Tagline = tagline;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getLastActive() {
        return LastActive;
    }

    public void setLastActive(String lastActive) {
        LastActive = lastActive;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
