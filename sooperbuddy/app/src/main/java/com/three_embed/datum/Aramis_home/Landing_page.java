package com.three_embed.datum.Aramis_home;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.CallbackManager;
import com.google.gson.Gson;
import com.three_embed.datum.Facebook_login.FBResponse_pojo;
import com.three_embed.datum.Facebook_login.Facebook_login;
import com.three_embed.datum.Utility.CirclePageIndicator;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.ViewPager.Viewpager_Adapter;
import org.json.JSONObject;
import java.util.ArrayList;
/**
 * <h1>Landing_page</h1>
 * <P>
 *
 * </P>*/
public class Landing_page extends AppCompatActivity implements View.OnClickListener
{
    private Facebook_login login_with_facebook;
    private Facebook_service_login facebook_service_login;
    /**
     * facebook login call back mamanger*/
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        login_with_facebook=new Facebook_login(Landing_page.this);
        callbackManager = CallbackManager.Factory.create();
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_landing_page);

        /**
         * Reading the action bar then Hiding in the Action bar exist.*/
        android.support.v7.app.ActionBar actionBar=getSupportActionBar();
        if(actionBar!=null)
        {
            actionBar.hide();
        }
        /**
         * Method to initialize the Xml content.*/
        initialization();
    }
    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume()
    {
        super.onResume();

    }
    /**
     * initializing the xml content.
     * */
    private void initialization()
    {

        /**
         * Fonts for hum tum*/
        Typeface gothamRoundedMedium = Typeface.createFromAsset(getAssets(), "fonts/GothamRoundedMedium_21022.ttf");

        Typeface gothamRoundedBook = Typeface.createFromAsset(getAssets(), "fonts/GothamRoundedBook_21018.ttf");

        facebook_service_login=new Facebook_service_login(Landing_page.this);

        ViewPager horizontal_slider = (ViewPager) findViewById(R.id.horizontal_pager);
        PagerAdapter fragment_PagerAdapter = new Viewpager_Adapter(getSupportFragmentManager());
        assert horizontal_slider != null;
        horizontal_slider.setAdapter(fragment_PagerAdapter);

        CirclePageIndicator mCirclePageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        assert mCirclePageIndicator != null;
        mCirclePageIndicator.setViewPager(horizontal_slider);

        RelativeLayout facebook_button = (RelativeLayout) findViewById(R.id.fabook_login);
        assert facebook_button != null;
        facebook_button.setOnClickListener(this);

        TextView term_text = (TextView) findViewById(R.id.termtextone);
        assert term_text != null;
        term_text.setTypeface(gothamRoundedBook);

        TextView term_condition = (TextView) findViewById(R.id.termandcondition);
        assert term_condition != null;
        term_condition.setTypeface(gothamRoundedMedium);
        term_condition.setMovementMethod(LinkMovementMethod.getInstance());

        TextView term_and = (TextView) findViewById(R.id.andtext);
        assert term_and != null;
        term_and.setTypeface(gothamRoundedBook);

        TextView term_privacy_police = (TextView) findViewById(R.id.privacypolicy);
        assert term_privacy_police != null;
        term_privacy_police.setMovementMethod(LinkMovementMethod.getInstance());
        term_privacy_police.setTypeface(gothamRoundedMedium);


        TextView login_with_facebook_tv = (TextView) findViewById(R.id.login_with_facebook);
        assert login_with_facebook_tv != null;
        login_with_facebook_tv.setTypeface(gothamRoundedBook);

    }

    /**
     * Override of onClick of View.Onclick listener interface. */
    @Override
    public void onClick(View v)
    {

        switch (v.getId())
        {



            case R.id.fabook_login:
            {
                if(Utility.isNetworkAvailable(getApplicationContext()))
                {
                    doFacebook_login();
                }else
                {
                    Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.networknotavailable),Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    /**
     * <h2>doFacebook_login</h2>
     * <P>
     *   Doing Facebook login .
     * </P>*/
    private void doFacebook_login()
    {
        /**
         * Refreshing the token for new call.*/
        login_with_facebook.Refresh_Token();

        ArrayList<String> credential=login_with_facebook.createFacebook_requestData();
        login_with_facebook.facebook_login(callbackManager, credential, new Facebook_login.Facebook_callback() {
            @Override
            public void sucess(JSONObject json)
            {
                FBResponse_pojo fbResponse_pojo = new Gson().fromJson(json.toString(), FBResponse_pojo.class);
                /**
                 * DO social Login.*/
                facebook_service_login.socialLogin(fbResponse_pojo);
            }

            @Override
            public void error(String error) {
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void cancel(String cancel) {
                Toast.makeText(getApplicationContext(), cancel, Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode != RESULT_OK)
        {
            return;
        }
        switch (requestCode)
        {
            case ServiceUrl.FACEBOOK_REQUEST:
                callbackManager.onActivityResult(requestCode, resultCode, data);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onBackPressed()
    {
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        finish();
    }

}
