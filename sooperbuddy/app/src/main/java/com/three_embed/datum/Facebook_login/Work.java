package com.three_embed.datum.Facebook_login;

/**
 * Created by shobhit on 6/5/16.
 */
public class Work
{
    private String id;

    private Position position;

    private User_Location location;

    private Employer employer;

    private String start_date;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Position getPosition ()
    {
        return position;
    }

    public void setPosition (Position position)
    {
        this.position = position;
    }

    public User_Location getLocation ()
    {
        return location;
    }

    public void setLocation (User_Location location)
    {
        this.location = location;
    }

    public Employer getEmployer ()
    {
        return employer;
    }

    public void setEmployer (Employer employer)
    {
        this.employer = employer;
    }

    public String getStart_date ()
    {
        return start_date;
    }

    public void setStart_date (String start_date)
    {
        this.start_date = start_date;
    }




}
