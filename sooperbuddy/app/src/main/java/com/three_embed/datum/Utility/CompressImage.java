package com.three_embed.datum.Utility;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 * Created by shobhit on 22/6/16.
 */
public class CompressImage
{
    private ImageLoadingUtils utils;
    private static CompressImage compressImage=null;

    private CompressImage(Activity activity)
    {
        utils = new ImageLoadingUtils(activity);
    }

/**
 * <h2>CompressImage</h2>
 * <P>
 *     getting instance og the Image.
 * </P>*/
    public static CompressImage getInstance(Activity activity)
    {
        if(compressImage==null)
        {
            compressImage=new CompressImage(activity);
            return compressImage;
        }else
        {
            return compressImage;
        }
    }

/**
 * <h2>compressImage</h2>
 * <P>
 *
 * </P>
 * @param image_file_path
 * @param callback
 * */
    public void compressImage(String image_file_path,Compressed_Callback callback)
    {
        DataHolder dataHolder=new DataHolder();
        dataHolder.filePath=image_file_path;
        dataHolder.callback=callback;
        new ImageCompressionAsyncTask().execute(dataHolder);
    }

    private class ImageCompressionAsyncTask extends AsyncTask<DataHolder, Void, String>
    {
        boolean error=false;
        String errorText="";
        Compressed_Callback backgroundCallback=null;
        @Override
        protected String doInBackground(DataHolder... params)
        {
            backgroundCallback=params[0].callback;
            String filePath = compressImage(params[0].filePath);
            return filePath;
        }

        public String compressImage(String filePath)
        {
            Bitmap scaledBitmap = null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath,options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;
            float maxHeight = 816.0f;
            float maxWidth = 612.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth)
            {
                if (imgRatio < maxRatio)
                {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16*1024];

            try
            {
                bmp = BitmapFactory.decodeFile(filePath,options);
            }
            catch(OutOfMemoryError exception)
            {
                error=true;
                errorText=exception.toString();
                exception.printStackTrace();
            }
            try
            {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            }
            catch(OutOfMemoryError exception)
            {
                error=true;
                errorText=exception.toString();
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float)options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth()/2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                    Log.d("EXIF", "Exif: " + orientation);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                    Log.d("EXIF", "Exif: " + orientation);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            } catch (IOException e)
            {
                error=true;
                errorText=e.toString();
                e.printStackTrace();
            }
            FileOutputStream out = null;
            String filename = getFilename();
            try
            {
                out = new FileOutputStream(filename);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

            } catch (FileNotFoundException e)
            {
                error=true;
                errorText=e.toString();
                e.printStackTrace();
            }
            return filename;
        }


        public String getFilename()
        {
            File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
            if (!file.exists()) {
                file.mkdirs();
            }
            String uriSting = (file.getAbsolutePath() + "/"+ System.currentTimeMillis() + ".jpg");
            return uriSting;

        }



        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            if(error)
            {
               backgroundCallback.onError(errorText);
            }
            else
            {
                backgroundCallback.onSucess(result);
            }
        }
    }

    private class DataHolder
    {
        String filePath;
        Compressed_Callback callback;
    }

    public interface Compressed_Callback
    {
        void onSucess(String filePath);
        void onError(String error);
    }
}
