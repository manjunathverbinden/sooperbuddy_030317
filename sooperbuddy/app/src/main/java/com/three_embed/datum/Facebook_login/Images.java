package com.three_embed.datum.Facebook_login;

import java.io.Serializable;
/**
 * @since  14/11/16.
 */
public class Images implements Serializable
{

    private String height;

    private String source;

    private String width;

    public String getHeight ()
    {
        return height;
    }

    public void setHeight (String height)
    {
        this.height = height;
    }

    public String getSource ()
    {
        return source;
    }

    public void setSource (String source)
    {
        this.source = source;
    }

    public String getWidth ()
    {
        return width;
    }

    public void setWidth (String width)
    {
        this.width = width;
    }
}
