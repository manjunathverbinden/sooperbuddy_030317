package com.three_embed.datum.Aleret_dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.three_embed.datum.Aramis_home.R;
/**
 * @since  9/11/16.
 */
public class Status_aleret_dialog
{
    LinearLayout.LayoutParams layoutParams;
    private static Status_aleret_dialog STATUS_ALERET = new Status_aleret_dialog();
    private Dialog status_aleret;

    private Status_aleret_dialog()
    {
        layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    public static Status_aleret_dialog getInstance()
    {
        if (STATUS_ALERET == null)
        {
            STATUS_ALERET = new Status_aleret_dialog();
            return STATUS_ALERET;
        } else {
            return STATUS_ALERET;
        }
    }

    public void status_AlertView(final Activity mactivity,final String[] status_list,final Status__Call_back status__call_back)
    {
        /**
         * Network failed aleret is visible to user.
         */
        if (status_aleret != null&&status_aleret.isShowing())
        {
            status_aleret.dismiss();
        }
        /**
         * Fonts for hum tum*/
        Typeface gothamRoundedMedium = Typeface.createFromAsset(mactivity.getAssets(), "fonts/MyriadPro-Regular.otf");
        Typeface gothamRoundedBold = Typeface.createFromAsset(mactivity.getAssets(), "fonts/MyriadPro-Bold.otf");

        ColorDrawable cd = new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        status_aleret = new Dialog(mactivity);
        status_aleret.getWindow().setBackgroundDrawable(cd);
        status_aleret.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = mactivity.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.status_selection_layout, null);
        ListView listView=(ListView)dialogView.findViewById(R.id.listView);
        listView.setAdapter(new ArrayAdapter<String>(mactivity,R.layout.status_view_item, status_list));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                if (status__call_back != null)
                {
                    status_aleret.dismiss();
                    status__call_back.onSelected(status_list[position],position);
                    status_aleret.cancel();
                }
            }
        });
        layoutParams.setMargins(30, 20, 30, 0);
        status_aleret.setContentView(dialogView,layoutParams);
        status_aleret.setCancelable(true);
        status_aleret.show();
    }



    public interface Status__Call_back
    {
        void onSelected(String selected_text,int position);
        void onCancel();

    }

}
