package com.three_embed.datum.AdapterClass;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.three_embed.datum.Aramis_home.R;
import java.util.ArrayList;

/**
 * @since  5/4/16.
 */
public class Fb_user_likes_list_Adapter extends BaseAdapter
{

    ArrayList<String> result;
    private LayoutInflater inflater=null;

    public Fb_user_likes_list_Adapter(LayoutInflater inflater,ArrayList<String> fb_user_likes)
    {
        result=fb_user_likes;
        this.inflater =inflater;
    }

    @Override
    public int getCount()
    {
        return result.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    public class Holder
    {
        TextView tv;
    }

    @SuppressLint({"InflateParams", "ViewHolder"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.fb_user_likes_item_layout, null);
        holder.tv=(TextView)rowView.findViewById(R.id.likes_title);
        holder.tv.setText(result.get(position));
        return rowView;
    }

}
