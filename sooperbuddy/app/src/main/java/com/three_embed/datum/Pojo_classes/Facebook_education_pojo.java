package com.three_embed.datum.Pojo_classes;

import com.three_embed.datum.Facebook_login.User_Education;

import java.util.ArrayList;

/**
 * Created by shobhit on 6/5/16.
 */
public class Facebook_education_pojo
{
    private ArrayList<User_Education> education;
    public ArrayList<User_Education> getEducation() {
        return education;
    }

    public void setEducation(ArrayList<User_Education> education) {
        this.education = education;
    }

}
