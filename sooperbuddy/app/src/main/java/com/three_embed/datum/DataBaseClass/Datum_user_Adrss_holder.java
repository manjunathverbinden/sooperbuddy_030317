package com.three_embed.datum.DataBaseClass;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shobhit on 26/2/16.
 */
public class Datum_user_Adrss_holder extends SQLiteOpenHelper
{
    private Context mcontext;
    private static final int DATABASE_VERSION =1;
    /**
     * Data base Manager.*/
    private static final String DATABASE_NAME = "HumTum_Manager";
    private static final String TABLE_NAME = "HumTum_user_address_details";
    private static final String KEY_ID = "id";
    private static final String USER_ID = "user_ID";
    private static final String ADDRESS = "user_address";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";

    public Datum_user_Adrss_holder(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mcontext = context;
    }




    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_USER_TABLE = "CREATE TABLE " +TABLE_NAME+ "("
                +KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +USER_ID +" TEXT,"+ADDRESS + " TEXT," +LATITUDE + " TEXT," +LONGITUDE + " TEXT"+")";
        db.execSQL(CREATE_USER_TABLE);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
    /**
     *Drop older table if existed OF OLDER VERSION*/
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        /**
         * Creating the new Database.*/
        onCreate(db);

    }


    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */
    public void addAddress(String user_id, String formatted_address, String lat, String lng)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        File database=mcontext.getDatabasePath(DATABASE_NAME);

        if (!database.exists())
        {
           /**
            * Database does not exist so copy it from assets here*/
            Log.i("Database", "add Drop Location Database Not Found");
        }
        else
        {
            Log.i("Database", "add Drop Location Database Found");
        }

        ContentValues values = new ContentValues();
        values.put(USER_ID, user_id);
        values.put(ADDRESS, formatted_address);
        values.put(LATITUDE,lat);
        values.put(LONGITUDE,lng);
        // Inserting Row
        db.insert(TABLE_NAME, null, values);
        db.close();
    }



    /**
     * <h2>getAllAddress</h2>
     * <P>
     *     Method loop through all database to get all the address list from
     *     the table of the given user id.
     * </P>
     * @param user_id  contains the id for which data is extracted from the table.
     * */
    public ArrayList<Database_item_details> getAllAddress(String user_id)
    {
        File database=mcontext.getDatabasePath(DATABASE_NAME);
        if(!database.exists())
        {
            /**
             *  Database does not exist so copy it from assets here
             *  */
            Log.i("Database", "getAllDropLocations Database Not Found");
        }
        else
        {
            Log.i("Database", "getAllDropLocations Database Found");
        }

        ArrayList<Database_item_details> addressList = new ArrayList<>();
       /**
        *Select All Query and where user_id is filtration.*/
        String selectQuery = "SELECT  * FROM " + TABLE_NAME +" WHERE " + USER_ID +" = "+ user_id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
       /**
        * looping through all rows and adding to list
        * */
        if (cursor.moveToFirst())
        {
            do
            {
                Database_item_details contact = new Database_item_details();
                contact.setId(Integer.parseInt(cursor.getString(0)));
                contact.setUser_id(cursor.getString(1));
                contact.setUser_address(cursor.getString(2));
                contact.setLatitude(cursor.getString(3));
                contact.setLongitude(cursor.getString(4));
                /**
                 * Adding each item one by one.*/
                addressList.add(contact);
            } while (cursor.moveToNext());
        }
        /**
         *  return contact list*/
        return addressList;
    }
}
