package com.three_embed.datum.Gcm_Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import com.google.android.gms.gcm.GcmListenerService;
import com.three_embed.datum.Utility.AppSession;
import com.three_embed.datum.Utility.VariableConstant;
import com.three_embed.datum.Aramis_home.Chat;
import com.three_embed.datum.Aramis_home.HomePageActivity;
import com.three_embed.datum.Aramis_home.R;
import com.three_embed.datum.Aramis_home.SplashScreen;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * <h1>MyGcmListenerService</h1>
 *<P>
 *    <h2>
 *        Class handel the Service response of the Data in android and push notification.
 *    </h2>
 *</P>
 * @since  28/3/16.
 * @author 3Embed.
 */
public class MyGcmListenerService extends GcmListenerService
{
    private AppSession appSession;
    /**
     * Called by the system when the service is first created.  Do not call this method directly.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        appSession=new AppSession(this);
    }
    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data)
    {
        String message = data.getString("message");
        String payload = data.getString("payload");

        if (message==null&&payload!=null)
        {
            Log.d("Message :",payload);
            /**
             * Opening message notification aleret*/
            sendNotification(payload, true);
        } else
        {
            /**
             * Opening its a match aleret.*/
            sendNotification(message, false);
        }

    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message,boolean isFrom_message)
    {
        String title_message="";
        String user_fb_id="",email_id="",sender_name="";

        try
        {
            JSONObject jsonObject=new JSONObject(message);
            /**
             * Opening the intent on click of notification.*/
            title_message=jsonObject.getString("alert");
            email_id=jsonObject.getString("from");

            if(jsonObject.has("senderFbid"))
            {
                user_fb_id=jsonObject.getString("senderFbid");
            }

            if(jsonObject.has("senderName"))
            {
                user_fb_id=jsonObject.getString("senderFbid");
            }
            if(jsonObject.has("fbId"))
            {
                user_fb_id=jsonObject.getString("fbId");
            }

        } catch (JSONException e)
        {
            e.printStackTrace();
        }


        Intent intent;
        /**
         * Checking the app status.*/
        if(!appSession.getIsAppLive())
        {
            Log.d("Message :","Message!");
            Log.d("Message :","Message!"+appSession.getIsAppLive());
            String type="0";
            if(isFrom_message)
            {
                type="1";
            }
            intent=new Intent(this,SplashScreen.class);
            Bundle bundle=new Bundle();
            bundle.putString("type",type);
            bundle.putString("mainTab", "1");
            bundle.putString("subTab", "0");
            bundle.putString("USER_FB_ID",user_fb_id);
            bundle.putString("USER_EMAIL",email_id);
            bundle.putString("USER_FIRST_NAME",sender_name);
            intent.putExtras(bundle);

        }else
        {
            if(isFrom_message)
            {
                Log.d("Message :","Message!");
                intent=new Intent(this,Chat.class);
                intent.putExtra("USER_FB_ID",user_fb_id);
                intent.putExtra("USER_EMAIL",email_id);
                intent.putExtra("USER_FIRST_NAME",sender_name);

            }else
            {
                intent=new Intent(this,HomePageActivity.class);
                /**
                 * Sending acknowledgement ot the homepage activity.*/
                Bundle bundle=new Bundle();
                bundle.putString("mainTab", "1");
                bundle.putString("subTab", "0");
                intent.putExtras(bundle);
            }
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

       Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =(NotificationCompat.Builder)new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.datum_one_logo)
                .setContentTitle("Message ")
                .setContentText(title_message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        if(!VariableConstant.isInChatScreen)
        {
            notificationManager.notify(0, notificationBuilder.build());
        }

    }

}
