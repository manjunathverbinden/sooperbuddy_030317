package com.three_embed.datum.Pojo_classes;

/**
 * Created by embed-pc on 15/12/15.
 */
public class LogOutPojo
{
    /*"errorFlag":"0",
            "errorMessage":"Logout Successfully"*/
    private String errorFlag;
    private String errorMessage;

    public String getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(String errorFlag) {
        this.errorFlag = errorFlag;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
