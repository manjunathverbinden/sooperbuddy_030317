package com.three_embed.datum.AdapterClass;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @since  2/5/16.
 */
public class View_holder_Chat_list extends RecyclerView.ViewHolder
{
    public TextView newMessageTime, newMessage,storeName, newMessageDate,newMessageCount;
    public ImageView storeImage;


    public View_holder_Chat_list(View view)
    {
        super(view);

        newMessageTime = (TextView) view.findViewById(com.three_embed.datum.chat_lib.R.id.newMessageTime);
        newMessage = (TextView) view.findViewById(com.three_embed.datum.chat_lib.R.id.newMessage);
        newMessageDate = (TextView) view.findViewById(com.three_embed.datum.chat_lib.R.id.newMessageDate);
        storeName = (TextView) view.findViewById(com.three_embed.datum.chat_lib.R.id.storeName);
        storeImage = (ImageView) view.findViewById(com.three_embed.datum.chat_lib.R.id.storeImage);
        newMessageCount = (TextView) view.findViewById(com.three_embed.datum.chat_lib.R.id.newMessageCount);

    }

}
