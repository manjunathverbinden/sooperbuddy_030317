package com.three_embed.datum.chat_lib;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Manager;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.util.Log;


import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.Map;

/**
 * @since 25/3/16.
 */
public class CouchDbController
{

    public String DB_NAME = "aramis";
    public static final String TAG = "CouchBaseEvents";
    Manager manager;
    public Database database;
    AndroidContext a_context;

    public CouchDbController(AndroidContext a_context)
    {
        this.a_context = a_context;

        try
        {
            manager = getManagerInstance();
            database = getDatabaseInstance();
        } catch (Exception e)
        {
            Log.e(TAG, "Error getting database", e);
        }
    }

    //making db or returing it if already present
    private Database getDatabaseInstance()
    {
        if ((this.database == null) & (this.manager != null))
        {
            try
            {
                this.database = manager.getExistingDatabase(DB_NAME);
                if (database == null)
                {
                    this.database = manager.getDatabase(DB_NAME);
                }
            } catch (CouchbaseLiteException e)
            {
                Log.e(TAG, "Error getting database", e);
            }
        }
        return database;
    }


    private Manager getManagerInstance() throws IOException
    {
        if (manager == null)
        {
            manager = new Manager(a_context,Manager.DEFAULT_OPTIONS);
        }
        return manager;
    }

    public String createIndexDocument()
    {

        Map<String, Object> map = new HashMap<>();
        
        Document document = database.createDocument();
        ArrayList<String> arr_userName = new ArrayList<>();
        ArrayList<String> arr_userDocId = new ArrayList<>();

        map.put("userNameArray", arr_userName);
        map.put("userDocIdArray", arr_userDocId );

        try {

            document.putProperties(map);

        } catch (CouchbaseLiteException e)
        {
            Log.e(TAG, "Error putting", e);
        }

        arr_userName= null;
        arr_userDocId= null;
        map=null;

        return document.getId();
    }



    @SuppressWarnings("unchecked")
    public void addToIndexDocument(String docId,String userName,String userdocId)
    {

        //database.clearDocumentCache();

        Document document = database.getDocument(docId);

        Map<String, Object> map_old = document.getProperties();

        if (map_old != null)
        {
            ArrayList<String> arrUserName = (ArrayList<String>) map_old.get("userNameArray");

            ArrayList<String> arrUserDocId = (ArrayList<String>) map_old.get("userDocIdArray");

            arrUserName.add(userName);

            arrUserDocId.add(userdocId);
            Map<String, Object> map_temp = new HashMap<>();
            map_temp.putAll(map_old);


            map_temp.put("userNameArray", arrUserName);

            map_temp.put("userDocIdArray", arrUserDocId);
            try
            {
                document.putProperties(map_temp);
            } catch (CouchbaseLiteException e)
            {
                Log.e(TAG, "Error putting", e);
            }
            map_temp = null;
        }
        map_old = null;
        document = null;

    }



    @SuppressWarnings("unchecked")
    public String getUserInformationDocumentId(String indexDocId,String userName)
    {
        String userDocId="";

        //database.clearDocumentCache();

        Document document = database.getDocument(indexDocId);

        Map<String, Object> map = document.getProperties();


        if (map != null)
        {
            ArrayList<String> arr = (ArrayList<String>) map.get("userNameArray");

            for (int i = arr.size() - 1; i >= 0; i--)
            {

                if ( arr.get(i).equals(userName))
                {
                    userDocId=((ArrayList<String>) map.get("userDocIdArray")).get(i);

                    break;
                }

            }

            arr=null;
        }

        document = null;
        map = null;

        return userDocId;

    }


    @SuppressWarnings("unchecked")
    public boolean checkUserDocExists(String docId,String userName)
    {

        boolean exists=false;

        //database.clearDocumentCache();

        Document document = database.getDocument(docId);

        Map<String, Object> map = document.getProperties();


        if (map != null)
        {
            ArrayList<String> arr = (ArrayList<String>) map.get("userNameArray");

            for (int i = arr.size() - 1; i >= 0; i--)
            {

                if ( arr.get(i).equals(userName))
                {
                    exists=true;
                    break;
                }

            }




            arr=null;}

        document = null;
        map = null;


        return exists;
    }





    public String createUserInformationDocument(Map<String,Object> map1) {

        Map<String, Object> map = new HashMap<>();

        //database.clearDocumentCache();



        Document document = database.createDocument();

        map.put("userId",map1.get("userId"));
        map.put("userName",map1.get("userName"));

        map.put("chatDocument",createChatDocument());



        map.put("unsentMessagesDocument",createUnsentMessagesDocument());






        try {


            document.putProperties(map);

        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error putting", e);
        }


        map=null;

        return document.getId();
    }





    public ArrayList<String>  getUserDocIds(String docId)

    {


        //database.clearDocumentCache();

        Document document = database.getDocument(docId);


        Log.d("log34", docId);


        Map<String, Object> map = document.getProperties();


        ArrayList<String> arr = new ArrayList<>();

        if (map != null) {


            arr.add((String)map.get("chatDocument"));


            arr.add((String)map.get("unsentMessagesDocument"));


        }

        return arr;
    }





    public Map<String, Object>  getUserInfo(String docId)

    {


        //database.clearDocumentCache();
        Document document = database.getDocument(docId);

        Map<String, Object> map = document.getProperties();



        Map<String, Object> userInfo=new HashMap<>();


        if (map != null) {




            userInfo.put("userId",map.get("userId"));
            userInfo.put("userName", map.get("userName"));

            userInfo.put("userPushToken", map.get("userPushToken"));



        }

        return userInfo;
    }






    public String    createUnsentMessagesDocument()
    {






        //database.clearDocumentCache();

        Document document = database.createDocument();
        Map<String, Object> map=new HashMap<>();

        ArrayList<JSONObject> arr = new ArrayList<>();

        map.put("unsentMessageArray", arr);
        try {
            document.putProperties(map);

        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error putting", e);
        }

        map=null;
        // database.close();
        return   document.getId();



    }







    public String  createChatDocument(){


        //database.clearDocumentCache();

        Document document = database.createDocument();
        Map<String, Object> map=new HashMap<>();

        ArrayList<String> arr1 = new ArrayList<>();
        ArrayList<String> arr2 = new ArrayList<>();

        map.put("receiverUidArray", arr1);
        map.put("receiverDocIdArray", arr2);

        try {
            document.putProperties(map);

        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error putting", e);
        }

        map=null;

        arr1=null;arr2=null;

        return   document.getId();
    }









    /****************************************************chatDatabaseStructure****************************************************/





    @SuppressWarnings("unchecked")
    public String createDocumentForChat(String timeInGmt,String receiverUid,String receiverName,String imageUrl,String user_fb_id,String status)
    {
        //database.clearDocumentCache();

        Document document = database.createDocument();
        String documentId = document.getId();
        Map<String ,Object> map=new HashMap<>();
        ArrayList<Map<String, Object>> arr = new ArrayList<>();
        map.put("messsageArray", arr);
        map.put("hasNewMessage", false);
        map.put("newMessage", "");
        map.put("newMessageTime", timeInGmt);
        map.put( "newMessageDate",timeInGmt);
        map.put("newMessageCount","0");
        /**
         *
         * last message date is required to show elements in socket_chatlist in sorted order
         *
         * */
        map.put("lastMessageDate",timeInGmt);
        map.put("receiver_uid_array", new ArrayList<String>());
        map.put("receiver_docid_array",new ArrayList<String>() );
        /**
         * Datum specific
         * */
        map.put("status",status);
        map.put("receiverImageUrl",imageUrl);
        map.put("receiverName",receiverName);
        map.put("selfDocId",documentId);
        map.put("selfUid",receiverUid);
        map.put("userFbid",user_fb_id);
        map.put("firstDate","Mon 07/Mar/2016");
        map.put("lastDate", "Mon 07/Mar/2016");
        try
        {
            document.putProperties(map);

        } catch (CouchbaseLiteException e)
        {
            Log.e(TAG, "Error putting", e);
        }

        map = null;
        arr=null;
        document = null;
        return documentId;

    }


    /**
     * <h2>updateReceiverNameAndReceiverLogoUrl</h2>
     * <p>
     *
     *     Updating user details in the couch db .
     * </p>
     * @param docId contains the user doc id.
     * @param receiverName user name to be update.
     * @param receiverImageUrl receiver profile picture .
     * @param user_status user current status.*/
    public void   updateReceiverNameAndReceiverLogoUrl(String docId,String receiverName,String receiverImageUrl,String user_status,String user_fb_id)
    {

        //database.clearDocumentCache();
        Document document = database.getDocument(docId);
        Map<String, Object> mapOld = document.getProperties();
        Map<String, Object> mapTemp = new HashMap<>();
        mapTemp.putAll(mapOld);
        mapTemp.put("receiverName", receiverName);
        mapTemp.put("receiverImageUrl", receiverImageUrl);
        mapTemp.put("status",user_status);
        mapTemp.put("userFbid",user_fb_id);
        try
        {
            document.putProperties(mapTemp);

        } catch (CouchbaseLiteException e)
        {
            Log.e(TAG, "Error putting", e);
        }
    }


    @SuppressWarnings("unchecked")
    public void addChatDocumentDetails(String receiverUid,String receiverDocId,String chatDocId)
    {

        //database.clearDocumentCache();

        Document document = database.getDocument(chatDocId);
        Map<String, Object> map = document.getProperties();
        ArrayList<String> receiverUidArray=(ArrayList<String>)map.get("receiverUidArray");
        ArrayList<String> receiverDocIdArray=(ArrayList<String>)map.get("receiverDocIdArray");
        receiverUidArray.add(receiverUid);
        receiverDocIdArray.add(receiverDocId);
        Map<String, Object> map_temp=new HashMap<>();
        map_temp.putAll(map);
        map_temp.put("receiverUidArray",receiverUidArray);
        map_temp.put("receiverDocIdArray",receiverDocIdArray);
        try
        {
            document.putProperties(map_temp);
        } catch (CouchbaseLiteException e)
        {
            Log.e(TAG, "Error putting", e);
        }

    }


    @SuppressWarnings("unchecked")
    public Map<String,Object> getAllChatDetails(String chatDocId)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(chatDocId);
        Map<String,Object> map=document.getProperties();
        return map;
    }


    @SuppressWarnings("unchecked")
    public Map<String,Object> getParticularChatInfo(String chatDocId)
    {

        //database.clearDocumentCache();

        Document document = database.getDocument(chatDocId);

        Map<String,Object> map=document.getProperties();

        return map;


    }


    @SuppressWarnings("unchecked")
    //to retrieve list of all messages from database


    public ArrayList<Map<String, Object>> retrieveAllMessages(String docId)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(docId);
        ArrayList<Map<String, Object>> arr = new ArrayList<>();
        try{

            Map<String, Object> map = document.getProperties();

            if (map != null) {

                arr = (ArrayList<Map<String, Object>>) map.get("messsageArray");

                map = null;
            }}catch(NullPointerException e){e.printStackTrace();}

        document = null;
        return arr;
    }


    /**
     *
     * status-0 not sent
     * status-1 sent
     * status-2 delivered
     * status-3 read
     *
     * */
    @SuppressWarnings("unchecked")
    public boolean updateMessageStatus(String docId, String messageId, int status) {

        boolean flag = false;

        //database.clearDocumentCache();

        Document document = database.getDocument(docId);

        try {
            Map<String, Object> map = document.getProperties();

            if (map == null) {
                return false;
            } else {

                Map<String, Object> mapMessages;

                Map<String, Object> mapTemp = new HashMap<>();


                mapTemp.putAll(map);

                ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) map.get("messsageArray");

                for (int i = arr.size() - 1; i >= 0; i--) {

                    if (flag) {
                        break;


                    }

                    mapMessages = (arr.get(i));
                    String Id = (String) mapMessages.get("id");



                    if (Id == null) {
                        return false;
                    }


                    if (Id.equals(messageId)) {

                        switch (status) {

                            case 0:
                                mapMessages.put("deliveryStatus", "1");
                                flag = true;
                                arr.set(i, mapMessages);
                                mapTemp.put("messsageArray", arr);


                                break;
                            case 1:
                                mapMessages.put("deliveryStatus","2");

                                flag = true;
                                arr.set(i, mapMessages);
                                mapTemp.put("messsageArray", arr);


                        }

                        try {
                            document.putProperties(mapTemp);

                        } catch (CouchbaseLiteException e) {
                            Log.e(TAG, "Error putting", e);
                        }

                    }

                }


                document = null;
                map = null;
                mapTemp = null;
                mapMessages = null;
                return true;


            }

        }catch(NullPointerException e){e.printStackTrace();}


        return false;
    }


    public void upDateMessageDetails(String docId,String messageId,String file_location,boolean isDownloaded)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(docId);

        Map<String, Object> map = document.getProperties();
        Map<String, Object> mapMessages;

        Map<String, Object> mapTemp = new HashMap<>();
        mapTemp.putAll(map);

        if(map!=null)
        {
            ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) map.get("messsageArray");

            for (int i = arr.size() - 1; i >= 0; i--)
            {

                mapMessages = (arr.get(i));

                if((boolean)mapMessages.get("isDate"))
                {continue;}

                String Id = (String) mapMessages.get("id");

                if (Id.equals(messageId))
                {
                    mapMessages.put("payloadurl",file_location);
                    mapMessages.put("isDownloaded",isDownloaded);
                    arr.set(i, mapMessages);
                    mapTemp.put("messsageArray", arr);
                    try {
                        document.putProperties(mapTemp);


                    } catch (CouchbaseLiteException e)
                    {
                        Log.e(TAG, "Error putting", e);
                    }
                    break;
                }
            }
        }
        document = null;
        map = null;
        mapTemp = null;
        mapMessages = null;
    }


    public void upSnapchatImageDeleted(String docId,String messageId,String deleted)
    {

        //database.clearDocumentCache();
        Document document = database.getDocument(docId);

        Map<String, Object> map = document.getProperties();
        Map<String, Object> mapMessages;

        Map<String, Object> mapTemp = new HashMap<>();
        mapTemp.putAll(map);

        if(map!=null)
        {
            ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) map.get("messsageArray");

            for (int i = arr.size() - 1; i >= 0; i--)
            {

                mapMessages = (arr.get(i));

                if((boolean)mapMessages.get("isDate"))
                {continue;}

                String Id = (String) mapMessages.get("id");

                if (Id.equals(messageId))
                {
                    mapMessages.put("isDeleted",deleted);
                    arr.set(i, mapMessages);
                    mapTemp.put("messsageArray", arr);
                    try
                    {
                        document.putProperties(mapTemp);
                    } catch (CouchbaseLiteException e)
                    {
                        Log.e(TAG, "Error putting", e);
                    }
                    break;
                }
            }
        }
        document = null;
        map = null;
        mapTemp = null;
        mapMessages = null;
    }

    @SuppressWarnings("unchecked")
    //for updating ts of message when a message has been resend

    public void updateMessageTs(String docId, String messageId, String ts)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(docId);

        Map<String, Object> map = document.getProperties();
        Map<String, Object> mapMessages;

        Map<String, Object> mapTemp = new HashMap<>();
        mapTemp.putAll(map);

        if(map!=null)
        {

            ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) map.get("messsageArray");

            for (int i = arr.size() - 1; i >= 0; i--)
            {

                mapMessages = (arr.get(i));


                if((boolean)mapMessages.get("isDate"))
                {continue;}

                String Id = (String) mapMessages.get("id");

                if (Id.equals(messageId))
                {
                    mapMessages.put("Ts", ts);
                    arr.set(i, mapMessages);
                    mapTemp.put("messsageArray", arr);
                    try {
                        document.putProperties(mapTemp);


                    } catch (CouchbaseLiteException e)
                    {
                        Log.e(TAG, "Error putting", e);
                    }
                    break;
                }
            }

        }

        document = null;
        map = null;
        mapTemp = null;
        mapMessages = null;

    }



    @SuppressWarnings("unchecked")
    //adding new message to the database
    public void addNewChatMessage(String documentId, Map<String, Object> map,String dateInGmt)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(documentId);
        Map<String, Object> mapOld = document.getProperties();

        if (mapOld != null)
        {
            ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) mapOld.get("messsageArray");
            arr.add(map);
            Map<String, Object> mapTemp = new HashMap<>();
            mapTemp.putAll(mapOld);
            mapTemp.put("lastMessageDate",dateInGmt);
            mapTemp.put("messsageArray", arr);
            try
            {
                document.putProperties(mapTemp);


            } catch (CouchbaseLiteException e)
            {
                Log.e(TAG, "Error putting", e);
            }
            mapTemp = null;
        }
        mapOld = null;
        document = null;

    }

    /**
     * <h2>deleteChat</h2>
     * <p>Delete complete chat from the chat history</p>
     * @param docId docid to be delete locally.
     * */
    public void deleteChat(String docId)
    {

        //database.clearDocumentCache();

        Document doc = database.getDocument(docId);

        Map<String, Object> mapTemp = new HashMap<>();


        Map<String, Object> mapOld = doc.getProperties();

        if (mapOld != null) {
            ArrayList<Map<String, Object>> arr = new ArrayList<>();
            mapTemp.putAll(mapOld);
            mapTemp.put("messsageArray", arr);
            mapTemp.put("firstDate","Mon 07/Mar/2016");
            mapTemp.put("lastDate", "Mon 07/Mar/2016");
            arr=null;
        }
        try {
            doc.putProperties(mapTemp);

        } catch (CouchbaseLiteException e)
        {
            Log.e(TAG, "Error putting", e);
        }
    }






    public void updateChatListOnViewingMessage(String docId)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(docId);
        Map<String, Object> map = document.getProperties();
        if(map==null)
        {
            return;
        }
        Map<String,Object> mapTemp=new HashMap<>();
        mapTemp.putAll(map);
        mapTemp.put("newMessageCount","0");
        mapTemp.put("hasNewMessage",false);
        try
        {
            document.putProperties(mapTemp);

        }catch(CouchbaseLiteException e)
        {
            e.printStackTrace();
        }

    }










    /**
     *
     * in each of the document we are storing the last message date and last message time so that if flag hasNewMessage is set
     * then we can show that and also it will help us to sort chats based on the most recent
     *
     * */

    public void updateChatListForNewMessage(String docId, String lastmessage, boolean hasNewMessage,String lastMessageDate,String lastmessagetime)


    {



        //database.clearDocumentCache();
        Document document = database.getDocument(docId);

        Map<String, Object> map = document.getProperties();




        if(map==null)

        {
            return;}
        String newMessageCount=(String)map.get("newMessageCount");

        int count=Integer.parseInt(newMessageCount);

        count+=1;

        Map<String, Object> mapTemp = new HashMap<>();
        mapTemp.putAll(map);
        mapTemp.put("hasNewMessage", hasNewMessage);
        if (hasNewMessage) {
            mapTemp.put("newMessage", lastmessage);
            mapTemp.put("newMessageTime", lastmessagetime);

            mapTemp.put("newMessageDate", lastMessageDate);


            mapTemp.put("newMessageCount",String.valueOf(count));
        }

        mapTemp.put("lastMessageDate", lastMessageDate);
        try {
            document.putProperties(mapTemp);

        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error putting", e);
        }


        map = null;
        document = null;


    }




    public String getNewMessageCount(String docId) {

        //database.clearDocumentCache();

        Document document = database.getDocument(docId);
        Map<String, Object> map = document.getProperties();
        document = null;


        return (String) map.get("newMessageCount");
    }





    @SuppressWarnings("unchecked")

    public Map<String,Object> getLastMessageDetails(String docId) {


        //database.clearDocumentCache();

        Document document = database.getDocument(docId);
        Map<String, Object> map = document.getProperties();

        Map<String, Object> result = new HashMap<>();
        if (map != null) {
            ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) map.get("messsageArray");


            if(arr.size()==0){
                return result;}



            Map<String, Object> lastMessage = arr.get(arr.size() - 1);

            String ts = (String) lastMessage.get("Ts");

            String type = (String) lastMessage.get("messageType");



            result.put("lastMessageDate", ts);
            result.put("lastMessageTime", ts);


            if (type.equals("0")) {


                result.put("lastMessage", lastMessage.get("message"));
            } else if (type.equals("1")) {
                result.put("lastMessage", "Image");
            } else if (type.equals("2")) {
                result.put("lastMessage", "Video");

            } else if (type.equals("3")) {
                result.put("lastMessage", "New Gif");
            } else if (type.equals("4")) {
                result.put("lastMessage", "Contact");
            } else {

                result.put("lastMessage", "Audio");
            }

        }
        document = null;

        return result;
    }




    @SuppressWarnings("unchecked")
    public boolean  drawBlueTickUptoThisMessage(String document_id, String id)

    {


        //database.clearDocumentCache();

        Document document = database.getDocument(document_id);

        try {
            Map<String, Object> map = document.getProperties();


            if (map == null) {


                return false;
            } else {

                Map<String, Object> mapMessages;

                Map<String, Object> mapTemp = new HashMap<>();


                mapTemp.putAll(map);


                ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) map.get("messsageArray");

                boolean flag = false;

                for (int i = arr.size() - 1; i >= 0; i--) {
                    mapMessages = (arr.get(i));
                    String Id = (String) mapMessages.get("id");

                    if ((boolean) mapMessages.get("isDate")) {
                        continue;
                    } else if (Id.equals(id)) {
                        flag = true;

                        for (int j = i; j >= 0; j--) {


                            mapMessages = arr.get(j);


                            if (mapMessages != null) {


                                Log.d("log77",mapMessages.toString());


                                if (!((boolean) mapMessages.get("isDate")) && ((boolean) mapMessages.get("isSelf")) &&   !(mapMessages.get("deliveryStatus").equals("0"))) {
                                    if (mapMessages.get("deliveryStatus").equals("3")) break;
                                    else {
                                        mapMessages.put("deliveryStatus", "3");

                                        arr.set(j, mapMessages);
                                    }
                                }

                            }
                        }
                        mapTemp.put("messsageArray", arr);


                    }


                    if (flag) {
                        try {
                            document.putProperties(mapTemp);

                        } catch (CouchbaseLiteException e) {
                            Log.e(TAG, "Error putting", e);
                        }
                        break;
                    }

                }


                document = null;
                map = null;
                mapTemp = null;
                mapMessages = null;


                return true;

            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        return false;



    }






    @SuppressWarnings("unchecked")
    public String   getDocumentIdOfReceiverChatlistScreen(String documentId,String receiverUid)

    {String docId=null;



        //database.clearDocumentCache();
        Document document = database.getDocument(documentId);

        Map<String, Object> map = document.getProperties();


        ArrayList<String> arrReceiverUid = (ArrayList<String>) map.get("receiverUidArray");
        ArrayList<String> arrReceiverDocid  = (ArrayList<String>) map.get("receiverDocIdArray");



        for(int i=0;i<arrReceiverUid.size();i++)

        {
            if(arrReceiverUid.get(i).equals(receiverUid))
            {

                docId=arrReceiverDocid.get(i);
            }


        }


        return  docId;}











    /************************************************/

    public String getLastDate(String documentId) {


        //database.clearDocumentCache();

        String lastdate="";

        Document document = database.getDocument(documentId);
        Map<String, Object> map;
        try{
            map = document.getProperties();
            lastdate=(String) map.get("lastDate");
        }


        catch(NullPointerException e){e.printStackTrace();}


        document = null;

        return lastdate;
    }





    public void updateLastDate(String documentId, String lastDate) {


        //database.clearDocumentCache();
        Document document = database.getDocument(documentId);
        Map<String, Object> map_old = document.getProperties();




        Map<String, Object> map_temp = new HashMap<>();
        map_temp.putAll(map_old);
        map_temp.put("lastDate", lastDate);

        try {
            document.putProperties(map_temp);

        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error putting", e);
        }
        map_old =null; map_temp= null;
        document = null;

    }


    public String getFirstDate(String documentId) {



        //database.clearDocumentCache();

        String firstdate="";

        Document document = database.getDocument(documentId);
        Map<String, Object> map;
        try{
            map = document.getProperties();
            firstdate=(String) map.get("firstDate");
        }


        catch(NullPointerException e){e.printStackTrace();}


        document = null;

        return firstdate;
    }





    public void updateFirstDate(String documentId, String firstDate) {


        //database.clearDocumentCache();
        Document document = database.getDocument(documentId);
        Map<String, Object> map_old = document.getProperties();




        Map<String, Object> map_temp = new HashMap<>();
        map_temp.putAll(map_old);
        map_temp.put("firstDate", firstDate);

        try {
            document.putProperties(map_temp);

        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error putting", e);
        }
        map_old =null; map_temp= null;
        document = null;


    }





    @SuppressWarnings("unchecked")
    public String  getDocumentIdOfReceiver(String document_id,String receiver_uid)

    {
        String doc_id=null;



        //database.clearDocumentCache();
        Document document = database.getDocument(document_id);

        Map<String, Object> map = document.getProperties();


        ArrayList<String> arr_receiver_uid = (ArrayList<String>) map.get("receiver_uid_array");
        ArrayList<String> arr_receiver_docid  = (ArrayList<String>) map.get("receiver_docid_array");



        for(int i=0;i<arr_receiver_uid.size();i++)

        {
            if(arr_receiver_uid.get(i).equals(receiver_uid))
            {

                doc_id=arr_receiver_docid.get(i);
            }


        }



        return  doc_id;
    }



    @SuppressWarnings("unchecked")
    public void  setDocumentIdOfReceiver(String document_id,String receiver_doc_id,String receiver_uid)

    {


        //database.clearDocumentCache();

        Document document = database.getDocument(document_id);

        Map<String, Object> map = document.getProperties();


        ArrayList<String> arr_receiver_uid = (ArrayList<String>) map.get("receiver_uid_array");
        ArrayList<String> arr_receiver_docid  = (ArrayList<String>) map.get("receiver_docid_array");

        boolean flag=false;

        for(int i=0;i<arr_receiver_uid.size();i++)

        {
            if(arr_receiver_uid.get(i).equals(receiver_uid))
            {
                arr_receiver_docid.remove(i);
                arr_receiver_docid.add(i, receiver_doc_id);
                flag=true;
                break;

            }}

        if(!flag){

            arr_receiver_uid.add(      receiver_uid  );
            arr_receiver_docid.add(receiver_doc_id);
        }
        Map<String, Object> map_temp = new HashMap<>();
        map_temp.putAll(map);


        map_temp.put("receiver_uid_array",arr_receiver_uid);

        map_temp.put("receiver_docid_array", arr_receiver_docid);


        try {
            document.putProperties(map_temp);

        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error putting", e);
        }


        arr_receiver_uid = null;
        arr_receiver_docid = null;
        document_id = null;
        map = null;
        map_temp = null;
        receiver_doc_id=null;
        document = null;


    }

    @SuppressWarnings("unchecked")

    public int  getPositionOfCurrentMessageDate(String document_id,String present_date)
    {
        int lastDatePosition=-1,presentDatePosition=-1;boolean found=false;



        //database.clearDocumentCache();
        Document document = database.getDocument(document_id);


        Map<String, Object> map_old = document.getProperties();

        Map<String, Object> map_messages;

        ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) map_old.get("messsageArray");

        for (int i = arr.size() - 1; i >= 0; i--) {
            map_messages = (arr.get(i));
            if ((boolean) map_messages.get("isDate")) {
                String date = (String) map_messages.get("date");
                // date=date.substring(0,9)   ;


                try{

                    SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MMM/yyyy");
                    Date date2 = sdf.parse(present_date);
                    Date date1 = sdf.parse(date);

                    if(date1.after(date2)){
                        lastDatePosition=i;}}catch(ParseException ex){
                    ex.printStackTrace();
                }

                if (date.equals(present_date)) {

                    presentDatePosition= i;found=true;break;
                }
            }}

        if(found){
            map_messages=null;map_old=null;document=null;



            return presentDatePosition;}
        else {


            Map<String,Object>  map=new HashMap<>();
            map.put("isDate", true);

            map.put("date", present_date);





            appendDoc(document_id,map , (lastDatePosition-1)    );



            map_messages=null;map_old=null;document=null;map=null;

/**
 *
 * this case wont be encountered now,but just put it for future reference
 * */
            if(lastDatePosition==0){return 0;}

            else{
                return (lastDatePosition-1);}
        }




    }


    @SuppressWarnings("unchecked")
    public void appendDoc(String documentId, Map<String, Object> map,int position)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(documentId);

        Map<String, Object> map_old = document.getProperties();

        if (map_old != null)
        {
            ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) map_old.get("messsageArray");

            if(position<0){arr.add(0,map);}
            else{
                arr.add(position,map);}
            Map<String, Object> map_temp = new HashMap<>();
            map_temp.putAll(map_old);


            map_temp.put("messsageArray", arr);
            try
            {
                document.putProperties(map_temp);
            } catch (CouchbaseLiteException e)
            {
                Log.e(TAG, "Error putting", e);
            }
            map_old = null;
            map_temp = null;
            document = null;
        }
    }

    /**
     * Storing the unsent message.*/
    @SuppressWarnings("unchecked")
    public void addUnsentMessage(String docId,Map<String,Object>  map)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(docId);
        Map<String, Object> mapOld = document.getProperties();
        Map<String, Object> map_temp = new HashMap<>();
        map_temp.putAll(mapOld);

        if (mapOld != null)
        {
            ArrayList<Map<String,Object>> arr = (ArrayList<Map<String,Object>>) mapOld.get("unsentMessageArray");

            arr.add(map);
            map_temp.put("unsentMessageArray", arr);

            try {
                document.putProperties(map_temp);

            } catch (CouchbaseLiteException e) {
                Log.e(TAG, "Error putting", e);
            }
        }

    }
    /**
     * Removing the unsent message from the db as that message sent.*/
    @SuppressWarnings("unchecked")
    public void removeUnsentMessage(String docId,String messageId)
    {
        boolean removed=false;
        //database.clearDocumentCache();
        Document document = database.getDocument(docId);
        Map<String, Object> mapOld = document.getProperties();
        Map<String, Object> mapTemp = new HashMap<>();
        mapTemp.putAll(mapOld);

        if (mapOld != null)
        {
            ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) mapOld.get("unsentMessageArray");
            if(arr.size()>0)
            {
                Map<String, Object> map;
                for (int i=0;i<arr.size();i++)
                {
                    map=arr.get(i);
                    String id = (String)map.get("id");

                    if(id.equals(messageId))
                    {
                        arr.remove(i);
                        removed=true;
                        break;
                    }
                }

                if(removed)
                {
                    mapTemp.put("unsentMessageArray",arr);
                    try
                    {
                        document.putProperties(mapTemp);

                    } catch (CouchbaseLiteException e) {
                        Log.e(TAG, "Error putting", e);
                    }
                }
            }
        }
    }




    @SuppressWarnings("unchecked")

    public ArrayList<Map<String, Object>> getUnsentMessages(String docId)
    {

        //database.clearDocumentCache();
        Document document = database.getDocument(docId);

        Map<String, Object> mapOld = document.getProperties();

        document=null;
        return (ArrayList<Map<String, Object>>)mapOld.get("unsentMessageArray");

    }

/**
 * <h2>deleteParticularChatMessage</h2>
 * <P>
 *     Deleting a particular chat message of the user.
 * </P>
 * @param docId the doc_id from where message is to be delete.
 * @param messageId the message id to be delete.
 * */
    @SuppressWarnings("unchecked")
    public void deleteParticularChatMessage(String docId,String messageId)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(docId);

        boolean deleted=false;

        Map<String, Object> mapOld = document.getProperties();
        Map<String, Object> mapTemp = new HashMap<>();
        mapTemp.putAll(mapOld);

        Map<String, Object> mapMessages;
        if (mapOld != null)
        {
            ArrayList<Map<String, Object>> arr = (ArrayList<Map<String, Object>>) mapOld.get("messsageArray");

            for (int i = arr.size() - 1; i >= 0; i--)
            {
                mapMessages = (arr.get(i));

                if((boolean)mapMessages.get("isDate")){continue;}

                String Id = (String) mapMessages.get("id");

                if (Id.equals(messageId))
                {
                    arr.remove(i);
                    deleted=true;
                    break;
                }
            }


            if(deleted)
            {
                mapTemp.put("messsageArray",arr);

                try
                {
                    document.putProperties(mapTemp);

                } catch (CouchbaseLiteException e)
                {
                    Log.e(TAG, "Error putting", e);
                }

            }
        }

    }


    public String getReceiverImageUrl(String docId)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(docId);
        return (String)document.getProperties().get("receiverImageUrl");
    }

    public String getReceiverFB_id(String docId)
    {
        //database.clearDocumentCache();
        Document document = database.getDocument(docId);
        return (String)document.getProperties().get("userFbid");
    }




}
