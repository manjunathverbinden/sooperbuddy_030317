package com.three_embed.datum.chat_lib;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
/**
 * Created by moda on 02/04/16.
 */
public class ViewHolderMessageReceived extends RecyclerView.ViewHolder  {


    TextView senderName,message,time;

    public ViewHolderMessageReceived(View view)
    {
        super(view);
        senderName=(TextView) view.findViewById(R.id.lblMsgFrom);
        message=(TextView) view.findViewById(R.id.txtMsg);
        time=(TextView) view.findViewById(R.id.ts);
    }
}
