package com.three_embed.datum.chat_lib;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.three_embed.datum.chat_lib.R;
import android.widget.VideoView;

/**
 * Created by moda on 15/04/16.
 */
public class ViewHolderVideoMedia extends RecyclerView.ViewHolder {


    public VideoView video;


    public ViewHolderVideoMedia(View view) {
        super(view);
        video = (VideoView) view.findViewById(R.id.imageView28);
    }
}
