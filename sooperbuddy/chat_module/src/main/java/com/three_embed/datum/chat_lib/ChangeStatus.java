package com.three_embed.datum.chat_lib;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * @since 05/04/16.
 * @author 3Embed.
 */
public class ChangeStatus extends Service
{
        @Override
        public IBinder onBind(Intent intent)
        {
            return null;
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId)
        {
            return START_NOT_STICKY;
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();

        }

        public void onTaskRemoved(Intent rootIntent)
        {
            JSONObject obj;
             obj=new JSONObject();
            try
            {
                obj.put("pushtoken", AppController.getInstance().getPushToken());
                obj.put("from", AppController.getInstance().getUserId());
                obj.put("status", "0");

                obj.put("device", "android");

            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            AppController.getInstance().emit(ServerEvents.getHeartbeat.value, obj);
            stopSelf();
        }
    }

