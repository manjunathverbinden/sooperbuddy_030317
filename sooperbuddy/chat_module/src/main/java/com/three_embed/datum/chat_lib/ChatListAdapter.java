package com.three_embed.datum.chat_lib;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by moda on 15/04/16.
 */

public class ChatListAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private ArrayList<ChatList_Chat_Item> mListData = new ArrayList<>();
    private Context mContext;

    public ChatListAdapter(Context mContext, ArrayList<ChatList_Chat_Item> mListData) {
        this.mListData = mListData;
        this.mContext = mContext;
    }


    @Override
    public int getItemCount() {
        return this.mListData.size();
    }


    @Override
    public int getItemViewType(int position) {
        return 1;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());



        View v = inflater.inflate(R.layout.socket_lp_f3_chat, viewGroup, false);
        viewHolder = new ViewHolderChat(v);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {


        ViewHolderChat vh2 = (ViewHolderChat) viewHolder;
        configureViewHolderChat(vh2, position);

    }


    private void configureViewHolderChat(ViewHolderChat vh, int position) {
        final   ChatList_Chat_Item chat =  mListData.get(position);
        if (chat != null) {


            vh.storeName.setText(chat.getReceiverName());


            vh.newMessage.setText(chat.getNewMessage());


            try {

                String formatedDate = Utilities.formatDate(Utilities.tsFromGmt(chat.getNewMessageTime()));

                if ((chat.getNewMessageTime().substring(0, 8)).equals((Utilities.tsInGmt().substring(0, 8)))) {


                    vh.newMessageDate.setText("Today");
                } else if ((Integer.parseInt((Utilities.tsInGmt().substring(0, 8))) - Integer.parseInt((chat.getNewMessageTime().substring(0, 8)))) == 1) {


                    vh.newMessageDate.setText("Yesterday");

                } else {
                    vh.newMessageDate.setText(formatedDate.substring(9, 24));

                }


                vh.newMessageTime.setText(formatedDate.substring(0, 9));


                if (chat.hasNewMessage()) {


                    vh.newMessageCount.setText(chat.getNewMessageCount());


                }

/**
 *
 * instead of setting visibility to gone we have to show lastmessage
 *
 * */
//            else{
//
//
//                vh.newMessageCount.setVisibility(View.GONE);
//
//
//
//
//            }


            }catch(NullPointerException e)
            {e.printStackTrace();
            }
            if(chat.getReceiverImageUrl()!=null)
            {
                final ViewHolderChat v = vh;
                Picasso.with(mContext).load(chat.getReceiverImageUrl()).transform(new Image_circule_trasform()).into( v.storeImage);

            }



            vh.storeImage.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                  /*  Intent i = new Intent(mContext, AllProductsFromStore.class);
                    i.putExtra("store", chat.getReceiverName());
                    mContext.startActivity(i);*/
                }
            });



        }
    }

}