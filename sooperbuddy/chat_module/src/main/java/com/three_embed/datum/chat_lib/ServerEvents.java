package com.three_embed.datum.chat_lib;

import io.socket.client.Socket;
/**
 * Created by moda on 02/04/16.
 */

public enum ServerEvents {


    getHeartbeat("Heartbeat"),
    getMessageResponseFromServer("MessageRes"),
    sendMessageToServer("Message"),
    chatHistory("GetMessages"),
    doubleTick("MessageStatusUpdate"),
    messageAck("MessageAck"),
    acknowledgementHistory("GetMessageAcks"),
    getCurrentTimeStatus("getCurrentTimeStatus"),
    changeOnlineStatus("ChangeOnlineStatus"),
    changeSt("changeSt"),
    typing("typing"),


    connect(Socket.EVENT_CONNECT),
    disconnect(Socket.EVENT_DISCONNECT),

    connectError( Socket.EVENT_CONNECT_ERROR),
    timeout(Socket.EVENT_CONNECT_TIMEOUT)



    ;






    public String value;



    ServerEvents(String value)
    {
        this.value = value;
    }


}