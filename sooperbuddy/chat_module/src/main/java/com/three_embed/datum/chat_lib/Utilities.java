package com.three_embed.datum.chat_lib;


import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @since  25/3/16.
 */
public class Utilities {


    public static String tsInGmt()
    {
        Date localTime = new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z");
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formater.format(localTime);
    }


    //converting time to localtime zone from gmt time

    public static String tsFromGmt(String tsingmt)
    {
        Date d;String s=null;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z");
        try
        {
            d=formater.parse(tsingmt);
            TimeZone tz=TimeZone.getDefault();
            formater.setTimeZone(tz);
            s=     formater.format(d);
        }
        catch( ParseException  e){
            e.printStackTrace();}


        return s;
    }


    public String gmtToEpoch(String tsingmt)
    {
        Date d;
        long epoch=0;
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z");

        try{
            d=formater.parse(tsingmt);

            epoch = d.getTime();

        }
        catch( ParseException  e){
            e.printStackTrace();}


        return String.valueOf(epoch);
    }


    public static long Daybetween(String date1,String date2,String pattern)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
        Date Date1 = null,Date2 = null;
        try{
            Date1 = sdf.parse(date1);
            Date2 = sdf.parse(date2);
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return (Date2.getTime() - Date1.getTime())/(24*60*60*1000);
    }





    @SuppressLint("SimpleDateFormat")
    public static String formatDate(String ts){

        String s=null;Date d=null;


        @SuppressLint("SimpleDateFormat") SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z");

        try{
            d=formater.parse(ts);

            formater = new SimpleDateFormat("HH:mm:ss EEE dd/MMM/yyyy z");
            s=     formater.format(d);
        }
        catch( ParseException e){
            e.printStackTrace();}


        return s;

    }


    public static String changeStatusDateFromGMTToLocal(String ts) {


        String s = null;
        Date d = null;


        @SuppressLint("SimpleDateFormat") SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");

        try{
            d=formater.parse(ts);

            TimeZone tz=TimeZone.getDefault();
            formater.setTimeZone(tz);

            s=     formater.format(d);
        }
        catch( ParseException e){
            e.printStackTrace();}


        return s;
    }



    public static String epochtoGmt (String tsingmt){


        Date d=null;String s=null;long epoch=0;


        @SuppressLint("SimpleDateFormat") SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z");
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));
        epoch = Long.parseLong( tsingmt );

        d = new Date( epoch);
        s=     formater.format(d);



        return s;
    }


}
