package com.three_embed.datum.chat_lib;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;
/**
 * <h2>AndroidVideoCapture</h2>
 * <P>
 *
 * </P>*/
public class AndroidVideoCapture extends AppCompatActivity implements View.OnClickListener
{

    private android.hardware.Camera myCamera;
    private MyCameraSurfaceView myCameraSurfaceView;
    private MediaRecorder mediaRecorder;
    ImageButton myButton;
    boolean recording;
    CountDownTimer t;
    public static int orientation;
    FrameLayout myCameraPreview;
    TextView timer;
    private String video_Path="";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if(getSupportActionBar()!=null)
        {
            getSupportActionBar().hide();
        }


        recording = false;
        setContentView(R.layout.activity_android_video_capture);
        //Get Camera for preview
        myCamera = getCameraInstance();
        if (myCamera == null)
        {
            Toast.makeText(AndroidVideoCapture.this,"Fail to get Camera", Toast.LENGTH_LONG).show();
            AndroidVideoCapture.this.finish();

        }

        /**
         * Creating the video file path*/

        myCameraSurfaceView = new MyCameraSurfaceView(this,myCamera);
        myCameraPreview = (FrameLayout) findViewById(R.id.videoview);
        myCameraPreview.addView(myCameraSurfaceView);

        myButton = (ImageButton) findViewById(R.id.mybutton);
        myButton.setOnClickListener(this);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        timer = (TextView) findViewById(R.id.tvTimer);

         /* Count down timer of 10 minutes */
        t = new CountDownTimer(30000 * 20, 1000)
        {

            @Override
            public void onTick(long millisUntilFinished)
            {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                timer.setText(String.format("%d:%02d", minutes, seconds));
            }

            @Override
            public void onFinish()
            {
                mediaRecorder.stop();  // stop the recording
                releaseMediaRecorder(); // release the MediaRecorder object

                releaseCamera();

                //Exit after saved
                Intent intent = new Intent();
                intent.putExtra("videopath",video_Path);
                setResult(RESULT_OK, intent);
                finish();
            }
        };
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        releaseMediaRecorder();
        releaseCamera();
    }


    private android.hardware.Camera getCameraInstance()
    {
// TODO Auto-generated method stub
        android.hardware.Camera c = null;
        try
        {
            c = android.hardware.Camera.open(); // attempt to get a Camera instance
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return c;
    }

    private boolean prepareMediaRecorder()
    {
        myCamera = getCameraInstance();
        setCameraDisplayOrientation(this, 0, myCamera);
        mediaRecorder = new MediaRecorder();
        myCamera.lock();
        myCamera.unlock();
        mediaRecorder.setPreviewDisplay(myCameraSurfaceView.getHolder().getSurface());
        mediaRecorder.setCamera(myCamera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setOutputFile(getFilePath());
        // No limit. Don't forget to check the space on disk.
        mediaRecorder.setMaxDuration(-1);
        mediaRecorder.setVideoFrameRate(30);
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mediaRecorder.setOrientationHint(AndroidVideoCapture.orientation);

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;

    }

    @Override
    protected void onPause()
    {
        super.onPause();         // release the camera immediately on pause event
    }

    @Override
    protected void onResume()
    {
        super.onResume();

    }


    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            myCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera() {
        if (myCamera != null) {
            myCamera.release();        // release the camera for other applications
            myCamera = null;
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v)
    {
        if(v.getId()==R.id.mybutton)
        {
            if (recording)
            {
                recording=false;
                // stop recording and release camera
                mediaRecorder.stop();  // stop the recording
                releaseMediaRecorder(); // release the MediaRecorder object
                //Exit after saved
                releaseCamera();
                if(t!=null)
                {
                    t.cancel();
                }
                Intent intent = new Intent();
                intent.putExtra("videopath",video_Path);
                setResult(RESULT_OK, intent);
                finish();
            } else
            {
                //Release Camera before MediaRecorder start
                releaseCamera();

                if (!prepareMediaRecorder())
                {
                    Toast.makeText(AndroidVideoCapture.this,"Fail in prepareMediaRecorder()!\n - Ended -",Toast.LENGTH_LONG).show();
                    finish();
                }

                mediaRecorder.start();
                recording = true;
                myButton.setBackgroundResource(R.drawable.rectangle);
                t.start();

            }

        }

    }

    public class MyCameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

        private SurfaceHolder mHolder;
        private android.hardware.Camera mCamera;

        public MyCameraSurfaceView(Context context, android.hardware.Camera camera) {
            super(context);
            mCamera = camera;
            mHolder = getHolder();
            mHolder.addCallback(this);
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int weight,int height)
        {

            if (mHolder.getSurface() == null)
            {
                return;
            }

            try
            {
                mCamera.stopPreview();
            } catch (Exception e)
            {
                e.printStackTrace();
            }

            try {
                setCameraDisplayOrientation(AndroidVideoCapture.this, 0, mCamera);
                mCamera.setPreviewDisplay(mHolder);
                //myCamera.setDisplayOrientation(90);
                mCamera.startPreview();

            } catch (Exception e) {
            }
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            // TODO Auto-generated method stub
            // The Surface has been created, now tell the camera where to draw the preview.
            try
            {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // TODO Auto-generated method stub

        }

    }

    public static void setCameraDisplayOrientation(Activity activity,int cameraId, android.hardware.Camera camera)
    {
        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();

        android.hardware.Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        AndroidVideoCapture.orientation = result;
        camera.setDisplayOrientation(result);
    }

    /**
     * <h2>getFilePath</h2>
     * <P>
     *     creating the file path to store video.
     * </P>*/
    public String getFilePath()
    {
        File folder;

        folder = new File(Environment.getExternalStorageDirectory().getPath() + "/Datum");
        if (!folder.exists() && !folder.isDirectory())
        {
            folder.mkdir();
        }
        long time= System.currentTimeMillis();
        File  file = new File(folder,"chat_video"+time+".mp4");
        video_Path=file.getPath();
        return video_Path;
    }

}