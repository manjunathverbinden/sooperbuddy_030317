package com.three_embed.datum.chat_lib;

/**
 * @since  3/3/16.
 */
public class ChatList_Chat_Item implements  Comparable<ChatList_Chat_Item>
{

    private String ReceiverUid,ProductId,ChatId,DocumentId,newMessage,newMessageCount,newMessageTime,newMessageDate,ReceiverName,ReceiverImageUrl;
    boolean isNewMessage;
    private String receiver_fb_id;
    private String status;
    private String message_Time_stamp;

    public String getMessage_Time_stamp()
    {
        return message_Time_stamp;
    }

    public void setMessage_Time_stamp(String message_Time_stamp) {
        this.message_Time_stamp = message_Time_stamp;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceiver_fb_id()
    {
        return receiver_fb_id;
    }

    public void setReceiver_fb_id(String receiver_fb_id)
    {
        this.receiver_fb_id = receiver_fb_id;
    }



    public String getReceiverName()
    {
        return ReceiverName;
    }

    public void setReceiverName(String ReceiverName) {
        this.ReceiverName = ReceiverName;
    }



    public String getReceiverImageUrl() {
        return ReceiverImageUrl;
    }

    public void setReceiverImageUrl(String ReceiverImageUrl)
    {
        this.ReceiverImageUrl = ReceiverImageUrl;
    }


    public String getDocumentId() {
        return DocumentId;
    }

    public void setDocumentId(String DocumentId) {
        this.DocumentId = DocumentId;
    }


    public String getNewMessageCount() {
        return newMessageCount;
    }

    public void setNewMessageCount(String newMessageCount) {
        this.newMessageCount = newMessageCount;
    }


    public String getNewMessage() {
        return newMessage;
    }

    public void setNewMessage(String newMessage) {
        this.newMessage = newMessage;
    }



    public String getNewMessageTime() {
        return newMessageTime;
    }

    public void setNewMessageTime(String newMessageTime) {
        this.newMessageTime = newMessageTime;
    }

    public String getNewMessageDate() {
        return newMessageDate;
    }

    public void setNewMessageDate(String newMessageDate) {
        this.newMessageDate = newMessageDate;
    }

    public boolean  hasNewMessage(){return isNewMessage;}

    public void sethasNewMessage(boolean isNewMessage){this.isNewMessage=isNewMessage;}



    //to identify store with which chat is done
    public String getReceiverUid() {
        return ReceiverUid;
    }

    public void setReceiverUid(String ReceiverUid) {
        this.ReceiverUid = ReceiverUid;
    }


    //to identify for which product chat was initiated
    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String ProductId) {
        this.ProductId = ProductId;
    }



    //to uniquely identify a chat
    public String getChatId() {
        return ChatId;
    }

    public void setChatId(String ChatId) {
        this.ChatId = ChatId;
    }

    /**
     * Compares this object to the specified object to determine their relative
     * order.
     *
     * @param another the object to compare to this instance.
     * @return a negative integer if this instance is less than {@code another};
     * a positive integer if this instance is greater than
     * {@code another}; 0 if this instance has the same order as
     * {@code another}.
     * @throws ClassCastException if {@code another} cannot be converted into something
     *                            comparable to {@code this} instance.
     */
    @Override
    public int compareTo(ChatList_Chat_Item another)
    {
        if(another!=null&&getNewMessageTime()!=null&&another.getNewMessageTime()!=null)
        {
            return -getNewMessageTime().compareTo(another.getNewMessageTime());

        }else if(getNewMessageTime()==null&&another.getNewMessageTime()!=null)
        {
            return 1;
        }else if(getNewMessageTime()!=null&&another.getNewMessageTime()==null)
        {
            return -1;
        }
        return 0;
    }
}