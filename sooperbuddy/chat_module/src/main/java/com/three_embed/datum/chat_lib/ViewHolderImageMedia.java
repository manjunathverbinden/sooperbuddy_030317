package com.three_embed.datum.chat_lib;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import com.three_embed.datum.chat_lib.R;
/**
 * Created by moda on 15/04/16.
 */
public class ViewHolderImageMedia extends RecyclerView.ViewHolder {


    public ImageView image;


    public ViewHolderImageMedia(View view) {
        super(view);
        image = (ImageView) view.findViewById(R.id.imageView28);
    }
}