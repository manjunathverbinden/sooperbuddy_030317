package com.three_embed.datum.chat_lib;

import java.util.List;
/**
 * */
public class Chat_Message_item
{
    private String message;
    private String MessageType;
    private String id;
    private String fromName;
    private String videoPath;
    private String imagepath;
    private String image_payload;
    private String audioPath;
    private String ts;
    private String date;
    private String DeliveryStatus;
    private String ContactInfo;
    private String isLikeSnap_Chat;
    private String isDeleted;
    private boolean isSelf,isDate;
    private String file_url;
    private boolean isDownloaded=false;

    public boolean isDownloaded()
    {
        return isDownloaded;
    }

    public void setIsDownloaded(boolean isDownloaded)
    {
        this.isDownloaded = isDownloaded;
    }

    public String getImage_payload()
    {
        return image_payload;
    }

    public void setImage_payload(String image_payload)
    {
        this.image_payload = image_payload;
    }


    public String getFile_url()
    {
        return file_url;
    }

    public void setFile_url(String file_url)
    {
        this.file_url = file_url;
    }

    private   List<String> placeInfo;

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsLikeSnap_Chat() {
        return isLikeSnap_Chat;
    }

    public void setIsLikeSnap_Chat(String isLikeSnap_Chat) {
        this.isLikeSnap_Chat = isLikeSnap_Chat;
    }

    public String getMessageId() {
        return id;
    }

    public void setMessageId(String id) {
        this.id = id;
    }



    public String getTS() {
        return ts;
    }

    public void setTS(String ts) {
        this.ts = ts;
    }


    public String getSenderName() {
        return fromName;
    }

    public void setSenderName(String fromName)
    {
        this.fromName = fromName;
    }

    public boolean isSelf()
    {
        return isSelf;
    }

    public void setIsSelf(boolean isSelf)
    {
        this.isSelf = isSelf;
    }

    /**
     * 0-text,1-image,2-video,3-location,4-contact,5-audio
     *
     *
     * */
    public void setMessageType(String MessageType)
    {
        this.MessageType = MessageType;
    }

    public String getMessageType()
    {
        return MessageType;
    }

    public String getTextMessage()
    {
        return message;
    }

    public void setTextMessage(String message) {
        this.message = message;
    }


    public void setImagePath(String imagepath)
    {
        this.imagepath = imagepath;
    }

    public String getImagePath()
    {
        return imagepath;
    }


    public void setVideoPath(String videoPath)
    {
        this.videoPath = videoPath;
    }

    public String getVideoPath() {
        return videoPath;
    }


    public void setPlaceInfo(List<String> placeInfo)
    {
        this.placeInfo = placeInfo;
    }

    public List<String> getPlaceInfo()
    {
        return placeInfo;
    }


    public void setContactInfo(String ContactInfo)
    {
        this.ContactInfo = ContactInfo;
    }

    public String getContactInfo() {
        return ContactInfo;
    }


    public void setAudioPath(String audioPath)
    {
        this.audioPath = audioPath;
    }

    public String getAudioPath()
    {
        return audioPath;
    }





    public boolean isDate()
    {
        return isDate;
    }

    public void setIsDate(boolean isDate)
    {
        this.isDate = isDate;
    }


    public void setDate(String date)
    {
        this.date = date;
    }


    public String getDate()
    {
        return date;
    }

    /**
     *
     * status-0 not sent
     * status-1 sent
     * status-2 delivered
     * status-3 read
     *
     * */
    public String getDeliveryStatus()
    {
        return DeliveryStatus;
    }

    public void setDeliveryStatus(String DeliveryStatus)
    {
        this.DeliveryStatus = DeliveryStatus;
    }


}
