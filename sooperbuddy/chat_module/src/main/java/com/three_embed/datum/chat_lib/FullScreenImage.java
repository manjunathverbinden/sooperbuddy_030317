package com.three_embed.datum.chat_lib;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.File;
import java.io.IOException;

/**
 * Created by moda on 14/04/16.
 */
public class FullScreenImage extends AppCompatActivity
{
    TouchImageView imgDisplay;
    private LinearLayout back_button;
    private String isSanpcaht="0",path="",receiver_do_id="",MessageId="";
    private CountDownTimer countDownTimer=null;
    private TextView time_counter;
    private boolean isFile_deleted=false;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.socket_layout_fullscreen_image);
        /**
         *Hiding the action bar in android.. */
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        time_counter=(TextView)findViewById(R.id.time_counter);

        back_button=(LinearLayout)findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        imgDisplay = (TouchImageView) findViewById(R.id.imgDisplay);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle extras=getIntent().getExtras();
        if(extras!=null)
        {
            path= extras.getString("imagePath");
            isSanpcaht=extras.getString("isSnapchat");
            receiver_do_id=extras.getString("RECEIVERID");
            MessageId=extras.getString("MESSAGEID");

            if(isSanpcaht!=null&&isSanpcaht.equalsIgnoreCase("1"))
            {
                isFile_deleted=false;
                time_counter.setVisibility(View.VISIBLE);
                count_down_timer();
            }else
            {
                time_counter.setVisibility(View.GONE);
                isFile_deleted=true;
            }
            try
            {
                imgDisplay.setImageBitmap(decodeSampledBitmapFromPath(path,250,250));
            }
            catch(OutOfMemoryError e)
            {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed()
    {
        if(isSanpcaht!=null&&isSanpcaht.equalsIgnoreCase("1"))
        {
            if(countDownTimer!=null&&!isFile_deleted)
            {
                countDownTimer.cancel();
                delete_Item(path);
                super.onBackPressed();
            }else
            {
                super.onBackPressed();
            }

        }else
        {
            super.onBackPressed();
        }

    }

    /**
     * <h2>decodeSampledBitmapFromPath</h2>
     * <P>
     *
     * </P>*/
    public Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,int reqHeight) throws IOException
    {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }
    /**
     * <h2>calculateInSampleSize</h2>
     * <P>
     *
     * </P>*/
    public int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight)
    {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }


    private void count_down_timer()
    {
        countDownTimer=new CountDownTimer(5000,1000)
        {
            @Override
            public void onTick(long millisUntilFinished)
            {
                String remaining_time=String.format("0%d",millisUntilFinished / 1000);
                update_UI(remaining_time);
            }
            @Override
            public void onFinish()
            {
                isFile_deleted=true;
                update_UI("00");
                delete_Item(path);
                onBackPressed();
            }
        };
        countDownTimer.start();
    }

    /**
     * Updating the ui for timer.*/
    private void update_UI(final String rememingtime)
    {
        FullScreenImage.this.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                time_counter.setText(rememingtime);
            }
        });
    }

    /**
     * Deleting the file from that path.*/
    private void delete_Item(String file_path)
    {
        File file=new File(file_path);
        /**
         * Checking is the file is exist or not if available then delete the app.*/
        if( file.exists())
        {
            file.delete();
            AppController.getInstance().upSnapchatImageDeleted(receiver_do_id,MessageId,"1");
        }
    }
}