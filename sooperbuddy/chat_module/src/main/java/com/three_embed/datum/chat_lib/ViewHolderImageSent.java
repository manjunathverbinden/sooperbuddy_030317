package com.three_embed.datum.chat_lib;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.three_embed.datum.chat_lib.R;
/**
 * Created by moda on 02/04/16.
 */
public class ViewHolderImageSent extends RecyclerView.ViewHolder  {


    TextView senderName,time;

    ImageView singleTick,doubleTickGreen,doubleTickBlue,clock,imageView;


    public ViewHolderImageSent(View view) {
        super(view);



        senderName=(TextView) view.findViewById(R.id.lblMsgFrom);

        imageView=(ImageView) view.findViewById(R.id.imgshow);

        time=(TextView) view.findViewById(R.id.ts);

        singleTick=  (ImageView) view.findViewById(R.id.single_tick_green);

        doubleTickGreen=  (ImageView) view.findViewById(R.id.double_tick_green);

        doubleTickBlue=  (ImageView) view.findViewById(R.id.double_tick_blue);

        clock=  (ImageView) view.findViewById(R.id.clock);





    }
}
