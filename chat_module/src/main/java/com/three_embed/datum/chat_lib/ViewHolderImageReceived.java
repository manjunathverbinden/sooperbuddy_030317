package com.three_embed.datum.chat_lib;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.three_embed.datum.chat_lib.R;
/**
 * Created by moda on 02/04/16.
 */
public class ViewHolderImageReceived extends RecyclerView.ViewHolder
{

    TextView senderName,time;
    ImageView imageView;
    ProgressBar progressBar;

    public ViewHolderImageReceived(View view)
    {
        super(view);

        senderName=(TextView) view.findViewById(R.id.lblMsgFrom);

        imageView=(ImageView) view.findViewById(R.id.imgshow);

        time=(TextView) view.findViewById(R.id.ts);

        progressBar=(ProgressBar)view.findViewById(R.id.progressbar);

    }
}
