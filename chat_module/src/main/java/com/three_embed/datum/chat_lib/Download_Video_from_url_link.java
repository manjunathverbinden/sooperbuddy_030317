package com.three_embed.datum.chat_lib;

import android.os.AsyncTask;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @since 24/6/16.
 */
public class Download_Video_from_url_link
{
    private static Download_Video_from_url_link download_file_from_url;
    private Download_Video_from_url_link()
    {}

    public static Download_Video_from_url_link getInstance()
    {
        if(download_file_from_url==null)
        {
            download_file_from_url=new Download_Video_from_url_link();
            return download_file_from_url;
        }else
        {
            return download_file_from_url;
        }
    }

    public void down_video_load_file(String url,String location_path,Download_video_load_sucess download_load_sucess)
    {
        Data_holder data_holder=new Data_holder();
        data_holder.url=url;
        data_holder.location=location_path;
        data_holder.download_load_sucess=download_load_sucess;

        /**
         * Doing AsyncTask task*/
        new Download().execute(data_holder);
    }


    private class Download extends AsyncTask<Data_holder,Void,String>
    {
        Data_holder local_data_holder=null;
        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected String doInBackground(Data_holder... params)
        {
            local_data_holder=params[0];
            /**
             * Checking weather the Directory is already Exist or not.*/
            File dir=new File(local_data_holder.location);
            if(!dir.exists() && !dir.isDirectory())
            {
                dir.mkdirs();
            }
            File file = null;
            try {
                URL myImageURL = new URL(local_data_holder.url);
                HttpURLConnection connection = (HttpURLConnection)myImageURL.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                OutputStream fOut = null;
                long time= System.currentTimeMillis();
                file = new File(dir,"chat_Video"+time+".mp4");
                fOut = new FileOutputStream(file);
                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = input.read(buffer)) > 0)
                {
                    fOut.write(buffer, 0, len1);
                }
                fOut.flush();
                fOut.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return null;
            }
            return file.getPath();
        }

        /**
         * <p>Runs on the UI thread after {@link #doInBackground}. The
         * specified result is the value returned by {@link #doInBackground}.</p>
         * <p/>
         * <p>This method won't be invoked if the task was cancelled.</p>
         *
         * @param url The result of the operation computed by {@link #doInBackground}.
         * @see #onPreExecute
         * @see #doInBackground
         * @see #onCancelled(Object)
         */
        @Override
        protected void onPostExecute(String url)
        {
            super.onPostExecute(url);
            if(url!=null&&!url.isEmpty())
            {
                local_data_holder.download_load_sucess.onSucess(url);
            }else
            {
                local_data_holder.download_load_sucess.onError("Error on Loading file!");
            }
        }
    }

    public interface Download_video_load_sucess
    {
        void onSucess(String location_paath);
        void onError(String error);
    }

    private class Data_holder
    {
        public String url;
        public String location;
        public Download_video_load_sucess download_load_sucess;
    }
}

