package com.three_embed.datum.chat_lib;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by moda on 02/04/16.
 */

public abstract class SocketResponseHandler
{
    public Context context;

    public abstract void execute (String event,JSONObject jsonObject) throws JSONException;

    public SocketResponseHandler (Context ctx)
    {
        this.context = ctx;
    }

    public void handleJSONObjectResponse(String event,JSONObject jsonObject) throws Exception
    {
        execute(   event,jsonObject);

    }
}