package com.three_embed.datum.chat_lib;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.multidex.MultiDex;
import android.util.Base64;
import android.util.Log;
import com.couchbase.lite.android.AndroidContext;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
/**
 * @since  24/2/16.
 */

public class AppController extends Application implements Application.ActivityLifecycleCallbacks,ConnectionService.ConnectionServiceCallback
{
    private String indexDocId;
    static  CouchDbController db;
    private String  unsentMessageDocId,userName,pushToken;
    private String userId;
    private String chatDocId;
    SharedPreferences sharedPref;
    private static AppController mInstance;
    private boolean flag=true;
    private static final String chatServerUrl ="http://52.66.118.7:5010/"; //User chat server uri here Ex.http://xxx.xx.xx.xxx:xxx
    private static Socket mSocket;
    private static int resumed,stopped;
    private static SocketResponseHandler socketResponseHandler;
    private Intent intent;
    public final String CHAT_COMMUNICATION = "com.three_embed.datum.home.intent.action.CHAT_COMMUNICATION";


    @Override
    public void onCreate()
    {
        MultiDex.install(getApplicationContext());
        super.onCreate();
        mInstance = this;
        registerActivityLifecycleCallbacks(this);
        db = new CouchDbController(new AndroidContext(this));
        intent = new Intent(this, ConnectionService.class);
        intent.putExtra(ConnectionService.TAG_INTERVAL,5);
        intent.putExtra(ConnectionService.TAG_URL_PING, "http://www.google.com");
        intent.putExtra(ConnectionService.TAG_ACTIVITY_NAME,this.getClass().getName());
        startService(intent);
        sharedPref = this.getSharedPreferences("defaultPreferences", Context.MODE_PRIVATE);
        indexDocId = sharedPref.getString("indexDoc", null);

        if (indexDocId == null)
        {
            indexDocId = db.createIndexDocument();
            sharedPref.edit().putString("indexDoc", indexDocId).apply();
        }

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock    wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My Tag");
        wl.acquire();

        initializeSocket();

        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putString("chatNotificationArray", new Gson().toJson(new ArrayList<String>()));
        prefsEditor.apply();

    }


    /**
     * This method is for use in emulated process environments.  It will
     * never be called on a production Android device, where processes are
     * removed by simply killing them; no user code (including this callback)
     * is executed when doing so.
     */
    @Override
    public void onTerminate()
    {
        super.onTerminate();
        AppController.getInstance().stopInternetCheckingService();
    }

    @Override
    public void registerActivityLifecycleCallbacks(ActivityLifecycleCallbacks callback) {
        super.registerActivityLifecycleCallbacks(callback);
    }


    public void getUserDocIdsFromDb(String userId)
    {

        String userDocId=db.getUserInformationDocumentId(indexDocId, userId);
        ArrayList<String> docIds = db.getUserDocIds(userDocId);
        chatDocId = docIds.get(0);
        unsentMessageDocId=docIds.get(1);

        getUserInfoFromDb(userDocId);

    }


    public void getUserInfoFromDb(String docId)
    {
        Map<String,Object> userInfo = db.getUserInfo(docId);
        userId =  (String)userInfo.get("userId");
        if(mSocket.connected())
        {
            emitHeartbeat("1");
            AppController.getInstance().informContactsOfChangingStatus("1");
            AppController.getInstance().getHistory();

        }
    }



    public  String getUserId()
    {
        return userId;
    }
    public  String getUserName()
    {
        return userName;
    }



    public static synchronized AppController getInstance() {
        return mInstance;
    }





    public   SharedPreferences getSharedPreferences(){

        return sharedPref;
    }







    public   String getIndexDocId(){

        return indexDocId;
    }



    public   String getunsentMessageDocId(){

        return unsentMessageDocId;
    }


    public  String getChatDocId()
    {
        return chatDocId;
    }





    public   String getPushToken(){

        return pushToken;
    }



    public CouchDbController getDbController()
    {
        return db;
    }


    public static void initializeSocket()
    {
        if (mSocket == null    )
        {
            synchronized (AppController.class)
            {
                if (mSocket == null  )
                {
                    try {

                        mSocket = IO.socket(chatServerUrl);
                        connectSocket();
                        initializeListeners();

                    } catch (URISyntaxException e)
                    {
                        e.printStackTrace();
                    }

                }
            }
        }
    }




    public static void connectSocket()
    {
        if (mSocket != null  &&  (  !mSocket.connected() )   ) {
            // Thread Safe. Might be costly operation in some case
            synchronized (AppController.class) {
                if (mSocket != null  && (  !mSocket.connected() )) {
                    mSocket.connect();

                }
            }
        }
    }


    public void emit(String eventName, JSONObject obj)
    {
        if(mSocket.connected())
        {
            mSocket.emit(eventName, obj, new Ack()
            {
                @Override
                public void call(Object... args)
                {
                }
            });
        }
    }

    public boolean isSocketConnected()
    {
        return mSocket != null && mSocket.connected();
    }

    private static void initializeListeners()
    {
        if(mSocket!=null)
        {
            mSocket.on(ServerEvents.chatHistory.value, chatHistory);
            mSocket.on(ServerEvents.getMessageResponseFromServer.value, getMessageResponseFromServer);
            mSocket.on(ServerEvents.sendMessageToServer.value, sendMessageToServer);
            mSocket.on(ServerEvents.getHeartbeat.value, getHeartbeat);
            mSocket.on(ServerEvents.messageAck.value, messageAck);
            mSocket.on(ServerEvents.connect.value, connect);
            mSocket.on(ServerEvents.disconnect.value, disconnect);
            mSocket.on(ServerEvents.connectError.value, connectError);
            mSocket.on(ServerEvents.timeout.value, timeout);
        }
    }

    public static  void initSocketHandlerInstance( SocketResponseHandler sktResponseHandler)
    {
        socketResponseHandler = sktResponseHandler;

    }
    /**
     *
     * has to enable or disable the send button acc to if socket is connected or not
     *
     * */
    private  static Emitter.Listener connect = new Emitter.Listener()
    {
        @Override
        public void call(Object... args)
        {
            if(socketResponseHandler!=null)
            {
                try{
                    socketResponseHandler.handleJSONObjectResponse(ServerEvents.connect.value, new JSONObject());}
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }

            AppController.getInstance().stopInternetCheckingService();
            AppController.getInstance().emitHeartbeat("1");
            AppController.getInstance().informContactsOfChangingStatus("1");
            AppController.getInstance().getHistory();
            String documentId=AppController.getInstance().unsentMessageDocId;

            if(documentId!=null)
            {
                ArrayList<Map<String, Object>> arr = db.getUnsentMessages(documentId);
                if (arr.size() > 0)
                {
                    for (int i = 0; i < arr.size(); i++)
                    {
                        Map<String, Object> map = arr.get(i);
                        JSONObject obj=new JSONObject();
                        try{
                            String type= (String) map.get("type");
                            String message=(String)map.get("message");
                            String url=(String)map.get("url");
                            String payload_data=(String)map.get("payload");
                            String sender_name=(String)map.get("senderName");
                            String sender_FBID=(String)map.get("senderFbid");

                            obj.put("from",map.get("from"));
                            obj.put("to",map.get("to"));
                            obj.put("isSnapchat", map.get("isSnapchat"));
                            obj.put("isDeleted", map.get("isDeleted"));
                            obj.put("senderName",sender_name);
                            obj.put("senderFbid",sender_FBID);

                            switch (type) {
                                case "0":
                                    try {
                                        obj.put("payload", Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT).trim());

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }

                                    break;
                                case "1":
                                    obj.put("payload", payload_data);
                                    obj.put("url", url);
                                    break;
                                case "2":
                                    obj.put("payload", payload_data);
                                    obj.put("url", url);
                                    break;
                                case "3":
                                    obj.put("payload", payload_data);
                                    obj.put("url", url);
                                    break;
                            }

                            obj.put("toDocId",map.get("toDocId") );
                            obj.put("id", map.get("id"));
                            obj.put("type", type);
                        }
                         catch(JSONException e)
                         {
                             e.printStackTrace();
                         }
                        AppController.getInstance().emit(ServerEvents.sendMessageToServer.value, obj);
                        String id = (String) map.get("id");
                        String receiverUid = (String) map.get("to");
                        String tsInGmt = Utilities.tsInGmt();
                        String docId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid);
                        db.updateMessageTs(docId, id, tsInGmt);
                    }
                }
            }
        }};


    private  static Emitter.Listener disconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {


            if(socketResponseHandler!=null)
            {
                try
                {
                    socketResponseHandler.handleJSONObjectResponse(ServerEvents.disconnect.value, new JSONObject());
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            AppController.getInstance().startInternetCheckingService();
            Log.d("log36", "disconnected");
        }};



    private  static Emitter.Listener connectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
        }};


    private  static Emitter.Listener timeout = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
        }};




    private  static Emitter.Listener chatHistory = new Emitter.Listener()
    {
        @Override
        public void call(Object... args)
        {
            Gson gson = new Gson();
            String json = gson.toJson(args);

            Log.d("log200","History123"+json);

            try
            {
                JSONArray mainObject = new JSONArray(json);
                JSONObject json_obj = mainObject.getJSONObject(0);

                Log.d("log201","In the account data.");

                json_obj = json_obj.getJSONObject("nameValuePairs");
                String receiverUid = json_obj.getString("from");
                String messageType = json_obj.getString("type");
                String actualMessage = json_obj.getString("payload").trim();
                String timestamp =  String.valueOf(json_obj.getString("timestamp"));
                String id = json_obj.getString("id");
                String isLike_snap_chat="0";
                String isDeleted="0";
                String image_Url="";
                if(!messageType.equalsIgnoreCase("0"))
                {
                    isLike_snap_chat=json_obj.getString("isSnapchat").trim();
                    isDeleted=json_obj.getString("isDeleted").trim();
                    image_Url=json_obj.getString("url").trim();
                }

                String documentId =AppController.getInstance().findDocumentIdOfReceiver(receiverUid);

                if(documentId==null)
                {
                    String receiverFbId="";
                    documentId=AppController.getInstance().create_user_new_doc_id(receiverUid, "UnKnown User", "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTIuYi3zWcT_-mcSiEdHCHfWZQQKdiCwPG7RXEK4QbelMiqLu5C", receiverFbId,"1");
                }

                JSONObject obj= new JSONObject();
                obj.put("from",AppController.getInstance().userId);
                obj.put("msgIds", new JSONArray(Arrays.asList(new String[]{id})));

                AppController.getInstance().emit(ServerEvents.messageAck.value,obj);
                AppController.getInstance().putMessageInDb(receiverUid,messageType,actualMessage,timestamp,id,documentId,isLike_snap_chat,isDeleted,image_Url);

                if(socketResponseHandler!=null&&socketResponseHandler.context!=null)
                {
                    try
                    {
                        socketResponseHandler.handleJSONObjectResponse(ServerEvents.chatHistory.value,json_obj);
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            catch(JSONException e)
            {
                e.printStackTrace();
            }
        }

    };

    private  static Emitter.Listener getMessageResponseFromServer = new Emitter.Listener()
    {
        @Override
        public void call(Object... args)
        {
            Gson gson = new Gson();
            String json = gson.toJson(args);

            Log.d("log12",json);

            try {


                JSONArray mainObject = new JSONArray(json);
                JSONObject json_obj = mainObject.getJSONObject(0);
                json_obj = json_obj.getJSONObject("nameValuePairs");


                if (json_obj.getInt("deliver") == 1) {
                    String id = json_obj.getString("id");
                    String docId = json_obj.getString("doc_id");

                    db.updateMessageStatus(docId, id, 0);
                    db.removeUnsentMessage(AppController.getInstance().unsentMessageDocId,id);

                }

                if(socketResponseHandler!=null) {
                    try {
                        socketResponseHandler.handleJSONObjectResponse(ServerEvents.getMessageResponseFromServer.value, json_obj);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

            catch(JSONException e){e.printStackTrace();}

        }

    };

    private  static Emitter.Listener sendMessageToServer = new Emitter.Listener() {
        @Override
        public void call(Object... args)
        {
            Gson gson = new Gson();
            String json = gson.toJson(args);

            Log.d("Log48","Got message");
            Log.d("Log49",json);

            try
            {
                JSONArray mainObject = new JSONArray(json);
                JSONObject json_obj = mainObject.getJSONObject(0);
                json_obj = json_obj.getJSONObject("nameValuePairs");
                String receiverUid = json_obj.getString("from");
                String messageType = json_obj.getString("type");
                String actualMessage = json_obj.getString("payload").trim();
                String timestamp = String.valueOf(json_obj.getString("timestamp"));
                String id = json_obj.getString("id");
                String isLike_snap_chat="0";
                String isDeleted="0";
                String image_Url="";
                if(!messageType.equalsIgnoreCase("0"))
                {
                    isLike_snap_chat=json_obj.getString("isSnapchat").trim();
                    isDeleted=json_obj.getString("isDeleted").trim();
                    image_Url=json_obj.getString("url").trim();

                }

                String documentId =AppController.getInstance(). findDocumentIdOfReceiver(receiverUid);

                if(documentId==null)
                {
                    String receiverFbId="";
                    documentId=AppController.getInstance(). create_user_new_doc_id(receiverUid,"Unkown user","https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTIuYi3zWcT_-mcSiEdHCHfWZQQKdiCwPG7RXEK4QbelMiqLu5C",receiverFbId,"1");
                }

                JSONObject obj = new JSONObject();
                obj.put("from", AppController.getInstance().userId);
                obj.put("msgIds", new JSONArray(Arrays.asList(new String[]{id})));

                AppController.getInstance().emit(ServerEvents.messageAck.value, obj);

                AppController.getInstance().putMessageInDb(receiverUid, messageType, actualMessage, timestamp, id, documentId,isLike_snap_chat,isDeleted,image_Url);

                if(socketResponseHandler!=null)
                {
                    try
                    {
                        socketResponseHandler.handleJSONObjectResponse(ServerEvents.sendMessageToServer.value, json_obj);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                } else
                {
                    AppController.getInstance().sendRegistrationToServer(receiverUid,messageType);
                }
            }

            catch(JSONException e){e.printStackTrace();}

        }

    };



    /**
     *
     * Nothing much to be done on getting response in heartbeat,maybe show a marker for internet presence in chatsscreen
     * */

    private  static Emitter.Listener getHeartbeat = new Emitter.Listener()
    {
        @Override
        public void call(Object... args)
        {
            Gson gson = new Gson();
            String json = gson.toJson(args);
            try
            {
                JSONArray mainObject = new JSONArray(json);
                JSONObject json_obj = mainObject.getJSONObject(0);
                json_obj = json_obj.getJSONObject("nameValuePairs");

                if(socketResponseHandler!=null)
                {
                    try
                    {
                        socketResponseHandler.handleJSONObjectResponse(ServerEvents.getHeartbeat.value, json_obj);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

            }
            catch(JSONException e)
            {
                e.printStackTrace();
            }

        }

    };


    private  static Emitter.Listener messageAck = new Emitter.Listener()
    {
        @Override
        public void call(Object... args) {
            Gson gson = new Gson();
            String json = gson.toJson(args);
            try {
                JSONArray mainObject = new JSONArray(json);
                JSONObject json_obj = mainObject.getJSONObject(0);
                json_obj = json_obj.getJSONObject("nameValuePairs");

                if(socketResponseHandler!=null) {

                    try {
                        socketResponseHandler.handleJSONObjectResponse(ServerEvents.messageAck.value, json_obj);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            catch(JSONException e){e.printStackTrace();}

        }

    };

    @SuppressWarnings("unchecked")
    public  String findDocumentIdOfReceiver(String ReceiverUid)
    {
        Map<String,Object> chatDetails= db.getAllChatDetails(AppController.getInstance().getChatDocId());
        ArrayList<String >   receiverUidArray= (ArrayList<String>) chatDetails.get("receiverUidArray");
        ArrayList<String >  receiverDocIdArray= (ArrayList<String>) chatDetails.get("receiverDocIdArray");

        for(int i=0;i<receiverUidArray.size();i++)
        {
            if(receiverUidArray.get(i).equals(ReceiverUid))
            {
                return receiverDocIdArray.get(i);
            }
        }
        return null;
    }


    public void getHistory()
    {
        JSONObject   obj=new JSONObject();
        if(userId!=null)
        {
            try
            {
                obj.put("msg_to", userId);
            }
            catch(JSONException e){e.printStackTrace();}

            emit(ServerEvents.chatHistory.value,obj);
        }else
        {
            Log.d("Error","NO user id");
        }

    }





    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }
    @Override
    public void onActivityResumed(Activity activity)
    {

        resumed++;

        if(resumed>stopped  && flag )
        {
            flag=false;
            emitHeartbeat("1");
            informContactsOfChangingStatus("1");
            getHistory();
        }


    }
    @Override
    public void onActivitySaveInstanceState(Activity activity,
                                            Bundle outState) {

    }
    @Override
    public void onActivityStarted(Activity activity) {

    }
    @Override
    public void onActivityStopped(Activity activity)
    {
        stopped++;
        if(resumed==stopped)
        {
            flag=true;
            emitHeartbeat("0");

            informContactsOfChangingStatus("0");
        }
    }


    public void emitHeartbeat(String status)
    {
        if(pushToken!=null)
        {
            JSONObject obj = new JSONObject();
            try {
                obj.put("pushtoken", pushToken);
                obj.put("from", userId);
                obj.put("status", status);
                obj.put("device", "android");

                Log.d("log38",obj.toString());

            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            emit(ServerEvents.getHeartbeat.value, obj);


        }
    }



    private void informContactsOfChangingStatus(String status)
    {
        JSONObject   obj=new JSONObject();
        try
        {
            obj.put("from",AppController.getInstance().userId);
            obj.put("status",status);
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
    }




    private void putMessageInDb(String receiverUid,String messageType,String actualMessage,String timestamp,String id,String receiverdocId,String isLike_snap_chat,String isDeleted,String url_data)
    {
        String tsInGmt=Utilities.epochtoGmt(timestamp);
        String ts=Utilities.formatDate(Utilities.tsFromGmt(tsInGmt));
        String presentDate  = ts.substring(9, 24);
        String lastDate = db.getLastDate(receiverdocId);
        boolean flag = false;
        int position = -1;

        try
        {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MMM/yyyy");
            Date date1 = sdf.parse(presentDate);
            Date date2 = sdf.parse(lastDate);

            if (date1.after(date2))
            {
                lastDate = presentDate;
                Map<String,Object>   map = new HashMap<>();
                map.put("isDate", true);
                map.put("date", presentDate);
                db.addNewChatMessage(receiverdocId, map,tsInGmt);
                db.updateLastDate(receiverdocId, lastDate);
            } else if (date1.equals(date2))
            {
                flag = false;
            } else
            {
                flag = true;
                String firstDate=db.getFirstDate(receiverdocId);

                if(   date1 .before(sdf.parse(firstDate)))
                {
                    db.updateFirstDate(receiverdocId,presentDate);
                    position=0;

                }
                else
                {
                    position = db.getPositionOfCurrentMessageDate(receiverdocId, presentDate);
                }
            }

        } catch (ParseException ex)
        {
            ex.printStackTrace();
        }

        switch (messageType) {
            case "0": {
                byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);
                String text = "";
                try {
                    text = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Map<String, Object> map = new HashMap<>();
                map.put("message", text);
                map.put("payloadurl", "");
                map.put("messageType", "0");
                map.put("isSelf", false);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("isSnapchat", isLike_snap_chat);
                map.put("isDeleted", isDeleted);
                map.put("id", id);
                map.put("isDate", false);
                if (flag)
                    db.appendDoc(receiverdocId, map, (position + 1));
                else
                    db.addNewChatMessage(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);
                break;
            }
            case "1": {
                Map<String, Object> map = new HashMap<>();
                map.put("message", "");
                map.put("messageType", "1");
                map.put("payload", actualMessage);
                map.put("payloadurl", url_data);
                map.put("isDownloaded", false);
                map.put("isSelf", false);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("isSnapchat", isLike_snap_chat);
                map.put("isDeleted", isDeleted);
                map.put("id", id);
                map.put("isDate", false);
                if (flag)
                    db.appendDoc(receiverdocId, map, (position + 1));
                else
                    db.addNewChatMessage(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, "New Image", true, tsInGmt, tsInGmt);
                break;
            }
            case "2": {
                Map<String, Object> map = new HashMap<>();
                map.put("message", "");
                map.put("messageType", "2");
                map.put("payload", actualMessage);
                map.put("payloadurl", url_data);
                map.put("isDownloaded", false);
                map.put("isSelf", false);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("isSnapchat", isLike_snap_chat);
                map.put("isDeleted", isDeleted);
                map.put("id", id);
                map.put("isDate", false);
                if (flag)
                    db.appendDoc(receiverdocId, map, (position + 1));
                else
                    db.addNewChatMessage(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, "New Video", true, tsInGmt, tsInGmt);
                break;
            }
            case "3": {
                Map<String, Object> map = new HashMap<>();
                map.put("message", "");
                map.put("messageType", "3");
                map.put("payload", actualMessage);
                map.put("payloadurl", url_data);
                map.put("isDownloaded", false);
                map.put("isSelf", false);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("isSnapchat", isLike_snap_chat);
                map.put("isDeleted", isDeleted);
                map.put("id", id);
                map.put("isDate", false);
                if (flag)
                    db.appendDoc(receiverdocId, map, (position + 1));
                else
                    db.addNewChatMessage(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, "New Giphy", true, tsInGmt, tsInGmt);
                break;
            }
        }
    }

    @Override
    public void hasInternetConnection()
    {
        if(!isSocketConnected())
        {
            mSocket.connect();
        }
    }

    @Override
    public void hasNoInternetConnection() {

    }



    private void stopInternetCheckingService()
    {
        stopService(new Intent(this, ConnectionService.class));
    }



    private void startInternetCheckingService()
    {
        startService(intent);
    }

    /**
     * Setting the user id for search data.*/
    public void setUserId(String userId)
    {
        this.userId=userId;
    }

    /**
     * Setting the push token to the server.*/
    public void setPushToken(String pushToken)
    {

        this.pushToken=pushToken;
    }

    /**
     * If the chat doc. for this receiver is not there then creating the new chat doc id and assigning to the chat db.*/
    public String create_user_new_doc_id(String ReceiverUid,String Receiver_name,String imageUrl,String receiver_fb_id,String status_of_user)
    {
        String docId= db.createDocumentForChat(Utilities.tsInGmt(),ReceiverUid,Receiver_name,imageUrl,receiver_fb_id,status_of_user);
        /**
         * Adding the chat module to the main user data base.*/
        db.addChatDocumentDetails(ReceiverUid,docId, com.three_embed.datum.chat_lib.AppController.getInstance().getChatDocId());
        return docId;
    }

    /**
     * <h2>update_user_details</h2>
     * <P>
     *
     * </P>*/
    public boolean update_user_details(String userId_email,String user_name,String user_profile_pic,String user_status,String fb_id)
    {
        String user_doc_id= AppController.getInstance().findDocumentIdOfReceiver(userId_email);
        if(user_doc_id!=null)
        {
            db.updateReceiverNameAndReceiverLogoUrl(user_doc_id,user_name,user_profile_pic,user_status,fb_id);
            return true;
        }else
        {
            return false;
        }
    }

    /**
     * <h2>getUser_details</h2>
     * <P>
     *
     * </P>*/
    public Map<String,Object> getUser_details(String doc_id)
    {
        Map<String,Object> user_details=new HashMap<>();
        user_details.putAll(db.getParticularChatInfo(doc_id));
        return user_details;
    }
    /**
     * Set the base context for this ContextWrapper.  All calls will then be
     * delegated to the base context.  Throws
     * IllegalStateException if a base context has already been set.
     *
     * @param base The new base context for this wrapper.
     */
    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);

    }


    public void delete_Chat(String chat_doc_id)
    {
        db.deleteChat(chat_doc_id);
    }



    public void delete_Particular_Message(String chat_doc_id, String message_id)
    {
        db.deleteParticularChatMessage(chat_doc_id, message_id);
    }

 public void upDateMessageDetails(String docId,String messageId,String file_location,boolean isDownloaded)
 {
     db.upDateMessageDetails(docId, messageId, file_location, isDownloaded);
 }

    public void upSnapchatImageDeleted(String docId,String messageId,String deleted)
    {
        db.upSnapchatImageDeleted(docId, messageId, deleted);
    }

    /**
     *<h2>sendRegistrationToServer</h2>
     * <p>
     *     <h3>
     *     </h3>
     * </p>*/
    private  void sendRegistrationToServer(String email_id,String type)
    {
        Log.d("Received","YEs");

        String title;
        switch (type) {
            case "2":
                title = "New Video";

                break;
            case "1":
                title = "New Image";
                break;
            default:
                title = "New Text";
                break;
        }
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(CHAT_COMMUNICATION);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra("EMAIL", email_id);
        broadcastIntent.putExtra("FBID", "");
        broadcastIntent.putExtra("NAME","");
        broadcastIntent.putExtra("TITLE",title);
        sendBroadcast(broadcastIntent);
    }

}

