package com.three_embed.datum.chat_lib;

/**
 * Created by shobhit on 10/5/16.
 */
public class Time_12hr_Converter
{

    private static Time_12hr_Converter tm12=new Time_12hr_Converter();

    public static Time_12hr_Converter getInstance()
    {
        return tm12;
    }

    /**
     * <h2>get12hr_formate</h2>
     * <P>
     *  Method split the given time in Formate {@code data_time.splite()}
     * </P>
     */
    public String get12hr_formate(String date_time)
    {
        String data[]=date_time.split(":");
        Integer hour=Integer.parseInt(data[0]);
        return String.format("%d:%s %s",getTime_In_12hr(hour),data[1],getAM_PM(hour));
    }

    /**
     * <h2>getTime_In_12hr</h2>
     * <P>
     *   Here checking if the hour time is greater then 12 then it is pm but in 24 hour time it si am .
     * </P>*/
    private int getTime_In_12hr(int hh)
    {
        if(hh>12)
        {
            return (hh-12);
        }else
        {
            return hh;
        }
    }

    /**
     * <h2>getAM_PM</h2>
     * <P>
     *
     * </P>*/
    private String getAM_PM(int hh)
    {
        if(hh>12)
        {
            if(hh==24)
            {
                return "am";
            }else
            {
                return "pm";
            }

        }else
        {
            return "am";
        }
    }

}
