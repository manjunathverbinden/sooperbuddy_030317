package com.three_embed.datum.chat_lib;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

/**
 * @since 23/04/16.
 */
public class ChatMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private ArrayList<Chat_Message_item> mListData = null;
    private final int MESSAGERECEIVED = 0;
    private final int MESSAGESENT = 1;
    private final int IMAGERECEIVED = 2;
    private final int IMAGESENT = 3;
    private final int VIDEOSENT=4;
    private final int VIDEORECEIVED=5;
    private final int GYPHYSENT=7;
    private final int GYPHYRECEIVED=6;
    private Context mContext;
    Time_12hr_Converter time_12hr_converter=null;
    private Download_file_from_url_link download_file_from_url_link=null;
    private Download_Video_from_url_link download_video_from_url_link=null;
    private String receiver_do_id=null;

    public ChatMessageAdapter(Context mContext, ArrayList<Chat_Message_item> mListData,String doc_id)
    {
        this.receiver_do_id=doc_id;
        this.mListData = mListData;
        this.mContext = mContext;
        time_12hr_converter=Time_12hr_Converter.getInstance();
        this.download_file_from_url_link=Download_file_from_url_link.getInstance();
        download_video_from_url_link=Download_Video_from_url_link.getInstance();
    }


    @Override
    public int getItemCount()
    {
        return this.mListData.size();
    }


    @Override
    public int getItemViewType(int position)
    {
        String type = mListData.get(position).getMessageType();

        if (mListData.get(position).isDate())
        {
            return 12;
        } else
        {
            if (mListData.get(position).isSelf())
            {
                switch (type) {
                    case "0":
                        return MESSAGESENT;
                    case "2":
                        return VIDEOSENT;
                    case "3":
                        return GYPHYSENT;
                    default:
                        return IMAGESENT;
                }

            } else {

                switch (type) {
                    case "0":
                        return MESSAGERECEIVED;
                    case "2":
                        return VIDEORECEIVED;
                    case "3":
                        return GYPHYRECEIVED;
                    default:
                        return IMAGERECEIVED;
                }
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v1;
        switch (viewType)
        {
            case VIDEORECEIVED:
                v1 = inflater.inflate(R.layout.socket_video_received, viewGroup, false);
                viewHolder = new ViewHolderImageReceived(v1);
                break;
            case MESSAGERECEIVED:
                v1 = inflater.inflate(R.layout.socket_message_received, viewGroup, false);
                viewHolder = new ViewHolderMessageReceived(v1);
                break;

            case IMAGERECEIVED:
                v1 = inflater.inflate(R.layout.socket_image_received, viewGroup, false);
                viewHolder = new ViewHolderImageReceived(v1);
                break;
            case GYPHYRECEIVED:
                v1 = inflater.inflate(R.layout.socket_image_received,viewGroup, false);
                viewHolder =new ViewHolderImageReceived(v1);
                break;

            case MESSAGESENT:
                v1 = inflater.inflate(R.layout.socket_message_sent,viewGroup, false);
                viewHolder = new ViewHolderMessageSent(v1);
                break;
            case VIDEOSENT:
                v1 = inflater.inflate(R.layout.socket_video_sent,viewGroup, false);
                viewHolder = new ViewHolderImageSent(v1);
                break;

            case IMAGESENT:
                v1 = inflater.inflate(R.layout.socket_image_sent, viewGroup, false);
                viewHolder = new ViewHolderImageSent(v1);
                break;
            case GYPHYSENT:
                v1 = inflater.inflate(R.layout.socket_image_sent, viewGroup, false);
                viewHolder = new ViewHolderImageSent(v1);
                break;

            default:
                v1 = inflater.inflate(R.layout.socket_date, viewGroup, false);
                viewHolder = new ViewHolderDate(v1);

        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
    {
        switch (viewHolder.getItemViewType())
        {
            case MESSAGERECEIVED:
                ViewHolderMessageReceived vh2 = (ViewHolderMessageReceived) viewHolder;
                configureViewHolderMessageReceived(vh2, position);

                break;
            case IMAGERECEIVED:
                ViewHolderImageReceived vh3 = (ViewHolderImageReceived) viewHolder;
                configureViewHolderImageReceived(vh3, position);
                break;
            case GYPHYRECEIVED:
                ViewHolderImageReceived vh7 = (ViewHolderImageReceived) viewHolder;
                configureViewHolderGyphyReceived(vh7, position);
                break;
            case VIDEORECEIVED:
                ViewHolderImageReceived vh4 = (ViewHolderImageReceived) viewHolder;
                configureViewHolderVideoReceived(vh4, position);
                break;
            case MESSAGESENT:
                ViewHolderMessageSent vh8 = (ViewHolderMessageSent) viewHolder;
                configureViewHolderMessageSent(vh8, position);
                break;
            case GYPHYSENT:
                ViewHolderImageSent vh11 = (ViewHolderImageSent) viewHolder;
                configureViewHolderGyphySent(vh11,position);
                break;
            case IMAGESENT:
                ViewHolderImageSent vh9 = (ViewHolderImageSent) viewHolder;
                configureViewHolderImageSent(vh9, position);
                break;
            case VIDEOSENT:
                ViewHolderImageSent vh10 = (ViewHolderImageSent) viewHolder;
                configureViewHolderImageSent(vh10,position);
                break;
            default:
                ViewHolderDate vh14 = (ViewHolderDate) viewHolder;
                configureViewHolderDate(vh14, position);
                break;
        }
    }


    private void configureViewHolderMessageReceived(ViewHolderMessageReceived vh2, final int position)
    {
        final Chat_Message_item message =  mListData.get(position);
        if (message != null)
        {
            vh2.senderName.setText(message.getSenderName());
            vh2.time.setText(time_12hr_converter.get12hr_formate(message.getTS()));
            try
            {
                vh2.message.setText(message.getTextMessage());
            }

            catch(OutOfMemoryError e){e.printStackTrace();}
        }
    }



    @SuppressLint("NewApi")
    private void configureViewHolderVideoReceived(final ViewHolderImageReceived vh2, final int position)
    {
        final Chat_Message_item message=mListData.get(position);

        Log.d("chatImagepath", "Receiver.!");
        Log.d("isSnapchat", message.getIsLikeSnap_Chat());
        Log.d("isSnapchat", message.getIsDeleted());


        if (message != null&&!message.getIsDeleted().equalsIgnoreCase("1"))
        {
            vh2.senderName.setText(message.getSenderName());
            vh2.time.setText(time_12hr_converter.get12hr_formate(message.getTS()));
            Bitmap bmp=null;
            try
            {
                if(!message.isDownloaded())
                {
                    Log.d("chatImagepath", ""+message.isDownloaded());
                    Log.d("chatImagepath", ""+message.getFile_url());
                    Log.d("chatImagepath", ""+message.getImage_payload());

                    if(message.getIsLikeSnap_Chat().equalsIgnoreCase("1"))
                    {
                        vh2.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.message_received));
                    }else
                    {
                        if(!message.getImage_payload().equals("video"))
                        {
                            byte[] data=decompress(message.getImage_payload());
                            bmp = BitmapFactory.decodeByteArray(data,0,data.length);
                        }

                        if(bmp!=null)
                        {
                            vh2.imageView.setImageBitmap(bmp);
                        }
                    }

                    /**
                     * Creating the destination location directory.*/
                    String destination_path=getFilePath();
                    vh2.progressBar.setVisibility(View.VISIBLE);
                    download_video_from_url_link.down_video_load_file(message.getFile_url(), destination_path, new Download_Video_from_url_link.Download_video_load_sucess() {
                        @Override
                        public void onSucess(String location_paath)
                        {
                            vh2.progressBar.setVisibility(View.GONE);
                            message.setIsDownloaded(true);
                            message.setFile_url(location_paath);
                            ChatMessageAdapter.this.notifyItemChanged(position);
                            AppController.getInstance().upDateMessageDetails(receiver_do_id, message.getMessageId(), location_paath, true);
                            Log.d("chatImagepath", location_paath);
                        }

                        @Override
                        public void onError(String error)
                        {
                            vh2.progressBar.setVisibility(View.GONE);
                            message.setIsDownloaded(false);
                            Log.d("chatImagepath", error);
                        }
                    });

                }else if(message.getFile_url()!=null)
                {
                    Log.d("chatImagepath", message.getFile_url());
                    if(message.getIsLikeSnap_Chat().equalsIgnoreCase("1"))
                    {
                        vh2.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.message_received));
                    }else
                    {
                        bmp= ThumbnailUtils.createVideoThumbnail(message.getFile_url(), MediaStore.Video.Thumbnails.MINI_KIND);
                        if(bmp==null)
                        {
                            message.setIsDeleted("1");
                            vh2.imageView.setImageBitmap(null);
                        }else
                        {
                            vh2.imageView.setImageBitmap(bmp);
                        }
                    }

                }else
                {
                    byte[] data=decompress(message.getImage_payload());
                    bmp = BitmapFactory.decodeByteArray(data,0,data.length);
                    if(bmp==null)
                    {
                        message.setIsDeleted("1");
                        vh2.imageView.setImageBitmap(null);
                    }else
                    {
                        vh2.imageView.setImageBitmap(bmp);
                    }
                }
            }
            catch(OutOfMemoryError | Exception e)
            {
                e.printStackTrace();
            }
        }else if(message!=null&&message.getIsLikeSnap_Chat().equalsIgnoreCase("1"))
        {
            vh2.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.message_read));
        }
    }



    private void configureViewHolderGyphyReceived(final ViewHolderImageReceived vh2, final int position)
    {
        final Chat_Message_item message=mListData.get(position);

        Log.d("Gif",message.getFile_url());

        if (message != null)
        {
            vh2.senderName.setText(message.getSenderName());
            vh2.time.setText(time_12hr_converter.get12hr_formate(message.getTS()));

            try
            {
                vh2.progressBar.setVisibility(View.VISIBLE);

                Glide.with(mContext).load(message.getFile_url())
                        .asGif()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .crossFade()
                        .listener(new RequestListener<String, GifDrawable>()
                        {
                            @Override
                            public boolean onException(Exception e, String model, Target<GifDrawable> target, boolean isFirstResource)
                            {
                                vh2.progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GifDrawable resource, String model, Target<GifDrawable> target, boolean isFromMemoryCache, boolean isFirstResource)
                            {
                                vh2.progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(vh2.imageView);
            }
            catch(OutOfMemoryError | Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("NewApi")
    private void configureViewHolderImageReceived(final ViewHolderImageReceived vh2, final int position)
    {
        final Chat_Message_item message=mListData.get(position);

        Log.d("isSnapchat", message.getIsLikeSnap_Chat());
        Log.d("isSnapchat", message.getIsDeleted());
        Log.d("chatImagepath", "Receiver.!");

        if (message != null&&!message.getIsDeleted().equalsIgnoreCase("1"))
        {
            vh2.senderName.setText(message.getSenderName());
            vh2.time.setText(time_12hr_converter.get12hr_formate(message.getTS()));
            Bitmap bmp;
            try
            {
                if(!message.isDownloaded())
                {
                    if(message.getIsLikeSnap_Chat().equalsIgnoreCase("1"))
                    {
                        vh2.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.message_received));
                    }else
                    {
                        byte[] data=decompress(message.getImage_payload());
                        bmp = BitmapFactory.decodeByteArray(data,0,data.length);
                        if(bmp!=null)
                        {
                            vh2.imageView.setImageBitmap(bmp);
                        }
                    }
                    /**
                     * Creating the destination location directory.*/
                    String destination_path=getFilePath();
                    vh2.progressBar.setVisibility(View.VISIBLE);

                    download_file_from_url_link.down_load_file(message.getFile_url(),destination_path, new Download_file_from_url_link.Download_load_sucess() {
                        @Override
                        public void onSucess(String location_paath)
                        {
                            vh2.progressBar.setVisibility(View.GONE);
                            message.setIsDownloaded(true);
                            message.setFile_url(location_paath);
                            ChatMessageAdapter.this.notifyItemChanged(position);
                            AppController.getInstance().upDateMessageDetails(receiver_do_id, message.getMessageId(),location_paath,true);
                            Log.d("chatImagepath", location_paath);
                        }

                        @Override
                        public void onError(String error)
                        {
                            vh2.progressBar.setVisibility(View.GONE);
                            message.setIsDownloaded(false);
                            Log.d("chatImagepath", error);
                        }
                    });

                }else if(message.getIsLikeSnap_Chat().equalsIgnoreCase("1")&&message.getIsDeleted().equalsIgnoreCase("1"))
                {
                    vh2.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.message_read));

                }else if(message.getFile_url()!=null)
                {
                    Log.d("chatImagepath", message.getFile_url());

                    if(message.getIsLikeSnap_Chat().equalsIgnoreCase("1"))
                    {
                        vh2.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.message_received));
                    }else
                    {
                        bmp=decodeSampledBitmapFromPath(message.getFile_url(),200,200);
                        if(bmp==null)
                        {
                            message.setIsDeleted("1");
                            vh2.imageView.setImageBitmap(null);
                        }else
                        {
                            vh2.imageView.setImageBitmap(bmp);
                        }
                    }

                }else
                {
                    byte[] data=decompress(message.getImage_payload());
                    bmp = BitmapFactory.decodeByteArray(data,0,data.length);
                    if(bmp==null)
                    {
                        message.setIsDeleted("1");
                        vh2.imageView.setImageBitmap(null);
                    }else
                    {
                        vh2.imageView.setImageBitmap(bmp);
                    }
                }
            }
            catch(OutOfMemoryError | Exception e)
            {
                e.printStackTrace();
            }
        }else if(message!=null&&message.getIsLikeSnap_Chat().equalsIgnoreCase("1"))
        {
            vh2.imageView.setBackground(mContext.getResources().getDrawable(R.drawable.message_read));
        }
    }




    private void configureViewHolderMessageSent(ViewHolderMessageSent vh2, final int position)
    {
        final Chat_Message_item message =  mListData.get(position);

        if (message != null) {

            vh2.senderName.setText(message.getSenderName());
            vh2.time.setText( time_12hr_converter.get12hr_formate(message.getTS()));

            try{
                vh2.message.setText(message.getTextMessage());

            }

            catch(OutOfMemoryError e)
            {
                e.printStackTrace();
            }
            String status=     message.getDeliveryStatus();

            switch (status) {
                case "3":

                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.GONE);

                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.VISIBLE);

                    break;
                case "2":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.GONE);

                    vh2.doubleTickGreen.setVisibility(View.VISIBLE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
                case "1":

                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.VISIBLE);

                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
                default:
                    vh2.clock.setVisibility(View.VISIBLE);
                    vh2.singleTick.setVisibility(View.GONE);
                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
            }


        }
    }

    private void configureViewHolderGyphySent(ViewHolderImageSent vh2, final int position)
    {
        final Chat_Message_item message=mListData.get(position);

        Log.d("Gif",message.getImagePath());

        if (message!= null)
        {
            vh2.senderName.setText(message.getSenderName());
            vh2.time.setText(time_12hr_converter.get12hr_formate(message.getTS()));
            try
            {
                Glide.with(mContext).load(message.getImagePath())
                        .asGif()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .crossFade()
                        .listener(new RequestListener<String, GifDrawable>()
                        {
                            @Override
                            public boolean onException(Exception e, String model, Target<GifDrawable> target, boolean isFirstResource)
                            {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GifDrawable resource, String model, Target<GifDrawable> target, boolean isFromMemoryCache, boolean isFirstResource)
                            {
                                return false;
                            }
                        })
                        .into(vh2.imageView);

            } catch (Exception e)
            {
                e.printStackTrace();
                message.setIsDeleted("1");
            }

            String status=message.getDeliveryStatus();

            switch (status) {
                case "3":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.GONE);
                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.VISIBLE);

                    break;
                case "2":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.GONE);

                    vh2.doubleTickGreen.setVisibility(View.VISIBLE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
                case "1":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.VISIBLE);
                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
                default:
                    vh2.clock.setVisibility(View.VISIBLE);
                    vh2.singleTick.setVisibility(View.GONE);
                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
            }
        }
    }


    /**
     * <h2></h2>
     * <P>
     *
     * </P>*/
    private void configureViewHolderImageSent(ViewHolderImageSent vh2, final int position)
    {
        final Chat_Message_item message=mListData.get(position);
        if (message!= null)
        {
            vh2.senderName.setText(message.getSenderName());
            vh2.time.setText(time_12hr_converter.get12hr_formate(message.getTS()));
            try {
                vh2.imageView.setImageBitmap(decodeSampledBitmapFromPath(message.getImagePath(),200,200));
            }
            catch(OutOfMemoryError e)
            {
                e.printStackTrace();
            } catch (IOException e)
            {
                e.printStackTrace();
                message.setIsDeleted("1");
            }

            String status=message.getDeliveryStatus();

            switch (status) {
                case "3":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.GONE);
                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.VISIBLE);

                    break;
                case "2":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.GONE);

                    vh2.doubleTickGreen.setVisibility(View.VISIBLE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
                case "1":
                    vh2.clock.setVisibility(View.GONE);
                    vh2.singleTick.setVisibility(View.VISIBLE);
                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
                default:
                    vh2.clock.setVisibility(View.VISIBLE);
                    vh2.singleTick.setVisibility(View.GONE);
                    vh2.doubleTickGreen.setVisibility(View.GONE);
                    vh2.doubleTickBlue.setVisibility(View.GONE);
                    break;
            }
        }
    }

    /**
     * <h2>configureViewHolderDate</h2>
     * <P>
     *     Video holder for showing the today or tomorrow or Date.
     * </P>*/
    private void configureViewHolderDate(ViewHolderDate vh2, final int position)
    {
        final Chat_Message_item message =  mListData.get(position);
        if (message != null)
        {
            vh2.date.setText(message.getDate());
        }
    }



    /**
     * <h2>decompress</h2>
     * <P>
     *
     * </P>*/
    public static byte[] decompress(String message)
    {
        byte[] data = Base64.decode(message,Base64.DEFAULT);

        ByteArrayOutputStream baoStream2 = new ByteArrayOutputStream();
        try {
            InputStream inStream = new GZIPInputStream(new ByteArrayInputStream(data));
            byte[] buffer = new byte[16384];
            int len;
            while ((len = inStream.read(buffer)) > 0)
            {
                baoStream2.write(buffer, 0, len);
            }
        } catch (IOException | OutOfMemoryError e)
        {
            e.printStackTrace();
        }
        return baoStream2.toByteArray();
    }

    /**
     * <h2>decodeSampledBitmapFromPath</h2>
     * <P>
     *
     * </P>*/
    public Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,int reqHeight) throws IOException
    {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth,reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }
    /**
     * <h2>calculateInSampleSize</h2>
     * <P>
     *
     * </P>*/
    public int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight)
    {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }


    private Bitmap rotate_Image(Bitmap bitmap_image_to_rotate,float angle)
    {
        Bitmap temp;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        temp = Bitmap.createBitmap(bitmap_image_to_rotate, 0, 0, bitmap_image_to_rotate.getWidth(), bitmap_image_to_rotate.getHeight(), matrix, true);
        return temp;

    }

    /**
     * <h2>getFilePath</h2>
     * <P>
     *     creating the file path to store image.
     * </P>*/
    public String getFilePath()
    {
        File folder;

        folder = new File(Environment.getExternalStorageDirectory().getPath() + "/holyfire");
        if (!folder.exists() && !folder.isDirectory())
        {
            folder.mkdir();
        }

        return folder.getPath();
    }


}