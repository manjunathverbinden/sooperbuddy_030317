package com.three_embed.datum.chat_lib;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.three_embed.datum.chat_lib.R;
/**
 * Created by rahul on 3/3/16.
 */
public class ViewHolderChat extends RecyclerView.ViewHolder {
    public TextView newMessageTime, newMessage,storeName, newMessageDate,newMessageCount;
    public ImageView    storeImage;


    public ViewHolderChat(View view) {
        super(view);


        newMessageTime = (TextView) view.findViewById(R.id.newMessageTime);
        newMessage = (TextView) view.findViewById(R.id.newMessage);
       newMessageDate = (TextView) view.findViewById(R.id.newMessageDate);
        storeName = (TextView) view.findViewById(R.id.storeName);
        storeImage = (ImageView) view.findViewById(R.id.storeImage);


        newMessageCount = (TextView) view.findViewById(R.id.newMessageCount);



    }
}
