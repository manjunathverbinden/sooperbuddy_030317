package com.three_embed.datum.Facebook_login;

/**
 * @since  14/11/16.
 */
public class Facebook_album_list
{
    private String id;

    private Albums albums;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Albums getAlbums ()
    {
        return albums;
    }

    public void setAlbums (Albums albums)
    {
        this.albums = albums;
    }

}
