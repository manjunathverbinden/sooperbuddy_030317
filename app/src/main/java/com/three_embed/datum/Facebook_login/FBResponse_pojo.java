package com.three_embed.datum.Facebook_login;

import java.util.ArrayList;

/**
 * Created by shobhit on 3/2/16.
 */
public class FBResponse_pojo
{
    private Picture picture;
    private String id;
    private String first_name;
    private String birthday;
    private String bio;
    private Albums albums;
    private String locale;
    private String last_name;
    private Age_range age_range;
    private String gender;
    private String email;
    private Likes likes;
    private Friends friends;
    private ArrayList<Work> work;
    private ArrayList<User_Education>  education;



    public ArrayList<Work> getWork() {
        return work;
    }

    public void setWork(ArrayList<Work> work) {
        this.work = work;
    }

    public ArrayList<User_Education> getEducation() {
        return education;
    }

    public void setEducation(ArrayList<User_Education> education) {
        this.education = education;
    }

    public Friends getFriends() {
        return friends;
    }

    public void setFriends(Friends friends) {
        this.friends = friends;
    }


    public Likes getLikes()
    {
        return likes;
    }

    public void setLikes(Likes likes)
    {
        this.likes = likes;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    public Picture getPicture ()
    {
        return picture;
    }

    public void setPicture (Picture picture)
    {
        this.picture = picture;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getBirthday ()
    {
        return birthday;
    }

    public void setBirthday (String birthday)
    {
        this.birthday = birthday;
    }

    public String getBio ()
    {
        return bio;
    }

    public void setBio (String bio)
    {
        this.bio = bio;
    }

    public Albums getAlbums ()
    {
        return albums;
    }

    public void setAlbums (Albums albums)
    {
        this.albums = albums;
    }

    public String getLocale ()
    {
        return locale;
    }

    public void setLocale (String locale)
    {
        this.locale = locale;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public Age_range getAge_range ()
    {
        return age_range;
    }

    public void setAge_range (Age_range age_range)
    {
        this.age_range = age_range;
    }

}
