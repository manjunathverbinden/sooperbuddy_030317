package com.three_embed.datum.Facebook_login;

/**
 * Created by shobhit on 6/5/16.
 */
public class User_Location
{
    private String id;

    private String name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }
}
