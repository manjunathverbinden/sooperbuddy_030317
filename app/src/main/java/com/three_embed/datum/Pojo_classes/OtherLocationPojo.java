package com.three_embed.datum.Pojo_classes;

/**
 * Created by embed-pc on 31/12/15.
 */
public class OtherLocationPojo
{
    int id;
    String location;
    String countryName;

    /**
     * Empty Constructor
     */
    public OtherLocationPojo() {}

    /**
     * <p>
     *     constructor to initialize id, location & countryname
     * </p>
     * @param id
     * @param location
     * @param countryName
     */
    public OtherLocationPojo(int id, String location, String countryName)
    {
        this.id = id;
        this.location = location;
        this.countryName = countryName;
    }

    /**
     * <p>
     *      constructor to initialize location & countryname
     * </p>
     * @param countryName
     * @param location
     */
    public OtherLocationPojo(String countryName, String location)
    {
        this.countryName = countryName;
        this.location = location;
    }

    // getting id
    public int getId()
    {
        return id;
    }

    // setting id
    public void setId(int id)
    {
        this.id = id;
    }

    // getting location
    public String getLocation()
    {
        return location;
    }

    // setting location
    public void setLocation(String location)
    {
        this.location = location;
    }

    // getting country name
    public String getCountryName()
    {
        return countryName;
    }

    // setting country name
    public void setCountryName(String countryName)
    {
        this.countryName = countryName;
    }
}
