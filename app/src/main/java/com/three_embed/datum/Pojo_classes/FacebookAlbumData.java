package com.three_embed.datum.Pojo_classes;

/**
 * Created by adminpc on 1/2/16.
 */
public class FacebookAlbumData
{
   /* "id":"1398012373821039",
            "created_time":"2014-06-14T12:54:11+0000",
            "name":"Mobile Uploads"*/
    private String id;
    private String created_time;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
