package com.three_embed.datum.Pojo_classes;

/**
 * Created by embed-pc on 24/11/15.
 */
public class Rest_password_validation_pojo
{
    private String errorMessage;

    private String errorFlag;

    public String getErrorMessage ()
    {
        return errorMessage;
    }

    public void setErrorMessage (String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getErrorFlag ()
    {
        return errorFlag;
    }

    public void setErrorFlag (String errorFlag)
    {
        this.errorFlag = errorFlag;
    }

}
