package com.three_embed.datum.Pojo_classes;

import java.util.ArrayList;

/**
 * Created by adminpc on 19/2/16.
 */
public class RecentVisitorsPojo
{
    private String errorFlag;
    private String errorMessage;
    private ArrayList<VisitorsDatas> Visitors;

    public String getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(String errorFlag) {
        this.errorFlag = errorFlag;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ArrayList<VisitorsDatas> getVisitors() {
        return Visitors;
    }

    public void setVisitors(ArrayList<VisitorsDatas> visitors) {
        Visitors = visitors;
    }
}
