package com.three_embed.datum.Pojo_classes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by adminpc on 19/1/16.
 */
public class MatchesListPojo implements Serializable
{
    private String errorFlag;
    private String errorMessage;
    private String TotalMatches;
    private String TotalBlocked;
    private String TotalDeleted;
    private ArrayList<MatchesListData> Matches;
    private ArrayList<MatchesListData> Blocked;
    private ArrayList<String> Deleted;

    public String getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(String errorFlag) {
        this.errorFlag = errorFlag;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getTotalMatches() {
        return TotalMatches;
    }

    public void setTotalMatches(String totalMatches) {
        TotalMatches = totalMatches;
    }

    public String getTotalBlocked() {
        return TotalBlocked;
    }

    public void setTotalBlocked(String totalBlocked) {
        TotalBlocked = totalBlocked;
    }

    public String getTotalDeleted() {
        return TotalDeleted;
    }

    public void setTotalDeleted(String totalDeleted) {
        TotalDeleted = totalDeleted;
    }

    public ArrayList<MatchesListData> getMatches() {
        return Matches;
    }

    public void setMatches(ArrayList<MatchesListData> matches) {
        Matches = matches;
    }

    public ArrayList<MatchesListData> getBlocked() {
        return Blocked;
    }


    public void setBlocked(ArrayList<MatchesListData> blocked) {
        Blocked = blocked;
    }

    public ArrayList<String> getDeleted() {
        return Deleted;
    }

    public void setDeleted(ArrayList<String> deleted) {
        Deleted = deleted;
    }
}
