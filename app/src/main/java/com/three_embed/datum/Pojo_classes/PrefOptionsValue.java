package com.three_embed.datum.Pojo_classes;

/**
 * Created by embed-pc on 11/12/15.
 */
public class PrefOptionsValue
{
   /* "Male",
            "Female",
            "Others"*/
     private String Male;
     private String Female;
     private String Others;

    public String getMale() {
        return Male;
    }

    public void setMale(String male) {
        Male = male;
    }

    public String getFemale() {
        return Female;
    }

    public void setFemale(String female) {
        Female = female;
    }

    public String getOthers() {
        return Others;
    }

    public void setOthers(String others) {
        Others = others;
    }
}
