package com.three_embed.datum.Pojo_classes;

import android.view.View;
import java.io.Serializable;

/**
 * <h>Class contains the each item details of the delete array list.</h>
 * <p>
 *     Class contains the all related parameter for each item of delete array_list and
 *     contains getter and setter method for them.
 * </p>
 *@since   22/1/16.
 * @author 3Embed.
 */
public class Delete_list_pojoclass implements Serializable
{
    private String delete_image_tag;
    private View delete_Image;
    private String delete_Image_url;


    public String getDelete_image_tag()
    {
        return delete_image_tag;
    }

    public void setDelete_image_tag(String delete_image_tag) {
        this.delete_image_tag = delete_image_tag;
    }


    public View getDelete_Image()
    {
        return delete_Image;
    }

    public void setDelete_Image(View delete_Image)
    {
        this.delete_Image = delete_Image;
    }

    public String getDelete_Image_url()
    {
        return delete_Image_url;

    }

    public void setDelete_Image_url(String delete_Image_url)
    {
        this.delete_Image_url = delete_Image_url;
    }
    @Override
    public String toString()
    {
        return this.delete_image_tag;
    }
}
