package com.three_embed.datum.Pojo_classes;

/**
 * Created by shobhit on 2/6/16.
 */
public class Facebook_album_data_holder
{
    private String name;
    private String url;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
