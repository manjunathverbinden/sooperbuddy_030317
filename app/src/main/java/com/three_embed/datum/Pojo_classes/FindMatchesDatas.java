package com.three_embed.datum.Pojo_classes;

import java.util.ArrayList;

/**
 * Created by embed-pc on 14/12/15.
 */
public class FindMatchesDatas
{
    private String distance;
    private String firstName;
    private String lastName;
    private String age;
    private String sharedLikes;
    private String pPic;
    private String pVideo;
    private String VideoThumnail;
    private String ImgCount;
    private String sex;
    private String persDesc;
    private String fbId;
    private ArrayList<String> other=new ArrayList<>();

    public ArrayList<String> getOtherImages()
    {
        return other;
    }

    public void setOtherImages(ArrayList<String> otherImages)
    {
        this.other = otherImages;
    }
    public String getDistance()
    {
        return distance;
    }

    public void setDistance(String distance)
    {
        this.distance = distance;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getAge()
    {
        return age;
    }

    public void setAge(String age)
    {
        this.age = age;
    }

    public String getSharedLikes()
    {
        return sharedLikes;
    }

    public void setSharedLikes(String sharedLikes)
    {
        this.sharedLikes = sharedLikes;
    }

    public String getpPic()
    {
        return pPic;
    }

    public void setpPic(String pPic)
    {
        this.pPic = pPic;
    }

    public String getpVideo()
    {
        return pVideo;
    }

    public void setpVideo(String pVideo)
    {
        this.pVideo = pVideo;
    }

    public String getVideoThumnail()
    {
        return VideoThumnail;
    }

    public void setVideoThumnail(String videoThumnail)
    {
        VideoThumnail = videoThumnail;
    }

    public String getImgCount()
    {
        return ImgCount;
    }

    public void setImgCount(String imgCount)
    {
        ImgCount = imgCount;
    }

    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getPersDesc()
    {
        return persDesc;
    }

    public void setPersDesc(String persDesc)
    {
        this.persDesc = persDesc;
    }

    public String getFbId()
    {
        return fbId;
    }

    public void setFbId(String fbId)
    {
        this.fbId = fbId;
    }
}
