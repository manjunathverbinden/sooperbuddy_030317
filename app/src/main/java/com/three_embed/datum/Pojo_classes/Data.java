package com.three_embed.datum.Pojo_classes;

/**
 * Created by nirav on 05/10/15.
 */
public class Data
{
    private String imagePath;
    public Data(String imagePath)
    {
        this.imagePath = imagePath;
    }

    public String getImagePath()
    {
        return imagePath;
    }

}
