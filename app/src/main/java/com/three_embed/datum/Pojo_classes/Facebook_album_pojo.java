package com.three_embed.datum.Pojo_classes;

import java.util.ArrayList;

/**
 * Created by adminpc on 1/2/16.
 */
public class Facebook_album_pojo
{
    /*"data":[],
            "paging":{}*/
    private ArrayList<FacebookAlbumData> data;

    public ArrayList<FacebookAlbumData> getData() {
        return data;
    }

    public void setData(ArrayList<FacebookAlbumData> data) {
        this.data = data;
    }
}
