package com.three_embed.datum.Pojo_classes;

/**
 * @since  4/3/16.
 * @author 3Embed.
 */
public class user_list_item_details
{
   private String Image_url;
   private String thumb_nail_url;
    private String video_url;

    public String getThumb_nail_url()
    {
        return thumb_nail_url;
    }

    public void setThumb_nail_url(String thumb_nail_url) {
        this.thumb_nail_url = thumb_nail_url;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getImage_url() {
        return Image_url;
    }

    public void setImage_url(String image_url) {
        Image_url = image_url;
    }
}
