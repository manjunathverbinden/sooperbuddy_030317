package com.three_embed.datum.Pojo_classes;

import java.util.ArrayList;

/**
 * Created by embed-pc on 14/12/15.
 */
public class FindMatchesPojo
{
   /* "errorFlag":"0",
            "errorMessage":"Matches Found",
            "TotalMatches":0,
            "TotalMatches2":[],
            "PlaceName":""*/
    private String errorFlag;
    private String errorMessage;
    private String TotalMatches;
    private ArrayList<FindMatchesDatas> TotalMatches2;
    private String PlaceName;

    public String getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(String errorFlag) {
        this.errorFlag = errorFlag;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getTotalMatches() {
        return TotalMatches;
    }

    public void setTotalMatches(String totalMatches) {
        TotalMatches = totalMatches;
    }

    public ArrayList<FindMatchesDatas> getTotalMatches2() {
        return TotalMatches2;
    }

    public void setTotalMatches2(ArrayList<FindMatchesDatas> totalMatches2) {
        TotalMatches2 = totalMatches2;
    }

    public String getPlaceName() {
        return PlaceName;
    }

    public void setPlaceName(String placeName) {
        PlaceName = placeName;
    }
}
