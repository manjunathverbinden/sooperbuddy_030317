package com.three_embed.datum.Directory_management;

import android.app.Activity;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.provider.MediaStore;
import com.three_embed.datum.Utility.ServiceUrl;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * <h1>Directory_creation</h1>
 * This class simply use to Manage the Directory creation and File of this App.
 * <p>
 *     This class First Receive the Activity in its constructor .
 *     then Give user to save the Bitmap file to store in Device.
 * </p>
 * @author 3Embed
 * @since 25/11/15.
 *@version 1.0.
 */
public class Directory_creation
{
    private String  isExternal_storage_exist="";
    private Activity activity;
    private File folder;
    /**
     * <h1>Directory_creation</h1>
     * Overriding the default constructor of the Directory_creation.
     * and receiving the activity reference .
     * Also checking the external storage status.*/
    public Directory_creation(Activity activity)
    {
        this.activity=activity;
        isExternal_storage_exist= Environment.getExternalStorageState();
    }

    /**
     * <h2>createDirectoryAndSaveFile</h2>
     * Method is used to store the Given bitmap parameter into a created file.
     * <P>
     *     Method first check weather External storage is Exist or not by
     *     storing the status of the External storage Status from Environment Variable.
     *     Then checking the External storage is available or not by checking the Environment MEDIA_MOUNTED.
     *     if available then creating a file of the to the external storage or else
     *     creating the file of path to the internal storage fo Device.
     *     Then checking that file is exist previously or not.
     *     and creating directory if that directory is not exist.
     *     Then creating a file for storing the image of the given name.
     *     Then checking if it is coming from Camera..
     *     Then checking that Image is exist or not .
     *     If Exist then deleting that Image for new Profile picture Image.
     *     else not Doing any thing.
     *     Then creating a FileOutputStream to store the Image
     *     and then flushing all the Open source of File write.
     * </P>
     * @param bitmap contain the current Image bitmap object to store..
     * */
    public File createDirectoryAndSaveFile(Bitmap bitmap)
    {
        OutputStream outStream = null;
        /**
         * Checking the External storage is Exist or not.*/
        if(Environment.MEDIA_MOUNTED.equals(isExternal_storage_exist))
        {
            folder = new File(Environment.getExternalStorageDirectory() + "/" + ServiceUrl.App_folder+ ServiceUrl.Directory_name);
        }else
        {

            folder = new File(activity.getFilesDir()+ "/" + ServiceUrl.App_folder+ ServiceUrl.Directory_name);
        }

        /**
         * Checking weather the Directory is already Exist or not.*/
        if(!folder.exists()&&!folder.isDirectory())
        {
            folder.mkdirs();
        }else
        {
            String[] children = folder.list();
            for (int i = 0;children!=null&&i< children.length; i++)
            {
                new File(folder, children[i]).delete();
            }
        }

        long time= System.currentTimeMillis();
        String profile_pic_name="profile_pic"+time+".png";
        File image = new File(folder,profile_pic_name);
        /**
         * Checking the File is previously exist or not.
         * if exist then deleting that file.*/
        if (image.exists())
        {
            image.delete();
        }

        try
        {
            outStream = new FileOutputStream(image);
            bitmap.compress(Bitmap.CompressFormat.PNG, 50,outStream);
            outStream.flush();
            outStream.close();

        }catch (IOException e)
        {
            e.printStackTrace();
        }

        return image;
    }

    /**
     * <h2>createDirectory_for_Video</h2>
     * Method is used to store the Given create a file for video formate mp4 and return it to the calling class..
     * <P>
     *     Method first check weather External storage is Exist or not by
     *     storing the status of the External storage Status from Environment Variable.
     *     Then checking the External storage is available or not by checking the Environment MEDIA_MOUNTED.
     *     if available then creating a file of the to the external storage or else
     *     creating the file of path to the internal storage fo Device.
     *     Then checking that file is exist previously or not.
     *     and creating directory if that directory is not exist.
     *     Then creating a file for storing the image of the given name.
     *     Then checking if it is coming from Camera..
     *     Then checking that Image is exist or not .
     *     If Exist then deleting that Image for new Profile picture Image.
     *     else not Doing any thing.
     *     Then creating a FileOutputStream to store the Image
     *     and then flushing all the Open source of File write.
     * </P>
     * */
    public File createDirectory_for_Video()
    {
        /**
         * Checking the External storage is Exist or not.*/
        if(Environment.MEDIA_MOUNTED.equals(isExternal_storage_exist))
        {
            folder = new File(Environment.getExternalStorageDirectory() + "/" + ServiceUrl.App_folder+ ServiceUrl.Directory_name_video);
        }else
        {

            folder = new File(activity.getFilesDir()+ "/" + ServiceUrl.App_folder+ ServiceUrl.Directory_name_video);
        }

        /**
         * Checking weather the Directory is already Exist or not.*/
        if(!folder.exists()&&!folder.isDirectory())
        {
            folder.mkdirs();
        }else
        {
            String[] children = folder.list();
            for (int i = 0;children!=null&&i< children.length; i++)
            {
                new File(folder, children[i]).delete();
            }
        }

        long time= System.currentTimeMillis();
        String profile_pic_name="profile_video"+time+".mp4";
        File video = new File(folder,profile_pic_name);
        /**
         * Checking the File is previously exist or not.
         * if exist then deleting that file.*/
        if (video.exists())
        {
            video.delete();
        }
        return video;
    }


    public File create_Thumbnail(File file)
    {
        File thumbnail_folder=null;
        Bitmap bitmap= ThumbnailUtils.createVideoThumbnail(file.getPath(), MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
        OutputStream outStream = null;
        /**
         * Checking the External storage is Exist or not.*/
        if(Environment.MEDIA_MOUNTED.equals(isExternal_storage_exist))
        {
            thumbnail_folder = new File(Environment.getExternalStorageDirectory() + "/" + ServiceUrl.App_folder+ ServiceUrl.Directory_name_video_thumbnail);
        }else
        {
            thumbnail_folder = new File(activity.getFilesDir()+ "/" + ServiceUrl.App_folder+ ServiceUrl.Directory_name_video_thumbnail);
        }

        /**
         * Checking weather the Directory is already Exist or not.*/
        if(!thumbnail_folder.exists()&&!thumbnail_folder.isDirectory())
        {
            thumbnail_folder.mkdirs();
        }else
        {
            String[] children = thumbnail_folder.list();
            for (int i = 0;children!=null&&i< children.length; i++)
            {
                new File(thumbnail_folder, children[i]).delete();
            }
        }

        long time= System.currentTimeMillis();
        String profile_pic_name="thumbnail_pic"+time+".png";
        File thumbnail_image = new File(thumbnail_folder,profile_pic_name);
        /**
         * Checking the File is previously exist or not.
         * if exist then deleting that file.*/
        if (thumbnail_image.exists())
        {
            thumbnail_image.delete();
        }
        try
        {
            outStream = new FileOutputStream(thumbnail_image);
            bitmap.compress(Bitmap.CompressFormat.PNG,50,outStream);
            outStream.flush();
            outStream.close();

        }catch (IOException e)
        {
            e.printStackTrace();
        }
        return thumbnail_image;
    }

    /**
     * <h2>create_ChatVideo_Thumbnail</h2>
     * <P>
     *     Creating the video thumb nail id.
     * </P>*/
    public File create_ChatVideo_Thumbnail(File file)
    {
        File thumbnail_folder=null;
        Bitmap bitmap= ThumbnailUtils.createVideoThumbnail(file.getPath(), MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
        OutputStream outStream = null;
        /**
         * Checking the External storage is Exist or not.*/
        if(Environment.MEDIA_MOUNTED.equals(isExternal_storage_exist))
        {
            thumbnail_folder = new File(Environment.getExternalStorageDirectory() + "/" + ServiceUrl.App_folder+ ServiceUrl.Directory_chat_video_thumbnail);
        }else
        {
            thumbnail_folder = new File(activity.getFilesDir()+ "/" + ServiceUrl.App_folder+ ServiceUrl.Directory_chat_video_thumbnail);
        }

        /**
         * Checking weather the Directory is already Exist or not.*/
        if(!thumbnail_folder.exists()&&!thumbnail_folder.isDirectory())
        {
            thumbnail_folder.mkdirs();
        }
        long time= System.currentTimeMillis();
        String profile_pic_name="thumbnail_pic"+time+".png";
        File thumbnail_image = new File(thumbnail_folder,profile_pic_name);
        /**
         * Checking the File is previously exist or not.
         * if exist then deleting that file.*/
        if (thumbnail_image.exists())
        {
            thumbnail_image.delete();
        }
        try
        {
            outStream = new FileOutputStream(thumbnail_image);
            bitmap.compress(Bitmap.CompressFormat.PNG,50,outStream);
            outStream.flush();
            outStream.close();

        }catch (IOException e)
        {
            e.printStackTrace();
        }
        return thumbnail_image;
    }
    /**
     * <h2>create_file_for_Chat_Image</h2>
     * <P>
     *
     * </P>*/

    public File create_file_for_Chat_Image()
    {
        File chat_images=null;
        /**
         * Checking the External storage is Exist or not.*/
        if(Environment.MEDIA_MOUNTED.equals(isExternal_storage_exist))
        {
            chat_images = new File(Environment.getExternalStorageDirectory() + "/" + ServiceUrl.App_folder+ ServiceUrl.CHAT_IMAGES);
        }else
        {
            chat_images = new File(activity.getFilesDir()+ "/" + ServiceUrl.App_folder+ ServiceUrl.CHAT_IMAGES);
        }
        /**
         * Checking weather the Directory is already Exist or not.*/
        if(!chat_images.exists() && !chat_images.isDirectory())
        {
            chat_images.mkdirs();
        }

        long time= System.currentTimeMillis();
        File chat_image_file=null;
        int image_number=0;
        /**
         * Checking the File is previously exist or not.
         * if exist then creating new file that file.*/
        do
        {
            image_number=image_number+1;
            String profile_pic_name="chat_image"+time+image_number+".png";
            chat_image_file = new File(chat_images,profile_pic_name);

        }while(chat_image_file.exists());

        return chat_image_file;
    }

}
