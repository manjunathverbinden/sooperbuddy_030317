package com.three_embed.datum.DataBaseClass;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by shobhit on 29/4/16.
 */
public class Match_List_Data_holder extends SQLiteOpenHelper
{
    private Context mcontext;
    private static final int DATABASE_VERSION =1;
    /**
     * Data base Manager.*/
    private static final String DATABASE_NAME = "HumTum_Match_list_Manager";
    private static final String TABLE_NAME = "HumTum_matchlist_details";
    private static final String KEY_ID = "id";
    private static final String FOR_ADMIN="foradmin";
    private static final String FB_ID = "fbid";
    private static final String USER_EMAIL_ID = "emailID";
    private static final String USER_PROFILE_PICTURE = "profilepicture";
    private static final String USER_FIRST_NAME="firstname";
    private static final String Age = "age";
    private static final String STATUS = "status";
    private static final String LASTACTIVE="LastActive";





    public Match_List_Data_holder(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mcontext = context;
    }


    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_USER_TABLE = "CREATE TABLE " +TABLE_NAME+ "("
                +KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+FOR_ADMIN +" TEXT,"+FB_ID +" TEXT,"+USER_EMAIL_ID +" TEXT,"+USER_PROFILE_PICTURE + " TEXT," +USER_FIRST_NAME + " TEXT," +LASTACTIVE + " TEXT,"+STATUS + " TEXT," +Age + " TEXT"+")";
        db.execSQL(CREATE_USER_TABLE);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */
    public void addSingle_Item(String admin_id,String fb_id,String user_email_id,String user_profile_picture,String user_first_name,String age,String status,String lastactive)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        File database=mcontext.getDatabasePath(DATABASE_NAME);

        if (!database.exists())
        {
            /**
             * Database does not exist so copy it from assets here*/
            Log.i("Database", "add Drop Location Database Not Found");
        }
        else
        {
            Log.i("Database", "add Drop Location Database Found");
        }

        ContentValues values = new ContentValues();
        values.put(FOR_ADMIN,admin_id);
        values.put(FB_ID,fb_id);
        values.put(USER_EMAIL_ID,user_email_id);
        values.put(USER_FIRST_NAME,user_first_name);
        values.put(USER_PROFILE_PICTURE,user_profile_picture);
        values.put(Age,age);
        values.put(STATUS,status);
        values.put(LASTACTIVE,lastactive);
        // Inserting Row
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    /**
     * <h2>getAllAddress</h2>
     * <P>
     *     Method loop through all database to get all the address list from
     *     the table of the given user id.
     * </P>
     * */
    public ArrayList<Match_list_data_pojo> getAllMatch_Data(String admin_id)
    {
        File database=mcontext.getDatabasePath(DATABASE_NAME);
        if(!database.exists())
        {
            /**
             *  Database does not exist so copy it from assets here
             *  */
            Log.i("Database", "getAllDropLocations Database Not Found");
        }
        else
        {
            Log.i("Database", "getAllDropLocations Database Found");
        }

        ArrayList<Match_list_data_pojo> addressList = null;
        /**
         *Select All Query and where user_id is filtration.*/
        String selectQuery = "SELECT  * FROM " + TABLE_NAME +" WHERE " + FOR_ADMIN +" = "+ admin_id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        /**
         * looping through all rows and adding to list
         * */
        if (cursor!=null&&cursor.moveToFirst())
        {
            addressList = new ArrayList<>();
            do
            {
                Match_list_data_pojo data=new Match_list_data_pojo();
                data.setFB_ID(cursor.getString(cursor.getColumnIndex(FB_ID)));
                data.setUSER_FIRST_NAME(cursor.getString(cursor.getColumnIndex(USER_FIRST_NAME)));
                data.setUSER_EMAIL_ID(cursor.getString(cursor.getColumnIndex(USER_EMAIL_ID)));
                data.setUSER_PROFILE_PICTURE(cursor.getString(cursor.getColumnIndex(USER_PROFILE_PICTURE)));
                data.setAge(cursor.getString(cursor.getColumnIndex(Age)));
                data.setSTATUS(cursor.getString(cursor.getColumnIndex(STATUS)));
                data.setLastActive(cursor.getString(cursor.getColumnIndex(LASTACTIVE)));
                /**
                 * Adding each item one by one.*/
                addressList.add(data);
            } while (cursor.moveToNext());
        }
        assert cursor != null;
        cursor.close();
        db.close();
        /**
         *  return contact list*/
        return addressList;
    }


    /**
     * <h2>delete_Item</h2>
     * <P>
     *
     * </P>
     */
    public boolean delete_Item(String user_id)
    {
        String query_list = "FB_ID =" + user_id;
        SQLiteDatabase db = this.getWritableDatabase();
        int item_deleted=db.delete(TABLE_NAME,query_list,null);
        db.close();
        if(item_deleted>0)
        {
            return true;
        }else
        {
            return false;
        }
    }

    /**
     * Method to clear the data from the Table*/
    public void ClearDataBae()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,null,null);
        db.close();
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        /**
         *Drop older table if existed OF OLDER VERSION*/
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        /**
         * Creating the new Database.*/
        onCreate(db);
    }
}
