package com.three_embed.datum.DataBaseClass;

/**
 * Created by shobhit on 26/2/16.
 */
public class Database_item_details
{

    int id;
    String user_id;
    String user_address;
    String latitude;
    String longitude;
    public Database_item_details()
    {
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getUser_id()
    {
        return user_id;
    }

    public void setUser_id(String user_id)
    {
        this.user_id = user_id;
    }

    public String getUser_address()
    {
        return user_address;
    }

    public void setUser_address(String user_address)
    {
        this.user_address = user_address;
    }

    public String getLatitude()
    {
        return latitude;
    }

    public void setLatitude(String latitude)
    {
        this.latitude = latitude;
    }

    public String getLongitude()
    {
        return longitude;
    }

    public void setLongitude(String longitude)
    {
        this.longitude = longitude;
    }

}
