package com.three_embed.datum.DataBaseClass;

/**
 * Created by shobhit on 29/4/16.
 */
public class Match_list_data_pojo
{

    private String FB_ID;
    private String USER_EMAIL_ID;
    private String USER_PROFILE_PICTURE;
    private String USER_FIRST_NAME;
    private String Age;
    private String STATUS;

    public String getLastActive() {
        return LastActive;
    }

    public void setLastActive(String lastActive) {
        LastActive = lastActive;
    }

    private String LastActive;


    public String getFB_ID()
    {
        return FB_ID;
    }

    public void setFB_ID(String FB_ID)
    {
        this.FB_ID = FB_ID;
    }

    public String getUSER_EMAIL_ID() {
        return USER_EMAIL_ID;
    }

    public void setUSER_EMAIL_ID(String USER_EMAIL_ID)
    {
        this.USER_EMAIL_ID = USER_EMAIL_ID;
    }

    public String getUSER_PROFILE_PICTURE()
    {
        return USER_PROFILE_PICTURE;
    }

    public void setUSER_PROFILE_PICTURE(String USER_PROFILE_PICTURE)
    {
        this.USER_PROFILE_PICTURE = USER_PROFILE_PICTURE;
    }

    public String getUSER_FIRST_NAME()
    {
        return USER_FIRST_NAME;
    }

    public void setUSER_FIRST_NAME(String USER_FIRST_NAME)
    {
        this.USER_FIRST_NAME = USER_FIRST_NAME;
    }

    public String getAge()
    {
        return Age;
    }

    public void setAge(String age)
    {
        Age = age;
    }

    public String getSTATUS()
    {
        return STATUS;
    }

    public void setSTATUS(String STATUS)
    {
        this.STATUS = STATUS;
    }
}
