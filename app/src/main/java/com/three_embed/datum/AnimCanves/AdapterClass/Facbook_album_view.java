package com.three_embed.datum.AdapterClass;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.three_embed.datum.Pojo_classes.Facebook_album_data_holder;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Aramis_home.R;
import java.util.ArrayList;


/**
 * @since  2/6/16.
 */
public class Facbook_album_view extends RecyclerView.Adapter<Facbook_album_view.ViewHolder>
{
    private ArrayList<Facebook_album_data_holder> mDataSet;
    private Activity mactivity;


    public Facbook_album_view(ArrayList<Facebook_album_data_holder> dataSet,Activity activity)
    {
        mDataSet = dataSet;
        this.mactivity=activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.facebook_picture_item,viewGroup,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position)
    {
        Log.d("Urlsdata", "" + mDataSet.get(position).getUrl());
        viewHolder.picture.setTag(Utility.get_Target_Place_Holder(mactivity, viewHolder.picture, viewHolder.progressBar));
        Picasso.with(mactivity)
                .load(mDataSet.get(position).getUrl())
                .placeholder(R.drawable.default_user_rectangle)
                .error(R.drawable.default_user_rectangle)
                .into((Target) viewHolder.picture.getTag());
        if(mDataSet.get(position).getName()!=null)
        {
            viewHolder.name.setText(mDataSet.get(position).getName());
        }

    }

    @Override
    public int getItemCount()
    {
        return mDataSet.size();
    }
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView picture;
        TextView name;
        ProgressBar progressBar;

        public ViewHolder(View v)
        {
            super(v);
            picture = (ImageView) v.findViewById(R.id.picture);
            name = (TextView) v.findViewById(R.id.name);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        }
    }
}