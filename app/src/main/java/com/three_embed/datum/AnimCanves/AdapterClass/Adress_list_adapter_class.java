package com.three_embed.datum.AdapterClass;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.three_embed.datum.DataBaseClass.Database_item_details;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Aramis_home.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * <h1>Adress_list_adapter_class</h1>
 * <P>
 *
 * </P>
 */

public class Adress_list_adapter_class extends ArrayAdapter<Database_item_details> implements AdapterView.OnItemClickListener
{
    private Activity mcontext;

    private static int tick_position=-1;
    private SessionManager sessionManager;
    private Typeface sans_regular,sans_semibold;

    public Adress_list_adapter_class(Activity context, ArrayList<Database_item_details> data)
    {
        super(context,R.layout.other_list_data_details,data);
        this.mcontext=context;
        sessionManager=new SessionManager(this.mcontext);
        /**
         * Creating the Response of Data inflater object.*/
        sans_regular= Typeface.createFromAsset(this.mcontext.getAssets(), "fonts/MyriadPro-Regular.otf");
        sans_semibold=Typeface.createFromAsset(this.mcontext.getAssets(),"fonts/MyriadPro-Semibold.otf");
    }

    /**
     * {@inheritDoc}
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Coustom_Hnadeler coustom_hnadeler;

        if(convertView==null)
        {
            LayoutInflater layoutInflater=(LayoutInflater)mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=layoutInflater.inflate(R.layout.other_list_data_details,null);
            coustom_hnadeler=new Coustom_Hnadeler();
            coustom_hnadeler.header_name_txt= (TextView) convertView.findViewById(R.id.adress_name);
            coustom_hnadeler.header_name_txt.setTypeface(sans_regular);
            coustom_hnadeler.details_txt=(TextView)convertView.findViewById(R.id.adress_details);
            coustom_hnadeler.details_txt.setTypeface(sans_semibold);
            coustom_hnadeler.tick_mark_image=(ImageView)convertView.findViewById(R.id.tick_mark);
            convertView.setTag(coustom_hnadeler);
        }else
        {
            coustom_hnadeler=(Coustom_Hnadeler)convertView.getTag();
        }
        /**
         * Setting the tick mark.*/
        if(tick_position==position)
        {
            coustom_hnadeler.tick_mark_image.setVisibility(View.VISIBLE);
        }else
        {
            coustom_hnadeler.tick_mark_image.setVisibility(View.INVISIBLE);
        }
        String adress=getItem(position).getUser_address();
        String header_name[]=adress.split(",");

        coustom_hnadeler.header_name_txt.setText(header_name[0]);
        coustom_hnadeler.details_txt.setText(adress);

        return convertView;
    }

    class Coustom_Hnadeler
    {
        TextView header_name_txt,details_txt;
        ImageView tick_mark_image;

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        /**
         * Holding the click position.*/
        tick_position=position;

        Database_item_details data=(Database_item_details)parent.getAdapter().getItem(position);
        String adress=data.getUser_address();
        String header_name[]=adress.split(",");
        change_Location(data.getLatitude(), data.getLongitude(),header_name[0],view);

    }


    private void change_Location(final String new_lat,final String newLong,final String place_name,final View view)
    {
        final ProgressBar progressBar=(ProgressBar)view.findViewById(R.id.changed_location_progressbar);
        progressBar.setVisibility(View.VISIBLE);

        JSONObject jsonObject=new JSONObject();

        try
        {
            jsonObject.put("ent_dev_id",Utility.getDeviceId(mcontext));
            jsonObject.put("ent_sess_token",sessionManager.getToken());
            jsonObject.put("ent_fbid",sessionManager.getFbId());
            jsonObject.put("ent_curr_lat",new_lat);
            jsonObject.put("ent_curr_long",newLong);
            jsonObject.put("ent_placename",place_name);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.CHANGEMYLOCATION, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback()
        {
            @Override
            public void onSuccess(String result)
            {
                progressBar.setVisibility(View.GONE);
                JSONObject repose;
                try
                {
                    repose=new JSONObject(result);

                    if(repose.getString("errorFlag").equals("0"))
                    {
                        Toast.makeText(mcontext,repose.getString("errorMessage"),Toast.LENGTH_SHORT).show();
                        ImageView imageView=(ImageView)view.findViewById(R.id.tick_mark);
                        imageView.setVisibility(View.VISIBLE);
                        /**
                         * Notifying the data set change.*/
                        notifyDataSetChanged();
                        sessionManager.setLatitude(new_lat);
                        sessionManager.setLogitude(newLong);
                        /**
                         * Attaching the Dat to the Activity result.*/
                        Intent intent=new Intent();
                        intent.putExtra("MESSAGE",place_name);
                        mcontext.setResult(Activity.RESULT_OK,intent);
                        /**
                         * Finishing the Actvity.*/
                        mcontext.finish();
                    }else
                    {
                        Toast.makeText(mcontext,repose.getString("errorMessage"),Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error)

            {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mcontext,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

}
