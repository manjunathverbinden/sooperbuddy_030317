package com.three_embed.datum.AdapterClass;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.three_embed.datum.Aramis_home.User_preference_setter.Chat_Fragment_list_data;

/**
 * @since  18/1/16.
 */
public class ChatPagerAdapter extends FragmentStatePagerAdapter
{
    int mNumOfTabs;
    private Fragment host_fragment=null;

    public ChatPagerAdapter(FragmentManager fm, int NumOfTabs)
    {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position)
        {
            case 0:
                this.host_fragment = new Chat_Fragment_list_data();
                return  this.host_fragment;
            default:
                return null;
        }
    }

    /**
     * Return the host fragment.*/
    public Fragment getHost_Fragment()
    {
        return this.host_fragment;
    }

    @Override
    public int getCount()
    {
        return mNumOfTabs;
    }
}