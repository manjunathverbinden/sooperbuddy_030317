package com.three_embed.datum.AdapterClass;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.three_embed.datum.Pojo_classes.Facebook_education_pojo;
import com.three_embed.datum.Pojo_classes.Facebook_likes_pojo;
import com.three_embed.datum.Pojo_classes.Facebook_user_friends_pojo;
import com.three_embed.datum.Pojo_classes.Faebook_user_work_pojo;
import com.three_embed.datum.Pojo_classes.SetPreferenceAlData;
import com.three_embed.datum.Pojo_classes.SetPreferencePojo;
import com.three_embed.datum.Utility.CircleTransform;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.SessionmangerForFacebook;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import com.three_embed.datum.Aramis_home.R;
import com.three_embed.datum.Aramis_home.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @since  21/1/16.
 */
public class MyProfileDetailsAdap extends ArrayAdapter<String>
{
    private String userName;
    private String age;
    private String aboutUser;
    private LayoutInflater inflater;
    private Context context;
    private SessionManager sessionManager;
    private SessionmangerForFacebook sessionmangerForFacebook;
    private int friends_count=0;
    private int likes_count=0;
    private String gender="";
    private Typeface GothamRoundedMedium,GothamRoundedLight,GothamRoundedBold;
    public boolean isDataFound=false;

    public MyProfileDetailsAdap(Context context,String userName,String age,String gender,String aboutUser,ArrayList<String> data)
    {
        super(context,0,data);
        this.friends_count=0;
        this.likes_count=0;
        this.userName=userName;
        this.context=context;
        this.age=age;
        this.aboutUser=aboutUser;
        this.gender=gender;
        sessionManager=new SessionManager(this.context);
        sessionmangerForFacebook=new SessionmangerForFacebook(this.context);
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        /**
         * Font style.*/
        GothamRoundedMedium=Typeface.createFromAsset(this.context.getAssets(),"fonts/MyriadPro-Regular.otf");
        GothamRoundedLight=Typeface.createFromAsset(this.context.getAssets(), "fonts/MyriadPro-Regular.otf");
        GothamRoundedBold=Typeface.createFromAsset(this.context.getAssets(),"fonts/MyriadPro-Bold.otf");
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Coustom_Hnadeler coustom_hnadeler;

        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.my_profile_details,null);
            coustom_hnadeler=new Coustom_Hnadeler();
            coustom_hnadeler.linear_logout= (LinearLayout) convertView.findViewById(R.id.linear_logout);
            coustom_hnadeler.textViewname= (TextView) convertView.findViewById(R.id.tV_userName);
            coustom_hnadeler.textViewname.setTypeface(GothamRoundedBold);
            coustom_hnadeler.textViewage= (TextView) convertView.findViewById(R.id.tV_userAge);
            coustom_hnadeler.textViewage.setTypeface(GothamRoundedBold);
            coustom_hnadeler.tV_About=(TextView)convertView.findViewById(R.id.tV_About);
            coustom_hnadeler.tV_About.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.tV_AbouUser= (TextView) convertView.findViewById(R.id.tV_AbouUser);
            coustom_hnadeler.tV_AbouUser.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.textViewAbout= (TextView) convertView.findViewById(R.id.tV_AboutText);
            coustom_hnadeler.textViewAbout.setTypeface(GothamRoundedLight);
            coustom_hnadeler.friends_count=(TextView)convertView.findViewById(R.id.friends_count);
            coustom_hnadeler.likes_count=(TextView)convertView.findViewById(R.id.facebook_interest_count);
            coustom_hnadeler.education_plcae_holder=(LinearLayout)convertView.findViewById(R.id.education_plcae_holder);
            coustom_hnadeler.work_place_holder=(LinearLayout)convertView.findViewById(R.id.work_place_holder);
            coustom_hnadeler.friends_place_holder=(LinearLayout)convertView.findViewById(R.id.friends_place_holder);
            coustom_hnadeler.facebook_interest_place_holder=(LinearLayout)convertView.findViewById(R.id.facebook_interest_place_holder);
            coustom_hnadeler.facebook_login_layout=(LinearLayout)convertView.findViewById(R.id.facebook_login_layout);
            coustom_hnadeler.user_education_title=(TextView)convertView.findViewById(R.id.user_education_title);
            coustom_hnadeler.user_education_title.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.user_work_title=(TextView)convertView.findViewById(R.id.user_work_title);
            coustom_hnadeler.user_work_title.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.friends_header=(TextView)convertView.findViewById(R.id.friends_header);
            coustom_hnadeler.friends_header.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.facebook_interest=(TextView)convertView.findViewById(R.id.facebook_interest);
            coustom_hnadeler.facebook_interest.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.linearlayout=(LinearLayout)convertView.findViewById(R.id.usersettingparent);
            coustom_hnadeler.textUserSetting=(TextView)convertView.findViewById(R.id.userSettingtext);
            coustom_hnadeler.textUserSetting.setTypeface(GothamRoundedMedium);
            coustom_hnadeler.progress_bar=(ProgressBar)convertView.findViewById(R.id.progress_bar);
            convertView.setTag(coustom_hnadeler);
        }else
        {
            coustom_hnadeler=(Coustom_Hnadeler)convertView.getTag();
        }
        coustom_hnadeler.textViewage.setText(String.format("%s", age));
        coustom_hnadeler.textViewname.setText(String.format("%s ,", userName));
        /**
         * Checking user contains user about.*/
        if(aboutUser!=null&&!aboutUser.isEmpty())
        {
            coustom_hnadeler.textViewAbout.setText(aboutUser);
        }
        else
        {
            coustom_hnadeler.textViewAbout.setVisibility(View.GONE);
        }

        coustom_hnadeler.tV_AbouUser.setText(userName);

        if(sessionManager.get_User_fb_login_status())
        {
            coustom_hnadeler.facebook_login_layout.setVisibility(View.VISIBLE);

            String user_work_list_data=sessionmangerForFacebook.getUserFbWorks();
            Faebook_user_work_pojo faebook_user_work_pojo=new Gson().fromJson(user_work_list_data,Faebook_user_work_pojo.class);
            if(faebook_user_work_pojo!=null&&faebook_user_work_pojo.getWork()!=null&&faebook_user_work_pojo.getWork().size()>0)
            {
                /**
                 * Adding the user Education
                 * Use this code print all the work details if you want that {@code facebook_education_pojo.getEducation().size()}
                 * in the place of 1.*/
                for(int count=0;count<1;count++)
                {
                    String employer_name=faebook_user_work_pojo.getWork().get(count).getEmployer()!=null?faebook_user_work_pojo.getWork().get(count).getEmployer().getName():"";
                    String  location_data=faebook_user_work_pojo.getWork().get(count).getLocation()!=null?faebook_user_work_pojo.getWork().get(count).getLocation().getName():"";
                    String work_position=faebook_user_work_pojo.getWork().get(count).getPosition()!=null?faebook_user_work_pojo.getWork().get(count).getPosition().getName():"";
                    String starting_data=faebook_user_work_pojo.getWork().get(count)!=null?faebook_user_work_pojo.getWork().get(count).getStart_date():"";
                    /**
                     * Creating the view.*/
                    coustom_hnadeler.work_place_holder.addView(setFacebook_work(employer_name,location_data,work_position,starting_data));
                }

            }else
            {
                coustom_hnadeler.work_place_holder.setVisibility(View.GONE);
                coustom_hnadeler.user_work_title.setVisibility(View.GONE);

            }

            String fb_user_education=sessionmangerForFacebook.getUserFbEducation();
            Facebook_education_pojo facebook_education_pojo=new Gson().fromJson(fb_user_education,Facebook_education_pojo.class);
            if(facebook_education_pojo!=null&&facebook_education_pojo.getEducation()!=null&&facebook_education_pojo.getEducation().size()>0)
            {
                /**
                 * Adding the user Education
                 * for all education details use this code {facebook_education_pojo.getEducation().size()}
                 * in the place of 1.*/
                for(int count=0;count<1;count++)
                {
                    String school_name=facebook_education_pojo.getEducation().get(count).getSchool()!=null?facebook_education_pojo.getEducation().get(count).getSchool().getName():"";
                    String school_type=facebook_education_pojo.getEducation().get(count).getType()!=null?facebook_education_pojo.getEducation().get(count).getType():"";
                    String year=facebook_education_pojo.getEducation().get(count).getYear()!=null?facebook_education_pojo.getEducation().get(count).getYear().getName():"";

                    /**
                     * Creating the view.*/
                    coustom_hnadeler.education_plcae_holder.addView(setFacebook_education(school_name,school_type,year));

                }

            }else
            {
                coustom_hnadeler.user_education_title.setVisibility(View.GONE);
                coustom_hnadeler.education_plcae_holder.setVisibility(View.GONE);
            }


            String friends_list=sessionmangerForFacebook.getUserFbFriends();
            Log.d("Facebokresponse",friends_list);
            Facebook_user_friends_pojo facebook_user_friends_pojo=new Gson().fromJson(friends_list, Facebook_user_friends_pojo.class);

            if(facebook_user_friends_pojo!=null&&facebook_user_friends_pojo.getFriends().getData()!=null&&facebook_user_friends_pojo.getFriends().getData().length>0)
            {
                /**
                 * Adding the user Education*/
                for(int count=0;count<facebook_user_friends_pojo.getFriends().getData().length;count++)
                {
                    friends_count=count+1;
                    String image_url="https://graph.facebook.com/"+facebook_user_friends_pojo.getFriends().getData()[count].getId()+"/picture?type=large";
                    coustom_hnadeler.friends_place_holder.addView(setFacebook_friends(image_url,facebook_user_friends_pojo.getFriends().getData()[count].getFirst_name()));

                }
                coustom_hnadeler.friends_count.setText(String.format("(%d)", friends_count));

            }else
            {
                coustom_hnadeler.friends_header.setVisibility(View.GONE);
                coustom_hnadeler.friends_place_holder.setVisibility(View.GONE);
                coustom_hnadeler.friends_count.setVisibility(View.GONE);
            }

           /* *
         * Adding the user Education*/
            String fb_likes=sessionmangerForFacebook.getUserFbLike();
            Log.d("FBResponse",fb_likes);
            Facebook_likes_pojo facebook_likes_pojo=new Gson().fromJson(fb_likes, Facebook_likes_pojo.class);
            if(facebook_likes_pojo!=null&&facebook_likes_pojo.getLikes().getData()!=null&&facebook_likes_pojo.getLikes().getData().length>0)
            {
                for(int count=0;count<facebook_likes_pojo.getLikes().getData().length;count++)
                {
                    likes_count=count+1;
                    String image_url;
                    if(facebook_likes_pojo.getLikes().getData()[count].getPhotos()!=null&&facebook_likes_pojo.getLikes().getData()[count].getPhotos().getData()[0].getSource()!=null)
                    {
                        image_url=facebook_likes_pojo.getLikes().getData()[count].getPhotos().getData()[0].getSource();
                    }else
                    {
                        image_url="";
                    }
                    coustom_hnadeler.facebook_interest_place_holder.addView(setFacebook_likes(image_url,facebook_likes_pojo.getLikes().getData()[count].getName()));
                }
                coustom_hnadeler.likes_count.setText(String.format("(%d)", likes_count));

            }else
            {
                coustom_hnadeler.facebook_interest.setVisibility(View.GONE);
                coustom_hnadeler.facebook_interest_place_holder.setVisibility(View.GONE);
                coustom_hnadeler.likes_count.setVisibility(View.GONE);
            }

            /**
             * Hiding the facebook log in button.*/
            coustom_hnadeler.linear_logout.setVisibility(View.GONE);

        }else
        {
            coustom_hnadeler.facebook_login_layout.setVisibility(View.GONE);
            coustom_hnadeler.linear_logout.setVisibility(View.INVISIBLE);
        }
        /**
         * when the user click on this. Then it will redirect to Login page.
         */
        coustom_hnadeler.linear_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setUserLoginStatus(false);
                VariableConstant.isLogOut = true;
                Intent intent = new Intent(context, SplashScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(new Intent(intent));
            }
        });

        if(!isDataFound)
        {
            getUSerSettingDetails(coustom_hnadeler.linearlayout, coustom_hnadeler.progress_bar);
        }
        return convertView;
    }
    public void setDownLoadFlage()
    {
        isDataFound=false;
    }

    /**
     * Coustom handler.*/
    class Coustom_Hnadeler
    {
        TextView textViewname,textViewage,textViewAbout,tV_AbouUser,friends_count,likes_count,user_education_title,user_work_title,friends_header,facebook_interest,tV_About,textUserSetting;
        LinearLayout linear_logout,facebook_login_layout,education_plcae_holder,work_place_holder,friends_place_holder,facebook_interest_place_holder,linearlayout;
        ProgressBar progress_bar;
    }


    /**
     * <h2>setFacebook_friends</h2>
     * <P>
     *
     * </P>*/
    private View setFacebook_friends(String proflie_image_url,String userName)
    {
        @SuppressLint("InflateParams") View view=inflater.inflate(R.layout.facebook_friend_item,null);
        ImageView profile_image=(ImageView)view.findViewById(R.id.friend_profile_image);
        profile_image.setTag(get_Target_setup(profile_image));
        Picasso.with(context)
                .load(proflie_image_url)
                .transform(new CircleTransform())
                .error(R.drawable.chat_profile_default_image_frame)
                .placeholder(R.drawable.chat_profile_default_image_frame)
                .into((Target) profile_image.getTag());
        TextView name=(TextView)view.findViewById(R.id.name_of_the_user);
        name.setTypeface(GothamRoundedMedium);
        name.setText(userName);

        return view;
    }

    /**
     * <h2>setFacebook_likes</h2>
     * <P>
     *
     * </P>*/
    @SuppressLint("NewApi")
    private View setFacebook_likes(String pic_url,String likes_name)
    {
        @SuppressLint("InflateParams") View view=inflater.inflate(R.layout.facebook_friend_item,null);
        ImageView profile_image=(ImageView)view.findViewById(R.id.friend_profile_image);
        if(pic_url!=null&&!pic_url.isEmpty())
        {
            profile_image.setTag(get_Target_setup(profile_image));
            Picasso.with(context)
                    .load(pic_url)
                    .transform(new CircleTransform())
                    .error(R.drawable.chat_profile_default_image_frame)
                    .placeholder(R.drawable.chat_profile_default_image_frame)
                    .into((Target) profile_image.getTag());
        }else
        {
            //noinspection deprecation
            profile_image.setBackground(context.getResources().getDrawable(R.drawable.chat_profile_default_image_frame));
        }

        TextView name=(TextView)view.findViewById(R.id.name_of_the_user);
        name.setTypeface(GothamRoundedMedium);
        name.setText(likes_name);
        return view;
    }

    /**
     * <h2>setFacebook_likes</h2>
     * <P>
     *
     * </P>*/

    private View setFacebook_education(String schoole_name,String school_type,String year)
    {
        @SuppressLint("InflateParams") View view=inflater.inflate(R.layout.facebook_education_item,null);
        TextView school_name=(TextView)view.findViewById(R.id.school_name);
        school_name.setTypeface(GothamRoundedLight);
        school_name.setText(schoole_name);
        TextView type=(TextView)view.findViewById(R.id.schoole_type);
        type.setTypeface(GothamRoundedLight);
        type.setText(school_type);
        TextView school_year=(TextView)view.findViewById(R.id.schooling_year);
        school_year.setTypeface(GothamRoundedLight);
        school_year.setText(year);
        return view;
    }

    /**
     * <h2>setFacebook_likes</h2>
     * <P>
     *
     * </P>*/

    private View setFacebook_work(String employer,String location_details,String work_position,String work_start_date)
    {
        @SuppressLint("InflateParams") View view=inflater.inflate(R.layout.facebook_user_work_item,null);
        TextView employer_name=(TextView)view.findViewById(R.id.employer_name);
        employer_name.setText(employer);
        employer_name.setTypeface(GothamRoundedLight);
        LinearLayout location_Dtails=(LinearLayout)view.findViewById(R.id.location_details);
        TextView location=(TextView)view.findViewById(R.id.location);
        location.setTypeface(GothamRoundedLight);
        if(location_details!=null&&!location_details.isEmpty())
        {
            location.setText(location_details);
        }else
        {
            location_Dtails.setVisibility(View.GONE);
        }
        LinearLayout position_details=(LinearLayout)view.findViewById(R.id.position_details);
        TextView position=(TextView)view.findViewById(R.id.position);
        position.setTypeface(GothamRoundedLight);
        if(work_position!=null&&!work_position.isEmpty())
        {
            position.setText(work_position);
        }else
        {
            position_details.setVisibility(View.GONE);
        }

        LinearLayout linearLayout=(LinearLayout)view.findViewById(R.id.date_details);
        TextView start_date=(TextView)view.findViewById(R.id.start_date);
        start_date.setTypeface(GothamRoundedLight);
        if(work_start_date!=null&&!work_start_date.isEmpty())
        {
            start_date.setText(work_start_date);
        }else
        {
            linearLayout.setVisibility(View.GONE);
        }
        return view;

    }


    /**
     * <h2>get_Target_setup</h2>
     * <P>
     *
     * </P>*/
    private Target get_Target_setup(final ImageView imageView)
    {
        return new Target()
        {
            @SuppressLint("NewApi")
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
            {
                imageView.setBackground(new BitmapDrawable(context.getResources(),bitmap));

            }

            @SuppressLint("NewApi")
            @Override
            public void onBitmapFailed(Drawable errorDrawable)
            {
                imageView.setBackground(errorDrawable);
            }

            @SuppressLint("NewApi")
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable)
            {
                imageView.setBackground(placeHolderDrawable);

            }
        };
    }

    /**
     * <h2>getUSerSettingDetails</h2>
     * <P>
     *     Getting the user setting details like user height,weight etc.
     *     from the server.
     * </P>*/
    private void getUSerSettingDetails(final LinearLayout linearLayout,final ProgressBar progressBar)
    {
        SessionManager sessionManager=new SessionManager(context);
        if(Utility.isNetworkAvailable(context))
        {
            if (progressBar != null)
            {
                progressBar.setVisibility(View.VISIBLE);
            }

            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(context));
                jsonObject.put("ent_sess_token",sessionManager.getToken());
                jsonObject.put("ent_fbid",sessionManager.getFbId());
            } catch (JSONException e)
            {
                if (progressBar != null)
                {
                    progressBar.setVisibility(View.GONE);
                }
                e.printStackTrace();
            }

            Log.d("Getting user id:",jsonObject.toString());

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_USER_SETTING, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result) {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                    Log.d("Called time", result);

                    isDataFound=true;

                    Gson gson = new Gson();
                    SetPreferencePojo preferencePojo = gson.fromJson(result, SetPreferencePojo.class);
                    /**
                     * Handel of the response.*/
                    addPreferenceDatas(preferencePojo, linearLayout);
                }

                @Override
                public void onError(String error) {
                    if (progressBar != null) {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            });


        }else
        {
            Toast.makeText(context, "No Internet connection..!", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * This method will add all set preference datas at run time
     */
    private void addPreferenceDatas(SetPreferencePojo responsedata,LinearLayout linear_inflatePref)
    {
        if ("0".equals(responsedata.getErrorFlag()))
        {
            SetPreferenceAlData setPreferenceAlData;
            TextView header, selectedVallue;
            Activity activity = (Activity) context;
            for (int i = 0; i < responsedata.getData().size(); i++)
            {
                setPreferenceAlData = responsedata.getData().get(i);
                if(!setPreferenceAlData.getPreferenceTitle().equals("Age")&&!setPreferenceAlData.getPreferenceTitle().equals("Distance")&&!setPreferenceAlData.getPreferenceTitle().equals("Gender"))
                {
                    @SuppressLint("InflateParams") View child = activity.getLayoutInflater().inflate(R.layout.usersettingitem, null);
                    header = (TextView) child.findViewById(R.id.headeritem);
                    header.setTypeface(GothamRoundedBold);
                    header.setText(setPreferenceAlData.getPreferenceTitle());
                    selectedVallue = (TextView) child.findViewById(R.id.selected);
                    selectedVallue.setTypeface(GothamRoundedBold);

                    switch (setPreferenceAlData.getTypeOfPreference()) {
                        case "3":

                            if (setPreferenceAlData.getSelected_max() != null) {
                                selectedVallue.setText(setPreferenceAlData.getSelected_max());
                            } else {
                                selectedVallue.setText(context.getString(R.string.notSet));
                            }

                            break;
                        case "4":
                            if (setPreferenceAlData.getSelected() != null && setPreferenceAlData.getSelected().size() > 0)
                            {
                                StringBuilder builder = new StringBuilder();
                                for (String value : setPreferenceAlData.getSelected())
                                {
                                    builder.append(value);
                                    builder.append(" ");
                                }
                                selectedVallue.setText(builder);
                            } else
                            {
                                selectedVallue.setText(context.getString(R.string.notSet));
                            }

                            break;
                        default:
                            if (setPreferenceAlData.getSelected() != null && setPreferenceAlData.getSelected().size() > 0)
                            {
                                StringBuilder builder = new StringBuilder();
                                for (String value : setPreferenceAlData.getSelected())
                                {
                                    builder.append(value);
                                    builder.append(" ");
                                }
                                selectedVallue.setText(builder);
                            } else {
                                selectedVallue.setText(context.getString(R.string.notSet));
                            }
                            break;
                    }
                    linear_inflatePref.addView(child);
                }
            }
        }

    }
}
