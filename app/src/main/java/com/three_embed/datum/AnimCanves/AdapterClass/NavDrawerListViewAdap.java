package com.three_embed.datum.AdapterClass;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import com.three_embed.datum.Aramis_home.HomePageActivity;
import com.three_embed.datum.Aramis_home.ProfileAppSetting;
import com.three_embed.datum.Aramis_home.R;
import com.three_embed.datum.Aramis_home.RecentVisitors;
import com.three_embed.datum.Aramis_home.SetPreferenceActivty;

/**
 * @since  12/1/16.
 */
public class NavDrawerListViewAdap extends ArrayAdapter<String> implements AdapterView.OnItemClickListener
{
    private AppCompatActivity activity;
    private String[] drawerHeadingTitle;
    private String[] drawerHeadingName;
    private int[] drawerIcon;
    private HomePageActivity homePageActivity;
    SessionManager sessionManager;

    public NavDrawerListViewAdap(AppCompatActivity activity, String[] drawerHeadingTitle,String[] drawerHeadingName,int[] drawerIcon)
    {
        super(activity,0,drawerHeadingName);
        this.activity=activity;
        homePageActivity=(HomePageActivity)activity;
        this.drawerHeadingTitle=drawerHeadingTitle;
        this.drawerHeadingName=drawerHeadingName;
        this.drawerIcon=drawerIcon;
        sessionManager = new SessionManager(homePageActivity);
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        if (convertView==null)
        {
            LayoutInflater inflater= (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.single_row_profile_item,null);
            viewHolder=new ViewHolder();
            viewHolder.iV_discoveryIcon= (ImageView) convertView.findViewById(R.id.iV_discoveryIcon);
            viewHolder.tV_profileTitle= (TextView) convertView.findViewById(R.id.tV_profileTitle);
            viewHolder.tV_profileName= (TextView) convertView.findViewById(R.id.tV_profileName);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        viewHolder.iV_discoveryIcon.setImageResource(drawerIcon[position]);
        viewHolder.tV_profileTitle.setText(drawerHeadingTitle[position]);
        viewHolder.tV_profileTitle.setTypeface(homePageActivity.GothamRoundedMedium);
        viewHolder.tV_profileName.setText(drawerHeadingName[position]);
        viewHolder.tV_profileName.setTypeface(homePageActivity.GothamRoundedLight);
        return convertView;
    }

    class ViewHolder
    {
        ImageView iV_discoveryIcon;
        TextView tV_profileTitle,tV_profileName;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        DrawerLayout drawer = (DrawerLayout)activity.findViewById(R.id.drawer_layout);

        switch (position)
        {
            /**
             * opening ProfileDiscoveryPref activity
             */
            case 0:
                if (drawer != null) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                VariableConstant.isComming_fom_Home=true;
                Intent discoveryIntent=new Intent(activity,SetPreferenceActivty.class);
                activity.startActivity(discoveryIntent);
                activity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;
            /**
             * opening ProfileAppSetting activity
             */
            case 1:
                assert drawer != null;
                drawer.closeDrawer(GravityCompat.START);
                Intent appSettingIntent=new Intent(activity,ProfileAppSetting.class);
                activity.startActivity(appSettingIntent);
                activity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;
            /**
             * * Contact Datum
             */
            case 2:
                assert drawer != null;
                drawer.closeDrawer(GravityCompat.START);
                homePageActivity.contact_us.Show_Contact_Us_dilog_builder(activity);
                break;
            /**
             *  Share Datum
             */
            case 3:
                if (drawer != null)
                {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Utility.shareApp(activity, homePageActivity.getResources().getString(R.string.app_name), sessionManager.getFirstName()+" wants you to download"+homePageActivity.getResources().getString(R.string.app_name));
                break;

        }
        }
    }

