package com.three_embed.datum.AdapterClass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.three_embed.datum.Aramis_home.R;

import java.util.ArrayList;

/**
 * Created by adminpc on 2/2/16.
 */
public class UserProfileAdapter extends ArrayAdapter<String>
{
    private Context context;
    private ArrayList<String> aLUserProfile;
    private LayoutInflater inflater;

    public UserProfileAdapter(Context context,ArrayList<String> aLUserProfile) {
        super(context,0,aLUserProfile);
        this.context=context;
        this.aLUserProfile=aLUserProfile;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        convertView=inflater.inflate(R.layout.single_row_user_profile,null);
        TextView tV_userName= (TextView) convertView.findViewById(R.id.tV_userName);
        TextView tv_distance= (TextView) convertView.findViewById(R.id.tv_distance);
        TextView tV_AboutUser= (TextView) convertView.findViewById(R.id.tV_AboutUser);

        String user_name="name"+","+"age";
        //String distance="distance"+"meters"+"away";

        /**
         * setting user name,age & gender
         */
        tV_userName.setText(user_name);

        /**
         * setting distance
         */
       // tv_distance.setText(distance);

        /**
         * about
         */
        tV_AboutUser.setText("HELLO");

        return convertView;
    }
}
