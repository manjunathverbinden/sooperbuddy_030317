package com.three_embed.datum.AdapterClass;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.three_embed.datum.DataHolder.EachCardviewItem;
import com.three_embed.datum.Pojo_classes.FindMatchesDatas;
import com.three_embed.datum.Aramis_home.HomePageActivity;
import com.three_embed.datum.Aramis_home.R;
import java.util.List;

/**
 * @since 2/1/16.
 */
public class CardviewAdapter  extends BaseAdapter
{
    public List<FindMatchesDatas> parkingList;
    public Context mcontext;
    private final LayoutInflater mLayoutInflater;
    private HomePageActivity homePageActivity=null;

    public CardviewAdapter(List<FindMatchesDatas> apps,Context context)
    {
        this.parkingList = apps;
        this.mcontext = context;
        this.homePageActivity= (HomePageActivity) mcontext;
        this.mLayoutInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount()
    {
        return parkingList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position,View convertView,ViewGroup parent)
    {
        View cardview = convertView;
        EachCardviewItem eachCardviewItem;
        if (cardview == null)
        {
            cardview = mLayoutInflater.inflate(R.layout.item, parent, false);
            eachCardviewItem =EachCardviewItem.getInstance();
            eachCardviewItem.background = (FrameLayout) cardview.findViewById(R.id.background);
            eachCardviewItem.cardImage = (ImageView) cardview.findViewById(R.id.cardImage);
            eachCardviewItem.userInfo_tv= (TextView) cardview.findViewById(R.id.userInfo);
            eachCardviewItem.userInfo_tv.setTypeface(homePageActivity.GothamRoundedMedium);
            eachCardviewItem.Image_count= (TextView) cardview.findViewById(R.id.Image_count);
            eachCardviewItem.Image_count.setTypeface(homePageActivity.GothamRoundedMedium);
            eachCardviewItem.video_play_button= (ImageView) cardview.findViewById(R.id.video_play_button);
            eachCardviewItem.progressBar= (ProgressBar) cardview.findViewById(R.id.progress_bar);
            eachCardviewItem.cardImage.setTag(getTarget(eachCardviewItem.cardImage, eachCardviewItem.progressBar));
            cardview.setTag(eachCardviewItem);

        } else
        {
            eachCardviewItem = (EachCardviewItem)convertView.getTag();
        }

        /**
         * Hiding the progress bar for top view.*/
        if(position==1)
        {
            eachCardviewItem.progressBar.setVisibility(View.GONE);
        }

        /**
         * Checking video item having the video view.*/
        if(parkingList.get(position).getVideoThumnail()!=null&&!parkingList.get(position).getVideoThumnail().isEmpty())
        {
            eachCardviewItem.video_play_button.setVisibility(View.VISIBLE);
            Picasso.with(mcontext)
                    .load(parkingList.get(position).getVideoThumnail())
                    .error(R.drawable.default_user_rectangle)
                    .placeholder(R.drawable.default_user_rectangle)
                    .into((Target) eachCardviewItem.cardImage.getTag());

        }else if(parkingList.get(position).getpPic()!=null&&!parkingList.get(position).getpPic().equals(""))
        {
            eachCardviewItem.video_play_button.setVisibility(View.GONE);
            String url_data=parkingList.get(position).getpPic();
            if(url_data.contains("http://"))
            {
                url_data=url_data.replace("http://","https://");
            }
            Picasso.with(mcontext)
                    .load(url_data)
                    .error(R.drawable.default_user_rectangle)
                    .placeholder(R.drawable.default_user_rectangle)
                    .into((Target) eachCardviewItem.cardImage.getTag());
        }

        String user_info=parkingList.get(position).getFirstName()+" , "+parkingList.get(position).getAge();
        eachCardviewItem.userInfo_tv.setText(user_info);
        eachCardviewItem.Image_count.setText(parkingList.get(0).getImgCount());
        return cardview;
    }

    private Target getTarget(final ImageView imageView,final ProgressBar image_loader)
    {
        return new Target()
        {
            @SuppressLint("NewApi")
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
            {
                image_loader.setVisibility(View.GONE);
                imageView.setBackground(new BitmapDrawable(mcontext.getResources(), bitmap));

            }

            @SuppressLint("NewApi")
            @Override
            public void onBitmapFailed(Drawable errorDrawable)
            {
                image_loader.setVisibility(View.GONE);
                imageView.setBackground(errorDrawable);
            }

            @SuppressLint("NewApi")
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable)
            {
                imageView.setBackground(placeHolderDrawable);
                image_loader.setVisibility(View.VISIBLE);
            }
        };
    }

}
