package com.three_embed.datum.Aleret_dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Aramis_home.R;

/**
 *@since 4/6/16.
 * @author 3Embed.
 */
public class Contact_Us
{

    private static Contact_Us ALERET_DILOG_CHOOSER=null;
    private Dialog item_chooser_dialog;

    private Contact_Us()
    {}

    public static Contact_Us getInstance()
    {
        if(ALERET_DILOG_CHOOSER!=null)
        {
            return ALERET_DILOG_CHOOSER;
        }else
        {
            ALERET_DILOG_CHOOSER=new Contact_Us();
            return ALERET_DILOG_CHOOSER;
        }

    }

    /**
     * <h2>Show_aleret_dilog_builder</h2>
     * <P>
     *     Open a aleret and Show that the user to choose message type.
     * </P>
     * @param mactivity contains the activity reference.
     */
    public void Show_Contact_Us_dilog_builder(final Activity mactivity)
    {
        if(item_chooser_dialog!=null)
        {
            if(item_chooser_dialog.isShowing())
            {
                item_chooser_dialog.dismiss();
            }
        }

        /**
         * Fonts for hum tum*/
        Typeface gothamRoundedMedium = Typeface.createFromAsset(mactivity.getAssets(), "fonts/MyriadPro-Regular.otf");
        Typeface gothamRoundedBold = Typeface.createFromAsset(mactivity.getAssets(), "fonts/MyriadPro-Bold.otf");


        ColorDrawable cd=new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        item_chooser_dialog = new Dialog(mactivity);
        item_chooser_dialog.getWindow().setBackgroundDrawable(cd);
        item_chooser_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        item_chooser_dialog.getWindow().getAttributes().windowAnimations= R.style.Windows_animation;
        LayoutInflater inflater = mactivity.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.contact_us_layout, null);
        TextView title = (TextView) dialogView.findViewById(R.id.title);
        title.setTypeface(gothamRoundedBold);

        TextView item_text_noraml = (TextView) dialogView.findViewById(R.id.user_id);
        item_text_noraml.setTypeface(gothamRoundedMedium);

        TextView item_text_view_snap_chat = (TextView) dialogView.findViewById(R.id.gallery_text);
        item_text_view_snap_chat.setTypeface(gothamRoundedMedium);

        RelativeLayout parent_layout=(RelativeLayout)dialogView.findViewById(R.id.parent_layout);
        parent_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                item_chooser_dialog.dismiss();
            }
        });

        RelativeLayout normal=(RelativeLayout)dialogView.findViewById(R.id.normal);
        normal.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                item_chooser_dialog.dismiss();
                Utility.sendEmail(mactivity,mactivity.getString(R.string.report_issue_text),mactivity.getString(R.string.supportmail));
            }
        });
        RelativeLayout snap_chat=(RelativeLayout)dialogView.findViewById(R.id.snap_chat);
        snap_chat.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                item_chooser_dialog.dismiss();
                Utility.sendEmail(mactivity,mactivity.getString(R.string.suggestion_text),mactivity.getString(R.string.supportmail));
            }
        });


        Button cancel=(Button)dialogView.findViewById(R.id.acncel_button);
        cancel.setTypeface(gothamRoundedMedium);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                item_chooser_dialog.dismiss();
            }
        });
        item_chooser_dialog.setContentView(dialogView);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = item_chooser_dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        item_chooser_dialog.setCancelable(true);
        item_chooser_dialog.show();
    }


}
