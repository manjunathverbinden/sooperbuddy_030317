package com.three_embed.datum.Aleret_dialogs;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.three_embed.datum.Aramis_home.R;

/**
 * Created by shobhit on 13/5/16.
 */
public class User_hint_popup_layout
{
    private static User_hint_popup_layout user_hint_popup_layout=null;
    private PopupWindow user_hint=null;
    private LayoutInflater inflater = null;
    private TextView user_hint_tv=null;
    /**
     * Making it single tone*/
    User_hint_popup_layout()
    {}

    public static User_hint_popup_layout getInsatance()
    {
        if(user_hint_popup_layout==null)
        {
            return user_hint_popup_layout =new User_hint_popup_layout();
        }else
        {
            return user_hint_popup_layout ;
        }
    }


    /**
     * <h2>show_popup_block_user</h2>
     * <P>
     *
     * </P>*/

    public PopupWindow show_popup_user_hint(final View view,final Activity activity,String hint_text)
    {
        inflater = activity.getLayoutInflater();
        if(user_hint==null)
        {
            user_hint=new PopupWindow(activity);
        }else
        {
            user_hint.dismiss();
        }
        int popupWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
        int popupHeight = ViewGroup.LayoutParams.WRAP_CONTENT;
        View popupView = inflater.inflate(R.layout.user_hint_popup_layout,null);
        user_hint_tv=(TextView)popupView.findViewById(R.id.user_hint);
        user_hint_tv.setText(hint_text);
        user_hint.setFocusable(true);
        user_hint.setContentView(popupView);
        user_hint.setWidth(popupWidth);
        user_hint.setHeight(popupHeight);
        user_hint.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        view.post(new Runnable() {
            public void run() {
                user_hint.showAsDropDown(view,20,20);
            }
        });

        return user_hint;
    }


}
