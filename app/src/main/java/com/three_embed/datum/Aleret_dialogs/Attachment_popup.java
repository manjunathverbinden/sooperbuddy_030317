package com.three_embed.datum.Aleret_dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.three_embed.datum.Aramis_home.Chat;
import com.three_embed.datum.Aramis_home.R;
/**
 * <h2>Attachment_popup</h2>
 * <P>
 *     Showing aleret box to provide a chooser.
 * </P>
 * @author 3Embed
 * @since 14.06.2016
 */
public class Attachment_popup
{
    private static Attachment_popup Attach_POPUP=new Attachment_popup();
    private PopupWindow report_popup=null;

    private Attachment_popup()
    {
    }

    public static Attachment_popup getInstance()
    {
        if(Attach_POPUP==null)
        {
            Attach_POPUP=new Attachment_popup();
            return Attach_POPUP;
        }else
        {
            return Attach_POPUP;
        }
    }


    public void show_popup(View view,final Activity activity)
    {

        /**
         * Fonts for hum tum*/
        Typeface gothamRoundedMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/MyriadPro-Regular.otf");

        final Chat chat=(Chat)activity;
        LayoutInflater inflater = activity.getLayoutInflater();
        if(report_popup==null)
        {
            report_popup=new PopupWindow(activity);
        }
        ColorDrawable cd=new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        int popupWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
        int popupHeight = ViewGroup.LayoutParams.WRAP_CONTENT;
        @SuppressLint("InflateParams") View popupView = inflater.inflate(R.layout.socket_custom_dialog_options_menu, null);
        LinearLayout camera = (LinearLayout) popupView.findViewById(R.id.layoutPhoto);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                report_popup.dismiss();
                /**
                 * 0 for camera intent.*/
                chat.Open_attachment(0);
            }
        });

        TextView camera_tv = (TextView) popupView.findViewById(R.id.tvPhoto);
        camera_tv.setTypeface(gothamRoundedMedium);

        LinearLayout gallery = (LinearLayout) popupView.findViewById(R.id.layoutGallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                report_popup.dismiss();
                /**
                 * 1 for galery intent.*/
                chat.Open_attachment(1);

            }
        });

        TextView gallery_tv = (TextView) popupView.findViewById(R.id.tvGallery);
        gallery_tv.setTypeface(gothamRoundedMedium);

        report_popup.setBackgroundDrawable(cd);
        report_popup.setFocusable(true);
        report_popup.setWidth(popupWidth);
        report_popup.setHeight(popupHeight);
        report_popup.setContentView(popupView);
        report_popup.showAsDropDown(view,
                -150,-1);
    }


}
