package com.three_embed.datum.Aleret_dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.three_embed.datum.Utility.CircleTransform;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Aramis_home.Chat;
import com.three_embed.datum.Aramis_home.R;

/**
 * @since  20/2/16.
 * @author 3Embed.
 */
public class Its_Match_aleret
{
    private static Its_Match_aleret MATCH_ALERET=new Its_Match_aleret();
    private Dialog dialog_of_app_purches;
    private ImageView like_image,liked_image;

    private Its_Match_aleret()
    {
    }

    public static Its_Match_aleret getInstance()
    {
        if(MATCH_ALERET==null)
        {
            MATCH_ALERET=new Its_Match_aleret();
            return MATCH_ALERET;
        }else
        {
            return MATCH_ALERET;
        }
    }

    public void show_info_aleret(final Activity mactivity,String image_url_like,final String url_liked, final String name_liked, final String liked_email_id, final String liked_fb_id)
    {
        if(dialog_of_app_purches!=null)
        {
            if(dialog_of_app_purches.isShowing())
            {
                dialog_of_app_purches.dismiss();
            }
        }

        /**
         * Fonts for hum tum*/
        Typeface gothamRoundedMedium = Typeface.createFromAsset(mactivity.getAssets(), "fonts/MyriadPro-Regular.otf");
        Typeface gothamRoundedBold = Typeface.createFromAsset(mactivity.getAssets(), "fonts/MyriadPro-Bold.otf");
        Typeface stylish_data = Typeface.createFromAsset(mactivity.getAssets(), "fonts/MyriadPro-Regular.otf");

        ColorDrawable cd=new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        dialog_of_app_purches = new Dialog(mactivity);
        dialog_of_app_purches.getWindow().setBackgroundDrawable(cd);
        dialog_of_app_purches.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = mactivity.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.its_a_match_screen, null);

        TextView title=(TextView)dialogView.findViewById(R.id.matchScreen);
        title.setTypeface(stylish_data);

        like_image=(ImageView)dialogView.findViewById(R.id.iV_left);

        liked_image=(ImageView)dialogView.findViewById(R.id.iV_right);

        TextView name_persions=(TextView)dialogView.findViewById(R.id.name_persions);
        name_persions.setText(String.format("You and %s have liked each other.", name_liked));


        Picasso.with(mactivity).load(Uri.parse(image_url_like)).transform(new CircleTransform()).fit().into(like_image, new Callback() {
            @Override
            public void onSuccess() {

            }

            @SuppressLint("NewApi")
            @Override
            public void onError() {
                like_image.setBackground(ContextCompat.getDrawable(mactivity, R.drawable.chat_profile_default_image_frame));
            }
        });

        Picasso.with(mactivity).load(Uri.parse(url_liked)).transform(new CircleTransform()).fit().into(liked_image, new Callback() {
            @Override
            public void onSuccess() {

            }

            @SuppressLint("NewApi")
            @Override
            public void onError() {
                liked_image.setBackground(ContextCompat.getDrawable(mactivity, R.drawable.chat_profile_default_image_frame));
            }
        });

        TextView b1_text=(TextView)dialogView.findViewById(R.id.fist_b_txt);
        b1_text.setTypeface(gothamRoundedMedium);

        TextView b2_text=(TextView)dialogView.findViewById(R.id.second_b_txt);
        b2_text.setTypeface(gothamRoundedMedium);

        RelativeLayout send_message=(RelativeLayout)dialogView.findViewById(R.id.send_message_button);
        send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mactivity, Chat.class);
                intent.putExtra("USER_EMAIL", liked_email_id);
                intent.putExtra("USER_FIRST_NAME", name_liked);
                intent.putExtra("USER_PROFILE_IMAGE_URL", url_liked);
                intent.putExtra("USER_AGE", "22");
                intent.putExtra("USER_FB_ID", liked_fb_id);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mactivity.startActivity(intent);
                dialog_of_app_purches.dismiss();
            }
        });

        RelativeLayout keep_swapping=(RelativeLayout)dialogView.findViewById(R.id.keep_swapping);
        keep_swapping.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog_of_app_purches.dismiss();
            }
        });

        TextView tell_your_friend=(TextView) dialogView.findViewById(R.id.tell_your_friend_share);
        tell_your_friend.setTypeface(gothamRoundedBold);

        tell_your_friend.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog_of_app_purches.dismiss();
                Utility.shareApp(mactivity, mactivity.getString(R.string.app_name), mactivity.getString(R.string.shareText) + "\n" + mactivity.getString(R.string.gotolinktext) + mactivity.getString(R.string.holifireweblink));
            }
        });


        dialog_of_app_purches.setContentView(dialogView);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog_of_app_purches.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        dialog_of_app_purches.setCancelable(false);
        dialog_of_app_purches.show();
    }
}

