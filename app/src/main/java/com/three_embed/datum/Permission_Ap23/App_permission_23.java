package com.three_embed.datum.Permission_Ap23;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;
import com.three_embed.datum.Aramis_home.R;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>App_permission_23</h1>
 * <P>
 *
 * </P>*/
public class App_permission_23
{
    final public int REQUEST_CODE_PERMISSIONS=786;
    private List<String> permissionsNeeded=null;
    private List<String> permissionsList=null;
    private AlertDialog dialog_parent=null;

    public enum Permission
    {
        LOCATION,CAMERA,READ_EXTERNAL_STORAGE,WRITE_EXTERNAL_STORAGE,PHONE,RECORD_AUDIO
    }

    /**
     * Single tone class object.*/
    private static App_permission_23  app_permission=new App_permission_23();

    /**
     * <P>
     *     Private constructure to make this class as Single tone.
     * </P>
     */
    private App_permission_23()
    {}

    /**
     * <h2>getInstance</h2>
     * <p>
     *     Method need to acess the Object of this single tone class
     * </p>*/
    public static App_permission_23 getInstance()
    {
        return app_permission;
    }


    public boolean getPermission(final ArrayList<Permission> permission_list,Activity activity,boolean isFixed)
    {
        /**
         * Creating the List if not created .
         * if created then clear the list for refresh use.*/
        if(permissionsNeeded==null||permissionsList==null)
        {
            permissionsNeeded= new ArrayList<String>();
            permissionsList= new ArrayList<String>();
        }else
        {
            permissionsNeeded.clear();
            permissionsList.clear();
        }

if(dialog_parent!=null&&dialog_parent.isShowing())
{
    dialog_parent.dismiss();
    dialog_parent.cancel();
}
        for(int count=0;permission_list!=null&&count<permission_list.size();count++)
        {
            switch (permission_list.get(count))
            {
                case LOCATION:
                    if (!   addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION,activity))
                    {
                        permissionsNeeded.add("GPS Fine Location");
                    }
                    if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION,activity))
                    {
                        permissionsNeeded.add("GPS Course Location");
                    }
                    break;
                case RECORD_AUDIO:
                    if (!   addPermission(permissionsList,Manifest.permission.RECORD_AUDIO,activity))
                    {
                        permissionsNeeded.add("Record audio");
                    }
                    break;
                case CAMERA:
                    if (!addPermission(permissionsList, Manifest.permission.CAMERA,activity))
                    {
                        permissionsNeeded.add("Camera");
                    }
                    break;
                case READ_EXTERNAL_STORAGE:
                    if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE,activity))
                    {
                        permissionsNeeded.add("Write to external Storage");
                    }
                    break;
                case WRITE_EXTERNAL_STORAGE:
                    if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE,activity))
                    {
                        permissionsNeeded.add("Read to external Storage");
                    }
                    break;
                case PHONE:
                    if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE,activity))
                    {
                        permissionsNeeded.add("Read Phone State");
                    }
                    break;
                default:
                    break;
            }

        }
        if (permissionsList.size() > 0&&permissionsNeeded.size() > 0)
        {
            String message = "You need to grant access to " + permissionsNeeded.get(0);
            for (int i = 1; i < permissionsNeeded.size(); i++)
            {
                message = message + ", " + permissionsNeeded.get(i);
            }
            showMessageOKCancel_camera(message,activity,isFixed);
            return false;
        }
        else
        {
            return true;
        }
    }

    private boolean addPermission(List<String> permissionsList,String permission,Activity activity)
    {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
        {
            permissionsList.add(permission);
            return false;
        }else
        {
            return true;
        }
    }


    private void showMessageOKCancel_camera(final String message,final Activity mactivity,final boolean isFixed)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mactivity);
        alertDialog.setTitle("Note.");
        alertDialog.setMessage(message);
        alertDialog.setIcon(R.drawable.launch_con);
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();
                check_for_Permission(permissionsList.toArray(new String[permissionsList.size()]),mactivity);
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                if (isFixed) {
                    Toast.makeText(mactivity, "To Proceed fourther App need " + "\n" + message, Toast.LENGTH_LONG).show();
                    dialog.cancel();
                    mactivity.onBackPressed();
                } else {
                    Toast.makeText(mactivity, "To Proceed fourther App need " + "\n" + message, Toast.LENGTH_LONG).show();
                    dialog.cancel();
                }

            }
        });
        dialog_parent = alertDialog.show();
        dialog_parent.show();
    }


    public void check_for_Permission(String permissions[],Activity mactivity)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mactivity.requestPermissions(permissions,REQUEST_CODE_PERMISSIONS);
        }
    }
}