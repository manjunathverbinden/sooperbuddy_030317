package com.three_embed.datum.Utility;

/**
 * @since  11/12/15.
 */
public class VariableConstant
{
    /**
     * Cognito pool id for the aws and bucket constance.*/
    public static final String COGNITO_POOL_ID ="us-west-2:3ebce8d4-35f9-496c-8297-e6c9277dff2d"; /**Use your s3 cagnito poll id. Ex:us-east-1:fxxaxxxa-xxxa-441f-abe9-axxxaxxaa */
    public static final String AWS_BUCKET_NAME ="aramis-app"; /**Use your package folder name here  which is in S3 console.,Like : MyApp */
    public static final String PROFILE_PICTURE ="aramis-app/profille_picture"; //Your profile picture s3 console package.
    public static final String CHAT_IMAGES="aramis-app/chat_pic"; //Your chat picture s3 console package.
    public static final String BUCKET_NAME_CHAT_VIDEO ="aramis-app/chat_video"; //Your chat video s3 console package.


    public static final String SET_LIKES="1";
    public static final String SET_DIS_LIKES="2";
    public static final String FB_AUTHENTICATION_TYPE="1";
    public static final String DEVICE_TYPE="2";

    /**
     * Profile picture path.*/
    public static String profile_picture_path="";

    /**
     * Chat picture path.*/
    public static String chat_picture_path="";
    /**
     * User log out flag*/
    public static boolean isLogOut=false;

    /**
     * Managning the class flow*/
    public static boolean isComming_fom_Home=false;

    /**
     * Managning the class chat screen flow*/
    public static boolean isInChatScreen=false;

    /**
     * Checking weather a comming from preference class in to homepage.
     * always true to set both the condition.
     * */
    public static boolean isComming_from_prference=true;


    public static int LOCATION_ACTIVITY_REQUEST_CODE=1234;

    /**
     * Edit profile first time click then.*/
    public static boolean IS_FOR_FIRST_TIME_CLICK=true;
    /**
     * Like button first time pressed*/
    public static boolean isNotCommingFromHomePage=false;


}
