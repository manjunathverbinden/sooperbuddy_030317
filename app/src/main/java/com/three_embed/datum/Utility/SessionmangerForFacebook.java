package com.three_embed.datum.Utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @since  on 6/5/16.
 */
public class SessionmangerForFacebook
{
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    public static final String PREF_NAME="datum_facebook_session_manager";
    private static final String USER_FB_LIKE="userfb_likes_data";
    private static final String USER_FB_EDUCATION="userfb_education_data";
    private static final String USER_FB_WORKS="userfb_works_data";
    private static final String USER_FB_FRIENDS="userfb_friends_data";


    public SessionmangerForFacebook(Context context)
    {
        int PRIVATE_MODE = 0;
        preferences= context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor=preferences.edit();
        editor.apply();
    }


    public void clear_sessionmangerForFacebook()
    {
        editor.clear();
        editor.commit();
    }

    public void setUserFbWorks(String data)
    {
        editor.putString(USER_FB_WORKS, data);
        editor.commit();
    }

    public String getUserFbWorks()
    {
        return preferences.getString(USER_FB_WORKS,"");
    }


    public void setUserFbFriends(String data)
    {
        editor.putString(USER_FB_FRIENDS,data);
        editor.commit();
    }

    public String getUserFbFriends()
    {
        return preferences.getString(USER_FB_FRIENDS,"");
    }


    public void setUserFbEducation(String data)
    {
        editor.putString(USER_FB_EDUCATION,data);
        editor.commit();
    }

    public String getUserFbEducation()
    {
        return preferences.getString(USER_FB_EDUCATION,"");
    }

    public void setUserFbLike(String data)
    {
        editor.putString(USER_FB_LIKE,data);
        editor.commit();
    }

    public String getUserFbLike()
    {
        return preferences.getString(USER_FB_LIKE,"");
    }

}
