package com.three_embed.datum.Utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.three_embed.datum.Aramis_home.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>Utility</h1>
 * <p>
 *     Class contains all the commonly required method .
 *     and Provide the as according to the required activity.
 * </p>
 * @author 3Embed
 * @since  23/11/15.
 * @version 1.0.
 */
public class Utility
{

    /**
     * <h2>printLog</h2>
     * <P>
     * Method is used to print the given parameter data using Log.information data.
     * @param  msg It is anonymous lent of data can br received.
     * </P>
     */
    public static void printLog(String... msg)
    {
        String str="";
        for(String i : msg)
        {
            str= str+"\n"+i;
        }
        if(true)
        {
            Log.i("Datum", str);
        }
    }

    /**
     * <p>
     *     This method is used to hide keyboard when
     *     user taps anywhere else on the screen.
     *     apart from edit text.
     * </p>
     * @param activity contains the activity reference.
     */
    public static void close_soft_input_keypad(Activity activity)
    {
        InputMethodManager imm= (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    /**
     * <h2>isNetworkAvailable</h2>
     * This method check weather there is network connectivity available or not .
     * By the help of the ConnectionManager .
     * <p>
     *    Here First checking the Connection manager object is created or not.
     *    Then checking the Build version is less then LOLLIPOP then reading network info else
     *    Reading the network list .
     *    Due to Deprecation of networkinfo in Lollipop and above.
     *    If created then reading all the list of the available Network.
     *    Then checking one by one is there any network is connected or not.
     *    Any one is Connected then setting true to boolean value .
     * </p>
     * @param mcontext contain the given context of the activity when created.*/

    public static boolean isNetworkAvailable(Context mcontext)
    {
        ConnectivityManager connectivity = (ConnectivityManager) mcontext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                Network[] networks = connectivity.getAllNetworks();
                NetworkInfo networkInfo;
                for (Network mNetwork : networks) {
                    networkInfo = connectivity.getNetworkInfo(mNetwork);
                    if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED))
                    {
                        return true;
                    }
                }
            }else
            {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (NetworkInfo anInfo : info)
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
            }
        }
        return false;
    }

    /**
     * <h2>getProgressDialog</h2>
     * <p>
     * This method is used to create a Progress Dialog with the Given Context
     * and set message as Lading and also set progress dialog as Cancelable i.e {@code setCancelable(true)}.
     * </p>
     * @param context contain the given context of the activity when created.*/
    public static ProgressDialog getProgressDialog(Context context)
    {
        ProgressDialog dialog;
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB)
        {
            dialog = new ProgressDialog(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
        }else{
            dialog = new ProgressDialog(context,ProgressDialog.THEME_HOLO_LIGHT);
        }
        dialog.setCancelable(true);
        dialog.setMessage("Lading....");
        return dialog;
    }

    /**
     * <h2>isEmailValid</h2>
     * Method is to simple validate Email String Pattern.
     *Return boolean that specify the inout parameter
     *match with given expression .
     * <p>
     *this method always return boolean. True for the given TextField text match with the expression.
     * if not match then return false and Showing DialogBox to say not match .
     * </P>
     *@author 3embed
     *@param email contain the email text field text
     *@return boolean it is true for Valid and false for invalid
     *@see Pattern
     *@see Matcher
     */
    public static boolean isEmailValid(String email)
    {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * <h2>getCurrentGmtTime</h2>
     * Method return the current Date Time
     * <p>
     *     This method get The Current Date in the format "yyyy-MM-dd HH:mm:ss"
     *     and Return in String form
     * </p>
     * @see SimpleDateFormat
     */
    public static String getCurrentGmtTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",Locale.getDefault());
        return sdf.format(new Date());
    }

    /**
     * <h2>getCurrentGmtTime</h2>
     * Method return the current Date Time
     * <p>
     *     This method get The Current Date in the format "yyyy-MM-dd HH:mm:ss"
     *     and Return in String form "02/12/2015 12:40:57"
     * </p>
     * @see SimpleDateFormat
     */
    public static String getCurrentGmtTime_for_block_service()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",Locale.getDefault());
        return sdf.format(new Date());
    }
    /**
     * <h2>getCurrentDate</h2>
     * Method return the current Date Time
     * <p>
     *     This method get The Current Date in the format "yyyy-MM-dd HH:mm:ss"
     *     and Return in String form
     * </p>
     * @see SimpleDateFormat
     */
    public static String getCurrentDate()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
        return sdf.format(new Date());
    }
    /**
     * <h2>getDeviceId</h2>
     * This method return the Device Id By the Help of the
     * TelephonyManager manager Object.And return it as
     * String value.
     */
    public static String getDeviceId(Context context)
    {

        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    /**
     * <h1>openDate_Picker</h1>
     * Method is used to open a date picker and Pict a date.
     * <p>
     *     This method open a Date Picker dialog by DatePickerDialog object.
     *     Hen set the Max time as Current Date by the help of the {@code fromDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime())}.
     *     Also listening the response From the DatePicker and setting the Date to the Given Edit text.
     * </p>
     * @param editText contains the Edit text reference to which data has to set.
     * @param  activity contains the Activity reference of the calling Activity. */
    private static Calendar newCalendar;
    public static void openDate_Picker(Activity activity,final EditText editText)
    {
        if(newCalendar==null)
        {
            newCalendar = Calendar.getInstance();
        }

        final DatePickerDialog fromDatePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                view.updateDate(year,monthOfYear,dayOfMonth);

                newCalendar.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
                editText.setText(dateFormatter.format(newCalendar.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        fromDatePickerDialog.show();

    }

    /**
     * simple function for sending mail
     */
    public static void sendEmail(Context context,String subject_line,String mail_id)
    {
        String[] TO = {mail_id};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject_line);
        emailIntent.putExtra(Intent.EXTRA_TEXT, deviceInfo(context));

        try
        {
            context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Share app sending mail
     */
    public static void shareApp(Context context,String subject_line,String extraText)
    {
        String emailId="";
        String[] TO = {emailId};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject_line);
        emailIntent.putExtra(Intent.EXTRA_TEXT, extraText);

        try
        {
            context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * <h2>copyStream</h2>
     *
     * */
    public static void copyStream(InputStream input, OutputStream output) throws IOException
    {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }

    /**
     * <h2>ShowAlertDialog</h2>
     *This method  use to Show alert Dialog.
     * @see AlertDialog
     * <p>
     *this method create a aleret dialog box and set message as given parameter
     * and show alert box .
     * The alert box get dimissed when click ok button.
     * </P>
     * @author 3embed
     * @param msg	it contain given string msg to show in alert box
     * @param activity contains the current calling Activity reference.
     * @return		nothing
     */
    public static void ShowAlertDialog(String msg,Activity activity)
    {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(activity);
        alertDialog2.setMessage(msg);

        alertDialog2.setPositiveButton(activity.getResources().getString(R.string.Ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog2.show();
    }

    /**
     * <h2>adult_age_Validation</h2>
     * Method is used to check user is adult or not
     * <P>
     *     This method is used to validate the weather user is adult or not by subtracting current date with the
     *     user date of birth date.
     *     if User is not Adult then setting text color as Red and error as user is not adult
     *     returning 0.
     *     else
     *     Returning returning age.
     * </P>*/
    public static int adult_age_validation(String date)
    {

        if(validate_Date_format(date))
        {
            Date current_date,user_dob;
            Calendar current_calendar,dob_calendar;
            int adult_age=0;
            String currentTime= Utility.getCurrentDate();
            @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try
            {
                current_date=dateFormat.parse(currentTime);
                user_dob=dateFormat.parse(date);
                current_calendar= getCalendar(current_date);
                dob_calendar=getCalendar(user_dob);
                adult_age=current_calendar.get(Calendar.YEAR)-dob_calendar.get(Calendar.YEAR);

            } catch (Exception e)
            {
                e.printStackTrace();
            }
            if(adult_age>=18)
            {
                return adult_age;
            }else
            {
                return adult_age;
            }
        }else
        {
            return 0;
        }


    }

    /**
     * <h2>validateDate_format</h2>
     * Method use to validate a given Date as in the format yyyy-mm-dd.
     * If matches then return true or else return false.
     * @param date contain the date String to validate.*/
    private static boolean validate_Date_format(String date)
    {
        String expression ="^\\d{4}-\\d{2}-\\d{2}$";
        Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(date);
        return matcher.matches();

    }
    /**
     * <h2>getCalendar</h2>
     * Method use to get Calender object from the given Date reference.
     * <p>
     *     Method receive a Date reference as parameter and then create the Calendar object and set the date as
     *     given date reference .
     *     Then return that calender object.
     * </p>
     * @param date contains the Date reference to set in calender.*/
    private static Calendar getCalendar(Date date)
    {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    /**
     * <h2>get_Target_Place_Holder</h2>
     * <P>
     *     Method call a service create a target place for the Image and handel the progress bar Visibility on Image down Load.
     * </P>
     * @param imageView contain the iamge view where Iamge has to down load.
     * @param progressBar contain the progressbar for show image down loading.
     * @return  return the target object to set iun Image.*/
    public static Target get_Target_Place_Holder(final Activity activity,final View imageView,final ProgressBar progressBar)
    {
        Target target=new Target()
        {
            @SuppressLint("NewApi")
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
            {
                imageView.setBackground(new BitmapDrawable(activity.getResources(),bitmap));
                if(progressBar!=null)
                {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @SuppressLint("NewApi")
            @Override
            public void onBitmapFailed(Drawable errorDrawable)
            {
                imageView.setBackground(errorDrawable);
                if(progressBar!=null)
                {
                    progressBar.setVisibility(View.GONE);
                }
            }

            @SuppressLint("NewApi")
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable)
            {
                imageView.setBackground(placeHolderDrawable);
                if(progressBar!=null)
                {
                    progressBar.setVisibility(View.VISIBLE);
                }

            }
        };
        return target;
    }

/**
 * <h3>convert_Message_Time</h3>
 * <P>
 *
 * </P>*/
   public static String convert_Message_Time(String timestamp)
    {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",Locale.US);
        Date date;
        try
        {
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = format.parse(timestamp);
           return time_converter(date.getTime());
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Defining the times array list.*/
    public static final List<Long> times = Arrays.asList(
            TimeUnit.DAYS.toMillis(365),
            TimeUnit.DAYS.toMillis(30),
            TimeUnit.DAYS.toMillis(1),
            TimeUnit.HOURS.toMillis(1),
            TimeUnit.MINUTES.toMillis(1),
            TimeUnit.SECONDS.toMillis(1));
    /**
     * Defining the times string data.*/
    public static final List<String> timesString = Arrays.asList("year","month","day","hour","minute","second");

    /**
     * Time converter data.*/
    private static String time_converter(long duration)
    {
        duration=System.currentTimeMillis()-duration;

        StringBuilder res = new StringBuilder();
        for(int i=0;i< Utility.times.size(); i++)
        {
            Long current = Utility.times.get(i);
            long temp = duration/current;
            if(temp>0)
            {
                res.append(temp).append(" ").append(Utility.timesString.get(i) ).append(temp > 1 ? "s" : "").append(" ago");
                break;
            }
        }
        if("".equals(res.toString()))
            return "0 second ago";
        else
            return res.toString();
    }
    private static String deviceInfo(Context context) {

        String s = "Device-infos:";
        s += "\n OS Version: " + System.getProperty("os.version");
        s +="\nApp Version:"+context.getResources().getString(R.string.appversion);
        s += "\n Model: " + android.os.Build.MODEL;
        return s;
    }
}
