package com.three_embed.datum.Utility;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import com.three_embed.datum.chat_lib.AppController;
import com.three_embed.datum.Aramis_home.Communication_receiver;
/**
 * Created by shobhit on 12/5/16.
 */
public class Application_checker_service extends Service
{

    private AppSession appSession;
    Communication_receiver receiver=null;
    IntentFilter filter = new IntentFilter(AppController.getInstance().CHAT_COMMUNICATION);
    @Override
    public IBinder onBind(Intent intent)
    {
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new Communication_receiver();
        this.registerReceiver(receiver,filter);
        return null;
    }

    /**
     * Called by the system when the service is first created.  Do not call this method directly.
     */
    @Override
    public void onCreate()
    {
        super.onCreate();
        appSession=new AppSession(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        /**
         * On Start command service assigning the app is live.*/
        appSession.setIsAppLive(true);
        Log.d("Message :", "Active !");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy()
    {
        /**
         * On destroy of service class.*/
        super.onDestroy();
    }

    public void onTaskRemoved(Intent rootIntent)
    {
        Log.d("Message :", "Killed !");
        /**
         * Setting false to say my app is killed.*/
        appSession.setIsAppLive(false);
        unRegister_Regd_Broadcast();
        stopSelf();
    }

    public void unRegister_Regd_Broadcast()
    {
        if(receiver!=null)
        {
            this.unregisterReceiver(receiver);
            receiver=null;
        }

    }
}
