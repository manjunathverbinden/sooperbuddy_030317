package com.three_embed.datum.Utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @since  6/4/16.
 */
public class AppSession
{
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    public static final String PREF_NAME="app_session_manager";
    private static final String GCM_REG_ID="app_gcmregisterid";
    private static final String IS_APP_LIVE="is_App_live";

    private static final String FB_ID="facebook_id";



    public AppSession(Context context)
    {
        /*
      "0 " for private mode of acess of data.*/
        int PRIVATE_MODE = 0;
        preferences= context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor=preferences.edit();
        editor.apply();
    }


    public void clear_Appsession()
    {
        editor.clear();
        editor.commit();
    }
    /**
     * <p>
     *     to save session token
     * </p>
     * @return String
     */
    public String getGcmRegId()
    {
        return preferences.getString(GCM_REG_ID,"");
    }
    public void setGcmRegId(String token)
    {
        editor.putString(GCM_REG_ID,token);
        editor.commit();
    }

    public void setIsAppLive(boolean status)
    {
        editor.putBoolean(IS_APP_LIVE, status);
        editor.commit();
    }

    public boolean getIsAppLive()
    {
        return preferences.getBoolean(IS_APP_LIVE,false);
    }

    public String getFbId()
    {
        return preferences.getString(FB_ID, "");
    }

    public void setFbId(String fb_id)
    {
        editor.putString(FB_ID,fb_id);
        editor.commit();
    }
}
