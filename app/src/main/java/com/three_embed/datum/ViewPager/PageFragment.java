package com.three_embed.datum.ViewPager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.three_embed.datum.Aramis_home.R;

/**
 * <h1>PageFragment</h1>
 * PageFragment is Fragment class of Activity.
 * PageFragment fragment is simply a Fragment that gives different Screen of Image to the
 * View Pager Adapter.
 * It inflate a Layout And set Image according to the Given Number From the calling function.
 * and set the Image and set the Fragment view and return fragment reference to the View Pager adapter.
 * Created by embed-pc on 21/11/15.
 * @author 3embed
 * @since  21/11/15
 * @version 1.0
 */
public final class PageFragment extends Fragment
{
    private static final String saveInstance_Key="PageFragment:content";
    View fragment_view;
    ImageView imageView;

    public static PageFragment newInstance(String content)
    {
        PageFragment fragment = new PageFragment();

        fragment.mContent =content;
        fragment.mContentint =Integer.parseInt(content);

        return fragment;
    }

    private String mContent = "????";
    private int mContentint = 0;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if ((savedInstanceState != null) && savedInstanceState.containsKey(saveInstance_Key))
        {
            mContent = savedInstanceState.getString(saveInstance_Key);
            assert mContent != null;
            mContentint=Integer.parseInt(mContent);
        }

    }
    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        fragment_view=inflater.inflate(R.layout.view_pager_view_layout,null,false);
        imageView=(ImageView)fragment_view.findViewById(R.id.pager_image);
        try {
            switch (mContentint) {
                case 0:
                    imageView.setImageResource(R.drawable.sign_up_one);
                    break;
                case 1:
                    imageView.setImageResource(R.drawable.sign_up_one);
                    break;
                case 2:
                    imageView.setImageResource(R.drawable.sign_up_two);
                    break;
                case 3:
                    imageView.setImageResource(R.drawable.sign_up_three);
                    break;

                default:
                    break;
            }
        }catch (OutOfMemoryError error)
        {
            error.printStackTrace();
        }
        return fragment_view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString(saveInstance_Key,mContent);
    }
}
