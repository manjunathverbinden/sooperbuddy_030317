package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.three_embed.datum.Aleret_dialogs.Net_work_failed_aleret;
import com.three_embed.datum.Aramis_home.User_preference_setter.User_preference_Item_Controlar;
import com.three_embed.datum.Pojo_classes.SetPreferenceAlData;
import com.three_embed.datum.Pojo_classes.SetPreferencePojo;
import com.three_embed.datum.Pojo_classes.UpdatePreference;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;

/**
 * <h1>UserSetting</h1>
 * <p>
 *     User setting page is to set the user required setting .
 * </p>
 * @author 3Embed.
 * @since 25.08.2016.
 * */

public class UserSetting extends AppCompatActivity implements View.OnClickListener
{
    private SessionManager sessionManager;
    public TextView action_bar_title,done_button_txt;
    private RelativeLayout rL_setPref_doneBtn,update_progress_bar;
    private TextView connection_failed_text;
    public Typeface GothamRoundedMedium,GothamRoundedLight,GothamRoundedBold;
    private User_preference_Item_Controlar user_preference_item_controlar;
    private SetPreferencePojo preferencePojo;
    private ScrollView pref_details;
    private RelativeLayout get_pref_progress_bar,connection_failed_rl;
    private boolean is_Getprefernce_service_active=false;
    private Net_work_failed_aleret net_work_failed_aleret;
    private Intent starting_intent;
    private JSONArray serverSeleted;
    /**
     * Contains all the data list.*/
    private ArrayList<SetPreferenceAlData> preferenceAlDatas,preferenceAlDatas_before;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setting);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        /**
         *Setting coustom action bar in android.. */
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View customView = inflator.inflate(R.layout.discovery_setting_actionbar_layout, null);
        actionBar.setCustomView(customView);

        /**
         * setting the coustom action bar left and right margin as Zero by passing the 0,0 absloute value.
         * */
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);

        /**
         * if session expired then opening the landing page.*/
        starting_intent = new Intent(this,SplashScreen.class);


        sessionManager=new SessionManager(this);
        preferenceAlDatas_before=new ArrayList<>();
        /**
         * Creating the Response of Data inflater object.*/
        user_preference_item_controlar=User_preference_Item_Controlar.getInstance(UserSetting.this);

        /**
         * Litening the network failed aleret.*/
        net_work_failed_aleret= Net_work_failed_aleret.getInstance();
        /**
         * Font style.*/
        GothamRoundedMedium=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Regular.otf");
        GothamRoundedLight=Typeface.createFromAsset(getAssets(), "fonts/MyriadPro-Regular.otf");
        GothamRoundedBold=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Bold.otf");
        /**
         * method called to initialize all data members
         */
        initializeVariables();
        /**
         Calling this method to get all related data the data from the server*/
        getPreferences();
    }
    /**
     * In this method we used to initialize all data members
     */
    private void initializeVariables()
    {
        pref_details=(ScrollView)findViewById(R.id.pref_details);
        get_pref_progress_bar=(RelativeLayout)findViewById(R.id.progress_bar_rl);
        connection_failed_rl=(RelativeLayout)findViewById(R.id.connection_failed_rl);
        Button re_try_button = (Button) findViewById(R.id.retry_button);
        assert re_try_button != null;
        re_try_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Checking which service is required for retry.*/
                if (is_Getprefernce_service_active) {
                    /**
                     Calling this method to get all related data the data from the server*/
                    getPreferences();
                } else
                {
                    updatePreference(true);
                }

            }
        });

        rL_setPref_doneBtn= (RelativeLayout) findViewById(R.id.rL_setPref_doneBtn);
        update_progress_bar= (RelativeLayout) findViewById(R.id.update_progress_bar);
        action_bar_title=(TextView)findViewById(R.id.action_bar_title);
        assert action_bar_title != null;
        action_bar_title.setText(R.string.usersettingtext);
        action_bar_title.setTypeface(GothamRoundedBold);
        done_button_txt=(TextView)findViewById(R.id.tV_done);
        assert done_button_txt != null;
        done_button_txt.setTypeface(GothamRoundedMedium);
        connection_failed_text=(TextView)findViewById(R.id.connection_failed_text);
        rL_setPref_doneBtn.setOnClickListener(this);


    }

    /**
     * service call using OkHttp client for getting preferences
     * datas which is being set during sign-Up
     */
    private void getPreferences()
    {
        connection_failed_rl.setVisibility(View.GONE);
        get_pref_progress_bar.setVisibility(View.VISIBLE);
        is_Getprefernce_service_active=true;

        if(Utility.isNetworkAvailable(UserSetting.this))
        {
            final JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(getApplicationContext()));
                jsonObject.put("ent_sess_token",sessionManager.getToken());
                jsonObject.put("ent_fbid",sessionManager.getFbId());
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            Log.d("Request", jsonObject.toString());

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_USER_SETTING, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {

                    Log.d("Response", result);
                    if (get_pref_progress_bar != null)
                    {
                        get_pref_progress_bar.setVisibility(View.GONE);
                    }
                    Gson gson = new Gson();
                    preferencePojo = gson.fromJson(result, SetPreferencePojo.class);
                    /**
                     * Handlening the response ..*/
                    addPreferenceDatas(preferencePojo);
                }

                @Override
                public void onError(String error) {
                    if (get_pref_progress_bar != null) {
                        get_pref_progress_bar.setVisibility(View.GONE);
                    }
                    /**
                     * If error occurred then.*/
                    connection_failed_rl.setVisibility(View.VISIBLE);
                    connection_failed_text.setText(error);
                }
            });
        }else
        {
            if (get_pref_progress_bar != null)
            {
                get_pref_progress_bar.setVisibility(View.GONE);
            }
            /**
             * Show aleret here !*/
            connection_failed_rl.setVisibility(View.VISIBLE);
            /**
             * Showing aleret.*/
            net_work_failed_aleret.net_work_fail(UserSetting.this, new Net_work_failed_aleret.Internet_connection_Callback()
            {
                @Override
                public void onSucessConnection(String connection_Type)
                {

                }

                @Override
                public void onErrorConnection(String error)
                {

                }
            });
        }

    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {

            case R.id.rL_setPref_doneBtn :
                upDateUserSettings();
                break;
            case R.id.back_button :
                onBackPressed();
                break;
        }
    }


    /**
     * This method will add all set preference datas at run time
     */
    private void addPreferenceDatas(SetPreferencePojo preferencePojo)
    {
        if ("0".equals(preferencePojo.getErrorFlag()))
        {
            if(preferenceAlDatas_before.size()>0)
            {
                preferenceAlDatas_before.clear();
            }
            /**
             * Setting get preference service is not working now.*/
            is_Getprefernce_service_active=false;
            pref_details.setVisibility(View.VISIBLE);
            rL_setPref_doneBtn.setVisibility(View.VISIBLE);

            preferenceAlDatas =preferencePojo.getData();

            preferenceAlDatas_before.addAll(preferenceAlDatas);
            /**
             * Arragnging all the data according to the priority.*/
            Collections.sort(preferenceAlDatas);
            int preferenceSize=preferenceAlDatas.size();
            /**
             * Reading the Linear layout to inflate the List of data in the layout.*/
            LinearLayout linear_inflatePref = (LinearLayout) findViewById(R.id.linear_inflatePref);

            for (int i=0;i<preferenceSize;i++)
            {
                if (preferenceAlDatas.get(i).getTypeOfPreference().equals("1")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Age")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Distance")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Gender"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.radio_button(preferenceAlDatas.get(i)));

                }else if (preferenceAlDatas.get(i).getTypeOfPreference().equals("2")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Age")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Distance")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Gender"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.checkBox(preferenceAlDatas.get(i)));

                }else if (preferenceAlDatas.get(i).getTypeOfPreference().equals("3")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Age")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Distance")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Gender"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.numeric_Range(preferenceAlDatas.get(i)));

                } else if (preferenceAlDatas.get(i).getTypeOfPreference().equals("4")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Age")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Distance")&&!preferenceAlDatas.get(i).getPreferenceTitle().equals("Gender"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.text_Input(preferenceAlDatas.get(i)));
                }
            }

            /**
             * Creating the previously seleted list given server*/
            serverSeleted=creaste_Selected_list();
        }
        else if("121".equals(preferencePojo.getErrorFlag()))
        {
            Toast.makeText(UserSetting.this, UserSetting.this.getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
            starting_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            /**
             * Clearing the session manager for fresh user.*/
            sessionManager.clear_session_manager();
            UserSetting.this.finish();
            UserSetting.this.startActivity(starting_intent);
        }
    }


    /**
     * <p>
     *     This method is used to make array list for preference_id and options for doing
     *     service call of update preference.
     * </p>
     */
    private JSONArray creaste_Selected_list()
    {
        JSONArray selected_data=new JSONArray();

        try
        {
            for(int count=0;count<preferenceAlDatas.size();count++)
            {
                if(preferenceAlDatas.get(count).getTypeOfPreference().equals("4"))
                {
                    JSONObject edt_data=new JSONObject();
                    edt_data.put("pref_id",preferenceAlDatas.get(count).getId());
                    if(preferenceAlDatas.get(count).getSelected()==null)
                    {
                        edt_data.put("options","");
                    }else
                    {
                        edt_data.put("options", preferenceAlDatas.get(count).getSelected().get(0));
                    }
                    selected_data.put(edt_data);

                }else if(preferenceAlDatas.get(count).getTypeOfPreference().equals("3"))
                {
                    /**
                     * Creating a pojo class for data class for the */
                    JSONObject obj=new JSONObject();
                    obj.put("pref_id", preferenceAlDatas.get(count).getId());
                    JSONObject obj1 = new JSONObject();
                    obj1.put("Start",preferenceAlDatas.get(count).getSelected_min());
                    obj1.put("End",preferenceAlDatas.get(count).getSelected_max());
                    obj.put("options", obj1);
                    selected_data.put(obj);

                }else
                {
                    JSONObject choice_data=new JSONObject();
                    choice_data.put("pref_id", preferenceAlDatas.get(count).getId());
                    choice_data.put("options", new JSONArray(preferenceAlDatas.get(count).getSelected()));
                    selected_data.put(choice_data);
                }

            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        return selected_data;
    }

    /**
     * Setting the user details to user.*/
    JSONArray jsonArray;
    private void upDateUserSettings()
    {
        jsonArray=creaste_Selected_list();
        if(checkUserInteraction(jsonArray))
        {
            updatePreference(false);
        }else
        {
            /**
             * Showing aleret for mandatory field.*/
            show_mandatory_aleret(this);
        }

    }

    /**
     * <h2>updatePreference</h2>
     * <P>
     *   Method cal a async task to update the all the details of the prference changed to the server.
     *   Create Json object and pass all the parameter to Json object.
     * </P>
     */
    private void updatePreference(final boolean isFor_retry)
    {
            if(isFor_retry)
            {
                connection_failed_rl.setVisibility(View.GONE);
                get_pref_progress_bar.setVisibility(View.VISIBLE);
            }else
            {
                rL_setPref_doneBtn.setVisibility(View.GONE);
                update_progress_bar.setVisibility(View.VISIBLE);
            }

            if(Utility.isNetworkAvailable(UserSetting.this))
            {

                JSONObject jsonObject=new JSONObject();
                try
                {
                    jsonObject.put("ent_dev_id", Utility.getDeviceId(this));
                    jsonObject.put("ent_fbid",sessionManager.getFbId());
                    jsonObject.put("ent_sess_token",sessionManager.getToken());
                    jsonObject.put("selected",jsonArray);
                }
                catch (Exception e)
                {
                    e.printStackTrace();

                    if(update_progress_bar!=null)
                    {
                        update_progress_bar.setVisibility(View.GONE);
                    }
                }

                OkHttp3Connection.doOkHttp3Connection(ServiceUrl.UPDATE_USER_SETTING, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if (isFor_retry) {
                            if (get_pref_progress_bar != null) {
                                get_pref_progress_bar.setVisibility(View.GONE);
                            }

                        } else {
                            if (update_progress_bar != null) {
                                update_progress_bar.setVisibility(View.GONE);
                            }
                        }
                        UpdatePreference updatePreference;
                        Gson gson = new Gson();
                        updatePreference = gson.fromJson(result, UpdatePreference.class);

                        if (updatePreference.getErrorFlag().equals("0"))
                        {
                            Intent intent = new Intent(UserSetting.this,HomePageActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            VariableConstant.isComming_from_prference = true;
                            if (VariableConstant.isComming_fom_Home)
                            {
                                VariableConstant.isComming_fom_Home = false;
                                finish();
                                overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                            } else
                            {
                                startActivity(intent);
                                overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                                finish();
                            }

                        } else {
                            if (isFor_retry) {
                                if (get_pref_progress_bar != null) {
                                    get_pref_progress_bar.setVisibility(View.GONE);
                                    connection_failed_rl.setVisibility(View.VISIBLE);
                                }

                            } else {
                                if (update_progress_bar != null) {
                                    update_progress_bar.setVisibility(View.GONE);
                                    connection_failed_rl.setVisibility(View.VISIBLE);
                                }
                            }

                            Toast.makeText(UserSetting.this, updatePreference.getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(UserSetting.this, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }else
            {
                /**
                 * NO internet connection showing error.*/
                get_pref_progress_bar.setVisibility(View.GONE);
                update_progress_bar.setVisibility(View.GONE);
                pref_details.setVisibility(View.GONE);
                connection_failed_rl.setVisibility(View.VISIBLE);
                /**
                 * Showing aleret.*/
                net_work_failed_aleret.net_work_fail(UserSetting.this, new Net_work_failed_aleret.Internet_connection_Callback() {
                    @Override
                    public void onSucessConnection(String connection_Type) {

                    }

                    @Override
                    public void onErrorConnection(String error) {

                    }
                });
            }
    }

    /**
     * <h2>checkUserInteraction</h2>*/
    private boolean checkUserInteraction(JSONArray jsonArray) {
        return serverSeleted == null || !serverSeleted.toString().equalsIgnoreCase(jsonArray.toString());
    }
    /**
     * This method is used
     */
    @Override
    public void onBackPressed()
    {
        exist_on_doble_press();
    }

    private boolean backPressedToExitOnce=false;

    /**
     * <h2>exist_on_doble_press</h2>
     * <P>
     *
     * </P>*/
    private void exist_on_doble_press()
    {
        if(backPressedToExitOnce)
        {
            super.onBackPressed();
        }
        else
        {
            this.backPressedToExitOnce = true;
            Toast.makeText(UserSetting.this, R.string.againExitText, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    backPressedToExitOnce = false;
                }
            }, 2000);
        }
    }

    /**
     * <h2>gps_Setting_aleret</h2>
     * <p/>
     * <h3>
     * Method to open a aleret box to show internet setting intent on open of aleret.
     * </h3>
     * </P>
     */
    private void show_mandatory_aleret(Activity mactivity)
    {
        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(mactivity);
        alertDialog.setTitle(getString(R.string.notetext));
        alertDialog.setMessage(getString(R.string.usersettingText));
        alertDialog.setIcon(R.drawable.launch_con);
        alertDialog.setPositiveButton(getString(R.string.yesText), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                updatePreference(false);
            }
        });
        alertDialog.setNegativeButton(getString(R.string.notext), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.support.v7.app.AlertDialog dialog_parent = alertDialog.show();
        dialog_parent.show();
    }

}

