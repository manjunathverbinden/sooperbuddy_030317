package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.gson.Gson;
import com.three_embed.datum.AdapterClass.Chat_user_Details_adapter;
import com.three_embed.datum.AdapterClass.Profile_pic_adapter;
import com.three_embed.datum.Pojo_classes.GetProfilePojo;
import com.three_embed.datum.Pojo_classes.user_list_item_details;
import com.three_embed.datum.Utility.CirclePageIndicator;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class Chat_user_abouts extends Fragment
{
    private CirclePageIndicator profile_pic_indicator;
    private CoustomListview mListView;
    private ViewPager profile_pic_pager;
    private Activity parent_activity_context;
    private Chat chat_main_class;
    private SessionManager sessionManager;
    private ProgressBar loading_progress_bar;
    private Intent starting_intent;

    public Chat_user_abouts()
    {
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        parent_activity_context=getActivity();
        chat_main_class=(Chat)parent_activity_context;
        sessionManager=new SessionManager(parent_activity_context);
        /**
         * if session expired then opening the landing page.*/
        starting_intent = new Intent(parent_activity_context,SplashScreen.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        View fragment_view=inflater.inflate(R.layout.fragment_chat_user_abouts,container,false);
        loading_progress_bar=(ProgressBar)fragment_view.findViewById(R.id.loading_progress_bar);
        mListView = (CoustomListview)fragment_view.findViewById(R.id.user_details);
        @SuppressLint("InflateParams") View header =inflater.inflate(R.layout.customlistviewheader, null);
        profile_pic_pager=(ViewPager)header.findViewById(R.id.pager);
        profile_pic_indicator=(CirclePageIndicator)header.findViewById(R.id.indicator);
        RelativeLayout custoomlistview_heAder = (RelativeLayout) header.findViewById(R.id.listviewheader);
        mListView.setZoomRatio(chat_main_class.Screenwidth);
        mListView.setCustomListHeaderImageView(custoomlistview_heAder);
        mListView.addHeaderView(header);
        /**
         * Intializing the Data*/
        setImages(chat_main_class.user_profile_pic_url,make_other_images_Array(chat_main_class.user_other_images),chat_main_class.user_first_name,chat_main_class.user_age,chat_main_class.user_about,"","",chat_main_class.user_profile_video_url,"");
        getUser_Details(chat_main_class.user_fb_id);
        return fragment_view;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    /**
     * <h2>getUser_Details</h2>
     * <P>
     *     <h3>
     *
     *     </h3>
     * </P>
     * @param FB_ID contains the fb id user whose detail is called from the Server.*/
    private void getUser_Details(final String FB_ID)
    {

        loading_progress_bar.setVisibility(View.VISIBLE);
        JSONObject jsonObject=new JSONObject();

        try
        {
            jsonObject.put("ent_dev_id", Utility.getDeviceId(parent_activity_context));
            jsonObject.put("ent_sess_token",sessionManager.getToken());
            jsonObject.put("ent_fbid",FB_ID);
            jsonObject.put("ent_data_time",Utility.getCurrentGmtTime());
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_PROFILE, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if (loading_progress_bar != null) {
                    loading_progress_bar.setVisibility(View.INVISIBLE);
                }
                parse_Response_Data(result);
            }

            @Override
            public void onError(String error)
            {
                if (loading_progress_bar != null) {
                    loading_progress_bar.setVisibility(View.INVISIBLE);
                }

                Toast.makeText(parent_activity_context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }



    /**
     * <h2>parse_Response_Data</h2>
     * <P>
     *     <h3>
     *
     *     </h3>
     * </P>
     * @param result contains the result string to parse it.*/
    private void parse_Response_Data(String result)
    {
        Gson gson=new Gson();
        GetProfilePojo getProfilePojo=gson.fromJson(result,GetProfilePojo.class);

        switch (getProfilePojo.getErrorFlag())
        {
            case "0":
                setImages(getProfilePojo.getProfilePhoto(), getProfilePojo.getImages(), getProfilePojo.getFirstName(), getProfilePojo.getAge() + "", getProfilePojo.getAbout(), "", getProfilePojo.getVideoThumnail(), getProfilePojo.getProfileVideo(), getProfilePojo.getGender());
                break;
            case "122":
                Toast.makeText(parent_activity_context, parent_activity_context.getApplicationContext().getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
                starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                parent_activity_context.startActivity(starting_intent);
                break;
            default:
                Toast.makeText(parent_activity_context, getProfilePojo.getErrorMessage(), Toast.LENGTH_SHORT).show();
                break;
        }
    }



    /**
     * <h2>setImages</h2>
     * <p>
     *  Method initialize the Adapter and Data.
     * </p>*/
    private void setImages(String profilePic_url,ArrayList<String> imagelist,String name,String age,String aboutUser,String distance,String thumb_nail_url,String video_url_data,String gender)
    {
        ArrayList<user_list_item_details> list_data = new ArrayList<>();
        if(video_url_data!=null&&!video_url_data.isEmpty())
        {
            user_list_item_details data_item_details=new user_list_item_details();
            data_item_details.setThumb_nail_url(thumb_nail_url);
            data_item_details.setVideo_url(video_url_data);
            list_data.add(data_item_details);
        }
        user_list_item_details data_item_details_image=new user_list_item_details();
        data_item_details_image.setImage_url(profilePic_url);
        list_data.add(data_item_details_image);
        if (imagelist!=null&&imagelist.size()>0)
        {
            for(int count=0;count<imagelist.size();count++)
            {
                user_list_item_details data_item_details_image_other_list=new user_list_item_details();
                data_item_details_image_other_list.setImage_url(imagelist.get(count));
                list_data.add(data_item_details_image_other_list);
            }

        }
        Profile_pic_adapter profile_pic_adapter = new Profile_pic_adapter(parent_activity_context, list_data);
        profile_pic_pager.setAdapter(profile_pic_adapter);
        profile_pic_indicator.setViewPager(profile_pic_pager);
        ArrayList<String> imageAl=new ArrayList<>();
        imageAl.add(null);
        int age_of_user=Integer.parseInt(age);
        Chat_user_Details_adapter profileDetailsAdap=new Chat_user_Details_adapter(parent_activity_context,name,age_of_user,aboutUser,imageAl,distance,chat_main_class.user_tag_line,gender);
        mListView.setAdapter(profileDetailsAdap);
    }

    private ArrayList<String> make_other_images_Array(String other_images_list)
    {
        if(other_images_list!=null)
        {
            String data[]=other_images_list.split(",");
            ArrayList<String> other_image_list=new ArrayList<>();
            Collections.addAll(other_image_list, data);
            return other_image_list;
        }else
        {
            return null;
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }
}
