package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.three_embed.datum.Directory_management.Directory_creation;
import com.three_embed.datum.Permission_Ap23.App_permission_23;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.VariableConstant;

import java.io.File;
import java.util.ArrayList;

/**
 * <h1>Profile_picture_setting</h1>
 * <P>
 *    This class simple used to Open alert dialog for profile picture setting .
 *    and receive the user chosen picture and Store it a Datum-profile picture directory.
 * </P>
 * @author 3Embed
 * @since 24/11/15.
 * @version 1.0.
 */
public class Profile_picture_setting
{
    private Activity activity;
    private Dialog dialog;
    private ArrayList<App_permission_23.Permission> media_permission;
    private boolean isFor_Camera=false,isFor_Facebook=false;
    private Directory_creation directory_creation;
    private SessionManager sessionManager=null;
    /**
     * Override of Constructor of the class default Constructor
     * and receiving the creator of activity reference.*/
    public Profile_picture_setting(Activity mactivity)
    {
        activity=mactivity;
        media_permission=new ArrayList<>();
        directory_creation=new Directory_creation(activity);
        sessionManager=new SessionManager(activity);
    }

    /**
     * <h2>shoeAlert_Profile_picture_chooser</h2>
     * Method open a dialog and give user to choose a Image for profile picture to set.
     * Giving user to chooser to From Camera or Gallery. */
    public void shoeAlert_Profile_picture_chooser()
    {
        dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        LayoutInflater layoutInflater= (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View view=layoutInflater.inflate(R.layout.profile_picture_chooser,null);
        final RelativeLayout snap_rl=(RelativeLayout)view.findViewById(R.id.takesnap);
        final RelativeLayout gallery=(RelativeLayout)view.findViewById(R.id.gallery);
        final RelativeLayout facebook=(RelativeLayout)view.findViewById(R.id.facebook);
        if(!sessionManager.get_User_fb_login_status())
        {
            facebook.setVisibility(View.GONE);
        }
        final ImageView imageView=(ImageView)view.findViewById(R.id.cross_close_button);
        final Button cancel=(Button)view.findViewById(R.id.acncel_button);

        snap_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                /**
                 * Calling this method to Open Camera.*/
                dispatchTakePictureIntent_permission(activity);
            }
        });

        gallery.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                /**
                 * Calling this method to open Gallery..*/
                chooseFrom_Gallery_permission(activity);
            }
        });

        facebook.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
           /**
             * Opening the facebook activity. */
                Open_Facebook_intent();

            }
        });
        imageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }


    /**
     * <h2>dispatchTakePictureIntent</h2>
     * Method Open Camera to Take Picture with The Given result coade.
     * {@code REQUEST_IMAGE_CAPTURE }.
     * @param activity contains the current Activity reference to Open Camera*/

    private void dispatchTakePictureIntent_permission(Activity activity)
    {
        isFor_Camera=true;
        isFor_Facebook=false;
        App_permission_23 app_permission=App_permission_23.getInstance();
        media_permission.clear();
        media_permission.add(App_permission_23.Permission.CAMERA);
        media_permission.add(App_permission_23.Permission.WRITE_EXTERNAL_STORAGE);

        if (Build.VERSION.SDK_INT >= 23)
        {
            if(app_permission.getPermission(media_permission, activity, false))
            {
                dispatchCameraIntent(activity);
            }

        }else
        {
            dispatchCameraIntent(activity);
        }
    }
    /**
     * <h2>chooseFromGellery</h2>
     * Starting a intent to open an activty to open Gallery to give user to choose an Image From Gallery.
     * to set it as a profile pic.
     * @param activity contains the reference of calling activity.*/
    private void chooseFrom_Gallery_permission(Activity activity)
    {
        /**
         * Permission required.*/
        App_permission_23 app_permission=App_permission_23.getInstance();
        media_permission.clear();
        media_permission.add(App_permission_23.Permission.READ_EXTERNAL_STORAGE);

        if (Build.VERSION.SDK_INT >= 23)
        {
            if(app_permission.getPermission(media_permission,activity,false))
            {
                dispatch_Gallery_Intent(activity);
            }
        }else
        {
            dispatch_Gallery_Intent(activity);
        }
    }

    /**
     * <h2>open_Required_intent</h2>
     * Method Open Camera pic image from gallery.
     * {@code REQUEST_IMAGE_GALLERY }.
     */
    public void open_Required_intent(Activity mactivity)
    {
        /**
         * Checking is for camera..*/
        if(isFor_Camera)
        {
            dispatchCameraIntent(mactivity);
        }else
        {
            if(isFor_Facebook)
            {
                Open_Facebook_intent();
            }else
            {
                dispatch_Gallery_Intent(mactivity);
            }

        }

    }
    /**
     * <h2>dispatch_Gallery_Intent</h2>
     * Method Open Camera pic image from gallery.
     * {@code REQUEST_IMAGE_GALLERY }.
     * @param activity contains the current Activity reference to Open Camera*/

    private void dispatch_Gallery_Intent(Activity activity)
    {
        isFor_Camera=false;
        isFor_Facebook=false;
        Intent pickfromGallery = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickfromGallery.setType("image/*");
        activity.startActivityForResult(Intent.createChooser(pickfromGallery,"Select File"),ServiceUrl.REQUEST_IMAGE_GALLERY);
    }


    /**
     * <h2>dispatchCameraIntent</h2>
     * Method Open Camera to Take Picture with The Given result coade.
     * {@code REQUEST_IMAGE_CAPTURE }.
     * @param activity contains the current Activity reference to Open Camera*/

    private void dispatchCameraIntent(Activity activity)
    {
        isFor_Camera=false;
        isFor_Facebook=false;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null)
        {
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,setImageUri());
            activity.startActivityForResult(takePictureIntent,ServiceUrl.REQUEST_IMAGE_CAPTURE);
        }
        else
        {
            Toast.makeText(activity.getApplicationContext(),activity.getResources().getString(R.string.unable_to_open_camera),Toast.LENGTH_SHORT).show();
        }
    }

    /* *
     * <h2>setImageUri</h2>
     * <P>
     *    Creating the uri for the Image to store.
     * </P>*/
    private Uri setImageUri()
    {
        File file=directory_creation.create_file_for_Chat_Image();
        Uri imgUri = Uri.fromFile(file);
        VariableConstant.profile_picture_path =file.getAbsolutePath();
        return imgUri;
    }

    /**
     * <h2>Open_Facebook_intent</h2>
     * <P>
     *     Opening the facebook intent.
     * </P>*/
    private void Open_Facebook_intent()
    {
        isFor_Facebook=true;
        isFor_Camera=false;
        /**
         * Permission required.*/
        App_permission_23 app_permission=App_permission_23.getInstance();
        media_permission.clear();
        media_permission.add(App_permission_23.Permission.READ_EXTERNAL_STORAGE);
        media_permission.add(App_permission_23.Permission.WRITE_EXTERNAL_STORAGE);

        if (Build.VERSION.SDK_INT >= 23)
        {
            if(app_permission.getPermission(media_permission,activity,false))
            {
                Intent intent=new Intent(activity,Facebook_album.class);
                activity.startActivityForResult(intent, ServiceUrl.FACEBOOK_REQUEST_CODE);
            }
        }else
        {
            Intent intent=new Intent(activity,Facebook_album.class);
            activity.startActivityForResult(intent, ServiceUrl.FACEBOOK_REQUEST_CODE);
        }

    }

}
