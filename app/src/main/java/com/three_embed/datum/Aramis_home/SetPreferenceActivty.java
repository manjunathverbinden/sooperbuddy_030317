package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.three_embed.datum.Aleret_dialogs.Net_work_failed_aleret;
import com.three_embed.datum.Pojo_classes.SetPreferenceAlData;
import com.three_embed.datum.Pojo_classes.SetPreferencePojo;
import com.three_embed.datum.Pojo_classes.UpdatePreference;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.VariableConstant;
import com.three_embed.datum.Aramis_home.User_preference_setter.User_preference_Item_Controlar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
/**
 * <h1>SetPreferenceActivty</h1>
 * <p>
 *     This class is getting called from Register_activity. This class contains data like age, gender
 *     religion and distance. range of age should -5 & +5 of his original age. gender will opposite of
 *     original gender disatnce should be in between 0 to 100.
 * </p>
 * @author 3embed
 * @since 18-11-15
 */
public class SetPreferenceActivty extends AppCompatActivity implements View.OnClickListener
{
    private SessionManager sessionManager;
    public TextView action_bar_title,done_button_txt;
    private RelativeLayout rL_setPref_doneBtn;
    private RelativeLayout update_progress_bar;
    private TextView connection_failed_text;
    public Typeface GothamRoundedMedium,GothamRoundedLight,GothamRoundedBold;
    private User_preference_Item_Controlar user_preference_item_controlar;
    private  SetPreferencePojo preferencePojo;
    private ScrollView pref_details;
    private RelativeLayout get_pref_progress_bar,connection_failed_rl;
    private boolean is_Getprefernce_service_active=false;
    private Net_work_failed_aleret net_work_failed_aleret;
    private boolean ismandatory_field_selected=true;
    private String mandatory_field_title="";
    private Intent starting_intent;
    /**
     * Contains all the data list.*/
    private ArrayList<SetPreferenceAlData> preferenceAlDatas,preferenceAlDatas_before;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_preference_activty);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        /**
         *Setting coustom action bar in android.. */
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View customView = inflator.inflate(R.layout.discovery_setting_actionbar_layout, null);
        actionBar.setCustomView(customView);

        /**
         * setting the coustom action bar left and right margin as Zero by passing the 0,0 absloute value.
         * */
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);

        ImageView backbutton_image_iv =(ImageView)customView.findViewById(R.id.backbutton_image);
        /*Checking weather it is comming from home page or not*/

        if(VariableConstant.isComming_fom_Home)
        {
            assert backbutton_image_iv != null;
            backbutton_image_iv.setVisibility(View.VISIBLE);
        }

        /**
         * if session expired then opening the landing page.*/
        starting_intent = new Intent(this,SplashScreen.class);


        sessionManager=new SessionManager(this);
        preferenceAlDatas_before=new ArrayList<>();
        /**
         * Creating the Response of Data inflater object.*/
        user_preference_item_controlar=User_preference_Item_Controlar.getInstance(SetPreferenceActivty.this);

        /**
         * Litening the network failed aleret.*/
        net_work_failed_aleret=Net_work_failed_aleret.getInstance();

        /**
         * Font style.*/
        GothamRoundedMedium=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Regular.otf");
        GothamRoundedLight=Typeface.createFromAsset(getAssets(), "fonts/MyriadPro-Regular.otf");
        GothamRoundedBold=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Bold.otf");
        /**
         * method called to initialize all data members
         */
        initializeVariables();
        /**
         Calling this method to get all related data the data from the server*/
        getPreferences();
    }
    /**
     * In this method we used to initialize all data members
     */
    private void initializeVariables()
    {
        connection_failed_text=(TextView)findViewById(R.id.connection_failed_text);

        pref_details=(ScrollView)findViewById(R.id.pref_details);

        get_pref_progress_bar = (RelativeLayout) findViewById(R.id.progress_bar_rl);
        connection_failed_rl = (RelativeLayout) findViewById(R.id.connection_failed_rl);
        Button re_try_button = (Button) findViewById(R.id.retry_button);
        assert re_try_button != null;
        re_try_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Checking which service is required for retry.*/
                if (is_Getprefernce_service_active) {
                    /**
                     Calling this method to get all related data the data from the server*/
                    getPreferences();
                } else {
                    updatePreference(true);
                }

            }
        });



        rL_setPref_doneBtn= (RelativeLayout) findViewById(R.id.rL_setPref_doneBtn);
        update_progress_bar= (RelativeLayout) findViewById(R.id.update_progress_bar);
        action_bar_title = (TextView) findViewById(R.id.action_bar_title);
        assert action_bar_title != null;
        action_bar_title.setTypeface(GothamRoundedBold);
        done_button_txt = (TextView) findViewById(R.id.tV_done);
        assert done_button_txt != null;
        done_button_txt.setTypeface(GothamRoundedMedium);
        RelativeLayout back_button_rl = (RelativeLayout) findViewById(R.id.back_button);
        assert back_button_rl != null;
        rL_setPref_doneBtn.setOnClickListener(this);rL_setPref_doneBtn.setOnClickListener(this);
        back_button_rl.setOnClickListener(this);

    }
    /**
     * service call using OkHttp client for getting preferences
     * datas which is being set during sign-Up
     */
    private void getPreferences()
    {
        connection_failed_rl.setVisibility(View.GONE);
        get_pref_progress_bar.setVisibility(View.VISIBLE);
        is_Getprefernce_service_active=true;

        if(Utility.isNetworkAvailable(SetPreferenceActivty.this))
        {
            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(getApplicationContext()));
                jsonObject.put("ent_sess_token",sessionManager.getToken());
                jsonObject.put("ent_fbid",sessionManager.getFbId());
            } catch (JSONException e)
            {
                e.printStackTrace();
            }

            Log.d("Requestdata", jsonObject.toString());

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_PREFERENCES, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result) {
                    Log.d("Responsedata", result);

                    if (get_pref_progress_bar != null) {
                        get_pref_progress_bar.setVisibility(View.GONE);
                    }
                    Gson gson = new Gson();
                    preferencePojo = gson.fromJson(result, SetPreferencePojo.class);
                    /**
                     * Handlening the response ..*/
                    addPreferenceDatas(preferencePojo);
                }

                @Override
                public void onError(String error) {
                    if (get_pref_progress_bar != null) {
                        get_pref_progress_bar.setVisibility(View.GONE);
                    }
                    /**
                     * If error occurred then.*/
                    connection_failed_rl.setVisibility(View.VISIBLE);
                    connection_failed_text.setText(error);
                }
            });
        }else
        {
            if (get_pref_progress_bar != null)
            {
                get_pref_progress_bar.setVisibility(View.GONE);
            }
            /**
             * Show aleret here !*/
            connection_failed_rl.setVisibility(View.VISIBLE);
            /**
             * Showing aleret.*/
            net_work_failed_aleret.net_work_fail(SetPreferenceActivty.this, new Net_work_failed_aleret.Internet_connection_Callback()
            {
                @Override
                public void onSucessConnection(String connection_Type)
                {

                }

                @Override
                public void onErrorConnection(String error)
                {

                }
            });
        }

    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {

            case R.id.rL_setPref_doneBtn :
                updatePreference(false);
                break;
            case R.id.back_button :
                onBackPressed();
                break;
        }
    }

    /**
     * This method will add all set preference datas at run time
     */
    private void addPreferenceDatas(SetPreferencePojo preferencePojo)
    {
        if ("0".equals(preferencePojo.getErrorFlag()))
        {
            if(preferenceAlDatas_before.size()>0)
            {
                preferenceAlDatas_before.clear();
            }
            /**
             * Setting get preference service is not working now.*/
            is_Getprefernce_service_active=false;
            pref_details.setVisibility(View.VISIBLE);
            rL_setPref_doneBtn.setVisibility(View.VISIBLE);
            preferenceAlDatas =preferencePojo.getData();
            /**
             * Arragnging all the data according to the priority.*/
            Collections.sort(preferenceAlDatas);
            int preferenceSize=preferenceAlDatas.size();

            LinearLayout linear_inflatePref = (LinearLayout)findViewById(R.id.linear_inflatePref);
            for (int i=0;i<preferenceSize;i++)
            {
                if (preferenceAlDatas.get(i).getTypeOfPreference().equals("1"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.radio_button(preferenceAlDatas.get(i)));

                }else if (preferenceAlDatas.get(i).getTypeOfPreference().equals("2"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.checkBox(preferenceAlDatas.get(i)));

                }else if (preferenceAlDatas.get(i).getTypeOfPreference().equals("3"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.numeric_Range(preferenceAlDatas.get(i)));

                } else if (preferenceAlDatas.get(i).getTypeOfPreference().equals("4"))
                {
                    /**
                     * Adding whole chield layout ot the parent linear layout*/
                    assert linear_inflatePref != null;
                    linear_inflatePref.addView(user_preference_item_controlar.text_Input(preferenceAlDatas.get(i)));
                }
            }




            preferenceAlDatas =preferencePojo.getData();

            preferenceAlDatas_before.addAll(preferenceAlDatas);


        }
        else if("121".equals(preferencePojo.getErrorFlag()))
        {
            Toast.makeText(SetPreferenceActivty.this, SetPreferenceActivty.this.getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
            starting_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            /**
             * Clearing the session manager for fresh user.*/
            sessionManager.clear_session_manager();
            SetPreferenceActivty.this.finish();
            SetPreferenceActivty.this.startActivity(starting_intent);
        }
    }


    /**
     * <p>
     *     This method is used to make array list for preference_id and options for doing
     *     service call of update preference.
     * </p>
     */
    private JSONArray creaste_Selected_list()
    {
        JSONArray selected_data=new JSONArray();

        try
        {
            for(int count=0;count<preferenceAlDatas.size();count++)
            {
                if(preferenceAlDatas.get(count).getTypeOfPreference().equals("4"))
                {
                    JSONObject edt_data=new JSONObject();
                    edt_data.put("pref_id",preferenceAlDatas.get(count).getId());
                    if(preferenceAlDatas.get(count).getSelected()==null)
                    {
                        edt_data.put("options","");
                    }else
                    {
                        edt_data.put("options", preferenceAlDatas.get(count).getSelected().get(0));
                    }
                    selected_data.put(edt_data);

                }else if(preferenceAlDatas.get(count).getTypeOfPreference().equals("3"))
                {
                    /**
                     * Creating a pojo class for data class for the */
                    JSONObject obj=new JSONObject();
                    obj.put("pref_id", preferenceAlDatas.get(count).getId());
                    JSONObject obj1 = new JSONObject();
                    obj1.put("Start",preferenceAlDatas.get(count).getSelected_min());
                    obj1.put("End",preferenceAlDatas.get(count).getSelected_max());
                    obj.put("options", obj1);
                    selected_data.put(obj);

                }else
                {
                    JSONObject choice_data=new JSONObject();
                    choice_data.put("pref_id", preferenceAlDatas.get(count).getId());
                    choice_data.put("options", new JSONArray(preferenceAlDatas.get(count).getSelected()));
                    if(preferenceAlDatas.get(count).getPreferenceTitle().equals("Gender")&&!(preferenceAlDatas.get(count).getSelected()!=null&&preferenceAlDatas.get(count).getSelected().size()>0))
                    {
                        ismandatory_field_selected=false;
                        mandatory_field_title=preferenceAlDatas.get(count).getPreferenceTitle();
                    }

                    selected_data.put(choice_data);
                }

            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        return selected_data;
    }

    /**
     * <h2>updatePreference</h2>
     * <P>
     *   Method cal a async task to update the all the details of the prference changed to the server.
     *   Create Json object and pass all the parameter to Json object.
     * </P>
     */
    private void updatePreference(final boolean isFor_retry)
    {

        ismandatory_field_selected=true;
        mandatory_field_title="";

        JSONArray jsonArray=creaste_Selected_list();

        if(ismandatory_field_selected)
        {
            if(isFor_retry)
            {
                connection_failed_rl.setVisibility(View.GONE);
                get_pref_progress_bar.setVisibility(View.VISIBLE);
            }else
            {
                rL_setPref_doneBtn.setVisibility(View.GONE);
                update_progress_bar.setVisibility(View.VISIBLE);
            }

            if(Utility.isNetworkAvailable(SetPreferenceActivty.this))
            {

                JSONObject jsonObject=new JSONObject();
                try
                {
                    jsonObject.put("ent_dev_id",Utility.getDeviceId(this));
                    jsonObject.put("ent_fbid",sessionManager.getFbId());
                    jsonObject.put("ent_sess_token",sessionManager.getToken());
                    jsonObject.put("selected_value", jsonArray);
                }
                catch (Exception e)
                {
                    e.printStackTrace();

                    if(update_progress_bar!=null)
                    {
                        update_progress_bar.setVisibility(View.GONE);
                    }
                }

                OkHttp3Connection.doOkHttp3Connection(ServiceUrl.UPDATE_PREFERENCE, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String result)
                    {
                        if(isFor_retry)
                        {
                            if(get_pref_progress_bar!=null)
                            {
                                get_pref_progress_bar.setVisibility(View.GONE);
                            }

                        }else
                        {
                            if(update_progress_bar!=null)
                            {
                                update_progress_bar.setVisibility(View.GONE);
                            }
                        }
                        UpdatePreference updatePreference;
                        Gson gson = new Gson();
                        updatePreference = gson.fromJson(result, UpdatePreference.class);

                        if (updatePreference.getErrorFlag().equals("0"))
                        {
                            Intent intent = new Intent(SetPreferenceActivty.this,HomePageActivity.class);
                            VariableConstant.isComming_from_prference=true;
                            if (VariableConstant.isComming_fom_Home)
                            {
                                VariableConstant.isComming_fom_Home = false;
                                finish();
                                overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                            } else {
                                startActivity(intent);
                                overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                                finish();
                            }

                        } else
                        {
                            if(isFor_retry)
                            {
                                if(get_pref_progress_bar!=null)
                                {
                                    get_pref_progress_bar.setVisibility(View.GONE);
                                    connection_failed_rl.setVisibility(View.VISIBLE);
                                }

                            }else
                            {
                                if(update_progress_bar!=null)
                                {
                                    update_progress_bar.setVisibility(View.GONE);
                                    connection_failed_rl.setVisibility(View.VISIBLE);
                                }
                            }

                            Toast.makeText(SetPreferenceActivty.this, updatePreference.getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(String error)
                    {
                        Toast.makeText(SetPreferenceActivty.this, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }else
            {
                /**
                 * NO internet connection showing error.*/
                get_pref_progress_bar.setVisibility(View.GONE);
                update_progress_bar.setVisibility(View.GONE);
                pref_details.setVisibility(View.GONE);
                connection_failed_rl.setVisibility(View.VISIBLE);
                /**
                 * Showing aleret.*/
                net_work_failed_aleret.net_work_fail(SetPreferenceActivty.this, new Net_work_failed_aleret.Internet_connection_Callback()
                {
                    @Override
                    public void onSucessConnection(String connection_Type)
                    {

                    }

                    @Override
                    public void onErrorConnection(String error)
                    {

                    }
                });
            }

        }else
        {
            /**
             * Showing aleret for mandatory field.*/
            show_mandatory_aleret(this, mandatory_field_title);
        }

    }
    /**
     * This method is used
     */
    @Override
    public void onBackPressed()
    {
        if(VariableConstant.isComming_fom_Home)
        {
            VariableConstant.isComming_fom_Home = false;
            /**
             * Finishing the class.*/
            super.onBackPressed();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * <h2>gps_Setting_aleret</h2>
     * <p/>
     * <h3>
     * Method to open a aleret box to show internet setting intent on open of aleret.
     * </h3>
     * </P>
     */
    private void show_mandatory_aleret(Activity mactivity,String title)
    {
        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(mactivity);
        alertDialog.setTitle(R.string.note_aleret);
        alertDialog.setMessage(title+" "+getString(R.string.itsmandatory));
        alertDialog.setIcon(R.drawable.launch_con);
        alertDialog.setPositiveButton(R.string.ok_text, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.support.v7.app.AlertDialog dialog_parent = alertDialog.show();
        dialog_parent.show();
    }

}
