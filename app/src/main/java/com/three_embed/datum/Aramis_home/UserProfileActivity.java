package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.three_embed.datum.AdapterClass.Other_user_Profile_adafter;
import com.three_embed.datum.AdapterClass.Profile_pic_adapter;
import com.three_embed.datum.Aleret_dialogs.Report_dialog;
import com.three_embed.datum.Pojo_classes.GetProfilePojo;
import com.three_embed.datum.Pojo_classes.user_list_item_details;
import com.three_embed.datum.Utility.CirclePageIndicator;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.ShadowTransformer;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;




/**
 * <h1>UserProfileActivity</h1>
 * <P>
 *     Class use to view user details .
 * </P>*/
public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener
{
    private CoustomListview mListView;
    private ViewPager profile_pic_pager;
    private CirclePageIndicator profile_pic_indicator;
    private ShadowTransformer shadowTransformer;
    private int Screenwidth=0;
    private Activity context;
    private String facebook_id="";
    private String distance=null;
    private ArrayList<String> otherImages_list;
    private ImageButton dis_like_button,like_button;
    private String item_position=null;
    private SessionManager sessionManager;
    private ProgressBar progress_bar;
    private Intent starting_intent=null;
    private boolean isMatched_already=false;
    public Typeface GothamRoundedMedium,GothamRoundedBold;
    private  Other_user_Profile_adafter profileDetailsAdap;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null)
        {
            actionBar.hide();
        }else
        {
            ActionBar actionBar1=getActionBar();
            if(actionBar1!=null)
            {
                actionBar1.hide();
            }
        }
        /**
         * On Invalid token*/
        starting_intent=new Intent(this,SplashScreen.class);


        /**
         * Fonts for hum tum*/
        GothamRoundedMedium= Typeface.createFromAsset(getAssets(), "fonts/MyriadPro-Regular.otf");
        GothamRoundedBold=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Bold.otf");

        sessionManager=new SessionManager(this);
        TextView header_title = (TextView) findViewById(R.id.header_title);
        assert header_title != null;
        header_title.setTypeface(GothamRoundedBold);
        /**
         * Reading the screen height to set the max Zoom size of the Coustom Listvew Header..
         */
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        Screenwidth= displaymetrics.heightPixels;
        RelativeLayout relative_backbutton = (RelativeLayout) findViewById(R.id.relative_backbutton);
        assert relative_backbutton != null;
        relative_backbutton.setOnClickListener(this);
        context=UserProfileActivity.this;
        shadowTransformer=new ShadowTransformer();

        Intent intent = getIntent();
        distance = intent.getStringExtra("distance");
        if(distance==null)
        {
            distance="0.0"+" "+"Km away";
        }
        String first_name = intent.getStringExtra("firstName");
        String other_Images = intent.getStringExtra("OtherImages");
        /**
         * Setting the header title as User name.*/
        header_title.setText(first_name);
        String age = intent.getStringExtra("age");
        String profile_pPic = intent.getStringExtra("pPic");
        String profile_video = intent.getStringExtra("pVideo");
        String profile_video_thumbnail = intent.getStringExtra("VideoThumnail");
        facebook_id = intent.getStringExtra("fbId");
        String user_about = intent.getStringExtra("persDesc");
        item_position= intent.getStringExtra("Item_position");
        /**
         * Checking is matched.*/
        String isMatched = intent.getStringExtra("match_status");

        isMatched_already = isMatched != null && isMatched.equalsIgnoreCase("1");

        /**
         *Initializing the element.*/
        intializationView();

        if(other_Images !=null&&!other_Images.equals(""))
        {
            otherImages_list=careateArrayList(other_Images);
        }
        /**
         * Set the user Details.*/
       setImages(profile_pPic, otherImages_list, first_name, age, user_about, distance, profile_video_thumbnail, profile_video,"","");

        /**
         * Calling this method to get User details*/
       getUser_Details(facebook_id);
    }
    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume()
    {
        super.onResume();
        if(profileDetailsAdap!=null)
        {
            profileDetailsAdap.setDownLoadFlage();
        }
    }
    /**
     * <h2>intializationView</h2>
     * <P>
     *
     * </P>*/
    private void intializationView()
    {
        progress_bar=(ProgressBar)findViewById(R.id.progress_bar);

        dis_like_button=(ImageButton)findViewById(R.id.dis_like_button);
        assert dis_like_button != null;
        dis_like_button.setOnClickListener(this);
        like_button=(ImageButton)findViewById(R.id.like_button);
        assert like_button != null;
        like_button.setOnClickListener(this);

        if(isMatched_already)
        {
            dis_like_button.setVisibility(View.GONE);
            like_button.setVisibility(View.GONE);
        }else
        {
           /* *
         * Starting left in animation.*/
            Open_Animation();
        }



        mListView = (CoustomListview) findViewById(R.id.layout_listview);
        @SuppressLint("InflateParams") View header = LayoutInflater.from(this).inflate(R.layout.customlistviewheader,null);
        profile_pic_pager=(ViewPager)header.findViewById(R.id.pager);
        profile_pic_pager.setOnPageChangeListener(pageChangeListener);
        profile_pic_pager.setPageTransformer(false, shadowTransformer);
        profile_pic_indicator=(CirclePageIndicator)header.findViewById(R.id.indicator);
        RelativeLayout custoomlistview_heAder = (RelativeLayout) header.findViewById(R.id.listviewheader);
        mListView.setZoomRatio(Screenwidth);
        mListView.setCustomListHeaderImageView(custoomlistview_heAder);
        mListView.addHeaderView(header);
    }

    /**
     * <h2>getUser_Details</h2>
     * <P>
     *     <h3>
     *
     *     </h3>
     * </P>
     * @param FB_ID contains the fb id user whose detail is called from the Server.*/
    private void getUser_Details(final String FB_ID)
    {
        progress_bar.setVisibility(View.VISIBLE);
        JSONObject jsonObject=new JSONObject();

        try
        {
            jsonObject.put("ent_dev_id", Utility.getDeviceId(UserProfileActivity.this));
            jsonObject.put("ent_sess_token",sessionManager.getToken());
            jsonObject.put("ent_fbid",FB_ID);
            jsonObject.put("ent_data_time",Utility.getCurrentGmtTime());
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_PROFILE, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if (progress_bar != null) {
                    progress_bar.setVisibility(View.GONE);
                }
                parse_Response_Data(result);
            }

            @Override
            public void onError(String error)
            {
                if (progress_bar != null) {
                    progress_bar.setVisibility(View.GONE);
                }

                Toast.makeText(UserProfileActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * <h2>parse_Response_Data</h2>
     * <P>
     *     <h3>
     *
     *     </h3>
     * </P>
     * @param result contains the result string to parse it.*/
    private void parse_Response_Data(String result)
    {
        Gson gson=new Gson();
        GetProfilePojo getProfilePojo=gson.fromJson(result, GetProfilePojo.class);

        switch (getProfilePojo.getErrorFlag()) {
            case "0":

                setImages(getProfilePojo.getProfilePhoto(), getProfilePojo.getImages(), getProfilePojo.getFirstName(), getProfilePojo.getAge() + "", getProfilePojo.getAbout(), distance, getProfilePojo.getVideoThumnail(), getProfilePojo.getProfileVideo(), getProfilePojo.getGender(),getProfilePojo.getLastSeen());

                break;
            case "122":
                Toast.makeText(UserProfileActivity.this, getApplicationContext().getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
                starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(starting_intent);
                break;
            default:
                Toast.makeText(UserProfileActivity.this, getProfilePojo.getErrorMessage(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /**
     * <h2>setImages</h2>
     * <p>
     *  Method initialize the Adapter and Data.
     * </p>*/
    private void setImages(String profilePic_url,ArrayList<String> imagelist,String name,String age,String aboutUser,String distance,String thumb_nail_url,String video_url_data,String gender,String online_status)
    {
        ArrayList<user_list_item_details> list_data = new ArrayList<>();
        if(video_url_data!=null&&!video_url_data.isEmpty())
        {
            user_list_item_details data_item_details=new user_list_item_details();
            data_item_details.setThumb_nail_url(thumb_nail_url);
            data_item_details.setVideo_url(video_url_data);
            list_data.add(data_item_details);
        }
        user_list_item_details data_item_details_image=new user_list_item_details();
        data_item_details_image.setImage_url(profilePic_url);
        list_data.add(data_item_details_image);
        if (imagelist!=null&&imagelist.size()>0)
        {
            for(int count=0;count<imagelist.size();count++)
            {
                user_list_item_details data_item_details_image_other_list=new user_list_item_details();
                data_item_details_image_other_list.setImage_url(imagelist.get(count));
                list_data.add(data_item_details_image_other_list);
            }

        }

        Profile_pic_adapter profile_pic_adapter = new Profile_pic_adapter(UserProfileActivity.this, list_data);
        profile_pic_pager.setAdapter(profile_pic_adapter);
        profile_pic_indicator.setViewPager(profile_pic_pager);
        ArrayList<String> imageAl=new ArrayList<>();
        imageAl.add(null);

        int age_of_user=0;
        if(age!=null)
        {
            age_of_user=Integer.parseInt(age);
        }

        if(aboutUser==null)
        {
            aboutUser="";
        }
        if(gender==null)
        {
            gender="";
        }

        Other_user_Profile_adafter profileDetailsAdap = new Other_user_Profile_adafter(context, name, age_of_user, aboutUser, imageAl, distance, gender, true, online_status,facebook_id);
        mListView.setAdapter(profileDetailsAdap);
    }
    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v)
    {
        Intent intent=new Intent();
        switch (v.getId())
        {
            case R.id.relative_backbutton:
                overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                finish_Activity();
                break;
            case R.id.dis_like_button:
                intent.putExtra("MESSAGE","3");
                intent.putExtra("POSITION",item_position);
                intent.putExtra("FBID",facebook_id);
                setResult(RESULT_OK, intent);
                finish_Activity();
                break;
            case R.id.like_button:
                intent.putExtra("MESSAGE","2");
                intent.putExtra("POSITION",item_position);
                intent.putExtra("FBID",facebook_id);
                setResult(RESULT_OK, intent);
                finish_Activity();
                break;
        }
    }


    private void finish_Activity()
    {
        onBackPressed();
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed()
    {
        VariableConstant.isNotCommingFromHomePage=false;
        if(isMatched_already)
        {
            this.finish();
        }else
        {
            /**
             * Calling the close animation method.*/
            close_Animation(this);
        }

    }


    private void Open_Animation()
    {
        dis_like_button.setVisibility(View.VISIBLE);
        like_button.setVisibility(View.VISIBLE);

        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_in_animation);
        Animation animation_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.righ_in_animation);
        dis_like_button.startAnimation(animation_right);
        like_button.setAnimation(animation1);
    }

    /**
     * Close animation for User Profile activity*/
    private void close_Animation(final Activity activity)
    {
        dis_like_button.setVisibility(View.GONE);
        like_button.setVisibility(View.GONE);
        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_out_animaton);
        Animation animation_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.right_out_animation);
        animation_right.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                activity.finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        dis_like_button.startAnimation(animation_right);
        like_button.setAnimation(animation1);

    }


    public void report_user()
    {
        Report_dialog report_dialog = new Report_dialog(this, facebook_id);
        report_dialog.show();
    }

    /**
     * <h2>careateArrayList</h2>
     * <P>
     *
     * </P>
     * */
    private ArrayList<String> careateArrayList(String data)
    {
        ArrayList<String> data_Array=new ArrayList<>();
        String dataList[]=data.split(",");
        Collections.addAll(data_Array, dataList);
        return data_Array;
    }

    ViewPager.OnPageChangeListener pageChangeListener=new ViewPager.OnPageChangeListener()
    {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {

        }

        @Override
        public void onPageSelected(int position)
        {
        }

        @Override
        public void onPageScrollStateChanged(int state)
        {

        }
    };

}
