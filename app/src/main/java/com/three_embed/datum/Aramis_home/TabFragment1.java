package com.three_embed.datum.Aramis_home;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.three_embed.datum.AdapterClass.CardviewAdapter;
import com.three_embed.datum.Aleret_dialogs.Its_Match_aleret;
import com.three_embed.datum.Aleret_dialogs.Net_work_failed_aleret;
import com.three_embed.datum.Aleret_dialogs.Status_aleret_dialog;
import com.three_embed.datum.AnimCanves.SmallBang;
import com.three_embed.datum.AnimCanves.SmallBangListener;
import com.three_embed.datum.Cardview.SwipeFlingAdapterView;
import com.three_embed.datum.DataHolder.EachCardviewItem;
import com.three_embed.datum.Permission_Ap23.App_permission_23;
import com.three_embed.datum.Pojo_classes.FindMatchesDatas;
import com.three_embed.datum.Pojo_classes.FindMatchesPojo;
import com.three_embed.datum.Pojo_classes.Images_url_pojo_class;
import com.three_embed.datum.Pojo_classes.SetLike_response_pojo;
import com.three_embed.datum.Pojo_classes.SetPreferenceAlData;
import com.three_embed.datum.Pojo_classes.SetPreferencePojo;
import com.three_embed.datum.Pojo_classes.UpdatePreference;
import com.three_embed.datum.Utility.CircleTransform;
import com.three_embed.datum.Utility.Location_service;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Fragment class for used to show the Landing page after the Login.
 * This class contain the Different Animation.
 * And horizontal pager.
 * This Fragment activity first call a Service to get all the Details of the nearest Located people around you.
 * Created by 3Embed on 9/12/15.
 * @since 9/12/2015
 */
public class TabFragment1 extends Fragment implements View.OnClickListener,SwipeRefreshLayout.OnRefreshListener,Location_service.GetLocationListener
{
    boolean isCardview_loadmore_active=false;
    ImageView imageView;
    RelativeLayout search_background_layout,below_view;
    WaveDrawable waveDrawable;
    SwipeFlingAdapterView flingContainer;
    static CardviewAdapter cardviewAdapter;
    ArrayList<FindMatchesDatas> listData;
    SmallBang mSmallBang;
    Button dislike_button;
    Button like_button;
    int scroll_position = 0;
    SessionManager session;
    Activity mactivity;
    int ent_page_num =0;
    RelativeLayout rL_inviteFriends;
    String divId;
    HomePageActivity homePageActivity;
    public FindMatchesDatas temp_findMatchesDatas_cardview = null;
    public FindMatchesDatas last_swap_details=null;
    Intent intent;
    DefaultItemAnimator defaultItemAnimator;
    Intent starting_intent;
    TextView finding_text_txt;
    Its_Match_aleret match_data;
    String image_url="";
    Net_work_failed_aleret net_work_failed_aleret;
    App_permission_23 app_permission_23;
    ArrayList<App_permission_23.Permission> permissions;
    Location_service location_service=null;
    private Status_aleret_dialog status_aleret_dialog;
    private TextView status_text;
    private ProgressBar status_progress_bar;
    private RelativeLayout status_button;
    private  ArrayList<String> option_Data;
    private ArrayList<SetPreferenceAlData> preferenceAlDatas;
    private  ArrayList<String> selected_Data;
    /**
     * Receive the result from a previous call to
     * {@link #startActivityForResult(Intent, int)}.  This follows the
     * related Activity API as described there in
     * {@link Activity#onActivityResult(int, int, Intent)}.
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode==12345)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                String reponse_Data=data.getStringExtra("MESSAGE");
                /**
                 * Result code 2 for Like and
                 * 3 for dislike.
                 * */
                switch (reponse_Data)
                {
                    case "2":
                        like_button.performClick();
                        break;
                    case "3":
                        dislike_button.performClick();
                        break;
                }
            }
        }else if(requestCode==Location_service.REQUEST_CHECK_SETTINGS)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                location_service.checkLocationSettings();

            }
        }else
        {
            super.onActivityResult(requestCode,resultCode,data);
        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        boolean isDenine=false;
        switch (requestCode)
        {
            case 786:
            {
                for (int grantResult : grantResults)
                {
                    if (grantResult != PackageManager.PERMISSION_GRANTED)
                    {
                        isDenine = true;
                    }
                }
            }
            if(isDenine)
            {
                app_permission_23.getPermission(this.permissions,mactivity,false);
            }else
            {
                /**
                 * Calling the location changed service to get The current location.*/
                get_Current_Location();
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Set a hint to the system about whether this fragment's UI is currently visible
     * to the user. This hint defaults to true and is persistent across fragment instance
     * state save and restore.
     * <p/>
     * <p>An app may set this to false to indicate that the fragment's UI is
     * scrolled out of visibility or is otherwise not directly visible to the user.
     * This may be used by the system to prioritize operations such as fragment lifecycle updates
     * or loader ordering behavior.</p>
     *
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        /**
         * Litening the network failed aleret.*/
        net_work_failed_aleret = Net_work_failed_aleret.getInstance();
        /**
         * Initializing the list for holding the data.
         * Clearing the data for fresh new list if exist.*/
        listData=new ArrayList<>();
        listData.clear();
        /**
         * Receiving the actvity refreence of parent. */
        mactivity = getActivity();
        /**
         * Initializing web drawable to to get the data.*/
        waveDrawable = new WaveDrawable(ContextCompat.getColor(mactivity, R.color.pinkcolor),450);
        mSmallBang = SmallBang.attach2Window(mactivity);
        /**
         * Reading the device id.*/
        divId = Utility.getDeviceId(mactivity);

        status_aleret_dialog=Status_aleret_dialog.getInstance();
        /**
         * Initializing the Sessionmanager object.*/
        session = new SessionManager(mactivity);
        /**
         * Down casting the Activity to the Homactivity to acess the
         * Home page activity public objects.*/
        homePageActivity = (HomePageActivity)mactivity;
        /**
         * Initializing the default Animater for to the recycle view.*/
        defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setAddDuration(500);
        defaultItemAnimator.setRemoveDuration(500);
        /**
         * Initializing the intent for the user profile.*/
        intent = new Intent(mactivity,UserProfileActivity.class);
        /**
         * if session expired then opening the landing page.*/
        starting_intent = new Intent(mactivity,SplashScreen.class);
        /**
         * Permission checker for location.*/
        app_permission_23=App_permission_23.getInstance();
        permissions=new ArrayList<>();
        permissions.add(App_permission_23.Permission.LOCATION);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.tab_fragment_1, container, false);
        init_View(view);
        /**
         *Creating the object of aleret box of the match activity.*/
        match_data=Its_Match_aleret.getInstance();
        return view;
    }
    /**
     * <h2>init_View</h2>
     * Initializing the card view.
     */
    private void init_View(View view)
    {
        below_view=(RelativeLayout)view.findViewById(R.id.below_view);
        flingContainer = (SwipeFlingAdapterView) view.findViewById(R.id.frame);
        imageView = (ImageView) view.findViewById(R.id.image);
        rL_inviteFriends = (RelativeLayout) view.findViewById(R.id.rL_inviteFriends);
        rL_inviteFriends.setOnClickListener(this);
        finding_text_txt=(TextView)view.findViewById(R.id.finding_text);
        status_text=(TextView)view.findViewById(R.id.status_text);
        search_background_layout = (RelativeLayout) view.findViewById(R.id.search_layout);
        status_progress_bar=(ProgressBar)view.findViewById(R.id.status_progress_bar);
        /**
         * Checking the android version to avoid the deprecation problem..
         * <h1>Note :</h1>
         * {@code setBackgroundDrawable()} this method is deprecated method after  android JELLY_BEAN version .
         * to avoid the deprecation problem first checking the SDK version then applying the method according
         * to the required SDK version method.*/
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN)
        {
            search_background_layout.setBackgroundDrawable(waveDrawable);
        } else {
            search_background_layout.setBackground(waveDrawable);
        }

        /**
         * Defining a LinearInterpolator animation object for doing animation.
         * and passing that object to the Wave animator class.
         * @see LinearInterpolator
         * @see WaveDrawable*/
        Interpolator interpolator = new LinearInterpolator();
        waveDrawable.setWaveInterpolator(interpolator);

        /**
         * dislike button
         */
        dislike_button = (Button) view.findViewById(R.id.dislike_button);
        dislike_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                like(v);
                flingContainer.getTopCardListener().selectLeft();
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(1);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(0);
            }
        });

        status_button=(RelativeLayout)view.findViewById(R.id.status_button);
        status_button.setOnClickListener(this);
        /**
         * like button
         */
        like_button = (Button) view.findViewById(R.id.like_button);
        like_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                like(v);
                flingContainer.getTopCardListener().selectRight();
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(1);
            }
        });

        /**
         * rewind button
         */
        Button rewind_button = (Button) view.findViewById(R.id.rewind_button);
        rewind_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    /*
                    * Handel this.*/
                rewind_Service(v);
            }
        });

        /**
         *Defining the Card layout ... */
        flingContainer = (SwipeFlingAdapterView)view.findViewById(R.id.frame);
        intializecardViewListener(flingContainer);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(VariableConstant.isComming_from_prference||!homePageActivity.isMatch_data_found)
        {
            /**
             * Setting current tab for when comming from Preference.*/
            if(VariableConstant.isComming_from_prference)
            {
                homePageActivity.setCurrent_Tab();
                rL_inviteFriends.setVisibility(View.GONE);
                finding_text_txt.setText(R.string.search_text);
            }
            /**
             * If comming from the Preference.*/
            homePageActivity.isMatch_data_found=false;
            /**
             * hiding the card view*/
            switch_Card_View(false);
            waveDrawable.startAnimation();
            ent_page_num=0;
            /**
             * Clearing the List For get refresh Data.*/
            listData.clear();

            VariableConstant.isComming_from_prference=false;

            /**
             * Calling this method to get the Data...*/
            if (Build.VERSION.SDK_INT >= 23)
            {
                if(app_permission_23.getPermission(permissions,mactivity,false))
                {
                    /**
                     * getting the result.*/
                    get_Current_Location();
                }
            }else
            {
                /**
                 * getting the result.*/
                get_Current_Location();
            }
        }

        String urls=session.getIMages_url();
        Images_url_pojo_class images_url_pojo_class=new Gson().fromJson(urls, Images_url_pojo_class.class);

        if(images_url_pojo_class!=null&&images_url_pojo_class.getImageListUrl().size()>0)
        {
            image_url =images_url_pojo_class.getImageListUrl().get(0);
        }

        if (image_url != null&& !image_url.equals(""))
        {
            imageView.setTag(Utility.get_Target_Place_Holder(getActivity(),imageView,null));
            Picasso
                    .with(mactivity)
                    .load(image_url)
                    .transform(new CircleTransform())
                    .resize(250,250)
                    .error(R.drawable.chat_profile_default_image_frame)
                    .placeholder(R.drawable.chat_profile_default_image_frame)
                    .into((Target) imageView.getTag());
        }
    }

    /**
     * Getting the Current Location of the user.*/
    private void get_Current_Location()
    {
        /**
         * Checking the location service.*/
        if(location_service==null)
        {
            location_service=new Location_service(mactivity,this);
        }else
        {
            /**
             * Checking the location setting.*/
            location_service.checkLocationSettings();
        }
    }

    /**
     * <h2>getMatchData</h2>
     *<p>
     *     Method call a asyanc task to get all the match data from the server
     *     by the help of the async task.
     *</p>
     */
    public void getMatchData()
    {
        if (inter_Check())
        {
            /**
             * Reading the page count.*/
            String no_of_page=Integer.toString(ent_page_num);
            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", divId);
                jsonObject.put("ent_page_num",no_of_page);
                jsonObject.put("ent_sess_token",session.getToken());
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            Log.d("Request_data", jsonObject.toString());

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.FIND_MATCHES, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    Log.d("Resultdata", result);

                    isCardview_loadmore_active = false;
                    findmatchesResposeHandler(result);
                    /**
                     * Increasing the page no for load more.
                     */
                    ent_page_num = ent_page_num + 1;
                }

                @Override
                public void onError(String error)
                {
                    Utility.printLog(error);
                }
            });

        }else
        {
            /**
             * Aleret for no Internet Connection.*/
            if(net_work_failed_aleret!=null)
            {
                net_work_failed_aleret.net_work_fail(mactivity, new Net_work_failed_aleret.Internet_connection_Callback() {
                    @Override
                    public void onSucessConnection(String connection_Type) {

                    }

                    @Override
                    public void onErrorConnection(String error) {

                    }
                });
            }
        }
    }

    /**
     * <h2>findmatchesResposeHandler</h2>
     * <p>
     *     Method handel the response of the getMatch service and
     *     convert it into the object type of class FindMatchesPojo.
     * </p>
     * @see FindMatchesPojo .
     * @param respose contains the reponse string data.
     */
    private void findmatchesResposeHandler(String respose)
    {
        FindMatchesPojo matchesPojo;
        Gson gson = new Gson();
        matchesPojo = gson.fromJson(respose, FindMatchesPojo.class);

        switch (matchesPojo.getErrorFlag())
        {
            case "0":
                if (ent_page_num == 0)
                {
                    /**
                     * Clearing the list refresh data for pull to refresh.*/
                    listData.clear();
                }

                if (matchesPojo.getTotalMatches2().size() > 0)
                {
                    homePageActivity.isMatch_found = true;
                    homePageActivity.isMatch_data_found = true;
                    /**
                     *Initalize a data .*/
                    generateData(matchesPojo.getTotalMatches2());
                    /**
                     * Stopping the web animation .*/
                    waveDrawable.stopAnimation();
                    /**
                     * Card view switcher.*/
                    switch_Card_View(true);
                } else
                {
                    if (ent_page_num > 0)
                    {
                        finding_text_txt.setText(mactivity.getString(R.string.nomatch_found));
                    } else
                    {
                        homePageActivity.isMatch_data_found = false;
                        homePageActivity.isMatch_found = false;
                        rL_inviteFriends.setVisibility(View.VISIBLE);
                        finding_text_txt.setText(mactivity.getString(R.string.nomatch_found));
                    }
                }
                break;
            case "122":
                homePageActivity.isMatch_found = false;
                Toast.makeText(mactivity, mactivity.getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
                starting_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                /**
                 * Clearing the session manager for fresh user.*/
                session.clear_session_manager();
                mactivity.finish();
                mactivity.startActivity(starting_intent);
                break;
            default:
                homePageActivity.isMatch_found = false;
                Utility.printLog(matchesPojo.getErrorMessage());
                break;
        }

    }

    /**
     * <h2>UpdateCurrent_Location</h2>
     * <P>
     *     Updating the current Location of the server.
     * </P>
     * @param location contains the current location of the user.
     * */
    private void UpdateCurrent_Location(Location location)
    {
        if(!Utility.isNetworkAvailable(mactivity))
        {
            Toast.makeText(mactivity,mactivity.getString(R.string.internet_not_available),Toast.LENGTH_SHORT).show();
            return;
        }
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_dev_id", Utility.getDeviceId(mactivity));
            jsonObject.put("ent_sess_token",session.getToken());
            jsonObject.put("ent_curr_lat", "" + location.getLatitude());
            jsonObject.put("ent_curr_long",""+location.getLongitude());
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.UPDATE_LOCATION, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result) {
                /**
                 * getting the user preference.*/
                getPreferences();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(mactivity, error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState)
    {
        super.onViewStateRestored(savedInstanceState);
    }



    /**
     * Method is used to listen the swap of the card view and perform action according to that.*/
    private void intializecardViewListener(SwipeFlingAdapterView swipeFlingAdapterView)
    {
        swipeFlingAdapterView.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {

            }

            /**This function works while card exited to the left..*/
            @Override
            public void onLeftCardExit(Object dataObject) {
                if (inter_Check()) {
                    temp_findMatchesDatas_cardview = listData.get(0);
                    listData.remove(0);
                    cardviewAdapter.notifyDataSetChanged();
                    likesOrDislikeService(VariableConstant.SET_DIS_LIKES, temp_findMatchesDatas_cardview.getFbId());
                } else {
                    cardviewAdapter.notifyDataSetChanged();
                    Toast.makeText(mactivity, mactivity.getString(R.string.networknotavailable), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                if (inter_Check()) {
                    temp_findMatchesDatas_cardview = listData.get(0);
                    /**
                     * Updating the card view */
                    listData.remove(0);
                    cardviewAdapter.notifyDataSetChanged();
                    likesOrDislikeService(VariableConstant.SET_LIKES, temp_findMatchesDatas_cardview.getFbId());
                } else {
                    cardviewAdapter.notifyDataSetChanged();
                    Toast.makeText(mactivity, mactivity.getString(R.string.networknotavailable), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onUpperCardExit(Object dataObject) {
                cardviewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                if (itemsInAdapter == 0) {
                    switch_Card_View(false);
                    waveDrawable.startAnimation();
                }
                isCardview_loadmore_active = itemsInAdapter < 4;
                /**
                 * Checking the list have remaining data is there or not.*/
                check_List_have_remaining_Data();
            }

            @Override
            public void onAdapterItemClick(Object dataObject) {
                Integer position = (Integer) dataObject;
                FindMatchesDatas findMatchesDatas = listData.get(position);
                intent.putExtra("distance", findMatchesDatas.getDistance());
                intent.putExtra("firstName", findMatchesDatas.getFirstName());
                intent.putExtra("lastName", findMatchesDatas.getLastName());
                intent.putExtra("age", findMatchesDatas.getAge());
                intent.putExtra("sharedLikes", findMatchesDatas.getSharedLikes());
                intent.putExtra("pPic", findMatchesDatas.getpPic());
                intent.putExtra("pVideo", findMatchesDatas.getpVideo());
                intent.putExtra("VideoThumnail", findMatchesDatas.getVideoThumnail());
                intent.putExtra("ImgCount", findMatchesDatas.getImgCount());
                intent.putExtra("sex", findMatchesDatas.getSex());
                intent.putExtra("fbId", findMatchesDatas.getFbId());
                intent.putExtra("persDesc", findMatchesDatas.getPersDesc());
                intent.putExtra("Item_position", "" + 0);
                startActivityForResult(intent, 12345);
            }


            @Override
            public void onScroll(float scrollProgressPercent, int isHorizontal, float scrollHorizontalProgressPercent) {
                if (scrollProgressPercent == 0.0) {
                    scroll_position = 0;
                } else {
                    scroll_position = scroll_position + 1;
                }
                /**
                 * Reading the current selected view.*/
                View view = flingContainer.getSelectedView();

                if (view != null && isHorizontal == 1 && scrollHorizontalProgressPercent < (-1.7)) {
                    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(0);
                    view.findViewById(R.id.item_swipe_left_indicator).setAlpha(0);
                } else if (view != null && scrollProgressPercent < -0.1 && isHorizontal == 0) {
                    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                    view.findViewById(R.id.item_swipe_left_indicator).setAlpha(0);
                } else if (view != null && scrollProgressPercent > 0.1 && isHorizontal == 0) {
                    view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(0);
                } else if (view != null) {
                    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(0);
                    view.findViewById(R.id.item_swipe_left_indicator).setAlpha(0);
                }
            }
        });

    }

    /**
     * Calling this method on Each time the Image is Removed when the
     * Image is removed.*/
    public static void removeBackground()
    {
        EachCardviewItem.getInstance().background.setVisibility(View.GONE);

        cardviewAdapter.notifyDataSetChanged();
    }
    /**
     * <h2>generateData</h2>
     * <P>
     *  adding the data to the list.
     * </P>
     * @param matcheslist contains the list of match item to be added in main list.
     */
    private void generateData(ArrayList<FindMatchesDatas> matcheslist)
    {
        /**
         * Initialize Card view adapter. and notifying the Card Adapter.
         */
        listData.addAll(matcheslist);

        /**
         * Notifying previously created list data.*/
        if (cardviewAdapter != null) {
            flingContainer.notifyi_data_setChanged();
        }
        cardviewAdapter = new CardviewAdapter(listData, mactivity);
        flingContainer.setAdapter(cardviewAdapter);
        cardviewAdapter.notifyDataSetChanged();

        //test
        flingContainer.notifyi_data_setChanged();
        cardviewAdapter.notifyDataSetChanged();
    }

    /**
     * Method is used to set the animation of bubble on the given view as a parameter ..
     * by calling a bang to set the view and then setting the listener
     * and listening the on start and end of animation..
     * @param  view Contains the view object in which animation occur.*/
    public void like(View view)
    {
        mSmallBang.bang(view);
        mSmallBang.setmListener(new SmallBangListener() {
            @Override
            public void onAnimationStart() {

            }

            @Override
            public void onAnimationEnd() {

            }
        });
    }

    /**
     * <h>LikesOrDislikeService</h>
     * <p>
     *     This is likesOrDislikeService used to like or disLike the image.
     *     when user click on like or dislike button. that will be performed
     *     depending upon the likesOrDislikesVar values. if it is 1 then
     *     setLikes service will be called and if value is 2 then dislike
     *     service will be called.
     * </p>
     * @see OkHttp3Connection .
     * @param likesOrDislikesVar .This value may be 1(like) or 2(disLike)
     */
    public void likesOrDislikeService(String likesOrDislikesVar, final String fb_Id)
    {
        if(listData.size()==0)
        {
            switch_Card_View(false);
            waveDrawable.startAnimation();
        }
        homePageActivity.likesOrDislikeService(likesOrDislikesVar, fb_Id, new HomeActivity_call_Back() {
            @Override
            public void onSuperlikeSucess(String sucess) {
                if (temp_findMatchesDatas_cardview != null) {
                    last_swap_details = temp_findMatchesDatas_cardview;
                }
                Gson gson = new Gson();
                SetLike_response_pojo setLike_response_pojo;
                setLike_response_pojo = gson.fromJson(sucess, SetLike_response_pojo.class);
                if (setLike_response_pojo.getErrorFlag().equals("0")) {
                    if (setLike_response_pojo.getErrorMessage().equals("Matched")) {
                        /**
                         * */
                        match_data.show_info_aleret(getActivity(), image_url, setLike_response_pojo.getPPic(), setLike_response_pojo.getUName(), setLike_response_pojo.getEmail(), setLike_response_pojo.getUFbId());
                    }
                } else if (setLike_response_pojo.getErrorFlag().equals("121") || setLike_response_pojo.getErrorFlag().equals("122")) {
                    homePageActivity.inValideToken(mactivity);
                }
                /**
                 * Checking that is card is in Loadmore mode.*/
                if (isCardview_loadmore_active) {
                    /**
                     * Load more on card view.*/
                    getMatchData();
                }
            }

            @Override
            public void onSuperlikeError(String error) {
                Toast.makeText(mactivity, error, Toast.LENGTH_SHORT).show();
                listData.add(0, temp_findMatchesDatas_cardview);
                flingContainer.notifyi_data_setChanged();
                cardviewAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.rL_inviteFriends :
                Utility.shareApp(mactivity, homePageActivity.getResources().getString(R.string.app_name),homePageActivity.sessionManager.getFirstName()+" wants you to download"+homePageActivity.getResources().getString(R.string.app_name));
                break;
            case R.id.status_button:
            {
                update_Status();
                break;
            }
        }


    }

    /**
     * <h2>inter_Check</h2>
     * <P>
     * Checking the internet connection is available or not.
     * </P>*/
    private boolean inter_Check()
    {

        return Utility.isNetworkAvailable(mactivity);

    }

    @Override
    public void onRefresh()
    {
        /**
         * On pull to refresh loading that data from first.*/
        ent_page_num=0;

        /**
         * Load more for data.*/
        getMatchData();

    }
    /**
     * <h2>rewind_Service</h2>
     * <P>
     *  Doing rewind service to get the Data.
     * </P>*/
    private void rewind_Service(final View view)
    {
        view.setClickable(false);
        if(last_swap_details!=null)
        {
            final JSONObject jsonObject=new JSONObject();

            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(mactivity));
                jsonObject.put("ent_sess_token", session.getToken());
                jsonObject.put("ent_fbid", session.getFbId());
                jsonObject.put("ent_user_fbid",last_swap_details.getFbId());
            } catch (JSONException e)
            {
                e.printStackTrace();
            }

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.REWIND, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback()
            {
                @Override
                public void onSuccess(String result)
                {
                    Log.d("Response Rewind",result);

                    try
                    {
                        JSONObject  jsonObject1=new JSONObject(result);
                        if(jsonObject1.getString("errorFlag").equalsIgnoreCase("0"))
                        {
                            listData.add(0,last_swap_details);
                            flingContainer.notifyi_data_setChanged();
                            cardviewAdapter.notifyDataSetChanged();
                        }

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    /**
                     * Setting the reference to null.*/
                    last_swap_details=null;
                    view.setClickable(true);
                }
                @Override
                public void onError(String error)
                {
                    view.setClickable(true);
                    Toast.makeText(mactivity,error,Toast.LENGTH_SHORT).show();
                }
            });
        }else
        {
            view.setClickable(true);
            Toast.makeText(mactivity, R.string.likedislikeText,Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Called when the fragment is no longer in use.  This is called
     * after {@link #onStop()} and before {@link #onDetach()}.
     */
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(location_service!=null)
        {
            location_service.stop_Location_Update();
            location_service.destroy_Location_update();
        }
    }

    @Override
    public void updateLocation(Location location)
    {
        /**
         * Updating location to the location service.
         * and stopping the location update.*/
        location_service.stop_Location_Update();
        UpdateCurrent_Location(location);
    }

    @Override
    public void location_Error(String error)
    {
        Toast.makeText(mactivity,error,Toast.LENGTH_SHORT).show();
    }

    /**
     * <h2>check_List_have_remaining_Data</h2>
     * <P>
     *     Check list is containing the data.
     * </P>*/
    public void check_List_have_remaining_Data()
    {
        homePageActivity.isMatch_found = !listData.isEmpty();
    }
    /**<h2>switch_Card_View</h2>
     * <P>
     * Switch the screen view in android.
     * </P>
     * @param Active_Card boolean show card to be active or not.
     */
    private void switch_Card_View(boolean Active_Card)
    {
        if(Active_Card)
        {
            search_background_layout.setVisibility(View.GONE);
            flingContainer.setVisibility(View.VISIBLE);
            below_view.setVisibility(View.VISIBLE);

        }else
        {
            flingContainer.setVisibility(View.GONE);
            below_view.setVisibility(View.GONE);
            search_background_layout.setVisibility(View.VISIBLE);
        }
    }
    /**
     * service call using OkHttp client for getting preferences
     * datas which is being set during sign-Up
     */
    private void getPreferences()
    {

        if (status_progress_bar != null)
        {
            status_button.setEnabled(false);
            status_text.setVisibility(View.GONE);
            status_progress_bar.setVisibility(View.VISIBLE);
        }

        if(!Utility.isNetworkAvailable(mactivity))
        {
            Toast.makeText(mactivity,mactivity.getString(R.string.internet_not_available),Toast.LENGTH_SHORT).show();
            return;
        }

        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_dev_id", Utility.getDeviceId(mactivity.getApplicationContext()));
            jsonObject.put("ent_sess_token",homePageActivity.sessionManager.getToken());
            jsonObject.put("ent_fbid",homePageActivity.sessionManager.getFbId());
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        Log.d("Requestdata", jsonObject.toString());

        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.GET_PREFERENCES, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                Log.d("Responsedata", result);

                if (status_progress_bar != null)
                {
                    status_button.setEnabled(true);
                    status_progress_bar.setVisibility(View.GONE);
                }
                Gson gson = new Gson();
                SetPreferencePojo preferencePojo = gson.fromJson(result,SetPreferencePojo.class);
                /**
                 * Handlening the response ..*/
                addPreferenceDatas(preferencePojo);
            }

            @Override
            public void onError(String error)
            {
                if (status_progress_bar != null)
                {
                    status_text.setVisibility(View.VISIBLE);
                    status_button.setEnabled(false);
                    status_progress_bar.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * This method will add all set preference datas at run time
     */
    private void addPreferenceDatas(SetPreferencePojo preferencePojo)
    {
        if ("0".equals(preferencePojo.getErrorFlag()))
        {
            preferenceAlDatas =preferencePojo.getData();
            int preferenceSize = preferenceAlDatas.size();
            for (int i = 0; i < preferenceSize; i++)
            {
                if (preferencePojo.getData().get(i).getPreferenceTitle().equalsIgnoreCase("Mood"))
                {

                    option_Data=preferencePojo.getData().get(i).getOptionsValue();
                    selected_Data=preferencePojo.getData().get(i).getSelected();
                    if(selected_Data!=null&&selected_Data.size()>0)
                    {
                        mactivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run()
                            {
                                status_text.setText(selected_Data.get(0));
                                status_text.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                }
            }
            /**
             * Calling the getMatCha data.*/
            getMatchData();

        }else if("121".equals(preferencePojo.getErrorFlag()))
        {
            Toast.makeText(mactivity,mactivity.getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
            starting_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            /**
             * Clearing the session manager for fresh user.*/
            homePageActivity.sessionManager.clear_session_manager();
            mactivity.finish();
            mactivity.startActivity(starting_intent);
        }
    }

    /**
     * <h2>update_Status</h2>
     * <P>
     *  Updating user status.
     * </P>*/
    private void update_Status()
    {
        String[] mobileArray = option_Data.toArray(new String[option_Data.size()]);
        status_aleret_dialog.status_AlertView(mactivity, mobileArray, new Status_aleret_dialog.Status__Call_back()
        {
            @Override
            public void onSelected(final String selected_text,int position)
            {
                if(!selected_text.isEmpty())
                {
                    selected_Data.clear();
                    selected_Data.add(selected_text);
                 /**
                   * Updating user status.*/
                    update_User_Status(selected_text);

                }

            }
            @Override
            public void onCancel()
            {

            }
        });
    }
    /**
     * <h1>update_User_Status</h1>
     * <P>
     *     Updatign the user details.
     * </P>*/
    private void update_User_Status(final String status)
    {
        if(!Utility.isNetworkAvailable(mactivity))
        {
            Toast.makeText(mactivity,mactivity.getString(R.string.internet_not_available),Toast.LENGTH_SHORT).show();
            return;
        }
        status_text.setVisibility(View.GONE);
        status_progress_bar.setVisibility(View.VISIBLE);

        JSONObject request_Data=new JSONObject();
        JSONArray jsonArray=creaste_Selected_list();
        try
        {
            request_Data.put("ent_dev_id", Utility.getDeviceId(mactivity));
            request_Data.put("ent_fbid", homePageActivity.sessionManager.getFbId());
            request_Data.put("ent_sess_token", homePageActivity.sessionManager.getToken());
            request_Data.put("selected_value", jsonArray);
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }
        Log.d("Request_status", request_Data.toString());

        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.UPDATE_PREFERENCE, OkHttp3Connection.Request_type.POST, request_Data, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                status_text.setVisibility(View.VISIBLE);
                status_progress_bar.setVisibility(View.GONE);
                UpdatePreference updatePreference;
                Gson gson = new Gson();
                updatePreference = gson.fromJson(result, UpdatePreference.class);
                if (updatePreference.getErrorFlag().equals("0"))
                {
                    mactivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            status_text.setText(status);
                        }
                    });
                    /**
                     * hiding the card view*/
                    switch_Card_View(false);
                    waveDrawable.startAnimation();
                    ent_page_num=0;
                    /**
                     * Status updated calling get matches.*/
                    getMatchData();
                }else
                {
                    Toast.makeText(mactivity,mactivity.getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error)
            {
                status_text.setVisibility(View.VISIBLE);
                status_progress_bar.setVisibility(View.GONE);
                Toast.makeText(mactivity,mactivity.getString(R.string.unable_to_update),Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**<h2>creaste_Selected_list</h2>
     * <p>
     *     This method is used to make array list for preference_id and options for doing
     *     service call of update preference.
     * </p>
     */
    private JSONArray creaste_Selected_list()
    {
        JSONArray selected_data=new JSONArray();
        try
        {
            for(int count=0;count<preferenceAlDatas.size();count++)
            {
                if(preferenceAlDatas.get(count).getTypeOfPreference().equals("4"))
                {
                    JSONObject edt_data=new JSONObject();
                    edt_data.put("pref_id",preferenceAlDatas.get(count).getId());
                    if(preferenceAlDatas.get(count).getSelected()==null)
                    {
                        edt_data.put("options","");
                    }else
                    {
                        edt_data.put("options", preferenceAlDatas.get(count).getSelected().get(0));
                    }
                    selected_data.put(edt_data);

                }else if(preferenceAlDatas.get(count).getTypeOfPreference().equals("3"))
                {
                    JSONObject obj=new JSONObject();
                    obj.put("pref_id", preferenceAlDatas.get(count).getId());
                    JSONObject obj1 = new JSONObject();
                    obj1.put("Start",preferenceAlDatas.get(count).getSelected_min());
                    obj1.put("End",preferenceAlDatas.get(count).getSelected_max());
                    obj.put("options", obj1);
                    selected_data.put(obj);

                }else
                {
                    JSONObject choice_data=new JSONObject();
                    choice_data.put("pref_id", preferenceAlDatas.get(count).getId());
                    choice_data.put("options", new JSONArray(preferenceAlDatas.get(count).getSelected()));
                    selected_data.put(choice_data);
                }

            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        return selected_data;
    }

}
