package com.three_embed.datum.Aramis_home;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.three_embed.datum.AdapterClass.ChatPagerAdapter;
import com.three_embed.datum.Aramis_home.User_preference_setter.Chat_Fragment_list_data;
/**
 * <h>TabFragment2</h>
 * <P>
 *Handel the Fragment number two.
 * Which contains two chield fragment and handel their data.
 * </P>*/
public class TabFragment2 extends Fragment
{
    static boolean page_one=false;
    private ChatPagerAdapter adapter=null;
    private String subTab;
    private HomePageActivity homePageActivity=null;
    private Activity  mactivity;



    /**
     * Set a hint to the system about whether this fragment's UI is currently visible
     * to the user. This hint defaults to true and is persistent across fragment instance
     * state save and restore.
     * <p/>
     * <p>An app may set this to false to indicate that the fragment's UI is
     * scrolled out of visibility or is otherwise not directly visible to the user.
     * This may be used by the system to prioritize operations such as fragment lifecycle updates
     * or loader ordering behavior.</p>
     *
     * @param isVisibleToUser true if this fragment's UI is currently visible to the user (default),
     *                        false if it is not.
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser)
    {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser)
        {
            if(adapter!=null)
            {
                Chat_Fragment_list_data chat_fragment_list_data= (Chat_Fragment_list_data) adapter.getHost_Fragment();
                if(chat_fragment_list_data!=null)
                {
                    chat_fragment_list_data.Load_match_list();
                }

            }

        }
    }

    /**
     * Receive the result from a previous call to
     * {@link #startActivityForResult(Intent, int)}.  This follows the
     * related Activity API as described there in
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle extras=getActivity().getIntent().getExtras();

        if(extras!=null)
        {
            subTab=extras.getString("subTab");
        }
        this.mactivity=getActivity();
        homePageActivity= (HomePageActivity) this.mactivity;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view=inflater.inflate(R.layout.tab_fragment_2, container, false);
        final TabLayout tabLayout = (TabLayout)view.findViewById(R.id.tab_layout);
        TextView textView=new TextView(mactivity);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setText(getActivity().getResources().getString(R.string.match_tab));
        textView.setTypeface(homePageActivity.GothamRoundedMedium);
        TabLayout.Tab tab = tabLayout.newTab();
        tab.setCustomView(textView);
        tabLayout.addTab(tab);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager)view.findViewById(R.id.view_pager);
        adapter = new ChatPagerAdapter(getChildFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                if (tab.getPosition() == 0)
                {
                    page_one=true;
                } else if (tab.getPosition() == 1)
                {
                    page_one=false;
                }
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });


        if(subTab!=null)
        {
            int sub_tab_no=Integer.parseInt(subTab);
            tabLayout.getTabAt(sub_tab_no).select();
            /**
             * Setting null for once open .
             * for next time it will open from the default one.*/
            subTab=null;
        }

        return view;

    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * Activity's lifecycle.
     */
    @Override
    public void onResume()
    {
        super.onResume();
    }


}