package com.three_embed.datum.Aramis_home.User_preference_setter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.three_embed.datum.Pojo_classes.SetPreferenceAlData;
import com.three_embed.datum.Utility.RangeSeekBar;
import com.three_embed.datum.Aramis_home.R;
import java.util.ArrayList;
/**
 * @since  24/2/16.
 * @author 3Embed.
 */
public class User_preference_Item_Controlar
{
    private static User_preference_Item_Controlar user_preference_item_controlar;
    private Activity mactivity;
    private LayoutInflater minflater;
    private Typeface GothamRoundedMedium;
    private Typeface GothamRoundedLight;

    String Unites[]={" ","Miles","Miles","Years","Kg","Pound"};

    private User_preference_Item_Controlar(Activity activity)
    {
        this.mactivity=activity;
        this.minflater=mactivity.getLayoutInflater();
        /**
         * Font style.*/
        GothamRoundedMedium= Typeface.createFromAsset(this.mactivity.getAssets(), "fonts/MyriadPro-Regular.otf");
        GothamRoundedLight=Typeface.createFromAsset(this.mactivity.getAssets(), "fonts/MyriadPro-Regular.otf");
    }

    public static User_preference_Item_Controlar getInstance(Activity activity)
    {
        if(user_preference_item_controlar==null)
        {
            user_preference_item_controlar=new User_preference_Item_Controlar(activity);
            return user_preference_item_controlar;
        }else
        {
            return user_preference_item_controlar;
        }

    }

    /**
     * <h2>checkBox</h2>
     * <P>
     *
     * </P>
     * */
    public View checkBox(final SetPreferenceAlData object)
    {
        @SuppressLint("InflateParams") View child =minflater.inflate(R.layout.discovery_setting_main_background, null);
        /**
         * set age Preference Title
         */
        TextView tv_ShowAges = (TextView) child.findViewById(R.id.header);
        tv_ShowAges.setText(String.format("%s :", object.getPreferenceTitle()));
        tv_ShowAges.setTypeface(GothamRoundedMedium);

        TextView tv1 = (TextView) child.findViewById(R.id.header_parameter1);
        tv1.setVisibility(View.GONE);

        TextView tv2 = (TextView) child.findViewById(R.id.header_parameter2);
        tv2.setVisibility(View.GONE);

        TextView tv3 = (TextView) child.findViewById(R.id.header_parameter3);
        tv3.setVisibility(View.GONE);

        TextView tv4 = (TextView) child.findViewById(R.id.header_parameter4);
        tv4.setVisibility(View.GONE);

        final ArrayList<String> data_list = object.getOptionsValue();
        final ArrayList<String> selected = object.getSelected();

        LinearLayout sec_Parent_Layout = (LinearLayout) child.findViewById(R.id.post_chield);
        for(int count=0;count<data_list.size();count++)
        {
            @SuppressLint("InflateParams") View inner_chield =minflater.inflate(R.layout.linear_toggle_button_background, null);
            final TextView textView=(TextView)inner_chield.findViewById(R.id.text_item_header);
            textView.setText(data_list.get(count));
            textView.setTypeface(GothamRoundedLight);
            final ToggleButton toggleButton=(ToggleButton)inner_chield.findViewById(R.id.toggleButton1);
            toggleButton.setText(null);
            toggleButton.setTextOn(null);
            toggleButton.setTextOff(null);
            toggleButton.setChecked(false);

            if(selected!=null&&selected.size()>0)
            {
                for(int selected_count=0;selected_count<selected.size();selected_count++)
                {
                    if(data_list.get(count).equals(selected.get(selected_count)))
                    {
                        toggleButton.setChecked(true);
                    }
                }
            }

            toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton arg0, boolean isChecked)
                {
                    if (isChecked)
                    {
                        if(object.getSelected()==null)
                        {
                            /**
                             * Doing this because some case seleted data ,ay be null then the
                             * Arraylist will be null and i can,t directly add the data there.*/
                            ArrayList<String> current_data= new ArrayList<>();
                            current_data.add(textView.getText().toString());
                            object.setSelected(current_data);
                        }else
                        {
                            object.getSelected().add(textView.getText().toString());
                        }

                    }else
                    {
                        if(object.getSelected()!=null)
                        {
                            object.getSelected().remove(textView.getText().toString());
                        }

                    }
                }
            });
            sec_Parent_Layout.addView(inner_chield);
        }
        return child;
    }

    /**
     * <h2>radio_button</h2>
     * <P>
     *
     * </P>
     * @return View Return This View Object ot add In the Parent List.*/
    public View radio_button(final SetPreferenceAlData object)
    {
        /**
         *All Toggle button holder.*/
        final ArrayList<Object> toggle_list=new ArrayList<>();
        @SuppressLint("InflateParams") View child = minflater.inflate(R.layout.discovery_setting_main_background, null);
        /**
         * set age Preference Title
         */
        TextView title = (TextView) child.findViewById(R.id.header);
        title.setText(String.format("%s :", object.getPreferenceTitle()));
        title.setTypeface(GothamRoundedMedium);

        TextView tv1 = (TextView) child.findViewById(R.id.header_parameter1);
        tv1.setVisibility(View.GONE);

        TextView tv2 = (TextView) child.findViewById(R.id.header_parameter2);
        tv2.setVisibility(View.GONE);

        TextView tv3 = (TextView) child.findViewById(R.id.header_parameter3);
        tv3.setVisibility(View.GONE);

        TextView tv4 = (TextView) child.findViewById(R.id.header_parameter4);
        tv4.setVisibility(View.GONE);

        ArrayList<String> religionList =object.getOptionsValue();
        final ArrayList<String> selected = object.getSelected();

        LinearLayout radio_button_list = (LinearLayout) child.findViewById(R.id.post_chield);
        for(int count=0;count<religionList.size();count++)
        {
            @SuppressLint("InflateParams") View inner_chield =minflater.inflate(R.layout.linear_toggle_button_background, null);
            final TextView textView=(TextView)inner_chield.findViewById(R.id.text_item_header);
            textView.setText(religionList.get(count));
            textView.setTypeface(GothamRoundedLight);
            final ToggleButton toggleButton=(ToggleButton)inner_chield.findViewById(R.id.toggleButton1);
            toggleButton.setText(null);
            toggleButton.setTextOn(null);
            toggleButton.setTextOff(null);
            toggleButton.setChecked(false);
            /**
             * Adding the toggle button to the Toggle button List.*/
            toggle_list.add(toggleButton);

            if(selected!=null&&selected.size()>0)
            {
                for(int selected_count=0;selected_count<selected.size();selected_count++)
                {
                    if(religionList.get(count).equals(selected.get(selected_count)))
                    {
                        toggleButton.setChecked(true);
                    }
                }
            }

            toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {

                @Override
                public void onCheckedChanged(CompoundButton arg0, boolean isChecked)
                {
                    if (isChecked)
                    {
                        /**
                         * Doing this because some case seleted data ,ay be null then the
                         * Arraylist will be null and i can,t directly add the data there.*/
                        ArrayList<String> current_data= new ArrayList<>();
                        current_data.add(textView.getText().toString());
                        object.setSelected(current_data);

                        for(int no_button=0;no_button<toggle_list.size();no_button++)
                        {
                            if(!toggle_list.get(no_button).equals(toggleButton))
                            {
                                ToggleButton toggleButton1=(ToggleButton)toggle_list.get(no_button);
                                toggleButton1.setChecked(false);
                            }

                        }

                    }else
                    {
                        if(object.getSelected()!=null)
                        {
                            /**
                             * Removing the item once UnSelected.*/
                            object.getSelected().remove(textView.getText().toString());
                        }
                    }
                }
            });
            radio_button_list.addView(inner_chield);
        }
        return child;
    }

    /**
     * <h2>numeric_Range</h2>
     * <p>
     *
     * </p>*/
    public View numeric_Range(final SetPreferenceAlData object)
    {
        @SuppressLint("InflateParams") View child =minflater.inflate(R.layout.discovery_setting_main_background, null);
        RangeSeekBar<Integer> rsbShowDistance;
        /**
         * starting range value is zero.*/
        int min_range_value;
        int max_range_value;

        if(object.getStart()!=null)
        {
            min_range_value=Integer.parseInt(object.getStart());
        }else
        {
            min_range_value=0;
        }
        if(object.getEnd()!=null)
        {
            max_range_value=Integer.parseInt(object.getEnd());
        }else
        {
            max_range_value=0;
        }

        /**
         * set age Preference Title
         */
        TextView tv_ShowAges = (TextView) child.findViewById(R.id.header);
        tv_ShowAges.setText(String.format("%s :", object.getPreferenceTitle()));
        tv_ShowAges.setTypeface(GothamRoundedMedium);


        rsbShowDistance = new RangeSeekBar<>(min_range_value, max_range_value, mactivity);

        if(object.getSelected_min()!=null&& !object.getSelected_min().trim().equals(""))
        {
            rsbShowDistance.setSelectedMinValue(Integer.parseInt(object.getSelected_min()) );
        }else
        {
            rsbShowDistance.setSelectedMinValue(0);
            object.setSelected_min(mactivity.getString(R.string.Zerro));
        }
        if(object.getSelected_max()!=null&& !object.getSelected_max().trim().equals(""))
        {
            rsbShowDistance.setSelectedMaxValue(Integer.parseInt(object.getSelected_max()));
        }else
        {
            rsbShowDistance.setSelectedMaxValue(0);
            object.setSelected_max("0");
        }

        /**
         * set minimum distance
         */
        final TextView mini_mum_range = (TextView) child.findViewById(R.id.header_parameter1);
        mini_mum_range.setText(object.getSelected_min());
        mini_mum_range.setTypeface(GothamRoundedMedium);

        /**
         * set maximum distance
         */
        final TextView maximum_range= (TextView) child.findViewById(R.id.header_parameter3);
        maximum_range.setText(object.getSelected_max());
        maximum_range.setTypeface(GothamRoundedMedium);

        TextView textView=(TextView) child.findViewById(R.id.header_parameter4);
        int unit=Integer.parseInt(object.getUnit());
        String title_2=Unites[unit];
        textView.setText(title_2);
        textView.setTypeface(GothamRoundedMedium);

        /**
         * Reading the Linear Layout to Add the Data.*/
        LinearLayout numeric_range_layout = (LinearLayout) child.findViewById(R.id.post_chield);
        rsbShowDistance.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> rangeSeekBar, Integer minValue, Integer maxValue) {
                object.setSelected_max("" + maxValue);
                object.setSelected_min("" + minValue);
                mini_mum_range.setText(object.getSelected_min());
                maximum_range.setText(object.getSelected_max());
            }
        });
        numeric_range_layout.addView(rsbShowDistance);
        /**
         * Returning the View .*/
        return child;
    }

    /**
     * <h2>text_Input</h2>
     * <P>
     *
     * </P>
     * */
    public View text_Input(final SetPreferenceAlData object)
    {
        @SuppressLint("InflateParams") View child =minflater.inflate(R.layout.discovery_setting_editetxt_background, null);
        String data;
        if(object.getSelected()==null)
        {
            data="";
        }else
        {
            data=object.getSelected().get(0);
        }

        /**
         * Hint*/
        String hint=object.getOptionsValue().get(0);


        TextView tv_ShowAges = (TextView) child.findViewById(R.id.title_header);
        tv_ShowAges.setText(object.getPreferenceTitle());
        tv_ShowAges.setTypeface(GothamRoundedMedium);

        final  EditText content_data=(EditText)child.findViewById(R.id.text_data);

        if(data!=null&&data.isEmpty())
        {
            content_data.setHint(hint);
        }else if(data!=null)
        {
            content_data.setText(data);
        }

        content_data.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }
            @Override
            public void afterTextChanged(Editable s)
            {
                String data=content_data.getText().toString();
                if(object.getSelected()==null)
                {
                    ArrayList<String> data_text= new ArrayList<>();
                    data_text.add(data);
                    object.setSelected(data_text);
                }else
                {
                    object.getSelected().set(0,data);
                }

            }
        });
        return child;
    }


}