package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.three_embed.datum.Pojo_classes.DeleteAccountPojo;
import com.three_embed.datum.Pojo_classes.LogOutPojo;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * This is ProfileAppSetting activity
 * <p>
 *     This class is getting called from ProfilePageFrag. This class contains entire information
 *     of AppSetting option which is there in ProfilePageFrag. in this we provide informaion like
 *     NeedHelp, Privacy policy, Terms of services & two major field like logout and delete account
 *     for respective user account.
 * </p>
 * @author 3embed
 * @see 12-12-2015
 * @version 1.0
 */
public class ProfileAppSetting extends AppCompatActivity implements View.OnClickListener
{
    SessionManager sessionManager;
    ProgressDialog progressDialog;
    TextView tV_logOut,tV_deleteAcc,tV_private_policy,tv_NeedHelp,title_text;
    RelativeLayout relative_backbutton;
    private Typeface GothamRoundedMedium,GothamRoundedBold;

    /**
     * <p>
     *     Called when the activity is first created.
     *     Oncreate is used to start ab activity.
     * </p>
     * @param savedInstanceState saving the state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_app_setting);
        initializeVariable();

        /**
         * Fonts for hum tum*/
        GothamRoundedMedium=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Regular.otf");
        GothamRoundedBold=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Bold.otf");
    }

    /**
     * This method is getting called from onCreate method.
     * in this method we initilize all data member
     */
    private void initializeVariable()
    {
        /**
         *Setting coustom action bar in android.. */
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View customView = inflator.inflate(R.layout.profile_setting_action_bar_alyout, null);
        actionBar.setCustomView(customView);

        /**
         * setting the coustom action bar left and right margin as Zero by passing the 0,0 absloute value.
         * */
        Toolbar parent = (Toolbar) customView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        sessionManager=new SessionManager(this);

        /**
         * Setting app title style.*/
        title_text=(TextView)findViewById(R.id.title_text_one);
        assert title_text != null;
        title_text.setTypeface(GothamRoundedBold);

        /**
         * logout
         */
        tV_logOut= (TextView) findViewById(R.id.tV_logOut);
        assert tV_logOut != null;
        tV_logOut.setTypeface(GothamRoundedMedium);
        tV_logOut.setOnClickListener(ProfileAppSetting.this);

        /**
         * delete account
         */
        tV_deleteAcc= (TextView) findViewById(R.id.tV_deleteAcc);
        assert tV_deleteAcc != null;
        tV_deleteAcc.setTypeface(GothamRoundedMedium);
        tV_deleteAcc.setOnClickListener(this);

        /**
         * private policy
         */
        tV_private_policy= (TextView) findViewById(R.id.tV_private_policy);
        assert tV_private_policy != null;
        tV_private_policy.setTypeface(GothamRoundedMedium);
        tV_private_policy.setOnClickListener(this);
        /**
         * Need Help
         */
        tv_NeedHelp= (TextView) findViewById(R.id.tv_NeedHelp);
        assert tv_NeedHelp != null;
        tv_NeedHelp.setTypeface(GothamRoundedMedium);
        tv_NeedHelp.setOnClickListener(this);

        progressDialog=Utility.getProgressDialog(this);

        /**
         * back icon
         */
        relative_backbutton= (RelativeLayout) findViewById(R.id.relative_backbutton);
        assert relative_backbutton != null;
        relative_backbutton.setOnClickListener(this);

    }

    /**
     * logout service
     */
    private void logOutService()
    {
        if(Utility.isNetworkAvailable(ProfileAppSetting.this))
        {
            progressDialog.show();

            JSONObject jsonObject=new JSONObject();

            try
            {
                jsonObject.put("ent_sess_token", sessionManager.getToken());
                jsonObject.put("ent_dev_id", Utility.getDeviceId(this));
            } catch (JSONException e)
            {
                e.printStackTrace();
            }

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.LOG_OUT, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result) {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        progressDialog.cancel();
                    }
                    Utility.printLog("log out response..." + result);

                    LogOutPojo logOutPojo;
                    Gson gson = new Gson();
                    logOutPojo = gson.fromJson(result, LogOutPojo.class);

                    /**
                     * success
                     */
                    if (logOutPojo.getErrorFlag().equals("0"))
                    {
                        sessionManager.clear_session_manager();
                        VariableConstant.isLogOut = true;
                        Toast.makeText(ProfileAppSetting.this, logOutPojo.getErrorMessage(), Toast.LENGTH_SHORT).show();
                        Intent logOutIntent = new Intent(ProfileAppSetting.this, SplashScreen.class);
                        logOutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        logOutIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        logOutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(logOutIntent);
                    } else
                    {
                        Toast.makeText(ProfileAppSetting.this, logOutPojo.getErrorMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onError(String error) {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        progressDialog.cancel();
                    }
                }
            });
        }else
        {
            Toast.makeText(ProfileAppSetting.this,"Internet not available!",Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * This method is used to do service call for deleting user account from admin side
     * and once the account will be deleted then it automatically redirect to login page.
     */
    private void deleteAccService()
    {
        if(Utility.isNetworkAvailable(ProfileAppSetting.this))
        {
            progressDialog.show();
            JSONObject jsonObject=new JSONObject();

            try
            {
                jsonObject.put("ent_sess_token", sessionManager.getToken());
                jsonObject.put("ent_dev_id", Utility.getDeviceId(this));
            } catch (JSONException e)
            {
                e.printStackTrace();
            }

            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.DELETE_ACCOUNT, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if (progressDialog!=null)
                    {
                        progressDialog.dismiss();
                        progressDialog.cancel();
                    }
                    Utility.printLog("response for delete account..."+result);

                    DeleteAccountPojo accountPojo;
                    Gson gson=new Gson();
                    accountPojo=gson.fromJson(result,DeleteAccountPojo.class);

                    /**
                     * success
                     */
                    if (accountPojo.getErrorFlag().equals("0"))
                    {
                        sessionManager.clear_session_manager();
                        VariableConstant.isLogOut=true;
                        Toast.makeText(ProfileAppSetting.this,accountPojo.getErrorMessage(),Toast.LENGTH_SHORT).show();
                        Intent deleteAccIntent=new Intent(ProfileAppSetting.this,SplashScreen.class);
                        deleteAccIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        deleteAccIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        deleteAccIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(deleteAccIntent);
                    }
                    else
                    {
                        Toast.makeText(ProfileAppSetting.this,accountPojo.getErrorMessage(),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onError(String error)
                {
                    if (progressDialog!=null)
                    {
                        progressDialog.dismiss();
                        progressDialog.cancel();
                    }
                    Toast.makeText(ProfileAppSetting.this,error,Toast.LENGTH_SHORT).show();
                }
            });
        }else
        {
            Toast.makeText(ProfileAppSetting.this,"Internet not available!",Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * <p>
     *     simple alert dialog box to confirm whether user want o remove
     *     his account or not.
     * </p>
     */

    private void deleteAccAlert()
    {
        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialog.setTitle("Alert!!");
        alertDialog.setMessage("Are you sure! You want to delete your account?");
        alertDialog.setIcon(R.drawable.launch_con);
        alertDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                deleteAccService();
                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        android.support.v7.app.AlertDialog dialog_parent = alertDialog.show();
        dialog_parent.show();
    }



    private void log_out_aleret()
    {
        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialog.setTitle("Alert!!");
        alertDialog.setMessage("Are you sure! You want to logout your account?");
        alertDialog.setIcon(R.drawable.launch_con);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                logOutService();
                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        android.support.v7.app.AlertDialog dialog_parent = alertDialog.show();
        dialog_parent.show();
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            /**
             * logout
             */
            case R.id.tV_logOut :
                progressDialog.setMessage("Logging out");
                log_out_aleret();
                break;

            /**
             * delete account
             */
            case R.id.tV_deleteAcc :
                progressDialog.setMessage("wait...");
                deleteAccAlert();
                break;
            /**
             * cancel
             */
            case R.id.relative_backbutton :
                finish();
                overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                break;

            /**
             * private policy
             */
            case R.id.tV_private_policy :
                overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                Intent browerIntent=new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.private_policy_link)));
                startActivity(browerIntent);
                break;
            case R.id.tv_NeedHelp :
                Utility.sendEmail(ProfileAppSetting.this,"Aramis","gethelp@sooperbuddy.com");
                break;
        }
    }

}
