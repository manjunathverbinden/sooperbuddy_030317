package com.three_embed.datum.Aramis_home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.three_embed.datum.Aleret_dialogs.Circule_progress_bar_dialog;
import com.three_embed.datum.Pojo_classes.Rest_password_validation_pojo;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.ServiceUrl;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h1>Forgot_password_aleret</h1>
 * <P>
 *    This class simple used to Open alert dialog for service call and  for the forgot Password
 *    and Done a service call for service forgot password .
 * </P>
 * @author 3Embed
 * @since 24/11/15.
 * @version 1.0.
 */
public class Forgot_password_aleret
{
    private Activity activity;
    private Dialog progress_bar_dialog;
    private boolean isCommingfrom_passwordReset=false;
    private Dialog dialog;
    private EditText editText;
    /**
     * Override of Constructor of the class default Constructor
     * and receiving the creator of activity reference.*/
    public Forgot_password_aleret(Activity mactivity)
    {
        activity=mactivity;
        /**
         * Creating the progress bar */
        progress_bar_dialog= Circule_progress_bar_dialog.getInstance().get_Circle_Progress_bar(mactivity);

    }
    /**
     * <h2>show_Forgot_Password_Alert</h2>
     * This method open a Alert box.
     * <P>
     *     Method  simply to open a alert box to the user to enter some data.
     *     on click of positive button validating the user data by validate_Data calling this method.
     *     On click of cancel button click closing the Alert dialog {@code  dialog.cancel()}.
     * </P>*/
    public void show_Forgot_Password_Alert()
    {
        final InputMethodManager imm = (InputMethodManager)activity.getSystemService(activity.getApplicationContext().INPUT_METHOD_SERVICE);
        dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        LayoutInflater layoutInflater= (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View view=layoutInflater.inflate(R.layout.forgot_password_alert_layout,null);
        final ImageView imageView=(ImageView)view.findViewById(R.id.cross_close_button);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                /**
                 *closing the Soft-input key*/
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                dialog.dismiss();
            }
        });

        editText=(EditText)view.findViewById(R.id.user_id);
        /**
         * setting the focous changed listener for Opening and Closing the SoftKey-Pad.*/
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                editText.post(new Runnable() {
                    @Override
                    public void run() {
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                });
            }
        });
        /**
         * Setting text watcher to the edit text.*/
        TextWatcher(editText);
        final Button Ok_button = (Button) view.findViewById(R.id.ok_button);
        Ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate_Data(editText)) {
                    if (Utility.isNetworkAvailable(activity))
                    {

                        password_Reset(editText.getText().toString());

                    } else {
                        Toast.makeText(activity.getApplicationContext(), activity.getResources().getString(R.string.networknotavailable), Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    /**
     * <h2>validate_Data</h2>
     * Method is used to validate the edit text data
     * <p>
     *     Method first check the edit text input data is empty or not by checking null and "" space .
     *     First checking the edit text input data is null or not is not the checking the input data is in
     *     email format o not.
     *     By calling a Method of utility class i.e {@code Utility.isEmailValid(editText.getText().toString())}
     *     if not particular for mate then setting the text color as Red.
     * </p>
     * @param editText contain the Edit text to Validate text of that Edit-text field.
     * */
    private boolean validate_Data(EditText editText)
    {
        if(editText.getText().toString().trim().equals(""))
        {
            editText.setError(activity.getResources().getString(R.string.UsidMandatory));
        //    editText.setHintTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.redcolor));
            return false;
        }else if(Utility.isEmailValid(editText.getText().toString().trim()))
        {
            return true;
        }else
        {
            editText.setError(activity.getResources().getString(R.string.emailnotvalid));
        //    editText.setTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.redcolor));
            return false;
        }
    }

    /**
     * <h2>TextWatcher</h2>
     * <p>
     *     Adding text watcher to EditText and Setting Error reference as Null.
     *     and the Setting Edit text hint text color as Light gery.
     * </p>
     * @param editText  contains the edit text reference to listen the TextWatcher.*/

    private void TextWatcher(final EditText editText)
    {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editText.setError(null);
                editText.setHintTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.trasparentblackcolor));
                editText.setTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.trasparentblackcolor));

            }
        });
    }

    /**
     * <h2>password_Reset</h2>
     * Method call a service to validate user email is registered or not.
     * <P>
     *
     * </P>
     * @param user_id contains the user_id to be validate from service.*/
    private void password_Reset(final String user_id)
    {
        if(progress_bar_dialog!=null)
        {
            progress_bar_dialog.show();
        }
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_email",user_id);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.URL + ServiceUrl.resetPassword, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if(progress_bar_dialog!=null)
                {
                    progress_bar_dialog.dismiss();
                }

                listening_resetPassword_response(result);
            }

            @Override
            public void onError(String error)
            {
                if(progress_bar_dialog!=null)
                {
                    progress_bar_dialog.dismiss();
                }
                isCommingfrom_passwordReset = true;
                if(dialog!=null)
                {
                    dialog.dismiss();
                }
                /**
                 * Showing user for retry.*/
                showDialogMsg_for_retyr(error, activity);
            }
        });
    }
    /**
     * <h2>listening_resetPassword_response</h2>
     * Method Listen the forgotPassword Service response  convert it to hte class type Rest_password_validation_pojo.
     * by the Help of Gson{@code Rest_password_validation_pojo loginResponse_pojo=gson.fromJson(response,Rest_password_validation_pojo.class)}.
     * Ten checking the success full of Service msg .
     * If Success full then working according to the response.
     * @param  response Contain the response data from the forgotPassword service call.*/

    private void listening_resetPassword_response(String response)
    {
        if(response!=null)
        {
            Gson gson = new Gson();
            Rest_password_validation_pojo rest_password_validation_pojo=gson.fromJson(response,Rest_password_validation_pojo.class);

            if(rest_password_validation_pojo.getErrorFlag().equals("0"))
            {
                if(dialog!=null)
                {
                    dialog.dismiss();
                }
                ShowAlertDialog(rest_password_validation_pojo.getErrorMessage(), activity);
            }else
            {
                /**
                 * Setting error text and Text color as Red for Email id is not registered.*/
                editText.setError(activity.getResources().getString(R.string.notregisteredemail));
                editText.setTextColor(ContextCompat.getColor(activity,R.color.redcolor));
            }
        }
    }

    /**
     * <h2>ShowAlertDialog</h2>
     *This method  use to Show alert Dialog.
     * @see AlertDialog
     * <p>
     *this method create a aleret dialog box and set message as given parameter
     * and show alert box .
     * The alert box get dimissed when click ok button.
     * </P>
     * @param msg	it contain given string msg to show in alert box
     * @param activity contains the current calling Activity reference.
     */
    private void ShowAlertDialog(String msg,Activity activity)
    {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(activity);
        alertDialog2.setMessage(msg);

        alertDialog2.setPositiveButton(activity.getResources().getString(R.string.Ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog2.show();
    }

    /**
     * Showing the alert box with positive and negative Button
     * <P>
     *     In the Positive button Calling signUp service again and.
     *     On negative button Cancelling the SignUp.
     * </P>
     * @param  errormsg contain Error msg*/

    private void showDialogMsg_for_retyr(String errormsg,Activity activity)
    {
        AlertDialog.Builder alert=new AlertDialog.Builder(activity);
        alert.setMessage(errormsg);
        /**
         * on Positive button again calling the Service for SignUp*/
        alert.setPositiveButton("Retry",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        if (isCommingfrom_passwordReset)
                        {
                            password_Reset(editText.getText().toString());
                        }
                    }
                });
        alert.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alert.show();
    }

}
