package com.three_embed.datum.Aramis_home;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.three_embed.datum.AdapterClass.RecentVisitorsAdap;
import com.three_embed.datum.Pojo_classes.RecentVisitorsPojo;
import com.three_embed.datum.Pojo_classes.VisitorsDatas;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *<h1>RecentVisitors</h1>
 * <P>
 *     RecentVisitors class is to Show the recently other user who have visited
 *     the holder user.
 *     it also handel is the user bought app or not.
 *     if user not bought the app then opening a aleret dialog box to buy this feature because
 *     Recent visiter is Paid feature.
 * </P>
 * */
public class RecentVisitors extends AppCompatActivity implements AdapterView.OnItemClickListener,SwipeRefreshLayout.OnRefreshListener
{
    private ListView listView_recentVisitor;
    private SessionManager sessionManager;
    private RelativeLayout no_data_available;
    private Typeface GothamRoundedMedium;
    private Typeface GothamRoundedBold;
    private TextView connection_failed_text;
    private RelativeLayout progress_bar_rl;
    private Intent user_profile;
    private RelativeLayout connection_failed_rl;
    private SwipeRefreshLayout swipe_refresh_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        setContentView(R.layout.activity_recent_visitors);
        /**
         * hiding default action bar of Appcompat.*/
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null)
        {
            actionBar.hide();
        }

        sessionManager=new SessionManager(this);
        user_profile=new Intent(RecentVisitors.this,UserProfileActivity.class);
        /**
         * Fonts for hum tum*/
        GothamRoundedMedium=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Regular.otf");
        GothamRoundedBold=Typeface.createFromAsset(getAssets(),"fonts/MyriadPro-Bold.otf");

        /**
         * Initializing the xml content.*/
        intialize_Content();

        /**
         * Calling this service to get the data..*/
        getRecentVisited_data();

    }

    /**
     * <h2>intialize_Content</h2>
     * <P>
     *
     * </P>
     * */
    private void intialize_Content()
    {
        /**
         * Internet connection failed layout.*/
        connection_failed_rl=(RelativeLayout)findViewById(R.id.connection_failed_rl);
        Button retry_button = (Button) findViewById(R.id.retry_button);
        assert retry_button != null;
        retry_button.setTypeface(GothamRoundedMedium);
        retry_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Calling this service to get the data..*/
                getRecentVisited_data();
            }
        });
        connection_failed_text=(TextView)findViewById(R.id.connection_failed_text);
        assert connection_failed_text != null;
        connection_failed_text.setTypeface(GothamRoundedMedium);

        listView_recentVisitor=(ListView)findViewById(R.id.listView_recentVisitor);
        assert listView_recentVisitor != null;
        listView_recentVisitor.setOnItemClickListener(this);
        swipe_refresh_layout= (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);
        assert swipe_refresh_layout != null;
        swipe_refresh_layout.setOnRefreshListener(this);

        no_data_available=(RelativeLayout)findViewById(R.id.no_data);

        TextView title_text_txt = (TextView) findViewById(R.id.title_text);
        assert title_text_txt != null;
        title_text_txt.setTypeface(GothamRoundedBold);

        progress_bar_rl=(RelativeLayout)findViewById(R.id.progress_bar_rl);

        TextView loadingtext = (TextView) findViewById(R.id.loadingtext);
        assert loadingtext != null;
        loadingtext.setTypeface(GothamRoundedMedium);


        RelativeLayout back_button = (RelativeLayout) findViewById(R.id.iV_back_button);
        assert back_button != null;
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.activity_close_scale, R.anim.activity_open_translate);
                finish();
            }
        });

        RelativeLayout not_purchased_layout = (RelativeLayout) findViewById(R.id.not_purchased_layout);
        assert not_purchased_layout != null;
        not_purchased_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        Button proced_to_buy = (Button) findViewById(R.id.proced_to_buy);
        assert proced_to_buy != null;
        proced_to_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 *Validating the service call.*/
                getRecentVisited_data();
            }
        });

        listView_recentVisitor.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                int topRowVerticalPosition = (listView_recentVisitor == null || listView_recentVisitor.getChildCount() == 0) ? 0 : listView_recentVisitor.getChildAt(0).getTop();
                swipe_refresh_layout.setEnabled(topRowVerticalPosition >= 0);
            }
        });

    }

    /**
     *<h2>getRecentVisited_data</h2>
     * <P>
     *     This method call a service to get all the recent
     *     visiter data from the server.
     * </P> */
    private void getRecentVisited_data()
    {
        connection_failed_rl.setVisibility(View.GONE);
        progress_bar_rl.setVisibility(View.VISIBLE);

        if(Utility.isNetworkAvailable(RecentVisitors.this))
        {
            JSONObject jsonObject=new JSONObject();

            try
            {
                jsonObject.put("ent_sess_token",sessionManager.getToken());
                jsonObject.put("ent_dev_id", Utility.getDeviceId(this));
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.RECENT_VISITER, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result) {
                    if (swipe_refresh_layout != null) {
                        swipe_refresh_layout.setRefreshing(false);
                    }

                    if (progress_bar_rl != null) {
                        progress_bar_rl.setVisibility(View.GONE);
                    }

                    Log.d("Recent visiter", result);

                    Gson gson = new Gson();
                    RecentVisitorsPojo recentVisitorsPojo;
                    recentVisitorsPojo = gson.fromJson(result, RecentVisitorsPojo.class);

                    switch (recentVisitorsPojo.getErrorFlag()) {
                        case "0":
                            if (recentVisitorsPojo.getVisitors() != null && recentVisitorsPojo.getVisitors().size() > 0) {
                                no_data_available.setVisibility(View.GONE);
                                RecentVisitorsAdap recentVisitorsAdap = new RecentVisitorsAdap(RecentVisitors.this, recentVisitorsPojo.getVisitors());
                                listView_recentVisitor.setAdapter(recentVisitorsAdap);
                                listView_recentVisitor.setVisibility(View.VISIBLE);
                                swipe_refresh_layout.setVisibility(View.VISIBLE);

                            } else {
                                no_data_available.setVisibility(View.VISIBLE);
                                swipe_refresh_layout.setVisibility(View.GONE);
                                listView_recentVisitor.setVisibility(View.GONE);
                            }
                            break;
                        case "1":
                            no_data_available.setVisibility(View.VISIBLE);
                            swipe_refresh_layout.setVisibility(View.GONE);
                            listView_recentVisitor.setVisibility(View.GONE);

                            break;
                        case "122":
                            Toast.makeText(RecentVisitors.this, RecentVisitors.this.getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
                            /**
                             * if session expired then opening the landing page.*/
                            Intent starting_intent = new Intent(RecentVisitors.this, SplashScreen.class);
                            starting_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            /**
                             * Clearing the session manager for fresh user.*/
                            sessionManager.clear_session_manager();
                            RecentVisitors.this.finish();
                            RecentVisitors.this.startActivity(starting_intent);

                            break;
                        default:
                            connection_failed_rl.setVisibility(View.VISIBLE);
                            connection_failed_text.setText(recentVisitorsPojo.getErrorMessage());
                            break;
                    }
                }

                @Override
                public void onError(String error) {
                    if (swipe_refresh_layout != null) {
                        swipe_refresh_layout.setRefreshing(false);
                    }

                    if (progress_bar_rl != null) {
                        progress_bar_rl.setVisibility(View.GONE);
                    }
                    connection_failed_rl.setVisibility(View.VISIBLE);
                    /**
                     * Setting the Error message.*/
                    connection_failed_text.setText(error);
                }
            });
        }else
        {
            if(progress_bar_rl!=null)
            {
                progress_bar_rl.setVisibility(View.GONE);
            }
            connection_failed_rl.setVisibility(View.VISIBLE);
        }


    }

    /**
     * Callback method to be invoked when an item in this AdapterView has
     * been clicked.
     * <p/>
     * Implementers can call getItemAtPosition(position) if they need
     * to access the data associated with the selected item.
     *
     * @param parent   The AdapterView where the click happened.
     * @param view     The view within the AdapterView that was clicked (this
     *                 will be a view provided by the adapter)
     * @param position The position of the view in the adapter.
     * @param id       The row id of the item that was clicked.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        VariableConstant.isNotCommingFromHomePage=true;
        VisitorsDatas visitorsDatas=(VisitorsDatas)parent.getAdapter().getItem(position);
        user_profile.putExtra("fbId",visitorsDatas.getuFbId());
        user_profile.putExtra("pPic",visitorsDatas.getpPic());
        user_profile.putExtra("firstName",visitorsDatas.getuName());
        user_profile.putExtra("match_status", visitorsDatas.getIsMatched());
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        startActivityForResult(user_profile,12345);
    }

    /**
     * Dispatch incoming result to the correct fragment.
     * @param requestCode contains the request code of opened activity.
     * @param resultCode result code of opened activity.
     * @param data return data.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
            if(requestCode==12345)
            {
                if(resultCode==RESULT_OK)
                {
                    String reponse_Data=data.getStringExtra("MESSAGE");
                    String fb_id=data.getStringExtra("FBID");
                    /**
                     * Result code 2 for Like and
                     * 3 for dislike.*/

                    switch (reponse_Data) {
                        case "2":
                            likesOrDislikeService(VariableConstant.SET_LIKES, fb_id);

                            break;
                        case "3":
                            likesOrDislikeService(VariableConstant.SET_DIS_LIKES, fb_id);
                            break;
                    }
                }
            }else
            {
                super.onActivityResult(requestCode,resultCode,data);
            }

    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }


    /**
     * <h>LikesOrDislikeService</h>
     * <p>
     *     This is likesOrDislikeService used to like or disLike the image.
     *     when user click on like or dislike button. that will be performed
     *     depending upon the likesOrDislikesVar values. if it is 1 then
     *     setLikes service will be called and if value is 2 then dislike
     *     service will be called.
     * </p>
     * @see OkHttp3Connection .
     * @param likesOrDislikesVar .This value may be 1(like) or 2(disLike)
     */
    public void likesOrDislikeService(String likesOrDislikesVar, final String fb_Id)
    {
        if(inter_Check())
        {
            JSONObject jsonObject=new JSONObject();

            try
            {
                jsonObject.put("ent_sess_token",sessionManager.getToken());
                jsonObject.put("ent_dev_id", Utility.getDeviceId(this));
                jsonObject.put("fbid",fb_Id);
                jsonObject.put("ent_data_time",Utility.getCurrentGmtTime());
                jsonObject.put("like",likesOrDislikesVar);
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.SET_LIKES, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    try
                    {
                        JSONObject jsonObject=new JSONObject(result);
                        if(jsonObject.getString("errorFlag").equals("0"))
                        {
                            Toast.makeText(getApplicationContext(),jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                            /**
                             * Refreshing the list.*/
                            getRecentVisited_data();
                        }else
                        {
                            Toast.makeText(getApplicationContext(),jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(String error)
                {
                    Toast.makeText(getApplicationContext(),error, Toast.LENGTH_SHORT).show();
                }
            });
        }else
        {
            Toast.makeText(getApplicationContext(),getApplicationContext().getString(R.string.networknotavailable), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * <h2>inter_Check</h2>
     * <P>
     * Checking the internet connection is available or not.
     * </P>*/
    private boolean inter_Check()
    {
        return Utility.isNetworkAvailable(this);
    }

    @Override
    public void onRefresh()
    {
        getRecentVisited_data();
    }
}