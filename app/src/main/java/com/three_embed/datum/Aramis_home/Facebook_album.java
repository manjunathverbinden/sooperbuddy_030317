package com.three_embed.datum.Aramis_home;

import android.app.Dialog;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.Gson;
import com.three_embed.datum.AdapterClass.Facbook_album_view;
import com.three_embed.datum.Aleret_dialogs.Circule_progress_bar_dialog;
import com.three_embed.datum.Facebook_login.Albums;
import com.three_embed.datum.Facebook_login.Photos;
import com.three_embed.datum.Pojo_classes.Facebook_album_data_holder;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.chat_lib.RecyclerItemClickListener;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * <h1>Facebook_album</h1>
 * <P>
 *
 * </P>*/
public class Facebook_album extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener
{
    private Facbook_album_view facebook_album_adapter;
    private Facbook_album_view facebook_item_adapter;
    private RecyclerView parentView, chield_images_items;
    private ProgressBar progress_bar_parent;
    private ArrayList<Facebook_album_data_holder> data=null;
    private ArrayList<Facebook_album_data_holder> item_list_Data=null;
    private RelativeLayout item_list_view_background;
    private boolean isChield_view_visible=false;
    private boolean isFor_album_request=false;
    private Dialog progressDialog=null;
    private String string_id="";
    private SwipeRefreshLayout swipe_refresh_layout;
    private RelativeLayout parent_view_01;
    private TextView data_not_found_data;
    private Download_file_from_url download_file_from_url;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext(), new FacebookSdk.InitializeCallback() {
            /**
             * Called when the sdk has been initialized.
             */
            @Override
            public void onInitialized() {
                /**
                 * Refreshing the acess token of the Facebook*/
                AccessToken.refreshCurrentAccessTokenAsync();
            }
        });

        setContentView(R.layout.activity_facebook_album);
        /**
         * Setting the back button.*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#AE1F23\">" + getString(R.string.app_name) + "</font>"));




        /**
         * Creating Image downloader*/
        download_file_from_url=Download_file_from_url.getInstance();
        /**
         * Progress dialog.*/
        progressDialog=Circule_progress_bar_dialog.getInstance().get_Circle_Progress_bar(Facebook_album.this);

        parent_view_01=(RelativeLayout)findViewById(R.id.parent_view_01);

        /**
         * swap to refresh lay out.*/
        swipe_refresh_layout= (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);
        assert swipe_refresh_layout != null;
        swipe_refresh_layout.setOnRefreshListener(this);

        data_not_found_data=(TextView)findViewById(R.id.nodata_found);
        /**
         * Initializing the default Animater for to the recycle view.*/
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setAddDuration(500);
        defaultItemAnimator.setRemoveDuration(500);

        chield_images_items =(RecyclerView)findViewById(R.id.listview_items);
        assert chield_images_items != null;
        chield_images_items.setItemAnimator(defaultItemAnimator);


        item_list_view_background=(RelativeLayout)findViewById(R.id.item_list_view_background);

        parentView =(RecyclerView)findViewById(R.id.listview);
        assert parentView != null;
        parentView.setVisibility(View.GONE);
        parentView.setItemAnimator(defaultItemAnimator);

        progress_bar_parent=(ProgressBar)findViewById(R.id.progress_bar_parent);

        parentView.addOnItemTouchListener(new RecyclerItemClickListener(Facebook_album.this, parentView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position)
            {
                Facebook_album_data_holder facebook_album_data_holder = data.get(position);
                string_id=facebook_album_data_holder.getId();
                /**
                 * setting true to say chield Item is viewing.*/
                isChield_view_visible = true;
                /**
                 * Extracting all the Image from that folder.*/
                getPicture_of_album(string_id);
            }

            @Override
            public void onItemLongClick(View view, int position)
            {
                Log.d("Clickeditem", "Clicked");
            }
        }));


        chield_images_items.addOnItemTouchListener(new RecyclerItemClickListener(Facebook_album.this, chield_images_items, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position)
            {
                /**
                 * setting true to say chield Item is viewing.*/
                isChield_view_visible = false;
                Facebook_album_data_holder facebook_album_data_holder = item_list_Data.get(position);
                String image_url=facebook_album_data_holder.getUrl();
                down_load_file(image_url);
            }

            @Override
            public void onItemLongClick(View view, int position)
            {
                Log.d("Clickeditem", "Clicked");
            }
        }));


        isChield_view_visible=false;

        /**
         * Getting the user albums details.*/
        get_User_album();

    }

    /**
     * This method is called whenever the user chooses to navigate Up within your application's
     * activity hierarchy from the action bar.
     * <p/>
     * <p>If a parent was specified in the manifest for this activity or an activity-alias to it,
     * default Up navigation will be handled automatically. See
     * {@link #getSupportParentActivityIntent()} for how to specify the parent. If any activity
     * along the parent chain requires extra Intent arguments, the Activity subclass
     * should override the method {@link #onPrepareSupportNavigateUpTaskStack(TaskStackBuilder)}
     * to supply those arguments.</p>
     * <p/>
     * <p>See <a href="{@docRoot}guide/topics/fundamentals/tasks-and-back-stack.html">Tasks and
     * Back Stack</a> from the developer guide and
     * <a href="{@docRoot}design/patterns/navigation.html">Navigation</a> from the design guide
     * for more information about navigating within your app.</p>
     * <p/>
     * <p>See the {@link TaskStackBuilder} class and the Activity methods
     * {@link #getSupportParentActivityIntent()}, {@link #supportShouldUpRecreateTask(Intent)}, and
     * {@link #supportNavigateUpTo(Intent)} for help implementing custom Up navigation.</p>
     *
     * @return true if Up navigation completed successfully and this Activity was finished,
     * false otherwise.
     */
    @Override
    public boolean onSupportNavigateUp()
    {
        onBackPressed();
        return true;
    }


    /**
     * <h2>get_User_album</h2>
     * <P>
     *"/me/friends"
     * </P>*/
    private void get_User_album()
    {
        progress_bar_parent.setVisibility(View.VISIBLE);
        /**
         * Requesting for the album request.*/
        isFor_album_request=true;

        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        progress_bar_parent.setVisibility(View.GONE);

                        if (response.getError()!= null)
                        {
                            AccessToken.refreshCurrentAccessTokenAsync();
                            if(swipe_refresh_layout!=null)
                            {
                                swipe_refresh_layout.setRefreshing(false);
                            }

                            /**
                             * Showing the parent Layout.*/
                            parent_view_01.setVisibility(View.GONE);
                            data_not_found_data.setVisibility(View.VISIBLE);
                            parentView.setVisibility(View.GONE);

                        } else
                        {
                            if(swipe_refresh_layout!=null)
                            {
                                swipe_refresh_layout.setRefreshing(false);
                            }
                            /**
                             * Showing the parent Layout.*/
                            parent_view_01.setVisibility(View.VISIBLE);
                            data_not_found_data.setVisibility(View.GONE);
                            parentView.setVisibility(View.VISIBLE);

                            Albums album;
                            try {
                                JSONObject album_details = object.getJSONObject("albums");

                                Log.d("ReponseFb", album_details.toString());

                                album = new Gson().fromJson(album_details.toString(), Albums.class);
                                data = new ArrayList<>();
                                for (int count = 0; count < album.getData().length; count++)
                                {
                                    Facebook_album_data_holder facebook_album_data_holder = new Facebook_album_data_holder();
                                    facebook_album_data_holder.setUrl(album.getData()[count].getPicture().getData().getUrl());
                                    facebook_album_data_holder.setId(album.getData()[count].getId());
                                    facebook_album_data_holder.setName(album.getData()[count].getName());
                                    data.add(facebook_album_data_holder);
                                }

                                Facebook_album.this.runOnUiThread(new Runnable() {

                                    @Override
                                    public void run()
                                    {
                                        parentView.setVisibility(View.VISIBLE);
                                        facebook_album_adapter = new Facbook_album_view(data, Facebook_album.this);
                                        parentView.setAdapter(facebook_album_adapter);
                                        parentView.setHasFixedSize(true);
                                        parentView.setLayoutManager(new GridLayoutManager(Facebook_album.this,3));
                                        facebook_album_adapter.notifyDataSetChanged();
                                    }
                                });

                            } catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields","albums{picture{url},name}");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * <h2>getPicture_of_album</h2>
     * <P>
     *  Getting all the picture details from the given album id.
     * </P>
     * @param album_id contains the album id from which picture to be extracted.*/
    private void getPicture_of_album(String album_id)
    {
        /**
         * Requesting for the album request.*/
        isFor_album_request=false;
        /**
         * Showing the progress dialog.*/
        progressDialog.show();

        final GraphRequest request = GraphRequest.newGraphPathRequest(
                AccessToken.getCurrentAccessToken(), album_id,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        progressDialog.dismiss();

                        if (swipe_refresh_layout != null) {
                            swipe_refresh_layout.setRefreshing(false);
                        }

                        if (response.getError() != null) {
                            AccessToken.refreshCurrentAccessTokenAsync();

                        } else {

                            isChield_view_visible = true;
                            item_list_view_background.setVisibility(View.VISIBLE);
                            Photos photos;
                            String response_json_object = String.valueOf(response.getJSONObject());
                            try {
                                item_list_Data = new ArrayList<>();
                                JSONObject jsonObject = new JSONObject(response_json_object);
                                photos = new Gson().fromJson(jsonObject.getJSONObject("photos").toString(), Photos.class);

                                for (int count = 0; count < photos.getData().length; count++) {
                                    Facebook_album_data_holder facebook_album_data_holder = new Facebook_album_data_holder();
                                    facebook_album_data_holder.setUrl(photos.getData()[count].getSource());
                                    facebook_album_data_holder.setId(photos.getData()[count].getId());
                                    item_list_Data.add(facebook_album_data_holder);
                                }

                                Facebook_album.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        facebook_item_adapter = new Facbook_album_view(item_list_Data, Facebook_album.this);
                                        chield_images_items.setAdapter(facebook_item_adapter);
                                        chield_images_items.setHasFixedSize(true);
                                        chield_images_items.setLayoutManager(new GridLayoutManager(Facebook_album.this, 2));
                                        facebook_item_adapter.notifyDataSetChanged();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("ReponseFb", "Error !");
                            }
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields","photos{source,name}");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed()
    {
        if(isChield_view_visible)
        {
            isFor_album_request=true;
            isChield_view_visible=false;
            facebook_album_adapter.notifyDataSetChanged();
            parent_view_01.setVisibility(View.VISIBLE);
            item_list_view_background.setVisibility(View.GONE);
        }else
        {
            super.onBackPressed();
        }
    }

    @Override
    public void onRefresh()
    {
        if(isFor_album_request)
        {
            /**
             * Getting the user albums details.*/
            get_User_album();

        }else
        {
            /**
             * Extracting all the Image from that folder.*/
            getPicture_of_album(string_id);
        }
    }

    private void down_load_file(String image_url)
    {
        progressDialog.show();

        String destination_path;
        /**
         * Checking the External storage is Exist or not.*/
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()))
        {
            destination_path=Environment.getExternalStorageDirectory() + "/" + ServiceUrl.App_folder+ ServiceUrl.CHAT_IMAGES;
        }else
        {
            destination_path=this.getFilesDir()+ "/" + ServiceUrl.App_folder+ ServiceUrl.CHAT_IMAGES;
        }

        download_file_from_url.down_load_file(image_url, destination_path, new Download_file_from_url.Download_load_sucess()
        {
            @Override
            public void onSucess(String location_paath)
            {
                progressDialog.dismiss();

                Intent intent=new Intent();
                intent.putExtra("FILE_URL",location_paath);
                setResult(RESULT_OK, intent);
                Facebook_album.this.finish();
            }
            @Override
            public void onError(String error)
            {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                Facebook_album.this.finish();
            }
        });

    }



}
