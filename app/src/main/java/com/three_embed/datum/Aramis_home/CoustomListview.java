package com.three_embed.datum.Aramis_home;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
/**
 * Class is used to create a custom List view.
 * Class is use to create a custom a list view which contain a Scalable Header view
 * on Scroll.
 *
 * Created by shobhit on 13/1/16.
 */
public class CoustomListview extends ListView implements OnScrollListener
{

    private View mView;
    private int mDrawableMaxHeight = -1;
    private int mImageViewHeight = -1;
    private int mDefaultImageViewHeight = 0;
    private int maxZoom_header_height=0;
    /**
     * Interface for reading the scroll percentage and all xy position and pass to the Method define in
     * class object definition.. */
    private interface OnOverScrollByListener
    {
        boolean overScrollBy(int deltaX, int deltaY, int scrollX,
                             int scrollY, int scrollRangeX, int scrollRangeY,
                             int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent);
    }

    /**
     *Interface to read the touch listener event to the touch event object definition. */
    private interface OnTouchEventListener
    {
        void onTouchEvent(MotionEvent ev);
    }


    public CoustomListview(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init(context);
    }


    public CoustomListview(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }


    public CoustomListview(Context context)
    {
        super(context);
        init(context);
    }


    public void init(Context context)
    {
        mDefaultImageViewHeight =context.getResources().getDimensionPixelSize(R.dimen.size_default_height);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        super.onLayout(changed, l, t, r, b);
        initViewsBounds();
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState)
    {
    }

    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX,
                                   int scrollY, int scrollRangeX, int scrollRangeY,
                                   int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent)
    {
        boolean isCollapseAnimation;

        isCollapseAnimation = scrollByListener.overScrollBy(deltaX, deltaY,
                scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX,
                maxOverScrollY, isTouchEvent);

        return isCollapseAnimation || super.overScrollBy(deltaX, deltaY,
                scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX,
                maxOverScrollY, isTouchEvent);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount)
    {
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt)
    {
        super.onScrollChanged(l, t, oldl, oldt);

        View firstView = (View) mView.getParent();
        if (firstView.getTop() < getPaddingTop() && mView.getHeight() > mImageViewHeight)
        {
            mView.getLayoutParams().height = Math.max(mView.getHeight() - (getPaddingTop() - firstView.getTop()), mImageViewHeight);
            firstView.layout(firstView.getLeft(), 0, firstView.getRight(), firstView.getHeight());
            mView.requestLayout();
        }
    }
    /**
     * Override of touch Listener and setting the object of interface of the OnTouchEventListener
     * method onTouchEvent method parameter.*/
    @Override
    public boolean onTouchEvent(MotionEvent ev)
    {
        Log.d("Cilck listen","Yes");
        touchListener.onTouchEvent(ev);
        return super.onTouchEvent(ev);
    }

    /**
     * Method receive the View in Which scall operation is performed.
     * @param iv contains the Image view in Which scale view is performed.*/
    public void setCustomListHeaderImageView(View iv)
    {
        mView = iv;

        //  mView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    /**
     *Method set the maximum width by calculation the given ration value.
     *  */
    private void initViewsBounds()
    {
        if (mImageViewHeight == -1)
        {
            mImageViewHeight = mView.getHeight();
            if (mImageViewHeight <= 0)
            {
                mImageViewHeight = mDefaultImageViewHeight;
            }
/**
 * Setting the max width of the Image on Zooming.7200
 * if the maxSize is Zero then settign default 1200 height else setting given Screen Height*/
            if(maxZoom_header_height==0)
            {
                mDrawableMaxHeight =maxZoom_header_height;
            }else
            {
                mDrawableMaxHeight =1200;
            }

        }
    }

    /**
     * Setting the Zoom ratio for MAx Zoom size.*/
    public void setZoomRatio(int maxSize)
    {
        maxZoom_header_height=maxSize;
    }

    /**
     * Increasing the Header Image size to on Scroll of the List view.
     * When List view scrolled then the overScrollBy method parameter is set and
     * this method is executed.
     * First checking the the is Image width is on maximum Height or not and the touch event.
     * checking the image width is less then that of MaxImage width.
     * then again checking the given view Height if grater or equal.
     * setting the new view width and height.
     * deltaY is grater then Zero ,Then
     * else
     * return true.
     */
    private OnOverScrollByListener scrollByListener = new OnOverScrollByListener()
    {
        @Override
        public boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY,
                                    int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent)
        {
            if (mView.getHeight() <= mDrawableMaxHeight && isTouchEvent)
            {
                if (deltaY < 0)
                {
                    if (mView.getHeight() - deltaY / 2 >= mImageViewHeight)
                    {
                        mView.getLayoutParams().height = mView.getHeight() - deltaY / 2 < mDrawableMaxHeight ?
                                mView.getHeight() - deltaY / 2 : mDrawableMaxHeight;
                        mView.requestLayout();
                    }
                } else
                {
                    if (mView.getHeight() > mImageViewHeight)
                    {
                        mView.getLayoutParams().height = mView.getHeight() - deltaY > mImageViewHeight ?
                                mView.getHeight() - deltaY : mImageViewHeight;
                        mView.requestLayout();
                        return true;
                    }
                }
            }
            return false;
        }
    };

    /**
     * Listening the on touch left event ..
     * Means when your user left its fingure from the screen the
     * Action up event occurred.
     * Here on Action UP starting the revert animation to get the revert action on Image.
     * @see MotionEvent*/
    private OnTouchEventListener touchListener = new OnTouchEventListener()
    {
        @Override
        public void onTouchEvent(MotionEvent ev)
        {
            if (ev.getAction() == MotionEvent.ACTION_UP)
            {
                if (mImageViewHeight - 1 < mView.getHeight())
                {
                    ResetAnimimation animation = new ResetAnimimation(mView, mImageViewHeight);
                    animation.setDuration(300);
                    mView.startAnimation(animation);
                }
            }
        }
    };

    /**
     * Animation class for revert the Image width to the initial width and Height of the
     * Image.
     * Class first listen the view to which Transformation  is apply and receive the targeted image width.
     * Then in override applyTransformation method decreasing the  Image size up to the targeted width and height.
     * @see android.view.animation.Animation
     */
    public class ResetAnimimation extends Animation
    {
        int targetHeight;
        int originalHeight;
        int extraHeight;
        View mView;

        /**
         * Default constructor of the ResetAnimimation class
         * @param  view Contain the current View in which Transformation is applied
         * @param targetHeight Contain the targeted initial height. */
        protected ResetAnimimation(View view, int targetHeight)
        {
            this.mView = view;
            this.targetHeight = targetHeight;
            originalHeight = view.getHeight();
            extraHeight = this.targetHeight - originalHeight;
        }

        /**
         * In this method decreasing the given view each time on given duration.
         * by requesting the lay out param.
         */
        @Override
        protected void applyTransformation(float interpolatedTime,Transformation t)
        {
            int newHeight;
            newHeight = (int) (targetHeight - extraHeight * (1 - interpolatedTime));
            mView.getLayoutParams().height = newHeight;
            mView.requestLayout();
        }
    }
}