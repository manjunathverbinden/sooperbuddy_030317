package com.three_embed.datum.Aramis_home;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.Gson;
import com.three_embed.datum.Aleret_dialogs.Circule_progress_bar_dialog;
import com.three_embed.datum.Facebook_login.Data;
import com.three_embed.datum.Facebook_login.FBResponse_pojo;
import com.three_embed.datum.Facebook_login.Facebook_album_list;
import com.three_embed.datum.Facebook_login.Facebook_photos_list;
import com.three_embed.datum.Facebook_login.Image_url_details;
import com.three_embed.datum.Pojo_classes.Facebook_education_pojo;
import com.three_embed.datum.Pojo_classes.Facebook_likes_pojo;
import com.three_embed.datum.Pojo_classes.Facebook_user_friends_pojo;
import com.three_embed.datum.Pojo_classes.Faebook_user_work_pojo;
import com.three_embed.datum.Pojo_classes.Images_url_pojo_class;
import com.three_embed.datum.Pojo_classes.LoginWithFbPojo;
import com.three_embed.datum.Utility.AppSession;
import com.three_embed.datum.Utility.OkHttp3Connection;
import com.three_embed.datum.Utility.ServiceUrl;
import com.three_embed.datum.Utility.SessionManager;
import com.three_embed.datum.Utility.SessionmangerForFacebook;
import com.three_embed.datum.Utility.Utility;
import com.three_embed.datum.Utility.VariableConstant;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
/**
 * @since  3/2/16.
 */
public class Facebook_service_login
{
    private Activity mactivity=null;
    private SessionManager sessionManager;
    private SessionmangerForFacebook sessionmangerForFacebook;
    private AppSession appSession;
    private Images_url_pojo_class images_url_pojo_class;
    private int user_age=0;
    private Dialog progress_bar_dialog;
    private String profile_pic_url="";
    private String user_about="";
    private String email="";
    String user_gender="Male";

    Facebook_service_login(Activity activity)
    {
        this.mactivity=activity;
        progress_bar_dialog= Circule_progress_bar_dialog.getInstance().get_Circle_Progress_bar(activity);
        sessionManager=new SessionManager(mactivity);
        sessionmangerForFacebook=new SessionmangerForFacebook(mactivity);
        appSession=new AppSession(mactivity);
        images_url_pojo_class=new Images_url_pojo_class();
    }
    /**
     * <h2>SocialLogin</h2>
     * <p>
     *   This method is being called from faceBookLogin() and accept all
     *   user details dats like first_name,email, gender, date_of_birth & from
     *   that datas we do service call for social login once when we get success
     *   then we call HomePageActivity class and save respose data like
     *   token in SessionManager class.
     * </p>
     * @see OkHttp3Connection
     * @see LoginWithFbPojo
     * @see VariableConstant
     */
    public void socialLogin(final FBResponse_pojo fb_response)
    {
        /**
         * Showing the progress dialog.*/
        if(progress_bar_dialog!=null)
        {
            progress_bar_dialog.show();
        }

        String pref_sex="Female",user_dob;
        int uper_prefered_age=0,lower_prefered_age=0;
        boolean isUser_adult;
        profile_pic_url="https://graph.facebook.com/"+fb_response.getId()+"/picture?height=1000&width=1000";

        if(fb_response.getBio()!=null&&!fb_response.getBio().isEmpty()) {
            user_about=fb_response.getBio();
        }else
        {
            user_about="";
        }

        if(fb_response.getFriends()!=null)
        {
            Facebook_user_friends_pojo facebook_user_friends_pojo = new Facebook_user_friends_pojo();
            facebook_user_friends_pojo.setFriends(fb_response.getFriends());
            String user_friends=new Gson().toJson(facebook_user_friends_pojo);
            sessionmangerForFacebook.setUserFbFriends(user_friends);
        }


        if(fb_response.getWork()!=null) {
            Faebook_user_work_pojo faebook_user_work_pojo = new Faebook_user_work_pojo();
            faebook_user_work_pojo.setWork(fb_response.getWork());
            String user_work=new Gson().toJson(faebook_user_work_pojo);
            sessionmangerForFacebook.setUserFbWorks(user_work);

        }
        if(fb_response.getLikes() != null) {
            Facebook_likes_pojo facebook_likes_pojo = new Facebook_likes_pojo();
            facebook_likes_pojo.setLikes(fb_response.getLikes());
            String user_likes=new Gson().toJson(facebook_likes_pojo);
            sessionmangerForFacebook.setUserFbLike(user_likes);
        }
        if(fb_response.getEducation()!=null)
        {
            Facebook_education_pojo facebook_education_pojo = new Facebook_education_pojo();
            facebook_education_pojo.setEducation(fb_response.getEducation());
            String user_education=new Gson().toJson(facebook_education_pojo);
            sessionmangerForFacebook.setUserFbEducation(user_education);
        }
        /**
         * Making Capital later to the First Text of the Gender.*/
        if(fb_response.getGender()!=null)
        {
            user_gender=fb_response.getGender().substring(0, 1).toUpperCase() +fb_response.getGender().substring(1);
        }
        /**
         * Checking weather the use have email id or not .
         * if don,t have then creating the user email id.
         * */
        if(fb_response.getEmail()!=null&&!fb_response.getEmail().isEmpty())
        {
            email=fb_response.getEmail();

        }else
        {
            email=fb_response.getId()+"@"+"facebook.com";
        }
        /**
         * Setting the prference of search gender.*/
        if(fb_response.getGender()!=null&&fb_response.getGender().equalsIgnoreCase("Female"))
        {
            pref_sex="Male";
        }
        /**
         * Formating the user birth date.*/
        user_dob=formate_Date(fb_response.getBirthday());
        /**
         * Checking the user is age.*/
        user_age=Utility.adult_age_validation(user_dob);

        /**
         * Checking the use is adult or not and then setting the user Upper age and user lower age
         * when user is adult.
         * setting user adult as true when use age is 18 else false;*/
        if(user_age>=18)
        {
            isUser_adult=true;
            uper_prefered_age=user_age+5;
            lower_prefered_age=user_age-5;

            if(lower_prefered_age<23)
            {
                lower_prefered_age=18;
            }
        }
        else
        {
            isUser_adult=false;
        }
        /**
         * Checking user id adult or not.*/
        if(isUser_adult)
        {
            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("Authenticationtype",VariableConstant.FB_AUTHENTICATION_TYPE);
                jsonObject.put("ent_Gender", user_gender);
                jsonObject.put("ent_access_token","");
                jsonObject.put("ent_city","Bangalore");
                jsonObject.put("ent_country","India");
                jsonObject.put("ent_dev_id", Utility.getDeviceId(mactivity));
                jsonObject.put("ent_dob",user_dob);
                jsonObject.put("ent_email",email);
                jsonObject.put("ent_fbid",fb_response.getId());
                jsonObject.put("ent_first_name",fb_response.getFirst_name());
                jsonObject.put("ent_last_name",fb_response.getLast_name());
                jsonObject.put("ent_password","3Embed");
                jsonObject.put("ent_pref_lower_age",lower_prefered_age+"");
                jsonObject.put("ent_pref_sex",pref_sex);
                jsonObject.put("ent_pref_upper_age",uper_prefered_age+"");
                jsonObject.put("ent_data_time",Utility.getCurrentGmtTime());
                jsonObject.put("ent_profile_pic",profile_pic_url);
                jsonObject.put("ent_device_type",VariableConstant.DEVICE_TYPE);
                jsonObject.put("ent_push_token",appSession.getGcmRegId());
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.FACEBOOK_LOGIN, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    Gson gson = new Gson();
                    LoginWithFbPojo loginWithFbPojo = gson.fromJson(result,LoginWithFbPojo.class);

                    switch (loginWithFbPojo.getErrorFlag())
                    {
                        case "0":
                            appSession.setFbId(fb_response.getId());
                            /**
                             * Clearing the sesson manager for fresh data to store.*/
                            sessionManager.clear_session_manager();
                            /**
                             * Storing all the required thing in Session manager.*/
                            sessionManager.setToken(loginWithFbPojo.getToken());
                            sessionManager.setFbId(loginWithFbPojo.getFbId());
                            sessionManager.setFirstName(fb_response.getFirst_name());
                            sessionManager.setLastName(fb_response.getLast_name());
                            sessionManager.set_User_fb_login_status(true);
                            sessionManager.setUserLoginStatus(true);
                            sessionManager.setAge(user_age + "");
                            sessionManager.set_User_email(email);

                                /* *
                                   * In this if loop. we check if IsLoginOrSignup is login then
                                   * it goes to HomePageActivity else sign_up page.*/
                            if (loginWithFbPojo.getIsLoginOrSignup() != null && loginWithFbPojo.getIsLoginOrSignup().equals("signup"))
                            {
                                /**
                                 * get other picture from facebook*/
                                getPicture_from_Facebook();

                            } else
                            {
                                sessionManager.setFbProfilePictureUri(profile_pic_url);
                                images_url_pojo_class.getImageListUrl().clear();
                                images_url_pojo_class.getImageListUrl().add(profile_pic_url);
                                /**
                                 * Storing the Image file url link to the Shared preference.*/
                                String image_url = new Gson().toJson(images_url_pojo_class);
                                sessionManager.addImagesUrl(image_url);
                                /**
                                 * Opening the Home page activity. activity.*/
                                start_Activity(true);
                            }
                            break;
                        case "1":
                            Toast.makeText(mactivity, mactivity.getApplicationContext().getString(R.string.sessionexpired), Toast.LENGTH_SHORT).show();
                            Intent starting_intent = new Intent(mactivity, SplashScreen.class);
                            starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            starting_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            sessionManager.setUserLoginStatus(false);
                            mactivity.startActivity(starting_intent);
                            break;
                        default:
                            Toast.makeText(mactivity, loginWithFbPojo.getErrorMessage(), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

                @Override
                public void onError(String error)
                {
                    if (progress_bar_dialog != null)
                    {
                        progress_bar_dialog.dismiss();
                    }
                    Utility.ShowAlertDialog(error, mactivity);
                }
            });
        }else
        {
            /**
             * User is not adult.*/
            Toast.makeText(mactivity,mactivity.getResources().getString(R.string.not_adult),Toast.LENGTH_SHORT).show();
        }

    }
    /**
     *<h2>create_other_image_list</h2>
     * <P>
     *     Method is use to make the other image list as a single String link with " ," separater.
     *     First receving the Image list and  by the help of loop merging them in a single string.
     *    Note : Max size of other link is is 5;
     *    first setting the size if list size is greater then five then makinh the max size is five else
     *    setting max size as array size.
     * </P>
     * @param album_picture_data_list contains the list of of other image list .*/

    private String create_other_image_list( ArrayList<String> album_picture_data_list)
    {
        String other_image_link = "";
        /**
         * Clearing the image list for refresh data.*/
        images_url_pojo_class.getImageListUrl().clear();

        if (album_picture_data_list!=null)
        {
            int maxsize=album_picture_data_list.size();

            for (int i=0;i<5&&i<maxsize;i++)
            {
                other_image_link=other_image_link +album_picture_data_list.get(i);

                /**
                 *putting the semicolon on image separetor.
                 * */
                if (maxsize> 1&&i<maxsize-1)
                {
                    other_image_link =other_image_link + ",";
                }
                /**
                 * adding other Images to other list in Array-list.*/
                images_url_pojo_class.getImageListUrl().add(album_picture_data_list.get(i));
            }
        }
        return other_image_link;
    }
    /**
     * <h2>getPicture_from_Facebook</h2>
     * <P>
     *  Picture details data.
     * </P>*/
    private void getPicture_from_Facebook()
    {
        AccessToken.refreshCurrentAccessTokenAsync();
        AccessToken accessToken=AccessToken.getCurrentAccessToken();
        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback()
                {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response)
                    {
                        if (response.getError() != null)
                        {
                            /**
                             * Opening the Home page activity. activity.*/
                            start_Activity(true);
                            Toast.makeText(mactivity,"Unable to get Images from facebook",Toast.LENGTH_SHORT).show();
                        } else
                        {
                            Facebook_album_list fbResponse_pojo = new Gson().fromJson(object.toString(), Facebook_album_list.class);
                            String user_id="";
                            for(Data data:fbResponse_pojo.getAlbums().getData())
                            {
                                if(data.getName().equalsIgnoreCase("Profile Pictures"))
                                {
                                    user_id=data.getId();
                                    break;
                                }
                            }
                            /**
                             * Checking*/
                            if(!user_id.isEmpty())
                            {
                                getPictures(user_id);
                            }else
                            {
                                /**
                                 * Opening the Home page activity. activity.*/
                                start_Activity(true);
                                Toast.makeText(mactivity,"Unable to get Images from facebook",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields","albums{name,id}");
        request.setParameters(parameters);
        request.executeAsync();
    }
    /**
     * <h2>getPictures</h2>
     * <P>
     *     Getting picture from album id.
     * </P>*/
    private void getPictures(String albumId)
    {
        AccessToken.refreshCurrentAccessTokenAsync();
        AccessToken accessToken=AccessToken.getCurrentAccessToken();

        GraphRequest request = GraphRequest.newGraphPathRequest(
                accessToken,
                "/" + albumId,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        if (response.getError() != null) {
                            Toast.makeText(mactivity, "Unable to get Images from facebook", Toast.LENGTH_SHORT).show();
                            /**
                             * Opening the Home page activity. activity.*/
                            start_Activity(true);
                        } else {
                            Facebook_photos_list facebook_picture_list = new Gson().fromJson(response.getJSONObject().toString(), Facebook_photos_list.class);
                            if (facebook_picture_list.getPhotos() != null) {
                                extract_Image_list(facebook_picture_list.getPhotos().getData());
                            } else {
                                Toast.makeText(mactivity, "Unable to get Images from facebook", Toast.LENGTH_SHORT).show();
                                start_Activity(false);
                            }
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "photos.limit(5){id}");
        request.setParameters(parameters);
        request.executeAsync();
    }
    /**
     * <h2>getBest_Quality_Image</h2>
     * <P>
     *     Getting best quality image from the server.
     * </P>
     * @param image_id contains the image id to be extracted.*/
    private void getBest_Quality_Image(String image_id)
    {
        AccessToken.refreshCurrentAccessTokenAsync();
        AccessToken accessToken=AccessToken.getCurrentAccessToken();

        final GraphRequest request = GraphRequest.newGraphPathRequest(accessToken,
                "/"+image_id,
                new GraphRequest.Callback()
                {
                    @Override
                    public void onCompleted(GraphResponse response)
                    {
                        if (response.getError() == null)
                        {
                            Image_url_details image_url_details=new Gson().fromJson(response.getJSONObject().toString(), Image_url_details.class);
                            other_image_url.add(image_url_details.getImages()[0].getSource());
                        }
                        if(counter_number==image_count-1)
                        {
                            Log.d("Imagesno",other_image_url.toString());
                            /**
                             *Creating and extracting the profile picture ,
                             * the Image array list of {,} separator and for the other image list.
                             * */
                            String other_images_list = create_other_image_list(other_image_url);
                            /**
                             * Uploading user other images ti the server.*/
                            uploadImage(profile_pic_url, other_images_list);

                        }else
                        {
                            counter_number=counter_number+1;
                            getBest_Quality_Image(data[counter_number].getId());
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "images");
        request.setParameters(parameters);
        request.executeAsync();
    }
    /**
     * <h2>extract_Image_list</h2>
     * <P>
     *     Extracting all the images from the facebook serve.
     * </P>*/
    ArrayList<String> other_image_url=new ArrayList<>();
    Data data[];
    int image_count=0;
    int counter_number;
    private void extract_Image_list(Data data_list[])
    {
        data=data_list;
        image_count=data.length;
        counter_number=0;
        getBest_Quality_Image(data[0].getId());
    }

    interface image_call_Back
    {
        void isIMage_found();
    }
    /**
     * <h2>uploadImage</h2>
     * <p>
     *     Method call a service to upload Image url to the server using a async task of Java using OK http service.
     * </p>
     * @param profile_url contains the profile pic url of user.
     * @param ref_image_url Contains the list of image url.
     */
    private void uploadImage(final String profile_url,final String ref_image_url)
    {
        if(Utility.isNetworkAvailable(mactivity))
        {
            JSONObject jsonObject=new JSONObject();
            try
            {
                jsonObject.put("ent_dev_id", Utility.getDeviceId(mactivity.getApplicationContext()));
                jsonObject.put("ent_sess_token", sessionManager.getToken());
                jsonObject.put("ent_profile_pic",profile_url);
                jsonObject.put("ent_other_urls",ref_image_url);
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            OkHttp3Connection.doOkHttp3Connection(ServiceUrl.UPLOAD_IMAGE, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    try
                    {
                        JSONObject jsonObject=new JSONObject(result);

                        if(jsonObject.getString("errorFlag").equals("0"))
                        {
                            if(user_about!=null&&!user_about.isEmpty())
                            {
                                /**
                                 * Storing the user bio data as user about.*/
                                upDate_User_About(user_about);
                            }else
                            {
                                /**
                                 *if user don't have the user about the opening the Video acvity. */
                                if(progress_bar_dialog!=null)
                                {
                                    progress_bar_dialog.dismiss();
                                }
                                sessionManager.setAbout(user_about);
                                /**
                                 * Storing the Image in to the session manager.*/
                                images_url_pojo_class.getImageListUrl().add(0,profile_pic_url);
                                String image_url=new Gson().toJson(images_url_pojo_class);
                                sessionManager.addImagesUrl(image_url);
                                /**
                                 * Opening the Home page activity. activity.*/
                                start_Activity(false);
                            }

                        }else
                        {
                            if(progress_bar_dialog!=null)
                            {
                                progress_bar_dialog.dismiss();
                            }

                            Toast.makeText(mactivity.getApplicationContext(),jsonObject.getString("errorMessage"),Toast.LENGTH_SHORT).show();
                        }
                    }catch (JSONException e)
                    {
                        if(progress_bar_dialog!=null)
                        {
                            progress_bar_dialog.dismiss();
                        }

                        Toast.makeText(mactivity.getApplicationContext(),"Incomparable json response!",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
                @Override
                public void onError(String error)
                {
                    if(progress_bar_dialog!=null)
                    {
                        progress_bar_dialog.dismiss();
                    }
                    Toast.makeText(mactivity.getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                }
            });

        }else
        {
            Utility.ShowAlertDialog(mactivity.getResources().getString(R.string.isNetworkAvailable), mactivity);
        }
    }

    /**
     * <h2>upDate_User_About</h2>
     * <P>
     *     Method updating the user profile about.
     * </P>*/
    private void upDate_User_About(final String user_new_About)
    {
        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_dev_id", Utility.getDeviceId(mactivity.getApplicationContext()));
            jsonObject.put("ent_sess_token", sessionManager.getToken());
            jsonObject.put("ent_pers_desc",user_new_About);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        OkHttp3Connection.doOkHttp3Connection(ServiceUrl.EDITPROFILE, OkHttp3Connection.Request_type.POST, jsonObject, new OkHttp3Connection.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String result)
            {

                if(progress_bar_dialog!=null)
                {
                    progress_bar_dialog.dismiss();
                }

                try {
                    JSONObject jsonObject=new JSONObject(result);
                    if(jsonObject.getString("errorFlag").equals("0"))
                    {
                        sessionManager.setAbout(user_new_About);
                        /**
                         * Storing the Image in to the session manager.*/
                        images_url_pojo_class.getImageListUrl().add(0,profile_pic_url);
                        String image_url=new Gson().toJson(images_url_pojo_class);
                        sessionManager.addImagesUrl(image_url);
                        /**
                         * Opening the Home page activity. activity.*/
                        start_Activity(false);
                    }else
                    {
                        Toast.makeText(mactivity.getApplicationContext(),jsonObject.getString("errorMessage"),Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e)
                {
                    Toast.makeText(mactivity.getApplicationContext(),"Incomparable json response!",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(String error)
            {
                if(progress_bar_dialog!=null)
                {
                    progress_bar_dialog.dismiss();
                }
                Toast.makeText(mactivity.getApplicationContext(),error,Toast.LENGTH_SHORT).show();
            }
        });
    }
    /**
     * <h2>start_Activity</h2>
     * <P>
     *  Starting the activity.
     * </P>*/
    private void start_Activity(boolean isLogin)
    {
        if (progress_bar_dialog != null)
        {
            progress_bar_dialog.dismiss();
        }
        /**
         * Putting user login status .*/
        sessionManager.setUserLoginStatus(true);

        if(isLogin)
        {
            /**
             * Opening the Home page activity. activity.*/
            Intent home_page_intent=new Intent(mactivity,HomePageActivity.class);
            home_page_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            home_page_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mactivity.startActivity(home_page_intent);
        }else
        {
            mactivity.startActivity(new Intent(mactivity,UserSetting.class));
        }

        /**
         * Finishing the activity.*/
        mactivity.finish();

    }

    /**<h2>formate_Date</h2>
     * <P>
     *     Method takes a date as String formate like mm/dd/yyyy
     *     then convert it in the formate yyyy-mm-dd
     *     and return it as string;
     * </P>*/
    private String formate_Date(String date)
    {
        String item[]=date.split("/");
        return item[2]+"-"+item[0]+"-"+item[1];
    }

}
