package com.three_embed.datum.Aramis_home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.three_embed.datum.Aramis_home.User_preference_setter.Chat_Fragment_list_data;
/**
 * Adapter class for view Pager to handel the Different Pages of
 * view pager by overriding of the getItem method of View Pager.
 * @author 3Embed
 * @since  9/12/15
 */
public class Tab_PagerAdapter extends FragmentStatePagerAdapter
{
    int mNumOfTabs;
    public Tab_PagerAdapter(FragmentManager fm, int NumOfTabs)
    {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }
    @Override
    public Fragment getItem(int position)
    {

        switch (position) {
            case 0:
                return new TabFragment1();
            case 1:
                return new TabFragment2();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}